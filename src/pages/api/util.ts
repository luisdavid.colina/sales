import { NextApiRequest, NextApiResponse } from 'next'
import { serialize, parse } from 'cookie'
import axios from 'axios'

const TOKEN_NAME = 'token'
const REFRESH_TOKEN_NAME = 'refresh-token'
const SECURE_TOKEN_URL = `https://securetoken.googleapis.com/v1/token?key=${process.env.NEXT_PUBLIC_FIREBASE_API_KEY}`

export const serializeToken = (token: string, expiresIn: number) => {
  const expireTime = expiresIn - 60

  const cookie = serialize(TOKEN_NAME, token, {
    maxAge: expireTime,
    expires: new Date(Date.now() + expireTime * 1000),
    httpOnly: true,
    secure: process.env.VERCEL_ENV === 'production',
    path: '/',
    sameSite: 'strict',
  })

  return cookie
}

export const serializeRefreshToken = (refreshToken: string) => {
  const cookie = serialize(REFRESH_TOKEN_NAME, refreshToken, {
    httpOnly: true,
    secure: process.env.VERCEL_ENV === 'production',
    path: '/',
    sameSite: 'strict',
  })
  return cookie
}
export const setAuthCookies = (
  res: NextApiResponse,
  token: string,
  refreshToken: string,
  expiresIn: number,
) => {
  const tokenCookie = serializeToken(token, expiresIn)
  const refreshTokenCookie = serializeRefreshToken(refreshToken)
  res.setHeader('Set-Cookie', [tokenCookie, refreshTokenCookie])
}

// TODO: remove specific cookie without overwrites the others
export const removeTokenCookie = (res: NextApiResponse) => {
  const cookie = serialize(TOKEN_NAME, '', {
    maxAge: -1,
    path: '/',
  })

  res.setHeader('Set-Cookie', cookie)
}

export const parseCookies = (req: NextApiRequest) => {
  if (req.cookies) return req.cookies

  const cookie = req.headers?.cookie
  return parse(cookie || '')
}

export const getTokenCookie = (req: NextApiRequest) => {
  const cookies = parseCookies(req)
  return cookies[TOKEN_NAME]
}

export const getRefreshTokenCookie = (req: NextApiRequest) => {
  const cookies = parseCookies(req)
  return cookies[REFRESH_TOKEN_NAME]
}

export const exchangeRefreshToken = async (
  res: NextApiResponse,
  refreshToken: string,
) => {
  let token = ''

  const payload = {
    grant_type: 'refresh_token',
    refresh_token: refreshToken,
  }

  try {
    const response = await axios.post(SECURE_TOKEN_URL, payload)

    if (response.status === 200) {
      const { id_token, expires_in, refresh_token } = response.data
      setAuthCookies(res, id_token, refresh_token, expires_in)
      token = id_token
    }
  } catch (err) {
      const error = err as any
       
    // eslint-disable-next-line
    console.log(error.message)
  }

  return token
}
