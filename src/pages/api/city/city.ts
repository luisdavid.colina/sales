const filterCityFields = async (data: any) => {
  try {
    const cities: any[] = []

    data.forEach((city: any) => {
      cities.push({
        id: city.id,
        name: city.nome,
        state: city.microrregiao.mesorregiao.UF.nome,
        uf: city.microrregiao.mesorregiao.UF.sigla,
        region: city.microrregiao.mesorregiao.UF.regiao.nome,
      })
    })

    return cities
  } catch (e) {
    //
  }
  return []
}

const getCityByUf = async (data: any, query: any) => {
  // Return a array of city objects with the given uf
  try {
    const cities: any[] = []

    // Find cities with given uf
    data.forEach((city: any) => {
      if (city.uf === query) {
        cities.push(city)
      }
    })

    return cities
  } catch (e) {
    const error = e as any
    return { message: error.message }
  }
}

const getCityByName = async (data: any, query: any) => {
  // Return a array of city objects with the given name
  try {
    const cities: any[] = []

    // Find cities with given name
    data.forEach((city: any) => {
      if (city.name.toUpperCase().includes(query.toUpperCase())) {
        cities.push(city)
      }
    })

    return cities
  } catch (e) {
    const error = e as any
    return { message: error.message }
  }
}

const getCityByRegion = async (data: any, query: any) => {
  // Return a array of city objects with the given region
  try {
    const cities: any[] = []

    // Find cities with given name
    data.forEach((city: any) => {
      if (city.region.toUpperCase().includes(query.toUpperCase())) {
        cities.push(city)
      }
    })

    return cities
  } catch (e) {
    const error = e as any
    return { message: error.message }
  }
}

const getCityByState = async (data: any, query: any) => {
  // Return a array of city objects with the given state
  try {
    const cities: any[] = []

    // Find cities with given name
    data.forEach((city: any) => {
      if (city.state.toUpperCase().includes(query.toUpperCase())) {
        cities.push(city)
      }
    })

    return cities
  } catch (e) {
    const error = e as any
    return { message: error.message }
  }
}

export {
  getCityByUf,
  getCityByName,
  getCityByRegion,
  getCityByState,
  filterCityFields,
}
