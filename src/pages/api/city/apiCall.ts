import axios from 'axios'
/*
const memoize = (func: any) => {
  const results: any = {}
  return (...args: any) => {
    const argsKey: any = JSON.stringify(args)
    if (!results[argsKey]) {
      results[argsKey] = func(...args)
    }
    return results[argsKey]
  }
}
*/
let firstSearch = true
let results: any
const memoize = (func: any) => {


  if (firstSearch) {
    results = func()
    firstSearch = false
  }
  return () => results
}

export const getAPI = memoize(async () => {
  try {
    const response = await axios.get(
      'https://servicodados.ibge.gov.br/api/v1/localidades/municipios',
    )

    return response.data
  } catch (error) {
    //
  }
  return []
})

export default getAPI
