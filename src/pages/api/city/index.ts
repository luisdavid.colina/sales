import getHandler from '@/util/getHandler'
import Contract from '@/types/Contract'
import { firestore, jsonContract } from 'lib/firebase-admin'

import getAPI from './apiCall'
import {
  getCityByUf,
  getCityByName,
  filterCityFields,
  getCityByRegion,
  getCityByState,
} from './city'

import {
  validateUF,
  validateName,
  validateRegion,
  validateState,
} from './validator'

import ApiError from './ApiError'

const handler = getHandler()

handler.get(async (req, res) => {
  try {
    const params = {
      uf: req.query.uf,
    }

    /*
    let cityByUF: any = []

    
    const ufIsValid = validateUF(String(params.uf))
    const data = await getAPI()
    const allCities = await filterCityFields(data)

    if (ufIsValid) {
      cityByUF = await getCityByUf(allCities, params.uf)
      if (cityByUF.length > 0) {
        cities = cityByUF
      } else {
        throw new ApiError('No city was found with the given parameters', 404)
      }
    } else {
      cities = allCities
    }
*/

    const snapshot = await firestore.collection('contracts').get()
    const contracts: any[] = []
    snapshot.forEach((doc) => {
      const contract = doc.data() as Contract
      contracts.push(jsonContract(contract))
    })
    const cities = contracts.map((contract) => {
      return contract.deliveryAddress.city
    })

    const uniqueCities = [...new Set(cities)].map((city) => {
      return {
        name: city,
      }
    })

    res.status(200).json(uniqueCities)
  } catch (e) {
    const error = e as any
    res.status(error.statusCode).json({ message: error.message })
  }
})

export default handler
