import ApiError from './ApiError'

const validateUF = (uf: string) => {
  // UF validation

  if (!uf) {
    return false
  }

  const isUpperCase = uf === uf.toUpperCase()

  if (!isUpperCase) {
    throw new ApiError('Invalid uf field, its value must be upper case', 400)
  }

  const hasTwoLetters = uf.length === 2

  if (!hasTwoLetters) {
    throw new ApiError('The uf field must have only two letters', 400)
  }

  return true
}

const validateName = (name: any) => {
  return !(name === null || name.length === 0)
}

const validateRegion = (region: any) => {
  return !(region === null || region.length === 0)
}

const validateState = (state: any) => {
  return !(state === null || state.length === 0)
}

export { validateUF, validateName, validateRegion, validateState }
