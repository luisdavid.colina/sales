import ProposalSchema from '@/schemas/ProposalSchema'
import Proposal from '@/types/Proposal'
import getHandler from '@/util/getHandler'
import {
  firestore,
  increment,
  serverTimestamp,
  jsonProposal,
} from 'lib/firebase-admin'
import { generateProposalPdf } from '@/util/utils'
import validate from 'middlewares/validate'
import checkSuperAdmin from '@/middlewares/checkSuperAdmin'

const handler = getHandler()

handler
  .post(async (req, res) => {
    const proposalReq = req.body

    const proposal = {
      ...proposalReq,
      createdAt: serverTimestamp(),
    }

    const batch = firestore.batch()

    const counterRef = firestore.collection('counters').doc('proposals')

    batch.set(counterRef, { counter: increment(1) }, { merge: true })
    await batch.commit()

    const snapshot = await counterRef.get()
    const counterData = snapshot.data()

    const count: number = snapshot.exists ? counterData?.counter : 1

    const { error, data } = await generateProposalPdf({
      ...proposal,
      proposalNumber: count,
    })

    if (error) {
      return res.status(400).json({ message: 'Could not create the pdf file' })
    }

    proposal.pdfKey = data?.pdfKey

    const docRef = await firestore.collection('proposals').add(proposal)

    await docRef.update({
      id: docRef.id,
      proposalNumber: count,
      status: 'opened',
    })
    return res.status(201).end()
  })
  .get(async (_, res) => {
    const snapshot = await firestore.collection('proposal').get()
    const proposals: any[] = []
    snapshot.forEach((doc) => {
      const proposal = doc.data() as Proposal
      proposals.push(jsonProposal(proposal))
    })
    res.json(proposals)
  })

export default checkSuperAdmin(validate(ProposalSchema, handler))
