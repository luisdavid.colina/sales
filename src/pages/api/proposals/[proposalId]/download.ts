/* eslint-disable no-console */
import Proposal from '@/types/Proposal'
import getHandler from '@/util/getHandler'
import { getProposalPdf, getProposalPdfUrl } from '@/util/utils'
import { firestore } from 'lib/firebase-admin'

const handler = getHandler()

handler
  .get(async (req, res) => {
    const { proposalId } = req.query

    try {
      const proposalRef = firestore.doc(`proposals/${proposalId}`)
      const snapshot = await proposalRef.get()

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'Proposal not found' })
      }

      const proposal = snapshot.data() as Proposal
      const buffer = await getProposalPdf(proposal.pdfKey || '')

      res.setHeader('Content-Type', 'application/pdf')
      res.setHeader(
        'Content-disposition',
        `attachment; filename=oportunidade-do-${proposal.company.legalName}.pdf`,
      )
      return res.status(200).send(buffer)
    } catch (err) {
      const error = err as any

      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })
  .post(async (req, res) => {
    const { contractId } = req.query

    try {
      const contractRef = firestore.doc(`proposals/${contractId}`)
      const snapshot = await contractRef.get()

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'contract not found' })
      }

      const proposal = snapshot.data() as Proposal
      const url = await getProposalPdfUrl(proposal)

      return res.status(200).json({ url })
    } catch (err) {
      const error = err as any

      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })

export default handler
