/* eslint-disable no-console */
import validate from '@/middlewares/validate'
import Proposal from '@/types/Proposal'
import ProposalSchema from '@/schemas/ProposalSchema'
import getHandler from '@/util/getHandler'
import { updateGenerateProposalPdf } from '@/util/utils'
import { firestore, serverTimestamp, jsonProposal } from 'lib/firebase-admin'
import checkSuperAdmin from '@/middlewares/checkSuperAdmin'

const handler = getHandler()

handler
  .get(async (req, res) => {
    const { proposalId } = req.query

    try {
      const proposalRef = firestore.doc(`proposals/${proposalId}`)
      const snapshot = await proposalRef.get()

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'proposal not found' })
      }

      const proposal = snapshot.data() as Proposal
      return res.status(200).json(jsonProposal(proposal))
    } catch (err) {
      const error = err as any
       
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })
  .put(async (req, res) => {
    const { proposalId } = req.query

    try {
      const proposal = { ...req.body }
      proposal.updateAt = serverTimestamp()
      const proposalRef = firestore.doc(`proposals/${proposalId}`)
      const snapshot = await proposalRef.get()

      const oldProposal = await (await proposalRef.get()).data()

      const { error, data } = await updateGenerateProposalPdf({
        ...oldProposal,
        ...proposal,
      })

      if (error) {
        return res
          .status(400)
          .json({ message: 'Could not create the pdf file' })
      }

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'proposal not found' })
      }

      await proposalRef.update({ ...proposal, pdfKey: data?.pdfKey })

      return res.status(200).json({ message: 'proposal updated' })
    } catch (err) {
      const error = err as any
       
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })

export default checkSuperAdmin(validate(ProposalSchema, handler))
