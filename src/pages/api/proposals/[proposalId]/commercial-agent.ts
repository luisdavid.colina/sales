import checkSuperAdmin from '@/middlewares/checkSuperAdmin'
import getHandler from '@/util/getHandler'
import { firestore } from 'lib/firebase-admin'

const handler = getHandler()

handler.put(async (req, res) => {
  const { commercialAgentId } = req.body

  const { proposalId } = req.query

  if (!commercialAgentId)
    return res.status(400).json({ message: 'commercial agent id is required' })

  const commercialAgentRef = firestore
    .collection('commercialAgents')
    .doc(commercialAgentId)

  let snapshot = await commercialAgentRef.get()
  const commercialAgent = snapshot.data()

  if (!snapshot.exists)
    return res.status(404).json({ message: 'commercial agent not found' })

  const proposalRef = firestore
    .collection('proposals')
    .doc(proposalId as string)

  snapshot = await proposalRef.get()

  if (!snapshot.exists)
    return res.status(404).json({ message: 'proposal not found' })

  await proposalRef.update({
    'productDetails.commercialAgent': commercialAgent,
  })

  return res.status(200).json({ message: 'proposal updated' })
})

export default checkSuperAdmin(handler)
