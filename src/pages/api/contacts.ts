import checkSuperAdmin from '@/middlewares/checkSuperAdmin'
import Contact from '@/types/Contact'
import getHandler from '@/util/getHandler'
import { firestore } from 'lib/firebase-admin'

const handler = getHandler()

handler.get(
  checkSuperAdmin(async (_, res) => {
    const snapshotContracts = await firestore.collection('contracts').get()
    const collectionContracts: any[] = []
    snapshotContracts.forEach((doc) => collectionContracts.push(doc.data()))

    const snapshotUsers = await firestore.collection('users').get()
    const collectionUsers: any[] = []
    snapshotUsers.forEach((doc) => collectionUsers.push(doc.data()))

    const deliveryAddresses: Contact[] = collectionContracts.map(
      (contract, i) => {
        const contact: Contact = {
          id: String(i),
          fullName: contract.company.admin.fullName,
          cpf: contract.company.admin.cpf,
          mobile: contract.company.admin.mobile,
          email: contract.company.admin.email,
          state: contract.deliveryAddress.state,
          city: contract.deliveryAddress.city,
        }
        return contact
      },
    )

    const contacts: Contact[] = collectionUsers.map((user) => {
      const contact: Contact = {
        id: user.uid,
        uid: user.uid,
        fullName: user.fullName,
        email: user.email,
        mobile: user.mobile,
        cpf: user.cpf,
      }
      return contact
    })
    const response: Contact[] = [...deliveryAddresses, ...contacts]

    const responseJson = [...new Set(response.map((item) => item.email))].map(
      (item) => {
        return response.find((contact) => contact.email === item)
      },
    )

    res.json(responseJson)
  }),
)

export default handler
