import { firestore } from '@/lib/firebase-admin'
import verifyToken from '@/middlewares/verifyToken'
import checkRoleOrPermissions from '@/middlewares/checkRoleOrPermissions'
import validate from '@/middlewares/validate'
import ParticipantSchema from '@/schemas/ParticipantSchema'
import getHandler from '@/util/getHandler'
import { ADMINS } from '@/constants'

const handler = getHandler()

handler.post(
  checkRoleOrPermissions(
    ADMINS,
    {
      businessRules: ['create'],
    },
    async (req, res) => {
      const participantName = req.body.name.trim().toLowerCase()

      const snapshot = await firestore
        .collection('participants')
        .where('name', '==', participantName)
        .get()

      if (snapshot.docs.length !== 0)
        throw new Error('participant already exists')

      const docRef = await firestore.collection('participants').add({
        name: participantName,
      })

      await docRef.update({ id: docRef.id })

      const participant = { id: docRef.id, name: participantName }
      res.status(201).json({ participant })
    },
  ),
)

export default verifyToken(validate(ParticipantSchema, handler))
