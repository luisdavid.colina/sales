import getHandler from '@/util/getHandler'
import axios from 'axios'

const handler = getHandler()

const FIREBASE_AUTH_URL = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${process.env.NEXT_PUBLIC_FIREBASE_API_KEY}`

handler.post(async (req, res) => {
  const payload = {
    email: req.body.email,
    password: req.body.password,
    returnSecureToken: true,
  }

  const response = await axios.post(FIREBASE_AUTH_URL, payload)

  if (response.status !== 200) return res.status(400).end()
  return res.status(200).end()
})

export default handler
