import getHandler from '@/util/getHandler'
import { SendTemplatedEmailCommand } from '@aws-sdk/client-ses'
import { send, SENDER } from '@/lib/sesClient'

const handler = getHandler()

handler.post(async (req, res) => {
  const { user } = req.body

  const params = {
    COMMUNICATION_CONTENT: `${user.twoFactorAuth.qr}`,
    COMMUNICATION_SUBJECT: 'Recuperar QR',
  }

  const email = {
    Destination: {
      CcAddresses: [],
      ToAddresses: user.email,
    },
    Source: SENDER,
    Template: 'NEW_COMMUNICATION',
    TemplateData: JSON.stringify(params),
    ConfigurationSetName: 'sales-ecosystem',
    ReplyToAddresses: [],
  }
  console.log(email)
  try {
    await send(new SendTemplatedEmailCommand(email))
  } catch (err) {
    const error = err as any

    console.log(error)
    return res.status(400).end()
  }

  return res.status(200).end()
})

export default handler
