import { auth, firestore } from '@/lib/firebase-admin'
import getHandler from '@/util/getHandler'
import axios from 'axios'
import { verifyToken } from 'node-2fa'
import { setAuthCookies } from '../util'

const handler = getHandler()

const FIREBASE_AUTH_URL = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${process.env.NEXT_PUBLIC_FIREBASE_API_KEY}`

handler.post(async (req, res) => {
  const payload = {
    email: req.body.email,
    password: req.body.password,
    returnSecureToken: true,
  }

  const response = await axios.post(FIREBASE_AUTH_URL, payload)

  if (response.status !== 200) return res.status(400).end()

  const { localId, idToken, expiresIn, refreshToken } = response.data

  const userSnapshot = await firestore.doc(`users/${localId}`).get()
  const user = userSnapshot.data()

  if (!user) return res.status(400).json({ message: 'user document not found' })

  const otpVerification = verifyToken(user.twoFactorAuth.secret, req.body.otp)

  const verificationFailed =
    !otpVerification || otpVerification.delta < 0 || otpVerification.delta > 4

  if (verificationFailed) {
    return res
      .status(400)
      .json({ error: 'otp', message: 'otp verification failed' })
  }

  const customToken = await auth.createCustomToken(localId, {
    isTwoFactorVerified: true,
  })

  setAuthCookies(res, idToken, refreshToken, expiresIn)

  return res.status(200).json({ authToken: customToken, refreshToken })
})

export default handler
