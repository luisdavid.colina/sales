import { SendTemplatedEmailCommand } from "@aws-sdk/client-ses";
import { send, SENDER } from "@/lib/sesClient";
import CommunicationSchema from "@/schemas/CommunicationSchema";
import getHandler from "@/util/getHandler";
import validate from "middlewares/validate";

import Communication from "@/types/Communication";

const handler = getHandler();

handler.post(async (req, res) => {
  const communication = {
    ...req.body,
  } as Communication;

  const params = {
    COMMUNICATION_CONTENT: communication.text,
    COMMUNICATION_SUBJECT: communication.subject,
  };

  const email = {
    Destination: {
      CcAddresses: [],
      ToAddresses: communication.to,
    },
    Source: SENDER,
    Template: "NEW_COMMUNICATION",
    TemplateData: JSON.stringify(params),
    ConfigurationSetName: "sales-ecosystem",
    ReplyToAddresses: [],
  };

  await send(new SendTemplatedEmailCommand(email));

  return res.status(201).end();
});

export default handler
