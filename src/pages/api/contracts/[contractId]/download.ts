/* eslint-disable no-console */
import Contract from '@/types/Contract'
import getHandler from '@/util/getHandler'
import { getContractPdf, getContractPdfUrl } from '@/util/utils'
import { firestore } from 'lib/firebase-admin'

const handler = getHandler()

handler
  .get(async (req, res) => {
    const { contractId } = req.query

    try {
      const contractRef = firestore.doc(`contracts/${contractId}`)
      const snapshot = await contractRef.get()

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'contract not found' })
      }

      const contract = snapshot.data() as Contract
      const buffer = await getContractPdf(contract.pdfKey)

      res.setHeader('Content-Type', 'application/pdf')
      res.setHeader(
        'Content-disposition',
        `attachment; filename=contracto-do-${contract.company.legalName}.pdf`,
      )
      return res.status(200).send(buffer)
    } catch (err) {
      const error = err as any
       
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })
  .post(async (req, res) => {
    const { contractId } = req.query

    try {
      const contractRef = firestore.doc(`contracts/${contractId}`)
      const snapshot = await contractRef.get()

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'contract not found' })
      }

      const contract = snapshot.data() as Contract
      const url = await getContractPdfUrl(contract)

      return res.status(200).json({ url })
    } catch (err) {
      const error = err as any
       
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })

export default handler
