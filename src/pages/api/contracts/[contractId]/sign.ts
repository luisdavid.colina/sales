/* eslint-disable no-console */
import {
  registerWebhook,
  sendDocumentToSign,
  sendSignatureList,
  uploadFile,
} from '@/lib/d4sign'
import checkSuperAdmin from '@/middlewares/checkSuperAdmin'
import Contract from '@/types/Contract'
import getHandler from '@/util/getHandler'
import { getContractPdf } from '@/util/utils'
import { firestore } from 'lib/firebase-admin'

const handler = getHandler()

handler.put(async (req, res) => {
  let result = null
  const { contractId } = req.query

  try {
    const contractRef = firestore.doc(`contracts/${contractId}`)
    const snapshot = await contractRef.get()

    if (!snapshot.exists) {
      return res.status(404).json({ message: 'contract not found' })
    }

    const contract = snapshot.data() as Contract
    const pdf = await getContractPdf(contract.pdfKey)

    const file = {
      content: pdf,
      name: contract.pdfKey,
    }

    result = await uploadFile(file)
    if (!result) throw new Error('could not upload d4sign document')
    console.log('uploaded file')

    contract.d4signUuidDocument = result.uuidDocument

    const d4signDocumentsRef = firestore.collection('d4signDocuments')

    await d4signDocumentsRef.doc(result.uuidDocument).set({
      id: result.uuidDocument,
      contractId: contract.id,
    })

    await contractRef.update({
      d4signUuidDocument: result.uuidDocument,
    })

    result = await sendSignatureList(contract)

    if (!result) throw new Error('could not send signature list')
    console.log('sent signature list')

    result = await sendDocumentToSign(contract)
    if (!result) throw new Error('could not send document to sign')
    console.log('sent document to sign')

    result = await registerWebhook(contract)
    if (!result) throw new Error('could not  register webhook')

    await contractRef.update({
      d4signStatus: 'AwaitingDocumentSign',
    })

    return res.status(200).json({ message: 'sent document to sign' })
  } catch (err) {
      const error = err as any
       
    console.log(error.message)
    return res.status(400).json({ message: error.message })
  }
})

export default checkSuperAdmin(handler)
