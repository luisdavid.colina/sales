import { FieldValue, firestore } from '@/lib/firebase-admin'
import getHandler from '@/util/getHandler'

const handler = getHandler()

handler.post(async (req, res) => {
  const contractId = req.query.contractId as string
  const contractRef = firestore.doc(`contracts/${contractId}`)
  const contractSnapshot = await contractRef.get()

  const contract = contractSnapshot.data()

  if (!contractSnapshot || !contract) return res.status(404).end()

  const commercialAgentEmail =
    contract.productDetails.commercialAgent.company.admin.email

  const adminEmail = contract.company.admin.email

  const signers = [
    adminEmail,
    commercialAgentEmail,
    'comercial@elementmkt.com.br',
    'pedrofraga@elementprepagos.com.br',
  ]

  const { type_post, email } = req.body

  if (type_post === '4') {
    if (signers.includes(email) && !contract.signers?.includes(email)) {
      const newFields = {
        signers: FieldValue.arrayUnion(email),
        d4signStatus: contract.d4signStatus,
      }

      if (contract.signers?.length === 3) {
        newFields.d4signStatus = 'Signed'
      }

      await contractRef.update(newFields)
      return res.status(200).end()
    }
  } else if (type_post === '3') {
    await contractRef.update({
      d4signStatus: 'Canceled',
      isActive: false,
    })

    return res.status(200).end()
  }

  return res.status(400).end()
})

export default handler
