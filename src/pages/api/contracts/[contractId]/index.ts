/* eslint-disable no-console */
import validate from '@/middlewares/validate'
import Contract from '@/types/Contract'
import ContractSchema from '@/schemas/ContractSchema'
import getHandler from '@/util/getHandler'
import {
  firestore,
  getContract,
  serverTimestamp,
  jsonContract,
} from 'lib/firebase-admin'
import checkSuperAdmin from '@/middlewares/checkSuperAdmin'

const handler = getHandler()

handler
  .get(async (req, res) => {
    const { contractId } = req.query

    try {
      const contractRef = firestore.doc(`contracts/${contractId}`)
      const snapshot = await contractRef.get()

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'contract not found' })
      }

      const contract = snapshot.data() as Contract
      return res.status(200).json(jsonContract(contract))
    } catch (err) {
      const error = err as any
       
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })
  .put(async (req, res) => {
    const { contractId } = req.query

    try {
      const contract = getContract(req)
      contract.updateAt = serverTimestamp()
      const contractRef = firestore.doc(`contracts/${contractId}`)
      const snapshot = await contractRef.get()

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'contract not found' })
      }

      await contractRef.update(contract)

      return res.status(200).json({ message: 'contract updated' })
    } catch (err) {
      const error = err as any
       
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })

export default checkSuperAdmin(validate(ContractSchema, handler))
