import checkSuperAdmin from '@/middlewares/checkSuperAdmin'
import ContractSchema from '@/schemas/ContractSchema'
import Contract from '@/types/Contract'
import getHandler from '@/util/getHandler'
import { createAdminUser } from '@/util/user'
import { generateContractPdf } from '@/util/utils'
import { firestore, getNewContract, jsonContract } from 'lib/firebase-admin'
import validate from 'middlewares/validate'

const handler = getHandler()

handler
  .post(async (req, res) => {
    const contract = getNewContract(req)
    try {
      const { error, data } = await generateContractPdf(contract)

      if (error) {
        return res
          .status(400)
          .json({ message: 'Could not create the pdf file' })
      }

      contract.pdfKey = data?.pdfKey

      await createAdminUser(contract)

      const docRef = await firestore.collection('contracts').add(contract)
      await docRef.update({ id: docRef.id })

      return res
        .status(201)
        .json({ message: 'contract created!', contractId: docRef.id })
    } catch (err) {
      const e = err as any
       
      console.log('Error', e, e.stack)
      return res.status(500).end()
    }
  })
  .get(async (req, res) => {
    const snapshot = await firestore.collection('contracts').get()
    const contracts: any[] = []
    snapshot.forEach((doc) => {
      const contract = doc.data() as Contract
      contracts.push(jsonContract(contract))
    })
    res.json(contracts)
  })

export default checkSuperAdmin(validate(ContractSchema, handler))
