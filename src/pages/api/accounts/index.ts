import getHandler from '@/util/getHandler'
import { AccountAgentType } from '@/schemas/AccountListAgentsSchema'
import checkRoleOrPermissions from 'middlewares/checkRoleOrPermissions'
import verifyToken from 'middlewares/verifyToken'
import { firestore, jsonUser } from '@/lib/firebase-admin'
import User from '@/types/User'
import { updateUser } from '@/util/utils'
import { ADMINS } from '@/constants'

const handler = getHandler()

handler
  .post(
    checkRoleOrPermissions(
      ADMINS,
      {
        profiles: ['write'],
      },
      async (req, res) => {
        const { accounts } = req.body
        const { user } = req

        await Promise.all(
          accounts.map((account: AccountAgentType) =>
            updateUser(account, user?.companyId || ''),
          ),
        )

        return res.status(200).end()
      },
    ),
  )
  .get(
    checkRoleOrPermissions(
      ADMINS,
      {
        profiles: ['read'],
      },
      async (req, res) => {
        const { user } = req
        if (!user?.companyId) return res.status(200).json([])
        const snapshot = await firestore
          .collection('users')
          .where('companyId', '==', user?.companyId)
          .get()
        const users: User[] = []

        snapshot.forEach((doc) => {
          const user = doc.data() as User
          users.push(jsonUser(user))
        })
        return res.status(200).json(users)
      },
    ),
  )

export default verifyToken(handler)
