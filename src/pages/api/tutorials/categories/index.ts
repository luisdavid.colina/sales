import { firestore } from '@/lib/firebase-admin'
import getHandler from '@/util/getHandler'


const handler = getHandler()

handler.post(async (req, res) => {
  const categoryName = req.body.title.trim()

  const docRef = await firestore.collection('tutorialCategories').add({
    title: categoryName,
  })

  await docRef.update({ id: docRef.id })

  const category = { id: docRef.id, title: categoryName }
  res.status(201).json({ category })
})
export default handler
