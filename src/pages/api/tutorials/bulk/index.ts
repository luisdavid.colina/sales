import { firestore } from 'lib/firebase-admin'
import validate from 'middlewares/validate'
import BulkTutorialSchema from '@/schemas/BulkTutorialSchema'
import Tutorial from '@/types/Tutorial'
import getHandler from '@/util/getHandler'
import verifyToken from '@/middlewares/verifyToken'

export const jsonTutorial = (tutorial: Tutorial) => {
  return tutorial
}

const handler = getHandler()

handler.post(async (req, res) => {
  const {
    tutorials,
    status,
  }: { tutorials: string[]; status: string } = req.body
  tutorials.forEach(async (tutorial) => {
    const tutorialRef = firestore.doc(`tutorials/${tutorial}`)
    const oldTutorial = (await tutorialRef.get()).data()
    try {
      await tutorialRef.update({ ...oldTutorial, status })
    } catch (err) {
      const error = err as any
      return res.status(400).json({ message: error.message })
    }
    return res.status(201).json({ message: 'tutorial updated' })
  })
})

export default handler
