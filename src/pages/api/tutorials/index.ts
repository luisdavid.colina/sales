import { firestore, serverTimestamp } from 'lib/firebase-admin'
import validate from 'middlewares/validate'
import TutorialSchema from '@/schemas/TutorialSchema'
import Tutorial from '@/types/Tutorial'
import getHandler from '@/util/getHandler'
import verifyToken from '@/middlewares/verifyToken'

export const jsonTutorial = (tutorial: Tutorial) => {
  return tutorial
}

const handler = getHandler()

handler
  .post(async (req, res) => {
    const tutorial: Tutorial = { ...req.body }
    if (tutorial.id === undefined) {
      try {
        const docRef = await firestore.collection('tutorials').add({
          ...tutorial,
          createdAt: serverTimestamp(),
          status: 'Publicado',
        })
        await docRef.update({
          id: docRef.id,
        })
        return res.status(201).json({ id: docRef.id })
      } catch (err) {
        const e = err as any

        console.log('Error', e, e.stack)
        return res.status(500).end()
      }
    }
    const tutorialRef = firestore.doc(`tutorials/${tutorial.id}`)
    const prevTutorial = tutorial
    try {
      await tutorialRef.update({ ...prevTutorial })
      return res.status(201).json({ message: 'tutorial updated' })
    } catch (err) {
      const error = err as any
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })
  .get(async (_, res) => {
    const snapshot = await firestore.collection('tutorials').get()
    const tutorials: any[] = []
    snapshot.forEach((doc) => {
      const tutorial = doc.data() as Tutorial
      tutorials.push(jsonTutorial(tutorial))
    })
    res.json(tutorials)
  })

export default handler
