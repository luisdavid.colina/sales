import { firestore } from '@/lib/firebase-admin'
import checkSuperAdmin from '@/middlewares/checkSuperAdmin'
import validate from '@/middlewares/validate'
import InvitationSchema from '@/schemas/InvitationSchema'
import getHandler from '@/util/getHandler'
import { generateTwoFactorAuth } from '@/util/utils'

const handler = getHandler()

handler.post(async (req, res) => {
  const invitation = req.body

  invitation.status = 'open'
  invitation.twoFactorAuth = generateTwoFactorAuth(invitation.email)

  const invitationsRef = firestore.collection('invitations')
  const docRef = await invitationsRef.add(invitation)

  await docRef.update({ id: docRef.id })

  return res.status(200).json({ message: 'invitation created' })
})

export default validate(InvitationSchema, handler)
