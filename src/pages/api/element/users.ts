import { SUPER_ADMIN_ROLE } from '@/constants'
import { auth, firestore } from '@/lib/firebase-admin'
import validate from '@/middlewares/validate'
import ElementUserSchema from '@/schemas/ElementUserSchema'
import getHandler from '@/util/getHandler'
import { generateTwoFactorAuth } from '@/util/utils'

const handler = getHandler()

handler.post(async (req, res) => {
  const { invitationId, ...payload } = req.body

  const invitationRef = firestore.collection('invitations').doc(invitationId)
  const invitationSnapshot = await invitationRef.get()

  if (!invitationSnapshot.exists) return res.status(400).end()

  const invitation = invitationSnapshot.data()

  if (invitation?.status !== 'open') return res.status(400).end()
  if (invitation?.cpf !== payload.cpf) return res.status(400).end()
  if (invitation?.email !== payload.email) return res.status(400).end()

  const user = await auth.createUser({
    disabled: false,
    email: payload.email,
    emailVerified: false,
    password: payload.password,
  })

  delete payload.password

  await auth.setCustomUserClaims(user.uid, { role: SUPER_ADMIN_ROLE })

  await firestore
    .collection('users')
    .doc(user.uid)
    .set({
      ...payload,
      uid: user.uid,
      role: SUPER_ADMIN_ROLE,
      twoFactorAuth: invitation.twoFactorAuth,
    })

  await invitationRef.update({ status: 'completed' })

  return res.status(201).end()
})

export default handler
