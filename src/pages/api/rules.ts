import { firestore, serverTimestamp } from '@/lib/firebase-admin'
import verifyToken from '@/middlewares/verifyToken'
import validate from '@/middlewares/validate'
import ProductRuleSchema from '@/schemas/ProductRuleSchema'
import getHandler from '@/util/getHandler'
import checkRoleOrPermissions from '@/middlewares/checkRoleOrPermissions'
import { ADMINS } from '@/constants'

const handler = getHandler()

handler.post(
  checkRoleOrPermissions(
    ADMINS,
    {
      businessRules: ['create'],
    },
    async (req, res) => {
      const {
        rule,
        product,
        totalValue,
        totalCommission,
        paymentPerMonth,
        estimatedRevenue,
        participantsCommissions,
      } = req.body

      if (!product.rule.id) {
        const snapshot = await firestore
          .collection('rules')
          .where('name', '==', product.rule.name.trim())
          .get()

        if (snapshot.docs.length !== 0)
          throw new Error('rule name already exists')
        const ruleDocRef = await firestore.collection('rules').add({
          ...rule,
          name: product.rule.name,
        })

        await ruleDocRef.update({ id: ruleDocRef.id })

        const productDocRef = await firestore.collection('products').add({
          ...product,
          totalValue,
          totalCommission,
          paymentPerMonth,
          estimatedRevenue,
          participantsCommissions,
          rule: {
            id: ruleDocRef.id,
            name: product.rule.name,
            ...rule,
          },
          createdAt: serverTimestamp(),
        })

        await productDocRef.update({ id: productDocRef.id })
      } else {
        const ruleDocRef = await firestore
          .collection('rules')
          .doc(product.rule.id)

        const ruleSnapshot = await ruleDocRef.get()
        const { exists } = ruleSnapshot

        const ruleData = ruleSnapshot.data()

        if (!exists) throw new Error(`rule doesn't exist`)

        const productDocRef = await firestore.collection('products').add({
          ...product,
          totalValue,
          totalCommission,
          paymentPerMonth,
          estimatedRevenue,
          participantsCommissions,
          rule: ruleData,
          createdAt: serverTimestamp(),
        })
        await productDocRef.update({ id: productDocRef.id })
      }

      res.status(201).json({ message: 'product created successfully' })
    },
  ),
)

export default verifyToken(validate(ProductRuleSchema, handler))
