import { firestore } from '@/lib/firebase-admin'
import verifyToken from '@/middlewares/verifyToken'
import validate from '@/middlewares/validate'
import checkRoleOrPermissions from '@/middlewares/checkRoleOrPermissions'
import SalesChannelSchema from '@/schemas/SalesChannelSchema'
import getHandler from '@/util/getHandler'
import { ADMINS } from '@/constants'

const handler = getHandler()

handler.post(
  checkRoleOrPermissions(
    ADMINS,
    {
      businessRules: ['create'],
    },
    async (req, res) => {
      const channel = {
        ...req.body,
        name: req.body.name.trim(),
      }

      const snapshot = await firestore
        .collection('salesChannels')
        .where('name', '==', channel.name)
        .get()

      if (snapshot.docs.length !== 0) throw new Error('channel already exists')

      const docRef = await firestore.collection('salesChannels').add(channel)

      await docRef.update({ id: docRef.id })

      const salesChannel = { ...channel, id: docRef.id }
      res.status(201).json({ salesChannel })
    },
  ),
)

export default verifyToken(validate(SalesChannelSchema, handler))
