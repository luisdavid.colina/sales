import Product from '@/types/Product'
import getHandler from '@/util/getHandler'
import { firestore, jsonProduct } from 'lib/firebase-admin'

const handler = getHandler()

handler.get(async (req, res) => {
  const snapshot = await firestore.collection('products').get()
  const products: any[] = []
  snapshot.forEach((doc) => {
    const product = doc.data() as Product
    products.push(jsonProduct(product))
  })
  res.json(products)
})

export default handler
