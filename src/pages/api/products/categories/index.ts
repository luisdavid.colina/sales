import { firestore } from '@/lib/firebase-admin'
import validate from '@/middlewares/validate'
import ProductCategorySchema from '@/schemas/ProductCategorySchema'
import getHandler from '@/util/getHandler'
import checkRoleOrPermissions from '@/middlewares/checkRoleOrPermissions'
import verifyToken from '@/middlewares/verifyToken'
import { ADMINS } from '@/constants'

const handler = getHandler()

handler.post(
  checkRoleOrPermissions(
    ADMINS,
    {
      businessRules: ['create'],
    },
    async (req, res) => {
      const categoryName = req.body.name.trim()

      const docRef = await firestore.collection('productsCategories').add({
        name: categoryName,
      })

      await docRef.update({ id: docRef.id })

      const category = { id: docRef.id, name: categoryName }
      res.status(201).json({ category })
    },
  ),
)

export default verifyToken(validate(ProductCategorySchema, handler))
