import { firestore } from '@/lib/firebase-admin'
import validate from '@/middlewares/validate'
import ProductFamilySchema from '@/schemas/ProductFamilySchema'
import getHandler from '@/util/getHandler'
import verifyToken from '@/middlewares/verifyToken'
import checkRoleOrPermissions from '@/middlewares/checkRoleOrPermissions'
import { ADMINS } from '@/constants'

const handler = getHandler()

handler.post(
  checkRoleOrPermissions(
    ADMINS,
    {
      businessRules: ['create'],
    },
    async (req, res) => {
      const familyName = req.body.name.trim()

      const docRef = await firestore.collection('productsFamilies').add({
        name: familyName,
      })

      await docRef.update({ id: docRef.id })

      const family = { id: docRef.id, name: familyName }
      res.status(201).json({ family })
    },
  ),
)

export default verifyToken(validate(ProductFamilySchema, handler))
