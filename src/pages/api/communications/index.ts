import CommunicationSchema from "@/schemas/CommunicationSchema";
import getHandler from "@/util/getHandler";
import { firestore, serverTimestamp } from "lib/firebase-admin";
import validate from "middlewares/validate";
import verifyToken from "@/middlewares/verifyToken";
import checkRoleOrPermissions from "@/middlewares/checkRoleOrPermissions";
import { ADMINS } from "@/constants";

import Communication from "@/types/Communication";

import communicationDefault from "@/forms/defaultStates/communication";

import { createCommunication } from "@/util/utils";

const handler = getHandler();

handler
  .post(async (req, res) => {
    const communication = {
      ...req.body,
      createdAt: serverTimestamp(),
    } as Communication;
    await createCommunication(communication);

    return res.status(201).end();
  })
  .get(async (_, res) => {
    const snapshot = await firestore.collection("communications").get();
    const communications: Communication[] = [];
    snapshot.forEach((doc) => {
      communications.push(doc.data() as Communication);
    });
    res.json(communications);
  });

export default verifyToken(validate(CommunicationSchema, handler));
