import { firestore, jsonInvoice, serverTimestamp } from 'lib/firebase-admin'
import validate from 'middlewares/validate'
import InvoiceSchema from '@/schemas/InvoiceSchema'
import Invoice from '@/types/Invoice'
import getHandler from '@/util/getHandler'
import checkRoleOrPermissions from '@/middlewares/checkRoleOrPermissions'
import { ADMINS } from '@/constants'
import verifyToken from '@/middlewares/verifyToken'

const handler = getHandler()

handler
  .post(async (req, res) => {
    const invoice: Invoice = { ...req.body }

    if (invoice.id === undefined) {
      try {
        const docRef = await firestore
          .collection('invoices')
          .add({ ...invoice, createdAt: serverTimestamp() })
        await docRef.update({
          id: docRef.id,
        })
        return res.status(201).json({ id: docRef.id })
      } catch (err) {
        const e = err as any

        console.log('Error', e, e.stack)
        return res.status(500).end()
      }
    }
    const invoiceRef = firestore.doc(`invoices/${invoice.id}`)
    const prevInvoice = invoice
    try {
      await invoiceRef.update({ ...prevInvoice })
      return res.status(201).json({ message: 'invoice updated' })
    } catch (err) {
      const error = err as any
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })
  .get(async (_, res) => {
    const snapshot = await firestore.collection('invoices').get()
    const invoices: any[] = []
    snapshot.forEach((doc) => {
      const invoice = doc.data() as Invoice
      invoices.push(jsonInvoice(invoice))
    })
    res.json(invoices)
  })

export default verifyToken(validate(InvoiceSchema, handler))
