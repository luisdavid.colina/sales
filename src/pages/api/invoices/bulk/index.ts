import { firestore, jsonInvoice } from 'lib/firebase-admin'
import validate from 'middlewares/validate'
import BulkInvoiceSchema from '@/schemas/BulkInvoiceSchema'
import Invoice from '@/types/Invoice'
import getHandler from '@/util/getHandler'
import verifyToken from '@/middlewares/verifyToken'

const handler = getHandler()

handler.post(async (req, res) => {
  const { invoices, status }: { invoices: string[]; status: string } = req.body
  invoices.forEach(async (invoice) => {
    const invoiceRef = firestore.doc(`invoices/${invoice}`)
    const oldInvoice = (await invoiceRef.get()).data()
    try {
      await invoiceRef.update({ ...oldInvoice, status })
    } catch (err) {
      const error = err as any
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
    return res.status(201).json({ message: 'invoice updated' })
  })
})

export default verifyToken(validate(BulkInvoiceSchema, handler))
