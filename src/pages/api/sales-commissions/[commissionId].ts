import { firestore, serverTimestamp } from '@/lib/firebase-admin'
import validate from '@/middlewares/validate'
import SalesCommissionSchema from '@/schemas/SalesCommissionSchema'
import getHandler from '@/util/getHandler'

const handler = getHandler()

handler
  .get(async (req, res) => {
    const { commissionId } = req.query

    try {
      const commissionRef = firestore.doc(`salesCommissions/${commissionId}`)

      const snapshot = await commissionRef.get()

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'sales-commission not found' })
      }

      const commission = snapshot.data()
      return res.status(200).json(commission)
    } catch (err) {
      const error = err as any
       
      // eslint-disable-next-line no-console
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })
  .put(async (req, res) => {
    const commission = req.body
    const { commissionId } = req.query

    const commissionRef = firestore.doc(`salesCommissions/${commissionId}`)

    const snapshot = await commissionRef.get()
    const { exists } = snapshot

    if (!exists) return res.status(404).end()

    commission.updateAt = serverTimestamp()
    await commissionRef.update(commission)

    return res.status(200).end()
  })

export default validate(SalesCommissionSchema, handler)
