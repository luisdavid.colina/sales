import { firestore, serverTimestamp } from '@/lib/firebase-admin'
import validate from '@/middlewares/validate'
import SalesCommissionSchema from '@/schemas/SalesCommissionSchema'
import Commission from '@/types/SalesCommission'
import getHandler from '@/util/getHandler'

const handler = getHandler()

handler
  .get(async (req, res) => {
    const snapshot = await firestore.collection('salesCommissions').get()
    const commissions: Commission[] = []
    snapshot.forEach((doc) => {
      commissions.push(doc.data() as Commission)
    })
    res.json(commissions)
  })
  .post(async (req, res) => {
    const commission = req.body

    const commissionsRef = firestore.collection('salesCommissions')

    commission.createdAt = serverTimestamp()
    const docRef = await commissionsRef.add(commission)
    await docRef.update({ id: docRef.id })

    res.status(201).end()
  })

export default validate(SalesCommissionSchema, handler)
