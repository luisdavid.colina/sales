import AccountListAgentsSchema, {
  AccountAgentType,
} from '@/schemas/AccountListAgentsSchema'
import getHandler from '@/util/getHandler'
import { SENDER, send } from 'lib/sesClient'
import { SendBulkTemplatedEmailCommand } from '@aws-sdk/client-ses'
import { firestore } from 'lib/firebase-admin'
import validate from 'middlewares/validate'
import EmailBulk from '@/types/EmailBulk'
import Company from '@/types/Company'
import checkRoleOrPermissions from 'middlewares/checkRoleOrPermissions'
import { createBulkDestination, createCompanyInvitation } from '@/util/utils'
import { CompanyRole } from '@/types/User'
import Invitation from '@/types/Invitation'
import { ADMINS, COMPANY_ROLES } from '@/constants'
import verifyToken from '@/middlewares/verifyToken'

const handler = getHandler()

const postHandler = checkRoleOrPermissions(
  ADMINS,
  {
    profiles: ['create'],
  },
  async (req, res) => {
    const { user } = req
    const { agents: accounts } = req.body
    const role = req.query.role as CompanyRole

    if (!COMPANY_ROLES.includes(role)) return res.status(400).end()

    const companyId = user?.companyId as string
    const companySnapshot = await firestore.doc(`companies/${companyId}`).get()
    const company = companySnapshot.data() as Company

    const emailBulk: EmailBulk = {
      Destinations: [],
      Source: SENDER,
      Template: 'NEW_USER',
      DefaultTemplateData: JSON.stringify({
        COMPANY_NAME: company.legalName || '',
      }),
    }

    const invitations: Invitation[] = await Promise.all(
      accounts.map(
        (account: AccountAgentType): Promise<Invitation> => {
          return createCompanyInvitation(account, company, role)
        },
      ),
    )

    emailBulk.Destinations = invitations.map(createBulkDestination)

    await send(new SendBulkTemplatedEmailCommand(emailBulk))

    return res.status(201).end()
  },
)

handler.post(postHandler)

export default verifyToken(validate(AccountListAgentsSchema, handler))
