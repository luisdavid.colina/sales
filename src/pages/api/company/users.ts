import { auth, firestore } from '@/lib/firebase-admin'
import validate from '@/middlewares/validate'
import CompanyUserSchema from '@/schemas/CompanyUserSchema'
import getHandler from '@/util/getHandler'
import { ADMIN_ROLE, COMMERCIAL_AGENT_ROLE } from '@/constants'

const handler = getHandler()

handler.post(async (req, res) => {
  const { invitationId, ...payload } = req.body
  const { company, password, ...contactInfo } = payload

  const invitationRef = firestore.collection('invitations').doc(invitationId)
  const invitationSnapshot = await invitationRef.get()

  if (!invitationSnapshot.exists) return res.status(400).end()

  const invitation = invitationSnapshot.data()

  if (invitation?.status !== 'open') return res.status(400).end()

  const user = await auth.createUser({
    password,
    disabled: false,
    email: invitation.email,
    emailVerified: false,
  })

  await auth.setCustomUserClaims(user.uid, { role: invitation.role })

  const snapshot = await firestore.doc(`companies/${company.cnpj}`).get()

  let companyId = invitation.company?.cnpj

  if (!snapshot.exists && invitation.role === ADMIN_ROLE) {
    await firestore.doc(`companies/${company.cnpj}`).set({
      cnpj: company.cnpj,
      comments: company.comments,
      email: company.email,
      legalName: company.legalName,
      mobile: company.mobile,
      phone: company.phone,
      tradingName: company.tradingName,
      website: company.website,
    })

    companyId = company.cnpj
  }

  const userDoc = {
    ...contactInfo,
    companyId,
    uid: user.uid,
    role: invitation.role,
    permissions: invitation.permissions || {},
    twoFactorAuth: invitation.twoFactorAuth,
  }

  if (invitation.role === COMMERCIAL_AGENT_ROLE) {
    userDoc.companyId = invitation.companyId
    userDoc.commercialAgentId = invitation.commercialAgentId
  }

  await firestore.collection('users').doc(user.uid).set(userDoc)

  await invitationRef.update({ status: 'completed' })

  return res.status(201).end()
})

export default handler
