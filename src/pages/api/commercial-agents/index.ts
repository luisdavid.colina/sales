import CommercialAgentSchema from '@/schemas/CommercialAgentSchema'
import CommercialAgent from '@/types/CommercialAgent'
import getHandler from '@/util/getHandler'
import { firestore, serverTimestamp } from 'lib/firebase-admin'
import validate from 'middlewares/validate'
import { createCommercialAgentInvitation } from '@/util/utils'
import User, { CompanyRole } from '@/types/User'
import verifyToken from '@/middlewares/verifyToken'
import { ELEMENT_COMPANY_ID } from '@/constants'

const handler = getHandler()

handler
  .post(async (req, res) => {
    const { authUser } = req
    const commercialAgent = {
      ...req.body,
      createdAt: serverTimestamp(),
    }

    let docRef = await firestore
      .collection('commercialAgents')
      .add(commercialAgent)

    await docRef.update({ id: docRef.id })

    commercialAgent.id = docRef.id

    const commercialAgentLink = {
      commercialAgentId: docRef.id,
      createdAt: serverTimestamp(),
    }

    const userSnapshot = await firestore.doc(`users/${authUser.uid}`).get()
    const user = userSnapshot.data() as User

    let companyId = ELEMENT_COMPANY_ID

    if (user.companyId) {
      companyId = user.companyId
    }

    createCommercialAgentInvitation(commercialAgent, companyId)

    docRef = await firestore
      .collection('commercialAgentsLinks')
      .add(commercialAgentLink)

    await docRef.update({ id: docRef.id })

    return res.status(201).end()
  })
  .get(async (_, res) => {
    const snapshot = await firestore.collection('commercialAgents').get()
    const commercialAgents: CommercialAgent[] = []
    snapshot.forEach((doc) => {
      commercialAgents.push(doc.data() as CommercialAgent)
    })
    res.json(commercialAgents)
  })

export default handler