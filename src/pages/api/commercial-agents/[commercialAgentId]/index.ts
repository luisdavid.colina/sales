/* eslint-disable no-console */
import validate from '@/middlewares/validate'
import CommercialAgent from '@/types/CommercialAgent'
import CommercialAgentSchema from '@/schemas/CommercialAgentSchema'
import getHandler from '@/util/getHandler'
import { firestore, serverTimestamp } from 'lib/firebase-admin'

const handler = getHandler()

handler
  .get(async (req, res) => {
    const { commercialAgentId } = req.query

    try {
      const commercialAgentRef = firestore.doc(
        `commercialAgents/${commercialAgentId}`,
      )
      const snapshot = await commercialAgentRef.get()

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'commercial agent not found' })
      }

      const commercialAgent = snapshot.data() as CommercialAgent
      return res.status(200).json(commercialAgent)
    } catch (err) {
      const error = err as any
       
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })
  .put(async (req, res) => {
    const { commercialAgentId } = req.query

    try {
      const commercialAgent = { ...req.body }
      commercialAgent.updateAt = serverTimestamp()
      const commercialAgentRef = firestore.doc(
        `commercialAgents/${commercialAgentId}`,
      )
      const snapshot = await commercialAgentRef.get()

      if (!snapshot.exists) {
        return res.status(404).json({ message: 'commercial agent not found' })
      }

      commercialAgentRef.update(commercialAgent)

      return res.status(200).json({ message: 'commercial agent updated' })
    } catch (err) {
      const error = err as any
       
      console.log(error.message)
      return res.status(400).json({ message: error.message })
    }
  })

export default validate(CommercialAgentSchema, handler)
