import checkSuperAdmin from '@/middlewares/checkSuperAdmin'
import MultiplierSchema from '@/schemas/MultiplierSchema'
import getHandler from '@/util/getHandler'
import { firestore, serverTimestamp } from 'lib/firebase-admin'
import validate from 'middlewares/validate'

const handler = getHandler()

handler
  .post(async (req, res) => {
    const document = req.body

    const { commercialAgentLinkId: linkId } = document

    const linkRef = firestore.collection('commercialAgentsLinks').doc(linkId)

    const snapshot = await linkRef.get()
    const link = snapshot.data()

    if (!snapshot.exists || !link) return res.status(400).end()

    const docRef = await firestore.collection('proposalRequests').add(document)

    await docRef.update({
      id: docRef.id,
      status: 'open',
      createdAt: serverTimestamp(),
      commercialAgentId: link.commercialAgentId,
    })

    return res.status(201).end()
  })
  .get(
    checkSuperAdmin(async (_, res) => {
      const snapshot = await firestore.collection('proposalRequests').get()
      const collection: any[] = []
      snapshot.forEach((doc) => collection.push(doc.data()))
      res.json(collection)
    }),
  )

export default validate(MultiplierSchema, handler)
