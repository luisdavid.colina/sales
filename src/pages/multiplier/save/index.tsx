import { makeStyles } from '@material-ui/core/styles'
import { Button } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  Header: {
    minHeight: '100vh',
    backgroundImage: 'url(/images/notification.jpg)',
    padding: '1em',
    backgroundPositionX: 'center',
    backgroundPositionY: 'bottom',
    [theme.breakpoints.up('md')]: {
      padding: '2em',
      paddingLeft: '4em',
      backgroundPositionY: 'top',
      backgroundSize: 'cover',
    },
  },
  HeaderText: {
    padding: '0.8em',
    borderRadius: '15px',
    fontFamily: '"Ubuntu", sans-serif',
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#f3f3f3',
    lineHeight: '1.2em',
    textShadow: '0 2px 0 rgb(0 0 0 / 7%)',
    marginBottom: '0.67em',
    fontSize: '2.3em',
    [theme.breakpoints.up('md')]: {
      padding: '1.6em',
      fontSize: '2.8em',
      textAlign: 'left',
      maxWidth: '90%',
    },
  },
  imageContainer: {
    textAlign: 'center',
    [theme.breakpoints.up('md')]: {
      textAlign: 'left',
    },
  },
  pe: {
    fontFamily: '"Ubuntu", sans-serif',
    fontWeight: 'bold',
    textAlign: 'left',
    color: '#f3f3f3',
    lineHeight: '1.2em',
    textShadow: '0 2px 0 rgb(0 0 0 / 7%)',
    marginBottom: '0.67em',
    fontSize: '0.7em',
  },
  submit: {
    backgroundColor: '#0077ff',
    padding: theme.spacing('20px', '70px', '20px', '70px'),
    fontSize: '20px',
    fontFamily: '"Ubuntu", sans-serif',
    fontWeight: 'bold',
    marginTop: '1em',
  },
}))

export default function save() {
  const classes = useStyles()
  return (
    <div className={classes.Header}>
      <div className={classes.HeaderText}>
        <div className={classes.imageContainer}>
          <img
            src="/logo.svg"
            alt="Sales Ecosystem logo"
            style={{ width: '100%', maxWidth: 400, height: 128 }}
          />
        </div>
        <p>PARABÉNS!</p>
        <p className={classes.pe}>
          Agradecemos o interesse em nossa solução de Pagamento. <br />
          Você receberá o contato por nosso Gerente de Novos Negócios para
          concluir a contratação com a Element.
        </p>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className={classes.submit}
        >
          Finalizar
        </Button>
      </div>
    </div>
  )
}
