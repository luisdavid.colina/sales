import React, { FC, useState, useEffect } from 'react'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import MultiplierForm from '@/forms/MultiplierForm'
import { Container, Grid, Link } from '@material-ui/core'
import { NextPageContext } from 'next'
import classesDefault, { classesMd } from '@/styles/Landing.styles'


function CheckIcon() {
  return (
    <CheckCircleIcon
      color="secondary"
      style={{ fontSize: 50, marginRight: '0.2em', marginBottom: '-0.3em' }}
    >
      add_circle
    </CheckCircleIcon>
  )
}

function Header({ classes }: { classes: any }) {
  return (
    <Container maxWidth="xl" style={classes.Header}>
      <div style={classes.imageContainer}>
        <img
          src="/logo.svg"
          alt="Sales Ecosystem logo"
          style={{ width: '100%', maxWidth: 400, height: 128 }}
        />
      </div>
      <p style={classes.HeaderText}>
        Aumente a produtividade da sua equipe sem aumentar seus custos
        trabalhistas
      </p>
    </Container>
  )
}

function CopyRight({ classes }: { classes: any }) {
  return (
    <div style={classes.CopyRight}>
      <p>
        {' '}
        © Direitos Reservados
        <b>
          {' '}
          <Link color="inherit" href="/">
            {' '}
            Sales Ecosystem{' '}
          </Link>{' '}
        </b>
        {new Date().getFullYear()}.
      </p>
    </div>
  )
}

function Benefits({ classes }: { classes: any }) {
  return (
    <Grid container direction="row" justify="center" alignItems="center">
      <Grid item xs={12} style={classes.benefitsh2}>
        Mais benefícios
      </Grid>
      <Grid item md={4}>
        <img width="100%" alt="" src="/images/card.png" />
      </Grid>
      <Grid item md={7}>
        <div style={classes.benefitsdesc}>
          <ul>
            <li style={classes.bli}>
              <p>
                {' '}
                <CheckIcon /> Carrega até
                <strong style={classes.blis}> R$ 50 mil por mês</strong>
              </p>
            </li>
            <li style={classes.bli}>
              <p>
                <CheckIcon /> Paga boletos até
                <strong style={classes.blis}> R$ 5.000 por dia</strong>
              </p>
            </li>
            <li style={classes.bli}>
              <p>
                {' '}
                <CheckIcon /> Faz
                <strong style={classes.blis}> compras online</strong> com total
                segurança
              </p>
            </li>
            <li style={classes.bli}>
              <p>
                {' '}
                <CheckIcon /> <strong style={classes.blis}> Recarrega </strong>
                celular
              </p>
            </li>
            <li style={classes.bli}>
              <p>
                {' '}
                <CheckIcon /> Realiza saques no
                <strong style={classes.blis}> Banco24Horas</strong>{' '}
              </p>
            </li>
            <li style={classes.bli}>
              <p>
                {' '}
                <CheckIcon /> Tem aceitação de
                <strong style={classes.blis}> 100% </strong> da equipe
              </p>
            </li>
            <li style={classes.bli}>
              <p>
                <CheckIcon /> Oferece total liberdade de
                <strong style={classes.blis}> escolha </strong>
              </p>
            </li>
            <li style={classes.bli}>
              <p>
                <CheckIcon /> Proporciona a
                <strong style={classes.blis}> inclusão</strong> no sistema
                financeiro sem faturas
              </p>
            </li>
            <li style={classes.bli}>
              <p>
                <CheckIcon />
                <strong style={classes.blis}> Sem restrição </strong> e sem
                consulta (SPC/Serasa).{' '}
              </p>
            </li>
          </ul>
        </div>
      </Grid>
    </Grid>
  )
}

interface Props {
  linkId: string
}

const Multiplier: FC<Props> = ({ linkId }) => {
  const [width, setWindowWidth] = useState(0)
  const [classes, setClasses] = useState(classesDefault)
  const updateDimensions = () => {
    const width = window.innerWidth
    setWindowWidth(width)
    if (Number(width) < 900) {
      setClasses(classesMd)
    } else {
      setClasses({ ...classesDefault })
    }
  }
  useEffect(() => {
    updateDimensions()
    window.addEventListener('resize', updateDimensions)

    return () => window.removeEventListener('resize', updateDimensions)
  }, [])

  return (
    <>
      <Header classes={classes} />
      <Container
        style={{ position: 'relative', marginBottom: '2em' }}
        maxWidth="xl"
      >
        <div style={classes.featuresBackground} />

        <Grid container direction="row" justify="center" alignItems="center">
          <Grid item md={5}>
            <p style={classes.textWhite}>
              A recente flexibilização da CLT no final de 2019, no que diz
              respeito ao artigo 457, alterado pela Lei 13.467/17, criou um
              <strong>
                {' '}
                novo ambiente nas relações entre Empregadores e Empregados.{' '}
              </strong>
              Tais medidas afastaram os riscos da promoção comercial e outras
              areas da empresa com vistas no incentivo ao melhor desempenho
              laboral, em atividades continuas e intermitentes, sazonais ou
              esporádicas.
            </p>
          </Grid>
          <Grid item md={4}>
            <img width="100%" alt="" src="/images/card2.png" />
          </Grid>
          <Grid item md={9}>
            <p style={{ fontSize: '1.4em', color: '#0a4d9c' }}>
              Com isso, surge a possibilidade da empresa reconhecer e
              <strong> premiar aqueles colaboradores </strong> que tiveram
              desempenho extraordinário sem adicionar novos custos ou riscos
              trabalhistas. <br />
              Nesse contexto, a ferramenta mais indicada é o{' '}
              <strong>
                {' '}
                uso de cartões de crédito prepagos Mastercard vinculados{' '}
              </strong>
              ao nome e CPF dos colaboradores da sua cadeia de negócios, gerando
              valor e maior rentabilidade.
            </p>
          </Grid>
        </Grid>
      </Container>
      <div style={classes.benefits}>
        <Benefits classes={classes} />
        <div>
          <MultiplierForm linkId={linkId} classes={classes}  />
        </div>
      </div>
      <CopyRight classes={classes} />
    </>
  )
}

export async function getServerSideProps(context: NextPageContext) {
  const linkId = context.query.linkId as string
  if (!linkId) {
    return {
      notFound: true,
    }
  }
  return { props: { linkId } }
}

export default Multiplier
