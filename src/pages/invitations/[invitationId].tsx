import SpinnerPage from '@/components/SpinnerPage'
import AccountForm from '@/forms/AccountForm'
import { firestore } from '@/lib/firebase'
import Invitation from '@/types/Invitation'
import { NextPageContext } from 'next'
import { useRouter } from 'next/router'
import { FC } from 'react'
import { useDocumentDataOnce } from 'react-firebase-hooks/firestore'

interface InvitationPageProps {
  invitationId: string
}

const InvitationPage: FC<InvitationPageProps> = ({ invitationId }) => {
  const router = useRouter()
  const query = firestore.collection('invitations').doc(invitationId)
  const [invitation, loading] = useDocumentDataOnce<Invitation>(query)

  if (loading) {
    return <SpinnerPage />
  }

  if (!invitation || invitation.status !== 'open') {
    router.push('/login')
    return <SpinnerPage />
  }

  return <AccountForm invitation={invitation} />
}

export function getServerSideProps(context: NextPageContext) {
  const invitationId = context.query.invitationId as string

  return { props: { invitationId } }
}

export default InvitationPage
