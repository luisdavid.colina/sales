import AuthForm from '@/forms/AuthForm/AuthForm'

const LoginPage = () => {
  return <AuthForm />
}

export default LoginPage
