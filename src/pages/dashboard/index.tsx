import React, { useState } from 'react'
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  Pie,
  PieChart,
  ResponsiveContainer,
  Sector,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts'
import { Avatar, Grid, Typography, LinearProgress } from '@material-ui/core'
import {
  FaWallet,
  FaChartBar,
  FaDollarSign,
  FaUserFriends,
  FaRegCreditCard,
  FaPiggyBank,
  FaShieldAlt,
} from 'react-icons/fa'
import styles from 'styles/Dashboard.module.css'

const pieChartData = [
  { name: 'Contas', value: 13 },
  { name: 'Cartones', value: 22 },
  { name: 'Seguros', value: 7 },
  { name: 'Licencias', value: 8 },
]
const lineChartData = [
  {
    name: 'Dia A',
    passado: 4000,
    atual: 2400,
    amt: 2400,
  },
  {
    name: 'Dia B',
    passado: 3000,
    atual: 1398,
    amt: 2210,
  },
  {
    name: 'Dia C',
    passado: 2000,
    atual: 9800,
    amt: 2290,
  },
  {
    name: 'Dia D',
    passado: 2780,
    atual: 3908,
    amt: 2000,
  },
  {
    name: 'Dia E',
    passado: 1890,
    atual: 4800,
    amt: 2181,
  },
  {
    name: 'Dia F',
    passado: 2390,
    atual: 3800,
    amt: 2500,
  },
  {
    name: 'Dia G',
    passado: 3490,
    atual: 4300,
    amt: 2100,
  },
  {
    name: 'Dia H',
    passado: 3990,
    atual: 8900,
    amt: 2900,
  },
]

function renderActiveShape(props: any) {
  const RADIAN = Math.PI / 180
  const {
    cx,
    cy,
    midAngle,
    innerRadius,
    outerRadius,
    startAngle,
    endAngle,
    fill,
    payload,
    percent,
    value,
  } = props
  const sin = Math.sin(-RADIAN * midAngle)
  const cos = Math.cos(-RADIAN * midAngle)
  const sx = cx + (outerRadius + 10) * cos
  const sy = cy + (outerRadius + 10) * sin
  const mx = cx + (outerRadius + 30) * cos
  const my = cy + (outerRadius + 30) * sin
  const ex = mx + (cos >= 0 ? 1 : -1) * 22
  const ey = my
  const textAnchor = cos >= 0 ? 'start' : 'end'

  return (
    <g>
      <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
        {payload.name}
      </text>
      <Sector
        cx={cx}
        cy={cy}
        innerRadius={innerRadius}
        outerRadius={outerRadius}
        startAngle={startAngle}
        endAngle={endAngle}
        fill={fill}
      />
      <Sector
        cx={cx}
        cy={cy}
        startAngle={startAngle}
        endAngle={endAngle}
        innerRadius={outerRadius + 6}
        outerRadius={outerRadius + 10}
        fill={fill}
      />
      <path
        d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`}
        stroke={fill}
        fill="none"
      />
      <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        textAnchor={textAnchor}
        fill="#333"
      >{`atual ${value}`}</text>
      <text
        x={ex + (cos >= 0 ? 1 : -1) * 12}
        y={ey}
        dy={18}
        textAnchor={textAnchor}
        fill="#999"
      >
        {`(${(percent * 100).toFixed(2)}%)`}
      </text>
    </g>
  )
}

export default function Dashboard() {
  const [activeIndex, setActiveIndexId] = useState(0)
  return (
    <>
      <Grid container spacing={6}>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <div className={styles.smallWidget}>
            <div>
              <Grid container item alignItems="center">
                <Grid item xs={12}>
                  <Typography variant="subtitle2" color="inherit">
                    CARTEIRA
                  </Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography variant="h4" color="inherit">
                    $12,678
                  </Typography>
                </Grid>
                <Grid item xs={3}>
                  <Avatar
                    style={{
                      width: 55,
                      height: 55,
                      backgroundColor: '#2898FF',
                    }}
                  >
                    <FaWallet />
                  </Avatar>
                </Grid>
              </Grid>
            </div>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Grid item xs={4}>
                <Typography variant="h6" color="primary">
                  ↓ 12%
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography>Desde o último mês</Typography>
              </Grid>
            </Grid>
          </div>
        </Grid>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <div className={styles.smallWidget}>
            <div>
              <Grid container item alignItems="center">
                <Grid item xs={12}>
                  <Typography variant="subtitle2" color="inherit">
                    TOTAL DE AGENTES
                  </Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography variant="h4" color="inherit">
                    1,600
                  </Typography>
                </Grid>
                <Grid item xs={3}>
                  <Avatar
                    style={{
                      width: 55,
                      height: 55,
                      backgroundColor: '#2898FF',
                    }}
                  >
                    <FaUserFriends />
                  </Avatar>
                </Grid>
              </Grid>
            </div>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Grid item xs={4}>
                <Typography variant="h6" color="primary">
                  ↑ 16%
                </Typography>
              </Grid>
              <Grid item xs={8}>
                <Typography>Desde o último mês</Typography>
              </Grid>
            </Grid>
          </div>
        </Grid>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <div className={styles.smallWidget}>
            <div>
              <Grid container item alignItems="center">
                <Grid item xs={12}>
                  <Typography variant="subtitle2" color="inherit">
                    PROGRESSO DA META
                  </Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography variant="h4" color="inherit">
                    75.5%
                  </Typography>
                </Grid>
                <Grid item xs={3}>
                  <Avatar
                    style={{
                      width: 55,
                      height: 55,
                      backgroundColor: '#2898FF',
                    }}
                  >
                    <FaChartBar />
                  </Avatar>
                </Grid>
              </Grid>
            </div>
            <br />
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Grid item xs={12}>
                <LinearProgress variant="determinate" value={75} />
              </Grid>
            </Grid>
          </div>
        </Grid>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <div className={styles.smallWidget}>
            <div>
              <Grid container item alignItems="center">
                <Grid item xs={12}>
                  <Typography variant="subtitle2" color="inherit">
                    LUCRO TOTAL
                  </Typography>
                </Grid>
                <Grid item xs={9}>
                  <Typography variant="h4" color="inherit">
                    $23,200
                  </Typography>
                </Grid>
                <Grid item xs={3}>
                  <Avatar
                    style={{
                      width: 55,
                      height: 55,
                      backgroundColor: '#2898FF',
                    }}
                  >
                    <FaDollarSign />
                  </Avatar>
                </Grid>
              </Grid>
            </div>
          </div>
        </Grid>
        <Grid item xs={12} md={8}>
          <div className={styles.longWidget}>
            <Typography variant="h6" color="inherit">
              ÚLTIMAS VENDAS
            </Typography>
            <ResponsiveContainer width="100%" height={300}>
              <LineChart
                width={500}
                height={300}
                data={lineChartData}
                margin={{
                  top: 15,
                  right: 15,
                  left: 0,
                  bottom: 5,
                }}
              >
                <CartesianGrid strokeDasharray="3 3" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
                <Legend />
                <Line
                  type="monotone"
                  dataKey="atual"
                  stroke="#2898ff"
                  activeDot={{ r: 8 }}
                />
                <Line type="monotone" dataKey="passado" stroke="#41b490" />
              </LineChart>
            </ResponsiveContainer>
          </div>
        </Grid>
        <Grid item xs={12} md={4}>
          <div className={styles.longWidget}>
            <Typography variant="h6" color="inherit">
              PRODUTOS
            </Typography>
            <ResponsiveContainer width="100%" height={260}>
              <PieChart width={200} height={200}>
                <Pie
                  activeIndex={activeIndex}
                  activeShape={renderActiveShape}
                  data={pieChartData}
                  innerRadius={60}
                  fill="#2898FF"
                  outerRadius={80}
                  dataKey="value"
                  onMouseEnter={(e: any, id: number) => setActiveIndexId(id)}
                />
              </PieChart>
            </ResponsiveContainer>
            <div>
              <Grid container item alignItems="center">
                <Grid item xs={1}>
                  <FaDollarSign
                    style={{
                      width: 25,
                      height: 25,
                      color: '#2898FF',
                    }}
                  />
                </Grid>
                <Grid item xs={2}>
                  <Typography variant="h6" color="primary">
                    22
                  </Typography>
                </Grid>
                <Grid item xs={1}>
                  <FaPiggyBank
                    style={{
                      width: 25,
                      height: 25,
                      color: '#2898FF',
                    }}
                  />
                </Grid>
                <Grid item xs={2}>
                  <Typography variant="h6" color="primary">
                    12
                  </Typography>
                </Grid>
                <Grid item xs={1}>
                  <FaRegCreditCard
                    style={{
                      width: 25,
                      height: 25,
                      color: '#2898FF',
                    }}
                  />
                </Grid>
                <Grid item xs={2}>
                  <Typography variant="h6" color="primary">
                    72
                  </Typography>
                </Grid>
                <Grid item xs={1}>
                  <FaShieldAlt
                    style={{
                      width: 25,
                      height: 25,
                      color: '#2898FF',
                    }}
                  />
                </Grid>
                <Grid item xs={1}>
                  <Typography variant="h6" color="primary">
                    11
                  </Typography>
                </Grid>
              </Grid>
            </div>
          </div>
        </Grid>
      </Grid>
    </>
  )
}
