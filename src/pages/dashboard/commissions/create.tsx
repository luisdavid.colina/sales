import { useState, forwardRef, useMemo, memo, useCallback, FC } from 'react'

import { commission } from '@/forms/defaultStates/salesCommission'
import SalesComissionSchema, {
  Commission,
  SalesCommission,
} from '@/schemas/SalesCommissionSchema'

import axios from 'axios'
import { useRouter } from 'next/router'

import useBoolean from '@/hooks/useBoolean'

import {
  Button,
  Typography,
  Grid,
  TextField,
  Paper,
  Box,
  CircularProgress,
  makeStyles,
} from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/DeleteOutline'
import {
  GridRowId,
  GridColDef,
  GridSelectionModelChangeParams,
  GridToolbarContainer,
  GridColumnsToolbarButton,
  GridToolbarContainerProps,
  GridFilterToolbarButton,
  GridEditCellPropsParams,
} from '@material-ui/data-grid'
import ComissionDataGrid from '@/components/tables/CommissionsTable/CommissionDataGrid'

import { blue } from '@material-ui/core/colors'

import SuccessModal from '@/components/SuccessModal'
import { priceFormatter } from '@/util/currency'

type CommissionRow = Commission & {
  id?: string
}

const useStyles = makeStyles((theme) => ({
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
  },
  buttonProgress: {
    color: blue[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}))

const columnsFixed: GridColDef[] = [
  {
    editable: true,
    field: 'from',
    headerName: 'Taxa ADM Min',
    flex: 1,
    type: 'number',
    valueFormatter: ({ value }) => `${priceFormatter.format(Number(value))}`,
  },
  {
    editable: true,
    field: 'to',
    headerName: 'Taxa ADM Max',
    flex: 1,
    type: 'number',
    valueFormatter: ({ value }) => `${priceFormatter.format(Number(value))}`,
  },
  {
    hide: true,
    field: 'salesChannel',
    headerName: 'Tipo de Venda',
    flex: 1,
  },
  {
    editable: true,
    field: 'loads',
    type: 'number',
    headerName: 'Comissão s/ Cargas',
    flex: 1,
    valueFormatter: ({ value }) => `${value}%`,
  },
]

const columnsRated: GridColDef[] = [
  {
    editable: true,
    field: 'from',
    headerName: 'Taxa ADM Min',
    flex: 1,
    type: 'number',
    valueFormatter: ({ value }) => `${value}%`,
  },
  {
    editable: true,
    field: 'to',
    headerName: 'Taxa ADM Max',
    flex: 1,
    type: 'number',
    valueFormatter: ({ value }) => `${value}%`,
  },
  {
    hide: true,
    field: 'salesChannel',
    headerName: 'Tipo de Venda',
    flex: 1,
  },
  {
    editable: true,
    field: 'loads',
    type: 'number',
    headerName: 'Comissão s/ Cargas',
    flex: 1,
    valueFormatter: ({ value }) => `${value}%`,
  },
]

type ToolbarProp = {
  addRow: () => void
  deleteRows: () => void
  disabledDelete: boolean
  title: string
}

export const withActions = ({
  addRow,
  deleteRows,
  disabledDelete,
  title,
}: ToolbarProp) =>
  memo(
    forwardRef<HTMLDivElement, GridToolbarContainerProps>((props, ref) => {
      return (
        <GridToolbarContainer
          style={{
            padding: '0.5em',
            background: '#fafafa',
            borderBottom: '1px solid #f0f0f0',
            justifyContent: 'space-between',
          }}
          ref={ref}
          {...props}
        >
          <Typography variant="subtitle2" color="primary">
            {title}
          </Typography>
          <div>
            <GridColumnsToolbarButton />
            <GridFilterToolbarButton />
            <Button
              size="small"
              color="primary"
              onClick={addRow}
              startIcon={<AddIcon />}
            >
              Inserir
            </Button>

            <Button
              size="small"
              color="primary"
              onClick={deleteRows}
              startIcon={<RemoveIcon />}
              disabled={disabledDelete}
            >
              Deletar
            </Button>
          </div>
        </GridToolbarContainer>
      )
    }),
  )

type CreateComissionProps = {
  commission?: {
    name: string
    fixedCommissions: Commission[]
    ratedCommissions: Commission[]
  }
}

const defaultCommission = new Array(3).fill(0).map((_, index) => ({
  ...commission,
  id: `${index}`,
}))

const CreateComission: FC<CreateComissionProps> = ({
  commission: {
    name: nameProp = '',
    fixedCommissions = [...defaultCommission],
    ratedCommissions = [...defaultCommission],
  } = {},
}) => {
  const router = useRouter()
  const classes = useStyles()
  const { value, toFalse, toTrue } = useBoolean({ initialValue: false })
  const {
    value: loading,
    toFalse: loadingFalse,
    toTrue: loadingTrue,
  } = useBoolean({ initialValue: false })
  const [name, setName] = useState<string>(nameProp)
  const [errors, setErrors] = useState<{
    name?: string
    fixedCommissions?: string
    ratedCommissions?: string
  }>()

  const [fixed, setFixed] = useState<CommissionRow[]>(fixedCommissions)
  const [rated, setRated] = useState<CommissionRow[]>(ratedCommissions)

  const [selectedFixed, setSelectedFixed] = useState<GridRowId[]>([])
  const [selectedRated, setSelectedRated] = useState<GridRowId[]>([])

  const selectFixed = useCallback((param: GridSelectionModelChangeParams) => {
    setSelectedFixed(param.selectionModel)
  }, [])

  const selectRated = useCallback((param: GridSelectionModelChangeParams) => {
    setSelectedRated(param.selectionModel)
  }, [])

  const removeFixed = useCallback(() => {
    setFixed((beforeFixed) =>
      beforeFixed.filter((item) => !selectedFixed.includes(item.id as string)),
    )
  }, [selectedFixed])

  const removeRated = useCallback(() => {
    setRated((beforeRated) =>
      beforeRated.filter((item) => !selectedRated.includes(item.id as string)),
    )
  }, [selectedRated])

  const addFixed = useCallback(() => {
    setFixed((beforeFixed) => [
      ...beforeFixed,
      {
        ...commission,
        id: `${Number(beforeFixed[beforeFixed.length - 1].id as string) + 1}`,
      },
    ])
  }, [])

  const addRated = useCallback(() => {
    setRated((beforeRated) => [
      ...beforeRated,
      {
        ...commission,
        id: `${Number(beforeRated[beforeRated.length - 1].id as string) + 1}`,
      },
    ])
  }, [])

  const saveCommission = async (value: any) => {
    loadingTrue()

    try {
      const response = await axios.post('/api/sales-commissions', value)

      if (response.status === 201) {
        toTrue()
      }
    } catch (err) {
      const error = err as any
       
      /* eslint no-console: ["error", { allow: ["error"] }] */
    }

    loadingFalse()
  }

  const handleSubmit = async () => {
    SalesComissionSchema.validate(
      {
        name,
        fixedCommissions: fixed,
        ratedCommissions: rated,
      },
      { abortEarly: false },
    )
      .then((value) => {
        saveCommission(value)
        // console.log(value)
      })
      .catch((err) => {
        const copyErrors = { ...errors }
        err.inner.forEach((e: { path: string; message: string }) => {
          copyErrors[e.path as keyof SalesCommission] = e.message
        })
        setErrors(copyErrors)
      })
  }

  const ToolbarFixed = useMemo(() => {
    return withActions({
      title: 'Tabla Fixa',
      addRow: addFixed,
      deleteRows: removeFixed,
      disabledDelete:
        selectedFixed.length === 0 || fixed.length === selectedFixed.length,
    })
  }, [selectedFixed, fixed])

  const ToolbarRated = useMemo(
    () =>
      withActions({
        title: 'Tabla Variale',
        addRow: addRated,
        deleteRows: removeRated,
        disabledDelete:
          selectedRated.length === 0 || rated.length === selectedRated.length,
      }),
    [selectedRated, rated],
  )

  const updateFixed = useCallback(
    ({ field, id, props: { value } }: GridEditCellPropsParams) => {
      setFixed((beforeFixed) => {
        const copy = beforeFixed.map((i) => ({ ...i }))
        const selectItem = copy.find((e) => e.id === id)
        const key = field as keyof Commission
        if (selectItem) {
          ;(selectItem[key] as unknown) = value as number
        }
        return copy
      })
    },
    [],
  )

  const updateRated = useCallback(
    ({ field, id, props: { value } }: GridEditCellPropsParams) => {
      setRated((beforeRated) => {
        const copy = beforeRated.map((i) => ({ ...i }))
        const selectItem = copy.find((e) => e.id === id)
        const key = field as keyof Commission
        if (selectItem) {
          ;(selectItem[key] as unknown) = value as number
        }
        return copy
      })
    },
    [],
  )

  const handleName = (e: any) => {
    setName(e.target.value)
  }

  const onSuccess = () => {
    toFalse()
    router.push('/dashboard/commissions')
  }

  return (
    <>
      <Typography variant="h4" component="h2" gutterBottom>
        Nova tabela de Comissionamento
      </Typography>

      <Paper elevation={4}>
        <Box p={5}>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            spacing={5}
          >
            <Grid item xs={12}>
              <TextField
                size="small"
                label="Nomo da tabela"
                fullWidth
                variant="outlined"
                value={name}
                onChange={handleName}
                error={!!errors?.name}
                helperText={errors?.name}
              />
            </Grid>
            <Grid item md={6}>
              <ComissionDataGrid
                commissions={fixed}
                columns={columnsFixed}
                onSelectionModelChange={selectFixed}
                selectionModel={selectedFixed}
                checkboxSelection
                disableSelectionOnClick
                onEditCellChangeCommitted={updateFixed}
                hideFooterPagination
                components={{
                  Toolbar: ToolbarFixed,
                }}
              />
            </Grid>
            <Grid item md={6}>
              <ComissionDataGrid
                commissions={rated}
                columns={columnsRated}
                onSelectionModelChange={selectRated}
                selectionModel={selectedRated}
                checkboxSelection
                disableSelectionOnClick
                onEditCellChangeCommitted={updateRated}
                hideFooterPagination
                components={{
                  Toolbar: ToolbarRated,
                }}
              />
            </Grid>
          </Grid>
        </Box>
        <div className={classes.footer}>
          <Button onClick={handleSubmit} variant="contained" color="primary">
            Salvar comisionamiento
          </Button>
          {loading && (
            <CircularProgress size={24} className={classes.buttonProgress} />
          )}
        </div>
      </Paper>

      <SuccessModal
        title="Novo comisionamiento inserido com sucesso"
        open={value}
        onClose={onSuccess}
      />
    </>
  )
}

export default CreateComission
