import { useState, FC, useEffect } from 'react'

import Link from 'next/link'

import { Commission } from '@/schemas/SalesCommissionSchema'

import SalesCommission from '@/types/SalesCommission'

import CommissionsAutocomplete from '@/components/autocompletes/CommissionsAutocomplete'

import { useRouter } from 'next/router'

import {
  Typography,
  Grid,
  Paper,
  Box,
  Button,
  makeStyles,
} from '@material-ui/core'
import { GridColDef, GridValueGetterParams } from '@material-ui/data-grid'
import Table from '@/components/Table'
import { priceFormatter } from '@/util/currency'

const useStyles = makeStyles((theme) => ({
  table: {
    fontSize: '13px',
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
    '& > *': {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  },
}))

const fixedColumns: GridColDef[] = [
  {
    field: 'range',
    headerName: 'Taxa ADM (R$)',
    flex: 1,
    type: 'number',
    valueGetter: (params: GridValueGetterParams) =>
      `${priceFormatter.format(Number(params.getValue('from')))} -
    ${priceFormatter.format(Number(params.getValue('to')))}`,
  },
  {
    field: 'salesChannel',
    type: 'string',
    headerName: 'Tipo de Venda',
    flex: 1,
  },
  {
    field: 'loads',
    type: 'number',
    headerName: 'Comissão s/ Cargas',
    flex: 1,
    valueFormatter: ({ value }) => `${value}%`,
  },
]

const ratedColumns: GridColDef[] = [
  {
    field: 'range',
    headerName: 'Taxa ADM (%)',
    flex: 1,
    type: 'number',
    valueGetter: (params: GridValueGetterParams) =>
      `${params.getValue('from')}% - ${params.getValue('to')}%`,
  },
  {
    field: 'salesChannel',
    headerName: 'Tipo de Venda',
    flex: 1,
  },
  {
    field: 'loads',
    flex: 1,
    type: 'number',
    headerName: 'Comissão s/ Cargas',
    valueFormatter: ({ value }) => `${value}%`,
  },
]

const Commissions: FC = () => {
  const router = useRouter()
  const classes = useStyles()

  const [
    salesCommission,
    setSalesCommission,
  ] = useState<SalesCommission | null>()

  const [fixed, setFixed] = useState<Commission[]>([])
  const [rated, setRated] = useState<Commission[]>([])

  const selectCommission = (commission: SalesCommission | null) => {
    setSalesCommission(commission)
  }

  const back = () => {
    router.push(`/dashboard/commercial-agents/`)
  }

  const editCommission = () => {
    router.push(`/dashboard/commissions/${salesCommission?.id}/edit`)
  }

  const newCommission = () => {
    router.push('/dashboard/commissions/create')
  }

  useEffect(() => {
    const updateCommissions = () => {
      if (salesCommission) {
        setFixed(
          salesCommission.fixedCommissions.map((item, idx) => ({
            ...item,
            id: `${idx}`,
          })),
        )
        setRated(
          salesCommission.ratedCommissions.map((item, idx) => ({
            ...item,
            id: `${idx}`,
          })),
        )
      } else {
        setFixed([])
        setRated([])
      }
    }
    updateCommissions()
  }, [salesCommission])

  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Comissão de vendas
        </Typography>
        <Box display="flex" justifyContent="space-between">
          <Link href="/dashboard/commercial-agents">
            <Button variant="text" color="primary">
              Multiplicadores
            </Button>
          </Link>
        </Box>
      </Grid>
      <Paper elevation={4}>
        <Box p={5}>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
            spacing={5}
          >
            <Grid item xs={12}>
              <CommissionsAutocomplete onChange={selectCommission} />
            </Grid>
            <Grid item md={6}>
              <Table
                className={classes.table}
                rows={fixed}
                columns={fixedColumns}
                density="compact"
              />
            </Grid>
            <Grid item md={6}>
              <Table
                className={classes.table}
                rows={rated}
                columns={ratedColumns}
                density="compact"
              />
            </Grid>
          </Grid>
        </Box>
        <div className={classes.footer}>
          <Button onClick={back} type="button" variant="contained">
            Voltar
          </Button>
          <Button
            onClick={editCommission}
            variant="contained"
            color="primary"
            disabled={!salesCommission}
          >
            Editar
          </Button>
          <Button onClick={newCommission} variant="contained" color="primary">
            Nova tabela
          </Button>
        </div>
      </Paper>
    </>
  )
}

export default Commissions
