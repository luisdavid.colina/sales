import { useState, MouseEvent } from 'react'
import NewCommunication from '@/components/Communication/NewCommunication'
import CommunicationsTable from '@/components/tables/CommunicationsTable'
import { Button, Grid, Typography, Modal } from '@material-ui/core'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import { firestore } from '@/lib/firebase'
import { useCollectionDataOnce } from 'react-firebase-hooks/firestore'
import { GridRowParams } from '@material-ui/data-grid'
import Communication from '@/types/Communication'
import useClickPreventionOnDoubleClick from '@/hooks/useClickPreventionOnDoubleClick'
import Error from 'next/error'
import SpinnerPage from '@/components/SpinnerPage'
import useBoolean from '@/hooks/useBoolean'
import CommunicationModal from '@/components/modals/CommunicationModal'

const Communications = () => {
  const [opens, setOpens] = useState(false)
  const query = firestore.collection('communications')
  const [communications, loading, error] = useCollectionDataOnce<Communication>(
    query,
  )
  const [selected, setSelected] = useState<Communication>()
  const { value: open, toFalse: closeModal, toTrue: openModal } = useBoolean({
    initialValue: false,
  })
  const onClick = (params: GridRowParams) => {
    setSelected(params.row as Communication)
    openModal()
  }

  const onDoubleClick = (params: GridRowParams, e: MouseEvent<any>) => {
    setSelected(params.row as Communication)
    openModal()
  }
  const [click, doubleClick] = useClickPreventionOnDoubleClick(
    onClick,
    onDoubleClick,
  )
  const handleOpen = () => {
    setOpens(true)
  }
  const handleClose = () => {
    setOpens(false)
  }

  if (loading) return <SpinnerPage />

  if (error) return <Error statusCode={500} />

  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Comunicação
        </Typography>
        <Button
          variant="text"
          color="primary"
          endIcon={<AddCircleIcon />}
          onClick={handleOpen}
        >
          Nova Mensagem
        </Button>
      </Grid>
      <CommunicationsTable
        onRowClick={click}
        onRowDoubleClick={doubleClick}
        communications={communications as Communication[]}
      />

      <Modal open={opens} onClose={handleClose}>
        <NewCommunication />
      </Modal>
      {open && selected && (
        <CommunicationModal
          open={open}
          onClose={closeModal}
          communication={selected}
        />
      )}
    </>
  )
}

export default Communications
