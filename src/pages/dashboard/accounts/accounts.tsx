import { useState, useCallback } from 'react'
import axios from 'axios'
import {
  Grid,
  makeStyles,
  Typography,
  Button,
  CircularProgress,
} from '@material-ui/core'
import { blue } from '@material-ui/core/colors'
import SuccessModal from '@/components/SuccessModal'
import AccountsTable, { Account } from '@/components/tables/AccountsTable'
import Permissions from '@/components/Permissions'
import useBoolean from '@/hooks/useBoolean'
import hasPermission from '@/hooks/hasPermission'

const useStyles = makeStyles((theme) => ({
  wrapper: {
    position: 'relative',
  },
  buttonProgress: {
    color: blue[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  footer: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
    '& > *': {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  },
}))
const Accounts = () => {
  const classes = useStyles()
  const disabledButton = !hasPermission({ profiles: ['write'] })
  const [accounts, setAccounts] = useState<Account[]>([])
  const {
    value: loading,
    toFalse: isNotLoading,
    toTrue: isLoading,
  } = useBoolean({
    initialValue: false,
  })
  const {
    value: success,
    toTrue: isSuccess,
    toFalse: isNotSuccess,
  } = useBoolean({
    initialValue: false,
  })

  const onClose = () => {
    isNotSuccess()
  }

  const handleSave = useCallback(async () => {
    try {
      isLoading()
      await axios.post('/api/accounts', { accounts })
      isSuccess()
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err)
      // TODO
    } finally {
      isNotLoading()
    }
  }, [accounts])

  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Perfis
        </Typography>
      </Grid>
      <Permissions permissions={{ profiles: ['read'] }}>
        <AccountsTable onChange={setAccounts} disabled={disabledButton} />
        <div className={classes.footer}>
          <Typography gutterBottom>
            RN = Regras de Negócios / CO = Comunicaçao / PE = Perfis
          </Typography>
          <div className={classes.wrapper}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              onClick={handleSave}
              disabled={disabledButton || loading}
            >
              Enviar alterações
            </Button>
            {loading && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
          </div>
        </div>
      </Permissions>
      <SuccessModal
        title="Usuários editados com sucesso"
        open={success}
        onClose={onClose}
      />
    </>
  )
}

export default Accounts
