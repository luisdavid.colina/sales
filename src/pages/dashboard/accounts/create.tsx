import { ChangeEvent, useState } from 'react'
import { useRouter } from 'next/router'
import axios from 'axios'
import CreateAccount, {
  FormData,
} from '@/forms/CreateAccountForm/CreateAccount'
import useBoolean from '@/hooks/useBoolean'
import SuccessModal from '@/components/SuccessModal'
import Permissions from '@/components/Permissions'

import {
  Grid,
  makeStyles,
  MenuItem,
  TextField,
  Typography,
  Paper,
  Container,
} from '@material-ui/core'
import {
  COMMERCIAL_AGENT_ROLE,
  DEVELOPER_ROLE,
  EXPLORER_ROLE,
} from '@/constants'

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: theme.spacing(2),
  },
  selectField: {
    minWidth: '200px',
  },
  content: {
    paddingTop: theme.spacing(4),
    width: '100%',
  },
}))

const Create = () => {
  const classes = useStyles()
  const router = useRouter()

  const [option, setOption] = useState<string>(DEVELOPER_ROLE)
  const {
    value: loading,
    toFalse: isNotLoading,
    toTrue: isLoading,
  } = useBoolean({
    initialValue: false,
  })
  const { value: success, toTrue: isSuccess } = useBoolean({
    initialValue: false,
  })

  const handleSuccess = (role: string) => (data: FormData) => {
    isLoading()
    axios
      .post(`/api/company/invitations/${role}`, data)
      .then(isSuccess)
      .finally(isNotLoading)
  }

  const onClose = () => {
    router.push('/dashboard/accounts')
  }

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target
    setOption(value)
  }

  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Licenças
        </Typography>
      </Grid>
      <Permissions permissions={{ profiles: ['create'] }}>
        <Paper elevation={4} className={classes.content}>
          <Container>
            <TextField
              className={classes.selectField}
              select
              label="Tipos de licenças"
              variant="outlined"
              size="small"
              value={option}
              onChange={handleChange}
              fullWidth
            >
              <MenuItem value={DEVELOPER_ROLE}>Desenvolvedores</MenuItem>
              <MenuItem value={EXPLORER_ROLE}>Exploradores</MenuItem>
              <MenuItem value={COMMERCIAL_AGENT_ROLE}>
                Usuários/Agentes Comerciais
              </MenuItem>
            </TextField>
          </Container>
          {option === DEVELOPER_ROLE && <br />}
          {option === EXPLORER_ROLE && (
            <CreateAccount
              loading={loading}
              onSuccess={handleSuccess(EXPLORER_ROLE)}
            />
          )}
          {option === COMMERCIAL_AGENT_ROLE && (
            <CreateAccount
              loading={loading}
              onSuccess={handleSuccess(COMMERCIAL_AGENT_ROLE)}
              checkboxDisabled
            />
          )}
        </Paper>
        <SuccessModal
          title="Usuários criados com sucesso"
          open={success}
          onClose={onClose}
        />
      </Permissions>
    </>
  )
}

export default Create
