import { Typography } from '@material-ui/core'
import Information from '@/components/Information'

const Formation = () => {
  return (
    <>
      <Typography variant="h4" component="h2" gutterBottom>
        Formação
      </Typography>
      <Information />
    </>
  )
}

export default Formation
