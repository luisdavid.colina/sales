import { useState, useEffect, MouseEvent } from 'react'

import Grid from '@material-ui/core/Grid'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import { FaEdit } from 'react-icons/fa'
import useContextMenu from '@/hooks/useContextMenu'
import useBoolean from '@/hooks/useBoolean'
import useCommercialAgents from '@/hooks/useCommercialAgents'
import useClickPreventionOnDoubleClick from '@/hooks/useClickPreventionOnDoubleClick'
import CommercialAgentsModal from '@/components/modals/CommercialAgentsModal'
import CommercialAgentsTable from '@/components/tables/CommercialAgentsTable'
import CommercialAgent from '@/types/CommercialAgent'
import Link from 'next/link'
import Error from 'next/error'
import { GridRowParams } from '@material-ui/data-grid'
import SpinnerPage from '@/components/SpinnerPage'
import { useRouter } from 'next/router'

import axios from 'axios'

const CommercialAgents = (): JSX.Element => {
  const router = useRouter()
  const { commercialAgents, loading, error } = useCommercialAgents()

  const { state, onOpen, onClose } = useContextMenu()
  const [selected, setSelected] = useState<any>()
  const { value: open, toFalse: closeModal, toTrue: openModal } = useBoolean({
    initialValue: false,
  })

  const onClick = (params: GridRowParams) => {
    setSelected(params.row)
    openModal()
  }

  const onDoubleClick = (params: GridRowParams, e: MouseEvent<any>) => {
    setSelected(params.row)
    onOpen(e)
  }

  const [click, doubleClick] = useClickPreventionOnDoubleClick(
    onClick,
    onDoubleClick,
  )

  const handleClose = () => {
    setSelected(undefined)
  }

  const editCommercialAgent = () => {
    handleClose()
    if (selected)
      router.push(`/dashboard/commercial-agents/${selected.id}/edit`)
  }

  if (loading) return <SpinnerPage />

  if (error) return <Error statusCode={500} />

  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Multiplicadores
        </Typography>
        <Box display="flex" justifyContent="space-between">
          <Link href="/dashboard/commissions">
            <Button variant="text" color="primary">
              Tabelas de comissões
            </Button>
          </Link>
        </Box>
      </Grid>
      <CommercialAgentsTable
        onRowClick={click}
        onRowDoubleClick={doubleClick}
        commercialAgents={commercialAgents as CommercialAgent[]}
      />
      {selected && (
        <Menu
          keepMounted
          open={state.mouseY !== null}
          onClose={onClose}
          anchorReference="anchorPosition"
          anchorPosition={
            state.mouseY !== null && state.mouseX !== null
              ? { top: state.mouseY, left: state.mouseX }
              : undefined
          }
        >
          <MenuItem onClick={editCommercialAgent}>
            <ListItemIcon>
              <FaEdit />
            </ListItemIcon>
            <Typography>Editar</Typography>
          </MenuItem>
        </Menu>
      )}
      <CommercialAgentsModal
        open={open}
        onClose={closeModal}
        commercialAgent={selected}
      />
    </>
  )
}

export default CommercialAgents
