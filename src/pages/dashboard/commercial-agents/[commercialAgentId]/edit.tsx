import { FC } from 'react'

import { privateFetch } from '@/util/fetch'
import { NextPageContext } from 'next'

import CommercialAgentForm from '@/forms/CommercialAgentForm'
import { Grid, makeStyles, Typography } from '@material-ui/core'
import axios from 'axios'
import { useRouter } from 'next/router'

import CommercialAgent from '@/types/CommercialAgent'

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: theme.spacing(2),
  },
  selectField: {
    minWidth: '200px',
  },
}))

type editCommercialAgentProps = {
  commercialAgent: CommercialAgent
  id: string
}

const EditCommercialAgent: FC<editCommercialAgentProps> = ({
  id,
  commercialAgent,
}) => {
  const router = useRouter()
  const classes = useStyles()

  const onSuccess = (commercialAgent: CommercialAgent) => {
    return axios.put(`/api/commercial-agents/${id}`, commercialAgent)
  }

  const callback = () => {
    router.push('/dashboard/commercial-agents')
  }

  return (
    <>
      <Grid
        className={classes.header}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Editar commercial agent
        </Typography>
      </Grid>
      <CommercialAgentForm
        commercialAgent={commercialAgent}
        onSuccess={onSuccess}
        code={200}
        titleModal="Agente comercial editada com sucesso"
        actionModal={callback}
      />
    </>
  )
}

export const getServerSideProps = async (context: NextPageContext) => {
  const id = context.query.commercialAgentId as string
  const {
    data: { company, salesCommissionId },
  } = await privateFetch(context).get(`/commercial-agents/${id}`)

  return {
    props: {
      id,
      commercialAgent: {
        company,
        salesCommissionId,
      } as CommercialAgent,
    },
  }
}

export default EditCommercialAgent
