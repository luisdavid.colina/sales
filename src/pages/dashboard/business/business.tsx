import { useState } from 'react'
import BusinessTable from '@/components/tables/BusinessTable'
import { Typography } from '@material-ui/core'
import useBoolean from '@/hooks/useBoolean'
import BusinessModal from '@/components/modals/BusinessModal'

const business = [
  {
    id: 1,
    invoice_owner_name: 'AURORA E FÁTIMA ESPORTES LTDA',
    invoice_number: 'NFB002398',
    invoice_issue_date: '05/04/2020',
    invoice_payment_date: '06/04/2020',
    invoice_price: 'R$ 400,00',
    status: 'Pago',
  },
  {
    id: 2,
    invoice_owner_name: 'KAMILLY E SARA FILMAGENS LTDA',
    invoice_number: 'NFB002397',
    invoice_issue_date: '05/04/2020',
    invoice_payment_date: '06/04/2020',
    invoice_price: 'R$ 500,00',
    status: 'Pago',
  },
  {
    id: 3,
    invoice_owner_name: 'ANDERSON E THEO DOCES & SALGADOS ME',
    invoice_number: 'NFB002396',
    invoice_issue_date: '05/04/2020',
    invoice_payment_date: '-',
    invoice_price: 'R$ 83.200,00',
    status: 'Aguardando Pagamento',
  },
  {
    id: 4,
    invoice_owner_name: 'MARTIN E ALEXANDRE COMERCIO DE BEBIDAS ME',
    invoice_number: 'NFB002395',
    invoice_issue_date: '05/04/2020',
    invoice_payment_date: '06/04/2020',
    invoice_price: 'R$ 150,00',
    status: 'Pago',
  },
  {
    id: 5,
    invoice_owner_name: 'CAUÃ E RAFAEL CONSULTORIA FINANCEIRA ME',
    invoice_number: 'NFB002394',
    invoice_issue_date: '05/04/2020',
    invoice_payment_date: '-',
    invoice_price: 'R$ 2.300,00',
    status: 'Cancelado',
  },
]

const Business = () => {
  const { value, toFalse, toTrue } = useBoolean({
    initialValue: false,
  })

  const [selected, setSelected] = useState<number | undefined>()

  const onClick = (params: any) => {
    setSelected(params.rowIndex)
    toTrue()
  }

  return (
    <>
      <Typography variant="h4" component="h2" gutterBottom>
        Meus Negócios
      </Typography>
      <BusinessTable onRowClick={onClick} business={business} />
      <BusinessModal
        open={value}
        onClose={toFalse}
        business={typeof selected === 'number' ? business[selected] : undefined}
      />
    </>
  )
}

export default Business
