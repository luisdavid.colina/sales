import CommercialAgentForm from '@/forms/CommercialAgentForm'
import ContractForm from '@/forms/ContractForm'
import {
  Grid,
  makeStyles,
  MenuItem,
  TextField,
  Typography,
} from '@material-ui/core'
import { ChangeEvent, useState } from 'react'
import axios from 'axios'
import { useRouter } from 'next/router'

import Contract from '@/types/Contract'
import CommercialAgent from '@/types/CommercialAgent'

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: theme.spacing(2),
  },
  selectField: {
    minWidth: '200px',
  },
}))

function CreateContract() {
  const router = useRouter()
  const classes = useStyles()
  const [option, setOption] = useState<string>(
    (router.query.option as string) || 'client',
  )

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target
    setOption(value)
  }

  const onSuccessContract = (contract: Contract) =>
    axios.post('/api/contracts', contract)

  const onSuccessCommercialAgent = (commercialAgent: CommercialAgent) =>
    axios.post('/api/commercial-agents', commercialAgent)

  const callback = () => {
    router.push('/dashboard/contracts')
  }

  const callbackCommercialAgent = () => {
    router.push('/commercial-agents')
  }

  return (
    <>
      <Grid
        className={classes.header}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Novo contrato
        </Typography>
        <TextField
          className={classes.selectField}
          select
          label="Tipo de contratante"
          variant="outlined"
          size="small"
          value={option}
          onChange={handleChange}
        >
          <MenuItem value="client">Cliente</MenuItem>
          <MenuItem value="agent">Agente Comercial</MenuItem>
        </TextField>
      </Grid>
      {option === 'client' && (
        <ContractForm
          onSuccess={onSuccessContract}
          code={201}
          titleModal="Contrato inserido com sucesso"
          actionModal={callback}
        />
      )}
      {option === 'agent' && (
        <CommercialAgentForm
          onSuccess={onSuccessCommercialAgent}
          code={201}
          titleModal="Agente comercial inserido com sucesso"
          actionModal={callbackCommercialAgent}
        />
      )}
    </>
  )
}

export default CreateContract
