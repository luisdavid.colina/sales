import { useState, MouseEvent } from 'react'
import axios from 'axios'
import Grid from '@material-ui/core/Grid'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import { FaSignature, FaEdit, FaDownload } from 'react-icons/fa'
import useContextMenu from '@/hooks/useContextMenu'
import useBoolean from '@/hooks/useBoolean'
import useClickPreventionOnDoubleClick from '@/hooks/useClickPreventionOnDoubleClick'
import ContractsTable from '@/components/tables/ContractsTable'
import ContractModal from '@/components/modals/ContractModal'
import Contract from '@/types/Contract'
import Link from 'next/link'
import Error from 'next/error'
import { GridRowParams } from '@material-ui/data-grid'
import { firestore } from '@/lib/firebase'
import { useCollectionDataOnce } from 'react-firebase-hooks/firestore'
import SpinnerPage from '@/components/SpinnerPage'
import { useRouter } from 'next/router'

const Contracts = () => {
  const router = useRouter()
  const query = firestore.collection('contracts').orderBy('createdAt', 'desc')
  const [contracts, loading, error] = useCollectionDataOnce<Contract>(query)

  const { state, onOpen, onClose } = useContextMenu()
  const [selected, setSelected] = useState<Contract>()
  const { value: open, toFalse: closeModal, toTrue: openModal } = useBoolean({
    initialValue: false,
  })

  const onClick = (params: GridRowParams) => {
    setSelected(params.row as Contract)
    openModal()
  }

  const onDoubleClick = (params: GridRowParams, e: MouseEvent<any>) => {
    setSelected(params.row as Contract)
    onOpen(e)
  }

  const [click, doubleClick] = useClickPreventionOnDoubleClick(
    onClick,
    onDoubleClick,
  )

  const handleClose = () => {
    setSelected(undefined)
  }

  const downloadFile = () => {
    handleClose()
  }

  const editContract = () => {
    handleClose()
    if (selected) router.push(`/dashboard/contracts/${selected.id}/edit`)
  }

  const requestSignatures = async () => {
    handleClose()
    if (selected)
      try {
        await axios.put(`/api/contracts/${selected.id}/sign`)
        router.reload()
      } catch (err) {
        const error = err as any

        // eslint-disable-next-line no-console
        console.log(error.message)
      }
  }

  if (loading) return <SpinnerPage />

  if (error) return <Error statusCode={500} />

  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Contratos
        </Typography>
        <Link href="/dashboard/contracts/create?option=client">
          <Button variant="text" color="primary" endIcon={<AddCircleIcon />}>
            Novo contrato
          </Button>
        </Link>
      </Grid>
      <ContractsTable
        onRowClick={click}
        onRowDoubleClick={doubleClick}
        contracts={contracts as Contract[]}
      />
      {selected && (
        <Menu
          keepMounted
          open={state.mouseY !== null}
          onClose={onClose}
          anchorReference="anchorPosition"
          anchorPosition={
            state.mouseY !== null && state.mouseX !== null
              ? { top: state.mouseY, left: state.mouseX }
              : undefined
          }
        >
          <MenuItem onClick={editContract}>
            <ListItemIcon>
              <FaEdit />
            </ListItemIcon>
            <Typography>Editar</Typography>
          </MenuItem>
          <MenuItem
            onClick={requestSignatures}
            disabled={selected.d4signStatus !== 'AwaitingUploadFile'}
          >
            <ListItemIcon>
              <FaSignature />
            </ListItemIcon>
            <Typography>Pedir assinatura</Typography>
          </MenuItem>

          <MenuItem
            onClick={downloadFile}
            component="a"
            download
            href={`/api/contracts/${selected.id}/download`}
          >
            <ListItemIcon>
              <FaDownload />
            </ListItemIcon>
            <Typography>Download</Typography>
          </MenuItem>
        </Menu>
      )}
      {open && selected && (
        <ContractModal
          open={open}
          onClose={closeModal}
          contractId={selected.id}
        />
      )}
    </>
  )
}

export default Contracts
