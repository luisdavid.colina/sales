import { FC } from 'react'

import { privateFetch } from '@/util/fetch'
import { NextPageContext } from 'next'

import ContractForm from '@/forms/ContractForm'
import { Grid, makeStyles, Typography } from '@material-ui/core'
import axios from 'axios'
import { useRouter } from 'next/router'

import Contract from '@/types/Contract'

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: theme.spacing(2),
  },
  selectField: {
    minWidth: '200px',
  },
}))

type editContractProps = {
  contract: Contract
  id: string
}

const EditContract: FC<editContractProps> = ({ id, contract }) => {
  const router = useRouter()
  const classes = useStyles()

  const onSuccessContract = (contract: Contract) => {
    return axios.put(`/api/contracts/${id}`, contract)
  }

  const callback = () => {
    router.push('/dashboard/contracts')
  }

  return (
    <>
      <Grid
        className={classes.header}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Editar contrato
        </Typography>
      </Grid>
      <ContractForm
        contract={contract}
        onSuccess={onSuccessContract}
        code={200}
        titleModal="Contrato editada com sucesso"
        actionModal={callback}
      />
    </>
  )
}

export const getServerSideProps = async (context: NextPageContext) => {
  const id = context.query.contractId as string
  try {
    const {
      data: { company, deliveryAddress, productDetails, revenue },
    } = await privateFetch(context).get(`/contracts/${id}`)

    return {
      props: {
        id,
        contract: {
          company,
          deliveryAddress,
          productDetails,
          revenue,
        } as Contract,
      },
    }
  } catch (err) {
    const error = err as any

    return {
      notFound: true,
    }
  }
}

export default EditContract
