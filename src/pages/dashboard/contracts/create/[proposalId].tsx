import { useDocumentDataOnce } from 'react-firebase-hooks/firestore'
import { firestore } from '@/lib/firebase'

import SpinnerPage from '@/components/SpinnerPage'
import CommercialAgentForm from '@/forms/CommercialAgentForm'
import ContractForm from '@/forms/ContractForm'
import {
  Grid,
  makeStyles,
  MenuItem,
  TextField,
  Typography,
} from '@material-ui/core'
import React, { ChangeEvent, useState, FC } from 'react'
import axios from 'axios'
import { useRouter } from 'next/router'
import Proposal from '@/types/Proposal'
import Contract from '@/types/Contract'
import CommercialAgent from '@/types/CommercialAgent'
import { NextPageContext } from 'next'
import defaultContract from '@/forms/defaultStates/contract'

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: theme.spacing(2),
  },
  selectField: {
    minWidth: '200px',
  },
}))

type Props = {
  id: string
}

const CreateContract: FC<Props> = ({ id }) => {
  const router = useRouter()
  const classes = useStyles()

  const [option, setOption] = useState<string>(
    (router.query.option as string) || 'client',
  )

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target
    setOption(value)
  }

  const onSuccessContract = (contract: Contract) =>
    axios.post('/api/contracts', contract)

  const onSuccessCommercialAgent = (commercialAgent: CommercialAgent) =>
    axios.post('/api/commercial-agents', commercialAgent)

  const callback = () => {
    router.push('/dashboard/contracts')
  }

  const query = firestore.collection('proposals').doc(id)

  const [proposal, loading] = useDocumentDataOnce<Proposal>(query)

  if (loading) {
    return <SpinnerPage />
  }
  if (!proposal) {
    router.push('/dashboard/proposals')
    return <SpinnerPage />
  }

  const contract = {
    ...defaultContract,
    ...proposal,
  } as Contract

  return (
    <>
      <Grid
        className={classes.header}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Novo contrato
        </Typography>
        <TextField
          className={classes.selectField}
          select
          label="Tipo de contratante"
          variant="outlined"
          size="small"
          value={option}
          onChange={handleChange}
          disabled
        >
          <MenuItem value="client">Cliente</MenuItem>
          <MenuItem value="agent">Agente Comercial</MenuItem>
        </TextField>
      </Grid>
      {option === 'client' && (
        <ContractForm
          contract={contract}
          onSuccess={onSuccessContract}
          code={201}
          titleModal="Contrato inserido com sucesso"
          actionModal={callback}
        />
      )}
      {option === 'agent' && (
        <CommercialAgentForm onSuccess={onSuccessCommercialAgent} />
      )}
    </>
  )
}

export const getServerSideProps = async (context: NextPageContext) => {
  const id = context.query.proposalId as string
  return { props: { id } }
}

export default CreateContract
