import ProposalForm from '@/forms/ProposalForm'
import { Grid, makeStyles, Typography } from '@material-ui/core'
import Proposal from '@/types/Proposal'
import axios from 'axios'
import { useRouter } from 'next/router'
import { NextPageContext } from 'next'
import React, { FC } from 'react'
import ProposalRequest from '@/types/ProposalRequest'
import { useDocumentDataOnce } from 'react-firebase-hooks/firestore'
import { firestore } from '@/lib/firebase'
import SpinnerPage from '@/components/SpinnerPage'
import defaultProposal from '@/forms/defaultStates/proposal'

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: theme.spacing(2),
  },
}))

type Props = {
  id: string
}

const CreateProposal: FC<Props> = ({ id }) => {
  const router = useRouter()
  const classes = useStyles()

  const query = firestore.collection('proposalRequests').doc(id)
  const [proposalRequest, loading] = useDocumentDataOnce<ProposalRequest>(query)

  if (loading) {
    return <SpinnerPage />
  }

  if (!proposalRequest) {
    router.push('/dashboard/proposals')
    return <SpinnerPage />
  }

  const proposal: Proposal = {
    ...defaultProposal,
    company: {
      ...defaultProposal.company,
      cnpj: proposalRequest.cnpj,
      legalName: proposalRequest.companyLegalName,
      mobile: proposalRequest.mobile,
      phone: proposalRequest.phone,
      email: proposalRequest.email,
      admin: {
        ...defaultProposal.company.admin,
        fullName: proposalRequest.contactFullName,
        companyArea: proposalRequest.companyArea,
        companyRole: proposalRequest.companyRole,
        mobile: proposalRequest.mobile,
        phone: proposalRequest.phone,
        email: proposalRequest.email,
      },
    },
    productDetails: {
      ...defaultProposal.productDetails,
      product: proposalRequest.product,
      campaign: {
        ...defaultProposal.productDetails.campaign,
        rechargeFrequency: proposalRequest.paymentFrequency,
        annualEstimatedAward: proposalRequest.totalIncome,

        administrationRate: '5.00',
      },
      cardQuantity: proposalRequest.commercialAgentsQuantity,
    },
  }

  const onSuccess = (proposal: Proposal) =>
    axios.post('/api/proposals', proposal)

  const callback = () => {
    router.push('/dashboard/proposals')
  }

  return (
    <>
      <Grid
        className={classes.header}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Nova oportunidade
        </Typography>
      </Grid>
      <ProposalForm
        proposal={proposal}
        onSuccess={onSuccess}
        code={201}
        titleModal="Proposta inserido com sucesso"
        actionModal={callback}
      />
    </>
  )
}

export const getServerSideProps = async (context: NextPageContext) => {
  const id = context.query.proposalRequestId as string

  return { props: { id } }
}

export default CreateProposal
