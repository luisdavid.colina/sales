import { FC } from 'react'

import { privateFetch } from '@/util/fetch'
import { NextPageContext } from 'next'

import ProposalForm from '@/forms/ProposalForm'
import { Grid, makeStyles, Typography } from '@material-ui/core'
import Proposal from '@/types/Proposal'
import axios from 'axios'
import { useRouter } from 'next/router'

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: theme.spacing(2),
  },
}))

type editProposalProps = {
  proposal: Proposal
  id: string
}

const EditProposal: FC<editProposalProps> = ({ proposal, id }) => {
  const router = useRouter()
  const classes = useStyles()

  const onSuccess = (proposal: Proposal) =>
    axios.put(`/api/proposals/${id}`, proposal)

  const callback = () => {
    router.push('/dashboard/proposals')
  }

  return (
    <>
      <Grid
        className={classes.header}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Editar oportunidade
        </Typography>
      </Grid>
      <ProposalForm
        proposal={proposal}
        onSuccess={onSuccess}
        code={200}
        titleModal="Proposta editada com sucesso"
        actionModal={callback}
      />
    </>
  )
}

export const getServerSideProps = async (context: NextPageContext) => {
  const id = context.query.proposalId as string
  try {
    const {
      data: { company, productDetails },
    } = await privateFetch(context).get(`/proposals/${id}`)

    return {
      props: {
        id,
        proposal: {
          company,
          productDetails,
        } as Proposal,
      },
    }
  } catch (err) {
      const error = err as any
       
    return {
      notFound: true,
    }
  }
}

export default EditProposal
