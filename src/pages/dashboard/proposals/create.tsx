import ProposalForm from '@/forms/ProposalForm'
import { Grid, makeStyles, Typography } from '@material-ui/core'
import Proposal from '@/types/Proposal'
import axios from 'axios'
import { useRouter } from 'next/router'

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: theme.spacing(2),
  },
}))

function CreateProposal() {
  const router = useRouter()
  const classes = useStyles()

  const onSuccess = (proposal: Proposal) =>
    axios.post('/api/proposals', proposal)

  const callback = () => {
    router.push('/dashboard/proposals')
  }

  return (
    <>
      <Grid
        className={classes.header}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Nova oportunidade
        </Typography>
      </Grid>
      <ProposalForm
        onSuccess={onSuccess}
        code={201}
        titleModal="Proposta inserido com sucesso"
        actionModal={callback}
      />
    </>
  )
}

export default CreateProposal
