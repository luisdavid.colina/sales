import { useState, MouseEvent } from 'react'
import Box from '@material-ui/core/Box'
import {
  createStyles,
  Button,
  Typography,
  Grid,
  makeStyles,
  Menu,
  Modal,
  MenuItem,
  ListItemIcon,
  Badge,
} from '@material-ui/core'

import SelectCommercialAgent from '@/components/SelectCommercialAgent/SelectCommercialAgent'
import { FaSignature, FaEdit, FaDownload, FaLink } from 'react-icons/fa'
import SpinnerPage from '@/components/SpinnerPage'
import ProposalsTable from '@/components/tables/ProposalsTable'

import ProposalModal from '@/components/modals/ProposalModal'
import { firestore } from '@/lib/firebase'
import Proposal from '@/types/Proposal'
import ProposalRequest from '@/types/ProposalRequest'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import Link from 'next/link'
import { useCollectionData } from 'react-firebase-hooks/firestore'

import useContextMenu from '@/hooks/useContextMenu'
import useClickPreventionOnDoubleClick from '@/hooks/useClickPreventionOnDoubleClick'
import ProposalRequestModal from '@/components/modals/ProposalRequestModal'

export const useStyles = makeStyles((theme) =>
  createStyles({
    button: {
      marginLeft: theme.spacing(3),
    },
  }),
)
const Proposals = () => {
  const classes = useStyles()
  const query = firestore
    .collection('proposals')
    .orderBy('proposalNumber', 'desc')
  const queryProposalRequest = firestore.collection('proposalRequests')
  const [
    proposalRequests,
    loadingProposalRequests,
  ] = useCollectionData<ProposalRequest>(queryProposalRequest)

  const count: number = Object.keys(proposalRequests || []).length

  const [proposals, loading] = useCollectionData<Proposal>(query)
  const { state, onOpen, onClose } = useContextMenu()
  const [selected, setSelected] = useState<Proposal | null>(null)
  const [modalOpened, setModalOpened] = useState<string | null>(null)

  const onClick = (params: any) => {
    const proposal = proposals?.find(
      (proposal) => proposal.id === params.row.id,
    )

    if (proposal) {
      setModalOpened('review-proposal')
      setSelected(proposal)
    }
  }

  const onDoubleClick = (params: any, e: MouseEvent<any>) => {
    const proposal = proposals?.find(
      (proposal) => proposal.id === params.row.id,
    )
    if (proposal) {
      setSelected(proposal)
      onOpen(e)
    }
  }

  const [click, doubleClick] = useClickPreventionOnDoubleClick(
    onClick,
    onDoubleClick,
  )

  function closeModal() {
    setSelected(null)
    setModalOpened(null)
  }

  function openAgentModal() {
    setModalOpened('change-agent')
  }

  function openProposalRequestModal() {
    setModalOpened('proposal-request')
  }

  if (loading || loadingProposalRequests) {
    return <SpinnerPage />
  }
  const isProposalSelected = !!selected
  const showProposalModal =
    modalOpened === 'review-proposal' && isProposalSelected
  const showAgentModal = modalOpened === 'change-agent' && isProposalSelected
  const showProposalRequestModal = modalOpened === 'proposal-request'

  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Oportunidade
        </Typography>

        <Box display="flex" justifyContent="space-between">
          <Button
            variant="text"
            color="primary"
            onClick={openProposalRequestModal}
          >
            <Badge badgeContent={count} color="primary">
              Lead
            </Badge>
          </Button>
          <Link href="/dashboard/proposals/create">
            <Button
              variant="text"
              color="primary"
              endIcon={<AddCircleIcon />}
              className={classes.button}
            >
              Nova oportunidade
            </Button>
          </Link>
        </Box>
      </Grid>
      <ProposalsTable
        onRowClick={click}
        onRowDoubleClick={doubleClick}
        proposals={proposals || []}
      />

      {selected && (
        <Menu
          keepMounted
          open={state.mouseY !== null}
          onClose={onClose}
          anchorReference="anchorPosition"
          anchorPosition={
            state.mouseY !== null && state.mouseX !== null
              ? { top: state.mouseY, left: state.mouseX }
              : undefined
          }
        >
          <Link href={`/dashboard/proposals/${selected?.id}/edit`}>
            <MenuItem onClick={onClose}>
              <ListItemIcon>
                <FaEdit />
              </ListItemIcon>
              <Typography>Editar</Typography>
            </MenuItem>
          </Link>

          <Link href={`/dashboard/contracts/create/${selected?.id}`}>
            <MenuItem onClick={onClose}>
              <ListItemIcon>
                <FaSignature />
              </ListItemIcon>
              <Typography>Contratar</Typography>
            </MenuItem>
          </Link>
          <MenuItem
            onClick={onClose}
            component="a"
            download
            href={`/api/proposals/${selected.id}/download`}
          >
            <ListItemIcon>
              <FaDownload />
            </ListItemIcon>
            <Typography>Download</Typography>
          </MenuItem>

          <MenuItem onClick={openAgentModal}>
            <ListItemIcon>
              <FaLink />
            </ListItemIcon>
            <Typography>Redirecionar</Typography>
          </MenuItem>
        </Menu>
      )}
      {selected && (
        <ProposalModal
          open={showProposalModal}
          onClose={closeModal}
          proposal={selected}
        />
      )}
      {selected && (
        <Modal open={showAgentModal} onClose={closeModal}>
          <SelectCommercialAgent proposal={selected} />
        </Modal>
      )}
      <ProposalRequestModal
        open={showProposalRequestModal}
        close={closeModal}
      />
    </>
  )
}

export default Proposals
