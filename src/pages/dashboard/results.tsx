import { useState, useCallback, useEffect } from 'react'
import Link from 'next/link'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import { GridFilterModelParams } from '@material-ui/data-grid'
import { CommercialAgents, PaymentReview } from '@/sections/results'
import TotalPayment, {
  totalPaymentRow,
} from '@/sections/results/TotalPayment/TotalPayment'

type commercialAgentType = {
  id: string
  agent_name: string
  cpf_cnpj: string
  value: string
  payment_method: string
  payment_method_id: string
}

type paymentType = {
  id: string
  payment_method: string
  total_volume_money: string
  available_money: string
  numbers_of_agents: number
}

type filterType = {
  columnField: string
  operatorValue: string
  value: string
}

type paymentsType = paymentType[]

const defaultCommercialAgents: commercialAgentType[] = [
  {
    id: '1',
    agent_name: 'AURORA E FÁTIMA ESPORTES LTDA',
    cpf_cnpj: '98765432123',
    value: 'R$ 3.200,00',
    payment_method: 'Adoro - Carteira Digital',
    payment_method_id: '1',
  },
  {
    id: '2',
    agent_name: 'KAMILLY E SARA FILMAGENS LTDA',
    cpf_cnpj: '23497135315',
    value: 'R$ 4.200,00',
    payment_method: 'Adoro - Carteira Digital',
    payment_method_id: '1',
  },
  {
    id: '3',
    agent_name: 'ANDERSON E THEO DOCES & SALGADOS ME',
    cpf_cnpj: '92645901842',
    value: 'R$ 1.500,00',
    payment_method: 'JoyCard - Cartão Pré-Pago',
    payment_method_id: '3',
  },
  {
    id: '4',
    agent_name: 'MARTIN E ALEXANDRE COMERCIO DE BEBIDAS" "ME',
    cpf_cnpj: '56770348579',
    value: 'R$ 800,00',
    payment_method: 'Adoro - Carteira Digital',
    payment_method_id: '1',
  },
  {
    id: '5',
    agent_name: 'CAUÃ E RAFAEL CONSULTORIA FINANCEIRA ME',
    cpf_cnpj: '28406345493',
    value: 'R$ 3.400,00',
    payment_method: 'JoyCard - Cartão Pré-Pago',
    payment_method_id: '3',
  },
  {
    id: '6',
    agent_name: 'CAUÃ E RAFAEL CONSULTORIA FINANCEIRA ME',
    cpf_cnpj: '10396384569',
    value: 'R$ 3.400,00',
    payment_method: 'JoyCard - Cartão Pré-Pago',
    payment_method_id: '3',
  },
  {
    id: '7',
    agent_name: 'CAUÃ E RAFAEL CONSULTORIA FINANCEIRA ME',
    cpf_cnpj: '37458950284',
    value: 'R$ 3.400,00',
    payment_method: 'JoyCard - Cartão Pré-Pago',
    payment_method_id: '3',
  },
  {
    id: '8',
    agent_name: 'CAUÃ E RAFAEL CONSULTORIA FINANCEIRA ME',
    cpf_cnpj: '20057394221',
    value: 'R$ 1.000,00',
    payment_method: 'Sem Produto',
    payment_method_id: '4',
  },
]

const defaultPayments: paymentsType = [
  {
    id: '1',
    payment_method: 'Adoro - Carteira Digital',
    total_volume_money: 'R$ 3.700,00',
    available_money: 'R$ 3.200,00',
    numbers_of_agents: 0,
  },
  {
    id: '2',
    payment_method: 'Transferências',
    total_volume_money: 'R$ 2.900,00',
    available_money: 'R$ 3.200,00',
    numbers_of_agents: 0,
  },
  {
    id: '3',
    payment_method: 'JoyCard - Cartão Pré-Pago',
    total_volume_money: 'R$ 200.000,00',
    available_money: 'R$ 3.200,00',
    numbers_of_agents: 0,
  },
  {
    id: '4',
    payment_method: 'Sem Produto',
    total_volume_money: 'R$ 200.000,00',
    available_money: 'R$ 3.200,00',
    numbers_of_agents: 0,
  },
]

const defaultTotalPayment: totalPaymentRow[] = [
  {
    id: '1',
    name: 'Comissão',
    value: 'R$ 350.000,00',
  },
  {
    id: '2',
    name: 'Premiaçôes',
    value: 'R$ 150.000,00',
  },
  {
    id: '3',
    name: 'Antecipação',
    value: 'R$ 200.000,00',
  },
  {
    id: '4',
    name: 'Formação',
    value: 'R$ 170.000,00',
  },
]

const PaymentConfirmation = () => {
  const [payment, setPayment] = useState<paymentsType>([...defaultPayments])
  const [commercialAgent, setCommercialAgent] = useState<commercialAgentType[]>(
    [...defaultCommercialAgents],
  )
  const [filter, setFilter] = useState<filterType[]>([])

  const updatePayment = (agents: commercialAgentType[] = commercialAgent) => {
    const paymentCopy: paymentsType = defaultPayments.map((payment) => ({
      ...payment,
      available_money: 'R$ 0,00',
    }))
    agents.forEach((agent) => {
      const paymentMethod = paymentCopy.find(
        (payment) => payment.payment_method === agent.payment_method,
      )
      if (paymentMethod) {
        console.log({paymentMethod})
        const agentValue = Number(
          String(agent.value).replace('R$ ', '').replace(',', '.'),
        )
        const availableMoney = Number(
          String(paymentMethod.available_money)
            .replace('R$ ', '')
            .replace(',', '.'),
        )
        const totalValue = agentValue + availableMoney
        paymentMethod.available_money = `R$ ${totalValue}`
        paymentMethod.numbers_of_agents += 1
      }
    })
    setPayment(paymentCopy)
  }

  const handleFilterCommercialAgents = useCallback(
    ({ filterModel: { items } }: GridFilterModelParams) => {
      const newFilters: filterType[] = []
      let isChanched = false
      filter.forEach((f) => {
        const itemFilter = items.find(
          (i) =>
            i.columnField === f.columnField &&
            i.operatorValue === f.operatorValue &&
            i.value === f.value,
        )
        if (itemFilter) {
          newFilters.push({ ...f })
        } else {
          isChanched = true
        }
      })
      if (isChanched) {
        setFilter(newFilters)
      }
    },
    [filter],
  )

  const filterPerMethod = useCallback(
    (value: string) => (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      e.stopPropagation()
      setFilter([
        {
          columnField: 'payment_method',
          operatorValue: 'equals',
          value,
        },
      ])
    },
    [],
  )

  const handleRowPayment = useCallback(
    (agentId: string) => (
      e: React.ChangeEvent<{
        name?: string | undefined
        value: unknown
      }>,
    ) => {
      e.stopPropagation()
      const { value: paymentId } = e.target
      setCommercialAgent((currentAgents) => {
        const agentsCopy = currentAgents.map((agent) => ({ ...agent }))
        const agent = agentsCopy.find((agent) => agent.id === agentId)
        if (agent) {
          const payment = defaultPayments.find(
            (payment) => payment.id === paymentId,
          )
          if (payment) {
            agent.payment_method_id = payment.id
            agent.payment_method = payment.payment_method
          }
        }
        return agentsCopy
      })
    },
    [],
  )

  useEffect(() => {
    updatePayment()
  }, [commercialAgent])

  return (
    <>
      <Grid container justify="space-between" spacing={3}>
        <Grid item lg={6}>
          <Typography variant="h4" component="h1" gutterBottom>
            Consolidação de Pagamentos
          </Typography>
          <Typography variant="h5" component="h2" gutterBottom>
            Agentes Comerciais
          </Typography>
        </Grid>
        <Grid item lg={3}>
          <Grid container justify="flex-end">
            <TotalPayment rows={defaultTotalPayment} />
            <Grid item xs={5}>
              <FormControlLabel
                control={<Checkbox color="primary" />}
                label={
                  <Typography variant="button" color="primary">
                    Aprovar
                  </Typography>
                }
                labelPlacement="start"
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <PaymentReview rows={payment} action={filterPerMethod} />
        </Grid>
        <Grid item xs={12}>
          <CommercialAgents
            rows={commercialAgent}
            action={handleRowPayment}
            payments={defaultPayments}
            filterModel={{ items: filter }}
            onFilterModelChange={handleFilterCommercialAgents}
          />
        </Grid>
      </Grid>
      <Grid container justify="flex-end" spacing={3}>
        <Grid item>
          <Link href="/dashboard/results" passHref>
            <Button variant="contained" color="primary" component="a" disabled>
              Cancelar
            </Button>
          </Link>
        </Grid>
        <Grid item>
          <Button variant="contained" color="primary">
            Enviar Remessa
          </Button>
        </Grid>
      </Grid>
    </>
  )
}

export default PaymentConfirmation
