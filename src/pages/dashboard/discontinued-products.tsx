import { useState, MouseEvent } from 'react'

import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import { Box, Button, Grid, Typography } from '@material-ui/core'
import Link from 'next/link'

import { FaRecycle } from 'react-icons/fa'

import { GridRowParams } from '@material-ui/data-grid'

import useContextMenu from '@/hooks/useContextMenu'
import DiscontinuedTable from '@/components/tables/DiscontinuedTable'

import { useStyles } from 'styles/Products.styles'

const rows = [
  {
    id: '1',
    code: '0001',
    product: 'JoyCard Incentive',
    family: 'Cartões Pré Pagos',
    category: 'Incentivo',
    channel: 'Multiplicador',
    validity: '10/01/2021',
  },
  {
    id: '2',
    code: '0002',
    product: 'Conta Digital Adoro',
    family: 'Carteira Digital',
    category: 'Banco',
    channel: 'Element',
    validity: '15/01/2021',
  },
]

const DiscontinuedProducts = () => {
  const classes = useStyles()

  const { state, onOpen, onClose } = useContextMenu()
  const [selected, setSelected] = useState<any>()

  const onClick = (params: GridRowParams, e: MouseEvent<any>) => {
    setSelected(params.row)
    onOpen(e)
  }

  const editContract = () => {
    onClose()
    if (selected) {
      console.log(selected)
    }
  }

  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Produtos Descontinuados
        </Typography>

        <Box display="flex">
          <Link href="/dashboard/rules">
            <Button variant="text" color="primary" className={classes.button}>
              Produtos Ativos
            </Button>
          </Link>
          <Button
            variant="text"
            color="primary"
            endIcon={<AddCircleIcon />}
            href="/dashboard/products/create"
          >
            Novo Produto
          </Button>
        </Box>
      </Grid>
      <DiscontinuedTable onRowClick={onClick} products={rows} />
      {selected && (
        <Menu
          keepMounted
          open={state.mouseY !== null}
          onClose={onClose}
          anchorReference="anchorPosition"
          anchorPosition={
            state.mouseY !== null && state.mouseX !== null
              ? { top: state.mouseY, left: state.mouseX }
              : undefined
          }
        >
          <MenuItem onClick={editContract}>
            <ListItemIcon>
              <FaRecycle />
            </ListItemIcon>
            <Typography>Ativar produto</Typography>
          </MenuItem>
        </Menu>
      )}
    </>
  )
}

export default DiscontinuedProducts
