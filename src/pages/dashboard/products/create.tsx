import ProductForm from '@/forms/ProductForm'
import { Grid, makeStyles, Typography } from '@material-ui/core'
import axios from 'axios'
import { useRouter } from 'next/router'

const useStyles = makeStyles((theme) => ({
  header: {
    marginBottom: theme.spacing(2),
  },
}))

const CreateProduct = () => {
  const router = useRouter()
  const classes = useStyles()

  const onSuccess = (contract: any) => axios.post('/api/product', contract)

  const callback = () => {
    router.push('/dashboard/rules')
  }

  return (
    <>
      <Grid
        className={classes.header}
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Novo Comissionamento
        </Typography>
      </Grid>
      <ProductForm
        onSuccess={onSuccess}
        code={201}
        titleModal="Produto inserido com sucesso"
        actionModal={callback}
      />
    </>
  )
}

export default CreateProduct
