import Link from 'next/link'
import { Typography, Button, Box, Grid } from '@material-ui/core'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import TutorialType from '@/types/Tutorial'
import Video from '@/components/Video'
import PictureAsPdf from '@material-ui/icons/PictureAsPdf'
import AttachFile from '@material-ui/icons/AttachFile'

interface TutorialModalProps {
  tutorial: TutorialType
}

const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      width: '90%',
      marginTop: theme.spacing(5),
      backgroundColor: '#FFF',
      margin: 'auto',
      boxShadow: theme.shadows[5],
      padding: '15px',
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
    },
  }),
)

export const TutorialModal = ({ tutorial }: TutorialModalProps) => {
  const classes = useStyles()
  return (
    <div>
      <Box display="flex" className={classes.container} flexDirection="column">
        <Grid container>
          <Grid item xs={12}>
            <Typography variant="h6" component="h6">
              {tutorial?.section}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography
              variant="h5"
              component="h5"
              style={{ color: '#2898FF' }}
            >
              {tutorial?.title}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <div style={{ minHeight: '300px', margin: '10px 0' }}>
              <Video url={tutorial?.media} />
            </div>
          </Grid>
          <Grid item xs={6} style={{ padding: '15px' }}>
            <Grid container direction="column" spacing={2}>
              <Grid item>
                <Typography variant="h6" component="h6">
                  Anexos:
                </Typography>
              </Grid>
              <Grid item>
                {tutorial.attachments !== undefined &&
                  tutorial?.attachments.map((attachment) => (
                    <div style={{ display: 'flex' }}>
                      <span
                        style={{ display: 'inline-flex', marginRight: '8px' }}
                      >
                        {attachment.includes('pdf') ? (
                          <PictureAsPdf style={{ color: '#2898FF' }} />
                        ) : (
                          <AttachFile style={{ color: '#2898FF' }} />
                        )}
                      </span>
                      <span>
                        <Link href={attachment}>
                          <a style={{ color: '#717171' }}>
                            {attachment.replaceAll(
                              'https://element-sales-ecosystem-attachments.s3.amazonaws.com/',
                              '',
                            )}
                          </a>
                        </Link>
                      </span>
                      <br />
                    </div>
                  ))}
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="body1">{tutorial?.subject}</Typography>
          </Grid>
        </Grid>
      </Box>
    </div>
  )
}

export default TutorialModal
