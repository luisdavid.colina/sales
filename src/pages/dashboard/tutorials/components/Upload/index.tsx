import React, { FC, useState, ChangeEvent } from 'react'

// Importing MaterialUI components
import Typography from '@material-ui/core/Typography'
import {
  Box,
  Button,
  TextField,
  Grid,
  InputBaseComponentProps,
  MenuItem,
  CircularProgress,
  FormControl,
  InputLabel,
  Select,
  Checkbox,
  ListItemText,
  OutlinedInput,
} from '@material-ui/core'
import Autocomplete, {
  createFilterOptions,
} from '@material-ui/lab/Autocomplete'
import { yupResolver } from '@hookform/resolvers/yup'
import { useForm, Controller } from 'react-hook-form'
import axios from 'axios'
import { useRouter } from 'next/router'
import { firestore } from '@/lib/firebase'
import { useCollectionDataOnce } from 'react-firebase-hooks/firestore'
import TutorialCategory from '@/types/TutorialCategory'
import { uploadAttachments } from '@/util/attachments'
import useBoolean from '@/hooks/useBoolean'
import TutorialSchema from '@/schemas/TutorialSchema'
import TutorialCategoryAutocomplete from '@/components/autocompletes/TutorialCategoryAutocomplete'
import Tutorial from '@/types/Tutorial'
import SuccessModal from '@/components/SuccessModal'
import Communication from '@/types/Communication'
import Video from '@/components/Video'
import { useStyles } from './styles'

const tutorialDefault: Tutorial = {
  title: '',
  section: '',
  subject: '',
  data: '',
  media: undefined,
  status: '',
}

interface UploadProps {
  onClose: (event: any) => void
  selected?: Tutorial
  close: any
}

const names = ['ADM', 'Desenvolvedor', 'Explorador', 'Usuario']

export const Upload: FC<UploadProps> = ({ onClose, selected, close }) => {
  const classes = useStyles()
  const [files, setFiles] = useState<any[]>()
  const [error, setError] = useState<string>('')
  const [loading, setLoading] = useState(false)
  const edit = selected !== undefined
  const [tutorial, setTutorial] = useState<Tutorial>(
    selected || tutorialDefault,
  )
  const query2 = firestore.collection('tutorialCategories')
  const [
    tutorialCategories,
    loading2,
  ] = useCollectionDataOnce<TutorialCategory>(query2)
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: tutorialDefault,
    resolver: yupResolver(TutorialSchema),
  })

  const {
    value: success,
    toTrue: isSuccess,
    toFalse: isNotSuccess,
  } = useBoolean({
    initialValue: false,
  })
  const router = useRouter()
  const onClosed = () => {
    isNotSuccess()
    router.reload()
    close()
  }
  const submit = async (data: Tutorial) => {
    setLoading(true)
    let response
    try {
      if (files !== undefined) {
        const fileNames: string[] = files?.map(
          (file) =>
            `https://element-sales-ecosystem-attachments.s3.amazonaws.com/${file.name.replaceAll(
              ' ',
              '-',
            )}`,
        )
        response = await axios.post('/api/tutorials', {
          ...data,
          attachments: tutorial.attachments
            ? tutorial.attachments.concat([...fileNames])
            : fileNames,
        })
        if (response.status === 201) {
          files.forEach(async (file) => {
            await uploadAttachments(file, file.name.replaceAll(' ', '-'))
          })
        }
      } else {
        response = await axios.post('/api/tutorials', data)
      }
      if (response.status === 201) isSuccess()
    } catch (err) {
      const error = err as any

      // eslint-disable-next-line no-console
      console.error(error)
    }
    setLoading(false)
  }

  const uploadFile = (event: any) => {
    const newFiles: File[] = Array.from(
      event.target.files || event.dataTransfer.files,
    )
    setFiles(newFiles)
  }

  const [personName, setPersonName] = useState([])

  const handleChange = (event: any) => {
    const {
      target: { value },
    } = event
    setPersonName(
      // On autofill we get a stringified value.
      typeof value === 'string' ? value.split(',') : value,
    )
  }

  return (
    <Box className={classes.container}>
      <form onSubmit={handleSubmit(submit)}>
        <Typography
          className={classes.subtitle}
          variant="h5"
          color="primary"
          paragraph
        >
          Novo Conteúdo
        </Typography>

        <Grid container spacing={2}>
          <Grid item xs={6}>
            <TextField
              fullWidth
              label="Título"
              variant="outlined"
              size="small"
              className={classes.input}
              autoFocus
              inputProps={{
                ...register('title'),
              }}
              error={!!errors.title}
              helperText={errors.title?.message}
              value={tutorial.title}
              onChange={(e: ChangeEvent<HTMLInputElement>) => {
                setTutorial({
                  ...tutorial,
                  title: e.target.value,
                })
              }}
            />
          </Grid>
          <Grid item xs={6} style={{ pointerEvents: edit ? 'none' : 'auto' }}>
            <Controller
              control={control}
              name="section"
              render={(props) => (
                <TutorialCategoryAutocomplete
                  value={props.field.value}
                  onChange={props.field.onChange}
                  error={!!errors.section}
                  helperText={errors.section?.message}
                />
              )}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              fullWidth
              id="standard-multiline-static"
              multiline
              rows={3}
              label="Descrição"
              variant="outlined"
              size="small"
              className={classes.input}
              inputProps={{
                ...register('subject'),
              }}
              value={tutorial.subject}
              onChange={(e: ChangeEvent<HTMLInputElement>) => {
                e.preventDefault()
                setTutorial({
                  ...tutorial,
                  subject: e.target.value,
                })
              }}
              error={!!errors.subject}
              helperText={errors.subject?.message}
              style={{ marginBottom: '10.5px' }}
            />
            <TextField
              fullWidth
              label="Mídia"
              variant="outlined"
              size="small"
              className={classes.input}
              inputProps={{
                ...register('media'),
              }}
              value={tutorial.media}
              onChange={(e: ChangeEvent<HTMLInputElement>) => {
                e.preventDefault()
                setTutorial({
                  ...tutorial,
                  media: e.target.value,
                })
              }}
              placeholder="URL do vídeo"
              error={!!errors.media}
              helperText={errors.media?.message}
              style={{ marginBottom: '10.5px' }}
            />
            <FormControl fullWidth>
              <InputLabel id="checkbox-label">Permitir</InputLabel>
              <Select
                fullWidth
                labelId="checkbox-label"
                id="multiple-checkbox"
                multiple
                variant="outlined"
                renderValue={(selected: any) => selected.join(', ')}
                value={personName}
                onChange={handleChange}
              >
                {names.map((name: string) => {
                  const nameNever = name as never
                  return (
                    <MenuItem key={name} value={name}>
                      <Checkbox checked={personName.indexOf(nameNever) > -1} />
                      <ListItemText primary={name} />
                    </MenuItem>
                  )
                })}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={6}>
            <div
              style={{
                width: 350,
                paddingTop: '10px',
                margin: 'auto',
                height: '100%',
              }}
            >
              <Video url={tutorial.media} />
            </div>
          </Grid>
          {!edit && (
            <>
              <Grid item xs={4}>
                <Button
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.button}
                  component="label"
                  htmlFor="upload-file"
                >
                  Upload de arquivo
                </Button>
              </Grid>
              <Grid item xs={6} style={{ display: 'none' }}>
                <input
                  onChange={uploadFile}
                  accept=".pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx,.txt,.rtf,.odt,.ods,.odp,.odg,.odf,.odb,.odm,.csv,.png,.jpg,.jpeg,.gif"
                  id="upload-file"
                  type="file"
                  multiple
                />
              </Grid>
              <Grid item xs={8} style={{ alignSelf: 'center' }}>
                <Typography variant="caption" color="inherit">
                  {files
                    ? files.map((f: File) => f.name).join(', ')
                    : error || 'Nenhum arquivo selecionado'}
                </Typography>
              </Grid>
            </>
          )}
          <div className={classes.footer}>
            <Button type="button" variant="contained" onClick={onClose}>
              Voltar
            </Button>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              onClick={() => submit(tutorial)}
              disabled={files === undefined && !edit}
            >
              {loading && <CircularProgress size={14} color="secondary" />}
              {!loading && 'Salvar'}
            </Button>
          </div>
        </Grid>
      </form>
      <SuccessModal
        title="Conteúdo carregado com sucesso"
        open={success}
        onClose={onClosed}
      />
    </Box>
  )
}

export default Upload
