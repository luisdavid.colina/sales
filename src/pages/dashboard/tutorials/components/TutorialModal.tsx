import { useState, MouseEvent } from 'react'
import axios from 'axios'
import { useRouter } from 'next/router'
import { firestore } from '@/lib/firebase'
import { useCollectionDataOnce } from 'react-firebase-hooks/firestore'
import Button from '@material-ui/core/Button'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import {
  GridRowParams,
  GridSelectionModelChangeParams,
  GridRowId,
  GridToolbarContainer,
  GridColumnsToolbarButton,
  GridFilterToolbarButton,
  GridValueFormatterParams,
} from '@material-ui/data-grid'
import MenuItem from '@material-ui/core/MenuItem'
import Tooltip from '@material-ui/core/Tooltip'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import Menu from '@material-ui/core/Menu'
import Modal from '@material-ui/core/Modal'
import { FaEdit, FaStopCircle } from 'react-icons/fa'
import useContextMenu from '@/hooks/useContextMenu'
import useClickPreventionOnDoubleClick from '@/hooks/useClickPreventionOnDoubleClick'
import Table from '@/components/Table'
import Tutorial from '@/types/Tutorial'
import CustomToolbar from './CustomToolbar'
import useStyles from './TutorialModal.styles'
import Upload from './Upload'

const columns = [
  {
    field: 'title',
    headerName: 'Título',
    flex: 1,
  },
  {
    field: 'section',
    headerName: 'Categoria',
    flex: 1,
  },
  {
    field: 'subject',
    headerName: 'Assunto',
    flex: 1,
  },
  {
    field: 'createdAt',
    headerName: 'Data',
    valueFormatter: (params: GridValueFormatterParams) => {
      const value = params.value as string
      return value
    },
    flex: 1,
  },
  {
    field: 'media',
    headerName: 'Mídia',
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
  },
]

const rows = [
  {
    id: '1',
    title: 'Título',
    section: 'Seção',
    subject: 'Assunto',
    media: 'Mídia',
    status: 'Status',
    createdAt: 'Data',
  },
]

export const TutorialModal = () => {
  const classes = useStyles()
  const [selected, setSelected] = useState<Tutorial>()
  const router = useRouter()
  const [open, setOpen] = useState(false)
  const [selectedFixed, setSelectedFixed] = useState<GridRowId[]>([])

  const { state, onOpen, onClose } = useContextMenu()
  const query = firestore.collection('tutorials')
  const [tutorial, loading] = useCollectionDataOnce<Tutorial>(query)

  const handleOpen = () => {
    setSelected(undefined)
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const onClick = (params: GridRowParams, e: MouseEvent<any>) => {
    setSelected(params.row as Tutorial)
    onOpen(e)
  }

  const onDoubleClick = (params: GridRowParams, e: MouseEvent<any>) => {
    setSelected(params.row as Tutorial)
    onOpen(e)
  }

  const [click, doubleClick] = useClickPreventionOnDoubleClick(
    onClick,
    onDoubleClick,
  )
  const selectFixed = async (param: GridSelectionModelChangeParams) => {
    setSelectedFixed(param.selectionModel)
    //
  }
  const deleteTutorial = async () => {
    if (selected)
      await firestore.collection('tutorials').doc(selected.id).delete()
    router.reload()
  }

  const editTutorial = () => {
    handleClose()
    setOpen(true)
  }

  const bulkEdit = async (status: string) => {
    const response = await axios.post('/api/tutorials/bulk', {
      tutorials: selectedFixed.map((row: GridRowId) => String(row)),
      status,
    })
    if (response.status === 201) router.reload()
  }
  const MyCustomToolbar = () => (
    <CustomToolbar selectedFixed={selectedFixed} onClickEdit={bulkEdit} />
  )

  return (
    <div>
      <Box display="flex" className={classes.container} flexDirection="column">
        <div style={{ display: 'Flex', justifyContent: 'space-between' }}>
          <Typography
            className={classes.subtitle}
            variant="h5"
            color="primary"
            paragraph
          >
            Tutoriales
          </Typography>
          <Button
            variant="text"
            color="primary"
            endIcon={<AddCircleIcon />}
            onClick={handleOpen}
          >
            Novo Conteúdo
          </Button>
        </div>
        <Table
          onRowClick={click}
          onRowDoubleClick={doubleClick}
          columns={columns}
          rows={tutorial || rows}
          loading={loading}
          onSelectionModelChange={selectFixed}
          selectionModel={selectedFixed}
          components={{
            Toolbar: MyCustomToolbar,
          }}
          checkboxSelection
          disableSelectionOnClick
          pageSize={6}
          rowsPerPageOptions={[6]}
          sortModel={[{ field: 'createdAt', sort: 'desc' }]}
        />
      </Box>
      {selected && (
        <Menu
          keepMounted
          open={state.mouseY !== null}
          onClose={onClose}
          anchorReference="anchorPosition"
          anchorPosition={
            state.mouseY !== null && state.mouseX !== null
              ? { top: state.mouseY, left: state.mouseX }
              : undefined
          }
        >
          <MenuItem onClick={editTutorial}>
            <ListItemIcon>
              <FaEdit />
            </ListItemIcon>
            <Typography>Edição</Typography>
          </MenuItem>

          <MenuItem onClick={deleteTutorial} component="a" download>
            <ListItemIcon>
              <FaStopCircle />
            </ListItemIcon>
            <Typography>Exclusão</Typography>
          </MenuItem>
        </Menu>
      )}
      <Modal open={open} onClose={handleClose}>
        <Box
          display="flex"
          className={classes.container}
          flexDirection="column"
          style={{ width: '80%' }}
        >
          <Upload onClose={handleClose} selected={selected} close={() => 1} />
        </Box>
      </Modal>
    </div>
  )
}

export default TutorialModal
