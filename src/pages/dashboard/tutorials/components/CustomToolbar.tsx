import { useState, forwardRef, MouseEvent } from 'react'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import Tooltip from '@material-ui/core/Tooltip'
import Button from '@material-ui/core/Button'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import Typography from '@material-ui/core/Typography'
import { FaEdit, FaDownload } from 'react-icons/fa'

import {
  GridRowParams,
  GridSelectionModelChangeParams,
  GridRowId,
  GridToolbarContainer,
  GridColumnsToolbarButton,
  GridFilterToolbarButton,
  GridValueFormatterParams,
} from '@material-ui/data-grid'
import useContextMenu from '@/hooks/useContextMenu'

function CustomToolbar({
  selectedFixed,
  onClickEdit,
}: {
  selectedFixed: GridRowId[]
  onClickEdit: any
}) {
  const [selected, setSelected] = useState(false)
  const { state, onOpen, onClose } = useContextMenu()
  const onClick = (e: MouseEvent<any>) => {
    onOpen(e)
    setSelected(!selected)
  }
  return (
    <>
      <GridToolbarContainer>
        <GridColumnsToolbarButton />
        <GridFilterToolbarButton />
        {selectedFixed.length > 0 && (
          <Tooltip title="Editar" style={{ marginLeft: 'auto' }}>
            <Button
              variant="text"
              color="primary"
              startIcon={<FaEdit />}
              onClick={onClick}
            >
              Status
            </Button>
          </Tooltip>
        )}
      </GridToolbarContainer>
      {selected && (
        <Menu
          keepMounted
          open={state.mouseY !== null}
          onClose={onClose}
          anchorReference="anchorPosition"
          anchorPosition={
            state.mouseY !== null && state.mouseX !== null
              ? { top: state.mouseY, left: state.mouseX }
              : undefined
          }
        >
          <MenuItem onClick={() => onClickEdit('Programado')}>
            <Typography>Programado</Typography>
          </MenuItem>

          <MenuItem onClick={() => onClickEdit('Publicado')}>
            <Typography>Publicado</Typography>
          </MenuItem>
          <MenuItem onClick={() => onClickEdit('Cancelado')}>
            <Typography>Cancelado</Typography>
          </MenuItem>
        </Menu>
      )}
    </>
  )
}

export default CustomToolbar
