import { useState } from 'react'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import InputLabel from '@material-ui/core/InputLabel'
import Modal from '@material-ui/core/Modal'
import FormControl from '@material-ui/core/FormControl'
import { firestore } from '@/lib/firebase'
import { useCollectionDataOnce } from 'react-firebase-hooks/firestore'
import Tutorial from '@/types/Tutorial'
import TutorialCategory from '@/types/TutorialCategory'
import TutorialGrid from './components/TutorialGrid'
import TutorialModal from './components/TutorialModal'
import { AnalyticsAndOperator } from '@aws-sdk/client-s3'

const Tutorials = () => {
  const [open, setOpen] = useState(false)
  const [categoryFilter, setCategoryFilter] = useState<string>('')
  const query = firestore.collection('tutorials')
  const [tutorial, loading] = useCollectionDataOnce<Tutorial>(query)

  const query2 = firestore.collection('tutorialCategories')
  const [
    tutorialCategories,
    loading2,
  ] = useCollectionDataOnce<TutorialCategory>(query2)

  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  const selectCategory = (e: any) => {
    setCategoryFilter(e.target.value)
  }

  return (
    <>
      <Typography
        variant="h4"
        component="h2"
        gutterBottom
        style={{ display: 'inline-flex' }}
      >
        Tutorial
      </Typography>
      <Button
        variant="text"
        color="primary"
        endIcon={<AddCircleIcon />}
        onClick={handleOpen}
        style={{ float: 'right' }}
      >
        Carregar Tutorial
      </Button>
      {!loading2 && tutorialCategories !== undefined && (
        <div>
          <FormControl>
            <InputLabel id="categories-label">Categorias</InputLabel>

            <Select
              id="categories"
              label="Categorias"
              labelId="categories-label"
              style={{ width: '200px' }}
              value={categoryFilter}
              onChange={selectCategory}
            >
              {tutorialCategories.map((category) => (
                <option key={category.title} value={category.title}>
                  {category.title}
                </option>
              ))}
            </Select>
          </FormControl>
          <br />
          <br />
        </div>
      )}

      {!loading && tutorial !== undefined && (
        <TutorialGrid
          tutorials={tutorial.filter((tuto) => {
            if (categoryFilter === '') return tuto.section !== ''
            return tuto.section === categoryFilter
          })}
        />
      )}

      {tutorial !== undefined && tutorial.length > 4 && (
        <>
          <Divider
            variant="middle"
            style={{ backgroundColor: '#71717130', margin: '10px' }}
          />
          <TutorialGrid
            tutorials={tutorial.reverse().filter((tuto) => {
              if (categoryFilter === '') return tuto.section !== ''
              return tuto.section === categoryFilter
            })}
          />
        </>
      )}
      <Modal open={open} onClose={handleClose}>
        <TutorialModal />
      </Modal>
    </>
  )
}

export default Tutorials
