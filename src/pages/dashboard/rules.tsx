/* eslint-disable no-console */
import Link from 'next/link'
import Error from 'next/error'
import { useRouter } from 'next/router'
import { FC } from 'react'
import { Box, Button, Grid, Typography } from '@material-ui/core'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import ProductsTable from '@/components/tables/ProductsTable'
import Product from '@/types/Product'
import { firestore } from '@/lib/firebase'
import { useCollectionDataOnce } from 'react-firebase-hooks/firestore'

import { useStyles } from 'styles/Products.styles'
import SpinnerPage from '@/components/SpinnerPage'

interface Props {
  products: Product[]
}

const Rules: FC<Props> = () => {
  const router = useRouter()
  const classes = useStyles()

  const query = firestore.collection('products').orderBy('createdAt', 'desc')

  const [products, loading, error] = useCollectionDataOnce<Product>(query)

  const createProduct = () => {
    router.push('/dashboard/products/create')
  }

  if (loading) {
    return <SpinnerPage />
  }

  if (error) {
    return <Error statusCode={500} />
  }

  return (
    <>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
      >
        <Typography variant="h4" component="h2" gutterBottom>
          Regras de Negócio
        </Typography>

        <Box display="flex" justifyContent="space-between">
          <Link href="/dashboard/discontinued-products">
            <Button variant="text" color="primary" className={classes.button}>
              Produto Inativo
            </Button>
          </Link>
          <Button
            onClick={createProduct}
            variant="text"
            color="primary"
            endIcon={<AddCircleIcon />}
          >
            Nova Regra
          </Button>
        </Box>
      </Grid>
      <ProductsTable products={products as Product[]} />
    </>
  )
}

export default Rules
