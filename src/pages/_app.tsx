import { useEffect } from 'react'
import { ThemeProvider } from '@material-ui/core/styles'
import { ptBR } from 'date-fns/locale'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import DateFnsUtils from '@date-io/date-fns'
import CssBaseline from '@material-ui/core/CssBaseline'
import { AppProps } from 'next/app'
import theme from '@/theme'
import SpinnerPage from '@/components/SpinnerPage'
import { useRouter } from 'next/router'
import AppShell from '@/components/AppShell'
import { SessionProvider } from '@/context/SessionContext'

const authRoute = '/dashboard'

interface AppRoutesProps {
  Component: AppProps['Component']
  pageProps: AppProps['pageProps']
}

function AppRoutes({ Component, pageProps }: AppRoutesProps) {
  const router = useRouter()

  if (router.pathname.includes(authRoute)) {
    return (
      <AppShell>
        <Component {...pageProps} />
      </AppShell>
    )
  }

  return <Component {...pageProps} />
}

export default function MyApp({ Component, pageProps }: AppProps) {
  useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles.parentElement?.removeChild(jssStyles)
    }
  }, [])

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ptBR}>
      <ThemeProvider theme={theme}>
        <SessionProvider>
          <CssBaseline />
          <AppRoutes Component={Component} pageProps={pageProps} />
        </SessionProvider>
      </ThemeProvider>
    </MuiPickersUtilsProvider>
  )
}
