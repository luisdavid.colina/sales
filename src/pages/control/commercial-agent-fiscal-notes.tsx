import Layout from '@/components/Layout'
import CommercialAgentsTable from '@/components/tables/ControlTables/CommercialAgentsTable'
import { Typography } from '@material-ui/core'

const CommercialAgent = () => {
  return (
    <Layout>
      <>
        <Typography variant="h4" component="h2" gutterBottom>
          NF Agente Comercial
        </Typography>
        <CommercialAgentsTable />
      </>
    </Layout>
  )
}

export default CommercialAgent
