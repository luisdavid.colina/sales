import Layout from '@/components/Layout'
import FiscalNotesTable from '@/components/tables/ControlTables/FiscalNotesTable'
import { Typography } from '@material-ui/core'

const FiscalNotes = () => {
  return (
    <Layout>
      <>
        <Typography variant="h4" component="h2" gutterBottom>
          Notas Fiscais
        </Typography>
        <FiscalNotesTable />
      </>
    </Layout>
  )
}

export default FiscalNotes
