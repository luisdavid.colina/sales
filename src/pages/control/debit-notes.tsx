import Layout from '@/components/Layout'
import DebitNotesTable from '@/components/tables/ControlTables/DebitNotesTable'
import { Typography } from '@material-ui/core'

const FiscalNotes = () => {
  return (
    <Layout>
      <>
        <Typography variant="h4" component="h2" gutterBottom>
          Notas de Débito
        </Typography>
        <DebitNotesTable />
      </>
    </Layout>
  )
}

export default FiscalNotes
