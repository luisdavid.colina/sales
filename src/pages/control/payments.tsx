import Layout from '@/components/Layout'
import PaymentsReleasesTable from '@/components/tables/ControlTables/PaymentReleasesTable'
import CanceledOrderTable from '@/components/tables/ControlTables/CanceledOrderTable'

import { Typography, Paper, Box } from '@material-ui/core'

import Tab from '@/components/common/Tab'
import Tabs from '@/components/common/Tabs'

import useTab from '@/hooks/useTab'

const Payments = () => {
  const { active: activeTab, handleActive: handleActiveTab } = useTab()

  return (
    <Layout>
      <>
        <Typography variant="h4" component="h2" gutterBottom>
          Liberação de Carga
        </Typography>
        <Paper square>
          <Tabs
            value={activeTab}
            indicatorColor="primary"
            textColor="primary"
            onChange={handleActiveTab}
            aria-label="disabled tabs example"
            variant="fullWidth"
          >
            <Tab label="Liberação de Cargas" />
            <Tab label="Item de  pedido cancelados ou rejeitados" />
          </Tabs>
        </Paper>
        <Box mt={5}>
          {activeTab === 0 && <PaymentsReleasesTable />}
          {activeTab === 1 && <CanceledOrderTable />}
        </Box>
      </>
    </Layout>
  )
}

export default Payments
