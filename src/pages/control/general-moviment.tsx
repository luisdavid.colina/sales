import Layout from '@/components/Layout'
import PrepaidCardsTable from '@/components/tables/ControlTables/PrepaidCardsTable'
import TransfersTable from '@/components/tables/ControlTables/TransfersTable'

import { Typography, Paper, Box } from '@material-ui/core'

import Tab from '@/components/common/Tab'
import Tabs from '@/components/common/Tabs'

import useTab from '@/hooks/useTab'

const GeneralMoviment = () => {
  const { active: activeTab, handleActive: handleActiveTab } = useTab()

  return (
    <Layout>
      <>
        <Typography variant="h4" component="h2" gutterBottom>
          Movimentação Geral
        </Typography>
        <Paper square>
          <Tabs
            value={activeTab}
            indicatorColor="primary"
            textColor="primary"
            onChange={handleActiveTab}
            aria-label="disabled tabs example"
            variant="fullWidth"
          >
            <Tab label="Cartões" />
            <Tab label="Transferências" />
          </Tabs>
        </Paper>
        <Box mt={5}>
          {activeTab === 0 && <PrepaidCardsTable />}
          {activeTab === 1 && <TransfersTable />}
        </Box>
      </>
    </Layout>
  )
}

export default GeneralMoviment
