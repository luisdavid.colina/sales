import Layout from '@/components/Layout'
import CardsTable from '@/components/tables/ControlTables/CardsTable'
import { Typography } from '@material-ui/core'

const CardsBase = () => {
  return (
    <Layout>
      <>
        <Typography variant="h4" component="h2" gutterBottom>
          Base de Cartões
        </Typography>
        <CardsTable />
      </>
    </Layout>
  )
}

export default CardsBase
