import Layout from '@/components/Layout'
import OrdersTable from '@/components/tables/ControlTables/OrdersTable'
import { Typography } from '@material-ui/core'

const FiscalNotes = () => {
  return (
    <Layout>
      <>
        <Typography variant="h4" component="h2" gutterBottom>
          Pedido
        </Typography>
        <OrdersTable />
      </>
    </Layout>
  )
}

export default FiscalNotes
