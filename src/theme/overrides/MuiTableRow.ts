const custom = {
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: `transparent`,
    },
    '&:nth-of-type(2n)': {
      backgroundColor: `white`,
    },
  },
}

export default custom
