const custom = {
  root: {
    backgroundColor: '#E4E4E4',
    boxShadow: '0px 4px 4px rgba(0, 0, 0, 0.25)',
  },
}

export default custom
