interface OperatorFees {
  // only for cards
  monthlyFee: string
  unlockFee?: string
  reissueFee?: string
  chargebackFee?: string
  atmWithdrawFee?: string
  markupRate?: string
  rechargePortalFee?: string
  rechargeInvoiceFee?: string
  p2pTransferFee?: string
  // only for transfers
  transferFee?: string
  rejectedTransactionFee?: string
  // only for checks
  checkFee?: string
  // only for vouchers
  voucherFee?: string
  emitionTicket?: any
  pixPf?: string
  pixPj?: string
  lotteryDeposit?: any
  lotteryWithdrawal?: string
  PDV?: string
  aditionalCard?: string
  expiredCard?: string
  codeQR?: string
  TED?: any
  SMS?: any
  taxa?: string
  price?: string
}

export default OperatorFees
