interface Participant {
  id?: string
  name: string
}

export default Participant
