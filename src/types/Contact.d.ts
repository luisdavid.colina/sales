import DeliveryAddress from './DeliveryAddress'

interface Contact {
  id?: string
  uid?: string
  fullName: string
  cpf: string
  mobile: string
  email: string
  city?: string
  state?: string
}

export default Contact
