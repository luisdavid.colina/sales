export type Commission = {
  id: string
  from: number
  to: number
  loads: number
  salesChannel: string
}

type SalesCommission = {
  id: string
  name: string
  fixedCommissions: Commission[]
  ratedCommissions: Commission[]
}

export default SalesCommission
