export interface TutorialCategory {
  id?: string
  title: string
}

export default TutorialCategory
