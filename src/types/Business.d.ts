interface Business {
  id: number
  invoice_owner_name: string
  invoice_number: string
  invoice_issue_date: string
  invoice_payment_date: string
  invoice_price: string
  status: string
}

export default Business
