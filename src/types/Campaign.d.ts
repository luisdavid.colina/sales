interface Campaign {
  name: string
  rechargeFrequency: string
  annualEstimatedAward?: string
  totalCommissionRate?: number
  isAdministrationRate: boolean
  // only when is_administration_rate is true
  administrationRate?: string
  // only when is_administration_rate is false
  administrationFee?: string
  // only when product category is a card
  issueFee?: string
  operatorFee?: string
}

export default Campaign
