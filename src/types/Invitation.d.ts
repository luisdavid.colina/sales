import { Role } from './User'
import TwoFactorAuth from './TwoFactorAuth'
import Company from './Company'

type InvitationCategory = 'company' | 'element' | 'commercialAgent'

type InvitationStatus = 'open' | 'canceled' | 'completed'

interface Invitation {
  id: string
  cpf: string
  email: string
  category: InvitationCategory
  status: InvitationStatus
  companyId?: string
  commercialAgentId?: string
  company?: Company
  role?: Role
  permissions?: string[]
  twoFactorAuth: TwoFactorAuth
}

export default Invitation
