interface CompanyOperatingFees {
  KYC?: string
  emitionTicket?: string
  rejectedTransactionFee?: string
  pixPf?: string
  pixPj?: string
  lotteryDeposit?: string
  lotteryWithdrawal?: string
  PDV?: string
  aditionalCard?: string
  expiredCard?: string
  codeQR?: string
  TED?: string
  SMS?: string
  balanceTransferFee: string
  minimumLoadAmount: string
  belowMinimumLoadFee: string
  emergencyLoadFee: string
  specialHandlingFee: string
  chargebackRate: string
}

export default CompanyOperatingFees
