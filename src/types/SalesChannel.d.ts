import Participant from './Participant'

interface SalesChannel {
  id: string
  name: string
  participants: Participant[]
}

export default SalesChannel
