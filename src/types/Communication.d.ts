import { FieldValue } from '@/lib/firebase'

interface Communication {
  id: string
  object?: string
  text?: string
  to: string[]
  sendingMethod?: string[]
  status?: string
  subject?: string
  createdAt?: FieldValue.Timestamp
}

export default Communication
