export type DestinationType = {
  Destination: {
    CcAddresses?: string[]
    ToAddresses?: string[]
  }
  ReplacementTemplateData: string
}
interface EmailType {
  Destinations: DestinationType[]
  Source: string
  Template: string
  DefaultTemplateData: string
  ReplyToAddresses?: string[]
}

export default EmailType
