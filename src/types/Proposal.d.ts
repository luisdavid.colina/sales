import { FieldValue } from '@/lib/firebase'
import Company from './Company'
import ProductDetails from './ProductDetails'

interface Proposal {
  id?: string
  proposalNumber?: number
  company: Company
  productDetails: ProductDetails
  pdfKey?: string
  createdAt?: FieldValue.Timestamp
}

export default Proposal
