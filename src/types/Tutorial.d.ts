export interface Tutorial {
  id?: string
  title: string
  section?: string
  subject?: string
  data?: string
  media?: string
  attachments?: string[]
  status?: string
  createdAt?: any
}

export default Tutorial
