import Company from './Company'
import SalesCommission from './SalesCommission'

interface CommercialAgent {
  id: string
  company: Company
  salesCommissionId: string
  salesCommission?: SalesCommission
}

export default CommercialAgent
