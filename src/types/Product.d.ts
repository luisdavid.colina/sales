import { FieldValue } from '@/lib/firebase'

export interface ProductCategory {
  id?: string
  name: string
}

export interface ProductFamily {
  id?: string
  name: string
}

interface SalesChannel {
  name: string
}

export interface Rule {
  id?: string
  name: string
  salesChannel?: SalesChannel
}

interface Product {
  id?: string
  name: string
  productCategory: ProductCategory
  productFamily: ProductFamily
  rule: Rule
  estimatedRevenue?: number
  participantsCommissions?: any[]
  totalCommission?: number
  totalValue?: number
  paymentPerMonth?: number
  createdAt?: FieldValue.Timestamp
}

export default Product
