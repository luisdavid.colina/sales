interface DeliveryAddress {
  postalCode: string
  street: string
  number: string
  complement?: string
  district: string
  city: string
  state: string
  receiver: string
  deliveryAgent: string
  isCentralizedDelivery: boolean
  deliveryService: string
}

export default DeliveryAddress
