type TwoFactorAuth = {
  secret: string
  uri: string
  qr: string
}

export default TwoFactorAuth
