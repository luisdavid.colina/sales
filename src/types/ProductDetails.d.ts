import Campaign from './Campaign'
import { CommercialAgent } from './CommercialAgent'
import CompanyOperatingFees from './CompanyOperatingFees'
import OperatorFees from './OperatorFees'
import CardFees from './CardFees'

type Product =
  | 'prepaid_card'
  | 'voucher'
  | 'check'
  | 'digital_account'
  | 'transfer'

interface ProductDetails {
  product: Product
  productBrand?: string
  campaign: Campaign
  // array of commercial agents ids
  commercialAgent: CommercialAgent
  operatorFees: OperatorFees
  // only for cards
  companyOperatingFees?: CompanyOperatingFees
  salesChannel?: string
  cardFees?: CardFees
  classAccount?: string
  isGiftCard?: boolean
  kindAccount?: string
  isInternationalCard?: boolean
  cardQuantity?: number
  isChipCard?: boolean
  issuer?: string
}

export default ProductDetails
