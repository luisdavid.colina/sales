import { FieldValue } from '@/lib/firebase'

type Invoice = {
  id?: string
  companyName?: string
  nfNumber?: string
  dateReceipt?: string
  datePayment?: string
  fileKey?: string
  amount?: string
  status?: string
  createdAt?: FieldValue.Timestamp
}

export default Invoice
