interface CardFees {
  // only for cards
  physicalCard?: string
  sendCard?: string
  cardWithdrawal?: string
}

export default CardFees
