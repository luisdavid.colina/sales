import { FieldValue } from '@/lib/firebase'
import Company from './Company'
import DeliveryAddress from './DeliveryAddress'
import ProductDetails from './ProductDetails'
import Revenue from './Revenue'

interface Contract {
  id: string
  company: Company
  productDetails: ProductDetails
  deliveryAddress: DeliveryAddress
  revenue: Revenue
  createdAt?: FieldValue.Timestamp
  pdfKey: string
  d4signUuidDocument?: string
  d4signStatus?: string
  isActive: boolean
  signers?: string[]
}

export default Contract
