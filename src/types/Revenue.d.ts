import { FieldValue } from '@/lib/firebase'

interface Revenue {
  calculation: string
  currency: string
  paymentMethod: string
  isInvoiceTopup: boolean
  issuingCompanyId?: number | null
  bankAccountId?: number | null
  serviceCodeId?: number | null
  signatureDate?: Date | FieldValue.Timestamp
  expirationDate?: Date | FieldValue.Timestamp
  isSigned: boolean
}

export default Revenue
