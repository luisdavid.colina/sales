import { FieldValue } from '@/lib/firebase'

interface Extract {
  id: string
  createdAt: FieldValue.Timestamp
  description: string
  price: string
  status: string
}

export default Extract
