export type Permissions = {
  create: boolean
  read: boolean
  write: boolean
  delete: boolean
}
export type Permisssion = keyof Permissions

export type Role =
  | 'superAdmin'
  | 'admin'
  | 'explorer'
  | 'developer'
  | 'commercialAgent'

export type CompanyRole = 'explorer' | 'developer' | 'commercialAgent'

export interface InterfaceCompanyPermissions<T> {
  profiles?: T
  communications?: T
  businessRules?: T
}
export type CompanyPermissions = InterfaceCompanyPermissions<Permissions>
export type CompanyPermission = keyof CompanyPermissions

interface User {
  uid: string
  fullName: string
  cpf: string
  companyRole: string
  companyArea: string
  phone: string
  mobile: string
  email: string
  role?: Role
  companyId?: string
  permissions?: CompanyPermissions<Permissions>
}

export default User
