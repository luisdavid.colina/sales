import { FieldValue } from '@/lib/firebase'

type Product =
  | 'prepaid_card'
  | 'voucher'
  | 'check'
  | 'digital_account'
  | 'transfer'

interface ProposalRequest {
  id: string
  contactFullName: string
  companyLegalName: string
  companyArea: string
  companyRole: string
  cnpj: string
  email: string
  mobile: string
  phone: string
  createdAt?: FieldValue.Timestamp
  product: Product
  paymentFrequency: string
  commercialAgentsQuantity: number
  totalIncome: string
}

export default ProposalRequest
