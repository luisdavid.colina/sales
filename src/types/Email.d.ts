interface EmailType {
  Destination: {
    CcAddresses?: string[]
    ToAddresses?: string[]
  }
  Source: string
  Template: string
  TemplateData: string
  ReplyToAddresses?: string[]
}

export default EmailType
