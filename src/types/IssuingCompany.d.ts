interface IssuingCompany {
  id: string
  cnpj: string
  legalName: string
  email: string
  website?: string
  comments?: string
  tradingName: string
  phone: string
  mobile?: string
  ie: string
  im: string
}

export default IssuingCompany
