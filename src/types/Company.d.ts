import User from './User'

interface Company {
  cnpj: string
  legalName: string
  email: string
  website?: string
  comments?: string
  tradingName: string
  phone: string
  mobile?: string
  admin: User
}

export default Company
