import { makeStyles, createStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) =>
  createStyles({
    container: {
      marginBottom: theme.spacing(4),
    },
    title: {
      fontWeight: 'bold',
      fontSize: '1.2rem',
    },
    subTitle: {
      fontWeight: 'bold',
    },
    chip: {
      marginLeft: '10px',
      minWidth: '70px',
    },
  }),
)
