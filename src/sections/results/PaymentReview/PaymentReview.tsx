import { FC } from 'react'
import Grid from '@material-ui/core/Grid'
import Chip from '@material-ui/core/Chip'
import Typography from '@material-ui/core/Typography'
import FilterListIcon from '@material-ui/icons/FilterList'
import { useStyles } from './PaymentReview.styles'

type actionProp = (
  value: string,
) => (e: React.MouseEvent<HTMLButtonElement>) => void

type PaymentReviewProps = {
  rows: {
    id: string
    payment_method: string
    total_volume_money: string
    available_money: string
    numbers_of_agents: number
  }[]
  action: actionProp
}

const PaymentReview: FC<PaymentReviewProps> = ({ rows, action }) => {
  const classes = useStyles()

  return (
    <Grid container spacing={3} className={classes.container}>
      {rows.map((item) => (
        <Grid item xs={6} key={`payment_review_${item.id}`}>
          <Typography className={classes.title}>
            {item.payment_method}
          </Typography>
          <Grid container spacing={1} alignItems="center">
            <Grid item xs={5}>
              <Typography
                variant="body1"
                component="div"
                className={classes.subTitle}
              >
                Agentes
                <Chip
                  component="button"
                  className={classes.chip}
                  label={item.numbers_of_agents}
                  clickable
                  onClick={action(item.payment_method)}
                  color="primary"
                  deleteIcon={<FilterListIcon />}
                  onDelete={action(item.payment_method)}
                  variant="outlined"
                />
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography variant="body1" className={classes.subTitle}>
                Volume financeiro{' '}
                <Typography
                  component="span"
                  color="primary"
                  variant="body1"
                  className={classes.subTitle}
                >
                  R$ 0,00
                </Typography>
              </Typography>
            </Grid>
          </Grid>
        </Grid>
      ))}
    </Grid>
  )
}

export default PaymentReview
