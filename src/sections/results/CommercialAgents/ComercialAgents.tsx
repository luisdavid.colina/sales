import { FC, useMemo } from 'react'

import { DataGridProps, GridColDef } from '@material-ui/data-grid'

import Table from 'components/Table'
import { columns, actionProp, paymentTypes } from './ComercialAgents.const'

type CommercialAgentsProps = {
  rows: {
    id: string
    agent_name: string
    cpf_cnpj: string
    value: string
    payment_method: string
    payment_method_id: string
  }[]
  action: actionProp
  payments: paymentTypes[]
} & Omit<DataGridProps, 'columns'>

const CommercialAgents: FC<CommercialAgentsProps> = ({
  rows,
  action,
  payments,
  ...others
}) => {
  const columnProp = useMemo<GridColDef[]>(() => columns(action, payments), [])
  return (
    <Table
      height={600}
      columns={columnProp}
      rows={rows}
      sortModel={[
        {
          field: 'value',
          sort: 'asc',
        },
      ]}
      {...others}
    />
  )
}

export default CommercialAgents
