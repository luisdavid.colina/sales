import React from 'react'
import Box from '@material-ui/core/Box'
import TextField from '@material-ui/core/TextField'
import MenuItem from '@material-ui/core/MenuItem'
import { GridCellParams, GridColDef } from '@material-ui/data-grid'

export type paymentTypes = {
  id: string
  payment_method: string
  total_volume_money: string
  available_money: string
  numbers_of_agents: number
}

export type actionProp = (
  value: string,
) => (
  e: React.ChangeEvent<{
    name?: string | undefined
    value: unknown
  }>,
) => void

const columns: (
  action: actionProp,
  payments: paymentTypes[],
) => GridColDef[] = (action, payments) => [
  {
    field: 'agent_name',
    headerName: 'Beneficiário',
    flex: 1,
  },
  {
    field: 'cpf_cnpj',
    headerName: 'CPF/CNPJ',
    flex: 1,
  },
  {
    field: 'value',
    headerName: 'Valor',
    flex: 1,
  },
  {
    field: 'payment_method',
    headerName: 'Liberação de Remessa',
    flex: 1,
    renderCell: ({ row }: GridCellParams) => (
      <Box
        display="flex"
        justifyContent="space-around"
        flex={1}
        alignItems="center"
      >
        <TextField
          fullWidth
          value={row.payment_method_id}
          onChange={action(row.id)}
          select
          variant="outlined"
          margin="dense"
        >
          {payments.map((pay) => (
            <MenuItem key={`${row.id}_${pay.payment_method}`} value={pay.id}>
              {pay.payment_method}
            </MenuItem>
          ))}
        </TextField>
      </Box>
    ),
  },
]

export { columns }
