import { FC } from 'react'
import Table from '@material-ui/core/Table'
import TableContainer from '@material-ui/core/TableContainer'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'

import { useStyles } from './TotalPayment.styles'

export declare type totalPaymentRow = {
  id: string
  name: string
  value: string
}

type PaymentReviewProps = {
  rows: totalPaymentRow[]
}

const TotalPayment: FC<PaymentReviewProps> = ({ rows }) => {
  const classes = useStyles()
  const total = 'R$ 870.000,00'
  return (
    <TableContainer>
      <Table aria-label="spanning table" size="small">
        <TableBody className={classes.body}>
          {rows.map((row: totalPaymentRow) => (
            <TableRow key={`total_payment_${row.id}`}>
              <TableCell align="left">{row.name}</TableCell>
              <TableCell align="right">{row.value}</TableCell>
            </TableRow>
          ))}
          <TableRow>
            <TableCell align="left">Total a Pagar</TableCell>
            <TableCell align="right">{total}</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default TotalPayment
