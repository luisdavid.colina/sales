import { makeStyles, createStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) =>
  createStyles({
    body: {
      '& .MuiTableCell-body': {
        borderBottomColor: theme.palette.grey[500],
        paddingBottom: '2px',
        paddingTop: '2px',
      },
      '& .MuiTableRow-root:nth-last-child(2) .MuiTableCell-body': {
        borderBottomColor: 'transparent',
      },
      '& .MuiTableRow-root:last-child': {
        '& .MuiTableCell-body': {
          paddingBottom: '4px',
          paddingTop: '4px',
          borderBottomColor: 'transparent',
          color: 'white',
          fontWeight: 600,
          backgroundColor: theme.palette.primary.main,
        },
        '& .MuiTableCell-body:first-child': {
          borderTopLeftRadius: theme.spacing(1),
          borderBottomLeftRadius: theme.spacing(1),
        },
        '& .MuiTableCell-body:last-child': {
          borderTopRightRadius: theme.spacing(1),
          borderBottomRightRadius: theme.spacing(1),
        },
      },
    },
  }),
)
