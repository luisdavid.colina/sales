import { STATES } from '@/constants'
import {
  Box,
  Typography,
  TextField,
  Button,
  MenuItem,
  Checkbox,
  FormGroup,
  FormControlLabel,
} from '@material-ui/core'
import useBoolean from '@/hooks/useBoolean'
import SuccessModal from '@/components/SuccessModal'
import AttachFileIcon from '@material-ui/icons/AttachFile'
import { useState, ChangeEvent, useEffect } from 'react'
import ContactsTable from '@/components/tables/ContactsTable'
import hasPermission from '@/hooks/hasPermission'
import axios from 'axios'
import communicationDefault from '@/forms/defaultStates/communication'
import Communication from '@/types/Communication'
import { useRouter } from 'next/router'
import { useStyles } from './NewCommunication.styles'

const COUNTRIES = ['Brasil']

const TEMP = ['']

const ACCESS_TYPES = ['Element']

const USER_LICENSES = ['ADM', 'Desenvolvedor', 'Explorador', 'Usuario']

const SENDING_METHODS = ['E-Mail', 'WhatsApp', 'SMS']

const NewCommunication = () => {
  const classes = useStyles()
  const [nextStep, setNextStep] = useState<number>(1)
  const [uf, setUf] = useState<string>('')
  const [citys, setCitys] = useState<string[]>(TEMP)
  const [loading, setLoading] = useState<boolean>(false)

  const [tos, setTos] = useState<string[]>([])
  const router = useRouter()
  const disabledButton = !hasPermission({ profiles: ['write'] })

  const [communication, setCommunication] = useState<Communication>(
    communicationDefault,
  )
  useEffect(() => {
    setLoading(true)
    axios
      .get(`/api/city?uf=DF`)
      .then((response) => {
        setCitys(
          response.data.map((city: any) => {
            return city.name
          }),
        )
      })
      .finally(() => {
        setLoading(false)
      })
  }, [])

  const onChangeUf = async (e: any) => {
    setLoading(true)
    setUf(e.target.value)
    const query: any = await axios.get(`/api/city?uf=${e.target.value}`)
    setCitys(
      query.data.map((city: any) => {
        return city.name
      }),
    )
    setLoading(false)
  }

  const handleSubject = (e: ChangeEvent<HTMLInputElement>) => {
    setCommunication({
      ...communication,
      subject: e.target.value,
    })
  }
  const handleText = (e: ChangeEvent<HTMLInputElement>) => {
    setCommunication({
      ...communication,
      text: e.target.value,
    })
  }

  const {
    value: success,
    toTrue: isSuccess,
    toFalse: isNotSuccess,
  } = useBoolean({
    initialValue: false,
  })

  const onClose = () => {
    isNotSuccess()
    router.reload()
  }

  const sendCommunication = async () => {
    const communicationTest = {
      ...communication,
      to: tos,
    }
    setCommunication(communicationTest)
    const resp = await axios.post('/api/communications', communicationTest)
    if (resp.status === 201) {
      isSuccess()
    }
  }

  return (
    <>
      <Box display="flex" className={classes.container} flexDirection="column">
        <Box className={classes.titleContainer} display="flex">
          <Typography
            className={classes.font}
            variant="h4"
            color="textSecondary"
          >
            Nova Mensagem
          </Typography>
        </Box>
        {nextStep === 1 && (
          <>
            <Typography variant="h3" className={classes.recipient}>
              Destinatarios
            </Typography>
            <Box
              display="flex"
              className={classes.messageContainer}
              flexDirection="column"
            >
              <Box display="flex" flex-direction="row">
                <TextField
                  select
                  className={classes.select}
                  size="small"
                  fullWidth
                  label="País"
                  value="Brasil"
                  inputProps={{
                    name: 'country',
                    id: 'country',
                  }}
                >
                  {COUNTRIES.map((country) => (
                    <MenuItem key={country} value={country}>
                      {country}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  select
                  className={classes.select}
                  size="small"
                  fullWidth
                  value={uf}
                  onChange={onChangeUf}
                  label="UF"
                  inputProps={{
                    name: 'uf',
                    id: 'uf',
                  }}
                >
                  {STATES.map((state) => (
                    <MenuItem key={state} value={state}>
                      {state}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  select
                  className={classes.select}
                  size="small"
                  fullWidth
                  label={loading ? 'Carregando...' : 'Cidade'}
                  inputProps={{
                    name: 'city',
                    id: 'city',
                  }}
                  disabled={loading}
                >
                  {citys.map((city) => (
                    <MenuItem key={city} value={city}>
                      {city}
                    </MenuItem>
                  ))}
                </TextField>

                <TextField
                  select
                  className={classes.select}
                  size="small"
                  fullWidth
                  label="Tipo de Acesso"
                  inputProps={{
                    name: 'access',
                    id: 'access',
                  }}
                  value={ACCESS_TYPES[0]}
                >
                  {ACCESS_TYPES.map((access) => (
                    <MenuItem key={access} value={access}>
                      {access}
                    </MenuItem>
                  ))}
                </TextField>
                <TextField
                  select
                  className={classes.select}
                  size="small"
                  fullWidth
                  label="Licença de Usuário"
                  inputProps={{
                    name: 'userType',
                    id: 'userType',
                  }}
                >
                  {USER_LICENSES.map((userType) => (
                    <MenuItem key={userType} value={userType}>
                      {userType}
                    </MenuItem>
                  ))}
                </TextField>
              </Box>
              <ContactsTable disabled={disabledButton} tos={setTos} />
            </Box>
            <Box
              display="flex"
              justifyContent="flex-end"
              className={classes.bottomContainer}
            >
              <Button
                variant="contained"
                color="primary"
                disabled={tos.length === 0}
                onClick={() => setNextStep(2)}
              >
                Avançar
              </Button>
            </Box>
          </>
        )}
        {nextStep === 2 && (
          <>
            <Typography variant="h3" className={classes.recipient}>
              Canal de envio
            </Typography>
            <Box
              display="flex"
              className={classes.sendingMethodContainer}
              flexDirection="row"
            >
              {SENDING_METHODS.map((sendingMethod) => (
                <div className={classes.imgContainer}>
                  <input
                    type="checkbox"
                    className={classes.input}
                    id={sendingMethod}
                    defaultChecked
                    onChange={() => {
                      const prevSendingMethod: string[] =
                        communication?.sendingMethod || []
                      if (
                        !communication.sendingMethod?.some(
                          (m) => m === sendingMethod,
                        )
                      ) {
                        setCommunication({
                          ...communication,
                          sendingMethod: [...prevSendingMethod, sendingMethod],
                        })
                      } else {
                        setCommunication({
                          ...communication,
                          sendingMethod: prevSendingMethod.filter(
                            (m) => m !== sendingMethod,
                          ),
                        })
                      }
                    }}
                  />
                  <label htmlFor={sendingMethod}>
                    <img
                      src={
                        // eslint-disable-next-line no-nested-ternary
                        sendingMethod === 'E-Mail'
                          ? '/images/email.png'
                          : sendingMethod === 'WhatsApp'
                          ? '/images/whatsapp.png'
                          : '/images/sms.png'
                      }
                      height="80px"
                      alt={sendingMethod}
                    />
                  </label>
                  <p>{sendingMethod}</p>
                </div>
              ))}
            </Box>
            <Box
              display="flex"
              justifyContent="space-between"
              className={classes.bottomContainer}
            >
              <Box display="flex" alignItems="center" />
              <Box
                display="flex"
                justifyContent="space-around"
                className={classes.bottomContainer}
              >
                <Button
                  variant="contained"
                  color="default"
                  onClick={() => setNextStep(1)}
                >
                  Voltar
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={() => setNextStep(3)}
                >
                  Avançar
                </Button>
              </Box>
            </Box>
          </>
        )}
        {nextStep === 3 && (
          <>
            <Box
              display="flex"
              className={classes.messageContainer}
              flexDirection="column"
            >
              <TextField
                id="standard-textarea"
                label="Assunto"
                placeholder="Assunto"
                multiline
                className={classes.subject}
                value={communication.subject}
                onChange={handleSubject}
              />

              <TextField
                id="standard-multiline-static"
                multiline
                rows={10}
                placeholder="Digite Aqui..."
                InputProps={{ disableUnderline: true }}
                value={communication.text}
                onChange={handleText}
              />
            </Box>

            <Box
              display="flex"
              justifyContent="space-between"
              className={classes.bottomContainer}
            >
              <Box display="flex" alignItems="center">
                <label htmlFor="upload">
                  <input id="upload" type="file" hidden />
                  <AttachFileIcon color="primary" />
                  <span>Anexar Arquivo</span>
                </label>
              </Box>
              <Box
                display="flex"
                justifyContent="space-around"
                className={classes.buttonContainer}
              >
                <Button
                  variant="contained"
                  color="default"
                  onClick={() => setNextStep(2)}
                >
                  Voltar
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={sendCommunication}
                >
                  Enviar
                </Button>
              </Box>
            </Box>
          </>
        )}
      </Box>
      <SuccessModal
        title="Comunicação enviada com sucesso"
        open={success}
        onClose={onClose}
      />
    </>
  )
}

export default NewCommunication
