import { makeStyles, createStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) =>
  createStyles({
    font: {
      fontSize: '16px',
      color: theme.palette.secondary.contrastText,
    },
    recipient: {
      fontSize: '18px',
      marginTop: theme.spacing(5),
      marginLeft: theme.spacing(3),
    },
    titleContainer: {
      padding: '18px',
      backgroundColor: theme.palette.primary.main,
    },
    container: {
      width: '80%',
      maxHeight: '80vh',
      marginTop: theme.spacing(5),
      backgroundColor: '#FFF',
      margin: 'auto',
      boxShadow: theme.shadows[5],
    },
    messageContainer: {
      marginTop: theme.spacing(2),
      padding: '0px 15px 0px 15px',
    },
    sendingMethodContainer: {
      marginTop: theme.spacing(2),
      padding: '0px 15px 0px 15px',
      margin: 'auto',
      minHeight: '300px',
    },
    subject: {
      marginBottom: theme.spacing(3),
      width: '100%',
    },
    bottomContainer: {
      backgroundColor: '#F0F0F0',
      padding: '10px',
    },
    selectContainer: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginBottom: theme.spacing(3),
    },
    select: {
      width: '200px',
      marginRight: '15px',
    },
    buttonContainer: {
      width: '170px',
      marginBottom: theme.spacing(2),
    },
    imgContainer: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      transition: 'all .2s ease-in-out',
      width: '150px',
      alignItems: 'center',
      cursor: 'pointer',
      '&:hover': {
        transform: 'scale(1.1)',
      },
    },
    input: {
      opacity: 0,
      '&:default + label img': {
        opacity: 0.3,
        transitionDuration: '0.4s',
      },
      '&:checked + label img': {
        opacity: 1,
        transitionDuration: '0.4s',
      },
    },
  }),
)
