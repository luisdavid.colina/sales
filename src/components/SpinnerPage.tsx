import { Box, CircularProgress } from '@material-ui/core'

const SpinnerPage = () => {
  return (
    <Box
      height="100vh"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <CircularProgress />
    </Box>
  )
}

export default SpinnerPage
