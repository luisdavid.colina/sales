import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

type InformationProps = {
  text?: string
}

const Information = ({ text }: InformationProps) => (
  <Grid container justify="center" alignItems="center">
    <img src="/assets/login/logo.svg" alt="Em breve" width="60%" />
    <Grid item xs={12}>
      <Typography color="primary" variant="h4" align="center">
        {text}
      </Typography>
    </Grid>
  </Grid>
)

Information.defaultProps = {
  text: 'Em breve',
}

export default Information
