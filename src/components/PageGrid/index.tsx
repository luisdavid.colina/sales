import React from 'react'
import Link from 'next/link'

import styles from 'styles/Home.module.css'

interface TitleProps {
  title: string
  to: string
}

const PageGrid: React.FC<TitleProps> = ({ title, children, to }) => {
  return (
    <div className={styles.homeGridContainer}>
      <Link href={to}>
        <a>
          {children}
          <span>{title}</span>
        </a>
      </Link>
    </div>
  )
}

export default PageGrid
