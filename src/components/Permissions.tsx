import { FC } from 'react'

import { Role, InterfaceCompanyPermissions, Permisssion } from '@/types/User'
import hasPermission from '@/hooks/hasPermission'
import Information from '@/components/Information'

type PermissionsProps = {
  permissions: InterfaceCompanyPermissions<Permisssion[]>
  roles?: Role[]
}

// TODO Information or Redirect
const PermissionsComponent: FC<PermissionsProps> = ({
  children,
  permissions,
  roles,
}) => {
  const show = hasPermission(permissions, roles)

  return show ? <>{children}</> : <Information text="Não autorizado" />
}

export default PermissionsComponent
