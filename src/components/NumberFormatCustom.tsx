import NumberFormat from 'react-number-format'

export const NumberFormatCustom = (props: any) => {
  const { inputRef, onChange, value, ...other } = props
  const first = String(value).substr(0, 2) === 'R$'
  if (!first)
    return (
      <NumberFormat
        {...other}
        getInputRef={inputRef}
        onValueChange={(values) => {
          onChange({
            target: {
              name: props.name,
              value: values.value,
            },
          })
        }}
        decimalSeparator=","
        thousandSeparator="."
        decimalScale={2}
        fixedDecimalScale
        allowEmptyFormatting
        allowNegative={false}
        isNumericString
        prefix="R$ "
      />
    )
  return (
    <NumberFormat
      {...props}
      decimalSeparator=","
      thousandSeparator="."
      isNumericString
    />
  )
}

export const NumberFormatRate = (props: any) => {
  const { inputRef, onChange, ...other } = props
  const MAX_VAL = 100
  const withValueCap = (inputObj: any) => {
    const { value } = inputObj
    if (value <= MAX_VAL) return true
    return false
  }
  return (
    <NumberFormat
      {...other}
      getInputRef={inputRef}
      onValueChange={(values) => {
        onChange({
          target: {
            name: props.name,
            value: values.value,
          },
        })
      }}
      decimalSeparator=","
      decimalScale={2}
      fixedDecimalScale
      isNumericString
      allowEmptyFormatting
      allowNegative={false}
      isAllowed={withValueCap}
    />
  )
}
