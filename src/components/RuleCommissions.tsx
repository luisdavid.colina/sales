import { FC, useEffect, useState } from 'react'
import { createRuleColumns, createRuleRows } from '@/util/helpers'

import { Product } from '@/schemas/ProductSchema'

import Participant from '@/types/Participant'

import CommissionsTable from './tables/CommissionsTable'

type Props = {
  product: Product
  rule: any
}

const getRuleParams = ({ rule }: Props) => {
  const { participants, commissionType, months } = rule
  return {
    participants: participants as Participant[],
    commissionType: commissionType as string,
    months: months as number,
  }
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const RuleCommissions: FC<Props> = ({ children, ...data }) => {
  const [rows, setRows] = useState<any>(null)
  const [columns, setColumns] = useState<any>(null)

  useEffect(() => {
    const params = getRuleParams(data)
    setRows(createRuleRows(params))
    setColumns(createRuleColumns(params))
  }, [])

  const showTable = rows && columns

  if (!showTable) return null

  return <CommissionsTable data={data} rows={rows} columns={columns} />
}

export default RuleCommissions
