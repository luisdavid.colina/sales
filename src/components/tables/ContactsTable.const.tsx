import {
  GridCellParams,
  GridValueFormatterParams,
} from '@material-ui/data-grid'
import Checkbox from '@material-ui/core/Checkbox'
import { CompanyPermission } from '@/types/User'
import { EXPLORER_ROLE } from '@/constants'

const formatPhoneNumber = (phoneNumberString: string): string => {
  const value = phoneNumberString
  const arrayValue: string[] = Array.from(value).reverse().slice(0, 4)
  arrayValue.push('-')
  const formatValue = arrayValue.concat(
    Array.from(value).reverse().slice(4, value.length),
  )
  const newValue = formatValue.reverse().join('').replace('11', '+55 (11) ')
  return newValue
}

type onClickCheckbox = (column: CompanyPermission, id: string) => () => void

const columnCheckbox = (
  column: CompanyPermission,
  onClick: onClickCheckbox,
) => ({ value, row }: GridCellParams) => (
  <Checkbox
    checked={Boolean(value)}
    disabled={row.role !== EXPLORER_ROLE}
    onChange={onClick(column, row.id)}
    inputProps={{ 'aria-label': 'primary checkbox' }}
  />
)

const getColumns = (onClick: onClickCheckbox, disabled: boolean) => [
  {
    field: 'fullName',
    headerName: 'Nome completo',
    flex: 1,
  },
  {
    field: 'cpf',
    headerName: 'CPF',
    flex: 1,
  },
  {
    field: 'email',
    headerName: 'E-mail',
    flex: 1,
  },
  {
    field: 'mobile',
    headerName: 'Telefone',
    valueFormatter: (params: GridValueFormatterParams) => {
      const value = params.value as string
      return formatPhoneNumber(value)
    },
    flex: 1,
  },
  {
    field: 'state',
    headerName: 'UF',
    flex: 1,
  },
  {
    field: 'city',
    headerName: 'Cidade',
    flex: 1,
  },
]

export default getColumns
