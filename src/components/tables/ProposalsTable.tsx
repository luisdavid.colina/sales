import { GridValueFormatterParams } from '@material-ui/data-grid'
import Proposal from '@/types/Proposal'
import { formatProposalStatus } from '@/util/helpers'
import Table, { Props as TableProps } from 'components/Table'
import { FC } from 'react'
import {
  getProductName,
  getTradingName,
  getCompanyName,
  getCnpj,
  getCommercialAgent,
} from './getters'

const columns = [
  {
    field: 'tradingName',
    headerName: 'Razão Social',
    valueGetter: getTradingName,
    flex: 1,
  },
  {
    field: 'companyCnpj',
    headerName: 'CNPJ',
    flex: 1,
    valueGetter: getCnpj,
  },
  {
    field: 'companyName',
    headerName: 'Nome',
    valueGetter: getCompanyName,
    flex: 1,
  },
  {
    field: 'agent',
    headerName: 'Agente Comercial',
    valueGetter: getCommercialAgent,
    flex: 1,
  },
  {
    field: 'productName',
    headerName: 'Produto',
    valueGetter: getProductName,
    flex: 1,
  },
  {
    field: 'proposalNumber',
    headerName: 'Proposta',
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    width: 120,
    valueFormatter: (params: any) => formatProposalStatus(params.value),
  },
  {
    field: 'createdAt',
    headerName: 'Data',
    valueFormatter: (params: GridValueFormatterParams) => {
      const value = params.value as string
      return value
    },
    width: 0,
  },
]

type Props = {
  proposals: Proposal[]
} & Omit<TableProps, 'rows' | 'columns' | 'components'>

const ProposalsTable: FC<Props> = ({ proposals, ...others }) => {
  return (
    <Table
      columns={columns}
      rows={proposals}
      sortModel={[{ field: 'createdAt', sort: 'desc' }]}
      {...others}
    />
  )
}

export default ProposalsTable
