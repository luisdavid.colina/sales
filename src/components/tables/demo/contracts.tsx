export const rows = [
  {
    id: '1',
    company: {
      name: 'Marvel Company',
      cnpj: '123456',
      trading_name: 'Marvel',
    },
    is_active: true,
  },
  {
    id: '2',
    company: {
      name: 'Nike Company',
      cnpj: '123456',
      trading_name: 'Nike',
    },
    is_active: true,
  },
  {
    id: '3',
    company: {
      name: 'Facebook Company',
      cnpj: '123456',
      trading_name: 'Facebook',
    },
    is_active: true,
  },
  {
    id: '4',
    company: {
      name: 'HBO Company',
      cnpj: '123456',
      trading_name: 'Company',
    },
    is_active: true,
  },
  {
    id: '5',
    company: {
      name: 'Amazon Company',
      cnpj: '123456',
      trading_name: 'Amazon',
    },
    is_active: true,
  },
  {
    id: '6',
    company: {
      name: 'Disney company',
      cnpj: '123456',
      trading_name: 'Disney',
    },
    is_active: true,
  },
  {
    id: '7',
    company: {
      name: 'Netflix Company',
      cnpj: '123456',
      trading_name: 'Netlix',
    },
    is_active: true,
  },
]
