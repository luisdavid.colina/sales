import Table from 'components/Table'

const columns = [
  {
    field: 'adm_tax',
    headerName: 'Taxa ADM(%)',
    flex: 1,
  },
  {
    field: 'sell_type',
    headerName: 'Tipo de Venda',
    flex: 1,
  },
  {
    field: 'commission',
    headerName: 'Comissão s/ Cargas',
    flex: 1,
  },
]

const rows = [
  {
    id: 1,
    adm_tax: '',
    sell_Type: '',
    commission: '',
  },
]

function VariableTable() {
  return <Table columns={columns} rows={rows} />
}

export default VariableTable
