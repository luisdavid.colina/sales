import Table, { Props as TableProps } from '@/components/Table'
import { Commission } from '@/schemas/SalesCommissionSchema'

type Props = {
  commissions: Commission[]
} & Omit<TableProps, 'rows'>

function CommissionTable({ commissions, columns, ...others }: Props) {
  return <Table columns={columns} rows={commissions} {...others} />
}

export default CommissionTable
