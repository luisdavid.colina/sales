import { useEffect, useState, useCallback, useMemo, FC } from 'react'
import axios from 'axios'
import Table from 'components/Table'
import { GridColDef } from '@material-ui/data-grid'
import { ROLES_PT, MODULE_PERMISSIONS, ADMINS } from '@/constants'
import User, { CompanyPermission, Permissions } from '@/types/User'
import useBoolean from '@/hooks/useBoolean'
import getColumns from './AccountsTable.const'

export type Account = User & {
  id: string
  roleName: string
  businessRules: boolean
  communications: boolean
  profiles: boolean
}

type AccountsTableProps = {
  onChange: (users: Account[]) => void
  disabled?: boolean
}

const hasPermissions = (
  { permissions, role }: User,
  module: CompanyPermission,
): boolean =>
  MODULE_PERMISSIONS.every(
    (permission) =>
      (permissions &&
        permissions[module] &&
        (permissions[module] as Permissions)[permission]) ||
      (role && ADMINS.includes(role)) ||
      false,
  )

const AccountsTable: FC<AccountsTableProps> = ({ onChange, disabled }) => {
  const [users, setUsers] = useState<Account[]>([])
  const [error, setError] = useState<string | undefined>()
  const {
    value: loading,
    toFalse: isNotLoading,
    toTrue: isLoading,
  } = useBoolean({
    initialValue: false,
  })

  const cleanUsers = useCallback(
    (users: User[]) =>
      users.map(
        (user) =>
          ({
            ...user,
            id: user.uid,
            roleName: (user.role && ROLES_PT[user.role]) || '-',
            businessRules: hasPermissions(user, 'businessRules'),
            communications: hasPermissions(user, 'communications'),
            profiles: hasPermissions(user, 'profiles'),
          } as Account),
      ),
    [],
  )

  useEffect(() => {
    const getUsers = async () => {
      try {
        isLoading()
        const res = await axios.get('/api/accounts')
        setUsers(cleanUsers(res.data as User[]))
      } catch (err) {
        // eslint-disable-next-line no-console
        console.error(err)
        setError('Erro do servidor')
      } finally {
        isNotLoading()
      }
    }
    getUsers()
  }, [])

  useEffect(() => onChange([...users]), [users])

  const onClick = useCallback(
    (column: CompanyPermission, id: string) => () => {
      setUsers((users) => {
        const usersCopy = users.map((user) => ({ ...user }))
        const currentUserIndex = users.findIndex((user) => user.uid === id)
        if (currentUserIndex !== -1) {
          usersCopy[currentUserIndex][column] = !usersCopy[currentUserIndex][
            column
          ]
        }
        return usersCopy
      })
    },
    [],
  )

  const columns = useMemo<GridColDef[]>(
    () => getColumns(onClick, disabled || false),
    [],
  )

  return (
    <Table columns={columns} rows={users} loading={loading} error={error} />
  )
}

AccountsTable.defaultProps = {
  disabled: false,
}

export default AccountsTable
