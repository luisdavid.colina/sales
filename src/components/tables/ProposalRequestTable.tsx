import { GridValueFormatterParams } from '@material-ui/data-grid'
import Table, { Props as TableProps } from 'components/Table'
import ProposalRequest from '@/types/ProposalRequest'

import { FC } from 'react'

const columns = [
  {
    field: 'contactFullName',
    headerName: 'Nome Completo',
    flex: 1,
  },
  {
    field: 'companyRole',
    headerName: 'Cargo',
    flex: 1,
  },
  {
    field: 'companyArea',
    headerName: 'Área',
    flex: 1,
  },
  {
    field: 'companyLegalName',
    headerName: 'Razão Social',
    flex: 1,
  },
  {
    field: 'cnpj',
    headerName: 'CNPJ',
    flex: 1,
  },
  {
    field: 'email',
    headerName: 'Email',
    flex: 1,
  },
  {
    field: 'mobile',
    headerName: 'Celular',
    flex: 1,
  },
  {
    field: 'phone',
    headerName: 'Telefone',
    flex: 1,
  },
  {
    field: 'createdAt',
    headerName: 'Data',
    valueFormatter: (params: GridValueFormatterParams) => {
      const value = params.value as string
      return value
    },
    width: 0,
  },
]

type Props = {
  proposal: ProposalRequest[]
} & Omit<TableProps, 'rows' | 'columns' | 'components'>

const ProposalRequestTable: FC<Props> = ({ proposal, ...others }) => {
  return (
    <Table
      columns={columns}
      rows={proposal}
      sortModel={[{ field: 'createdAt', sort: 'desc' }]}
      {...others}
    />
  )
}

export default ProposalRequestTable
