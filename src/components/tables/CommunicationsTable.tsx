import Link from 'next/link'
import { GridValueFormatterParams } from '@material-ui/data-grid'
import Table, { Props as TableProps } from 'components/Table'
import Communication from '@/types/Communication'
import CustomNoRowsOverlay from './CustomNoRowsOverlay'

function NoRowsOverlay() {
  return <CustomNoRowsOverlay text="Sem comunicações registradas" />
}

const timeStampToDate = (timeStamp: number) => {
  const date = new Date(timeStamp)
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  return `${day}/${month}/${year}`
}

const columns = [
  {
    field: 'to',
    headerName: 'Destinatario',
    valueFormatter: (params: GridValueFormatterParams) => {
      const value = params.value as string[]
      return value.join(', ')
    },
    flex: 1,
  },
  {
    field: 'subject',
    headerName: 'Assunto',
    flex: 2,
  },
  {
    field: 'text',
    headerName: 'Texto',
    flex: 1,
  },
  {
    field: 'sendingMethod',
    headerName: 'Meio de Envio',
    valueFormatter: (params: GridValueFormatterParams) => {
      const value = params.value as string[]
      return value.join(', ')
    },
    flex: 1,
  },
  {
    field: 'createdAt',
    headerName: 'Data',
    valueFormatter: (params: GridValueFormatterParams) => {
      const value = params.value as number
      return timeStampToDate(value)
    },
    flex: 1,
  },
  {
    field: 'object',
    headerName: 'Arquivo',
    renderCell: (params: GridValueFormatterParams) => {
      const { value } = params
      return (
        <Link href={String(value)}>
          {String(value).replaceAll(
            'https://element-sales-ecosystem-nfs.s3.amazonaws.com/',
            '',
          )}
        </Link>
      )
    },
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
  },
]

type Props = {
  communications: Communication[]
} & Omit<TableProps, 'rows' | 'columns' | 'components'>

function CommunicationsTable({ communications, ...others }: Props) {
  return (
    <Table
      columns={columns}
      rows={communications}
      components={{ NoRowsOverlay }}
      {...others}
    />
  )
}

export default CommunicationsTable
