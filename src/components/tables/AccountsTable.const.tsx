import { GridCellParams } from '@material-ui/data-grid'
import Checkbox from '@material-ui/core/Checkbox'
import { CompanyPermission } from '@/types/User'
import { EXPLORER_ROLE } from '@/constants'

type onClickCheckbox = (column: CompanyPermission, id: string) => () => void

const columnCheckbox = (
  column: CompanyPermission,
  onClick: onClickCheckbox,
) => ({ value, row }: GridCellParams) => (
  <Checkbox
    checked={Boolean(value)}
    disabled={row.role !== EXPLORER_ROLE}
    onChange={onClick(column, row.id)}
    inputProps={{ 'aria-label': 'primary checkbox' }}
  />
)

const getColumns = (onClick: onClickCheckbox, disabled: boolean) => [
  {
    field: 'fullName',
    headerName: 'Nome',
    flex: 1,
  },
  {
    field: 'cpf',
    headerName: 'CPF',
    flex: 1,
  },
  {
    field: 'email',
    headerName: 'E-mail',
    flex: 1,
  },
  {
    field: 'roleName',
    headerName: 'Papel',
    flex: 1,
  },
  {
    field: 'profileGruop',
    headerName: 'Grupo',
    flex: 1,
  },
  {
    field: 'businessRules',
    headerName: 'RN',
    type: 'boolean',
    width: 60,
    ...(disabled
      ? {}
      : { renderCell: columnCheckbox('businessRules', onClick) }),
  },
  {
    field: 'communications',
    headerName: 'CO',
    type: 'boolean',
    width: 60,
    ...(disabled
      ? {}
      : { renderCell: columnCheckbox('communications', onClick) }),
  },
  {
    field: 'profiles',
    headerName: 'PE',
    type: 'boolean',
    width: 60,
    ...(disabled ? {} : { renderCell: columnCheckbox('profiles', onClick) }),
  },
]

export default getColumns
