/* eslint-disable no-shadow */
import { FC } from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core'
import { DataGrid, GridColDef } from '@material-ui/data-grid'

import { dataGridLocalizations } from '@/util/localizations'
import CustomNoRowsOverlay from './CustomNoRowsOverlay'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    footer: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: theme.spacing(2),
      width: '100%',
      '& > *': {
        marginLeft: theme.spacing(2),
      },
    },
    root: {
      border: '1px solid #eff1f3',
      background: 'white',
      color:
        theme.palette.type === 'light'
          ? 'rgba(0,0,0,.85)'
          : 'rgba(255,255,255,0.85)',
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      WebkitFontSmoothing: 'auto',
      letterSpacing: 'normal',
      '& .MuiDataGrid-columnsContainer': {
        backgroundColor: theme.palette.type === 'light' ? '#fafafa' : '#1d1d1d',
      },
      '& .MuiDataGrid-iconSeparator': {
        display: 'none',
      },
      '& .MuiDataGrid-colCell, .MuiDataGrid-cell': {
        borderRight: `1px solid ${
          theme.palette.type === 'light' ? '#f0f0f0' : '#303030'
        }`,
      },
      '& .MuiDataGrid-columnsContainer, .MuiDataGrid-cell': {
        borderBottom: `1px solid ${
          theme.palette.type === 'light' ? '#f0f0f0' : '#303030'
        }`,
      },
      '& .MuiDataGrid-cell': {
        color:
          theme.palette.type === 'light'
            ? 'rgba(0,0,0,.85)'
            : 'rgba(255,255,255,0.65)',
      },
      '& .MuiPaginationItem-root': {
        borderRadius: 0,
      },
      '& .MuiDataGrid-cellEditing': {
        backgroundColor: 'rgb(255,215,115, 0.19)',
        color: '#1a3e72',
      },
      '& .Mui-error': {
        backgroundColor: `rgb(126,10,15, 0.1)`,
        color: '#750f0f !important',
      },
    },
  }),
)

interface SimulatorResulsTableProps {
  rows: any[]
  columns: GridColDef[]
}

const SimulatorResultsTable: FC<SimulatorResulsTableProps> = ({
  rows,
  columns,
}) => {
  const classes = useStyles()

  return (
    <div style={{ height: 320, width: '100%', background: 'white' }}>
      <div style={{ display: 'flex', height: '100%' }}>
        <DataGrid
          className={classes.root}
          columns={columns}
          rows={rows}
          columnBuffer={2}
          localeText={dataGridLocalizations}
          components={{
            Pagination: () => null,
            NoRowsOverlay: CustomNoRowsOverlay,
          }}
        />
      </div>
    </div>
  )
}

export default SimulatorResultsTable
