import { useEffect, useState, useCallback, useMemo, FC } from 'react'
import axios from 'axios'
import Table from 'components/Table'
import {
  GridRowId,
  GridColDef,
  GridSelectionModelChangeParams,
} from '@material-ui/data-grid'
import User, { CompanyPermission, Permissions } from '@/types/User'
import useBoolean from '@/hooks/useBoolean'
import DeliveryAddress from '@/types/DeliveryAddress'
import Contact from '@/types/Contact'
import SpinnerPage from '@/components/SpinnerPage'
import getColumns from './ContactsTable.const'

type ContactsTableProps = {
  disabled?: boolean
  tos: (to: string[]) => void
}

const ContactsTable: FC<ContactsTableProps> = ({ disabled, tos }) => {
  const [users, setUsers] = useState<Contact[]>([])
  const [error, setError] = useState<string | undefined>()
  const [loading2, setLoading2] = useState(false)
  const {
    value: loading,
    toFalse: isNotLoading,
    toTrue: isLoading,
  } = useBoolean({
    initialValue: false,
  })

  const [selectedFixed, setSelectedFixed] = useState<GridRowId[]>([])

  const [to, setTo] = useState<string[]>([])

  const selectFixed = async (param: GridSelectionModelChangeParams) => {
    setSelectedFixed(param.selectionModel)
    const emails: string[] = ['']
    emails.pop()
    for (let i = 0; i < users.length; i += 1) {
      if (
        param.selectionModel.find((ele) => users[i].id === ele) !== undefined
      ) {
        emails.push(users[i].email)
      }
    }
    setTo(emails)
  }

  useEffect(() => {
    isLoading()
    setLoading2(true)
    axios
      .get(`/api/contacts`)
      .then((response) => {
        setUsers(response.data)
        isNotLoading()
      })
      .catch((err) => {
        setError(`Erro do servidor: ${err.message}`)
      })
      .finally(() => {
        isNotLoading()
        setLoading2(false)
      })
  }, [])

  useEffect(() => tos([...to]), [to])

  const onClick = useCallback(
    (column: CompanyPermission, id: string) => () => {
      /*
      setUsers((users) => {
        const usersCopy = users.map((user) => ({ ...user }))
        const currentUserIndex = users.findIndex((user) => user.uid === id)
        if (currentUserIndex !== -1) {
          usersCopy[currentUserIndex][column] = !usersCopy[currentUserIndex][
            column
          ]
        }
        return usersCopy
      })
    */
    },
    [],
  )

  const columns = useMemo<GridColDef[]>(
    () => getColumns(onClick, disabled || false),
    [],
  )
  if (loading2) return <SpinnerPage />

  return (
    <Table
      columns={columns}
      rows={users}
      loading={loading}
      error={error}
      onSelectionModelChange={selectFixed}
      selectionModel={selectedFixed}
      checkboxSelection
    />
  )
}

ContactsTable.defaultProps = {
  disabled: false,
}

export default ContactsTable
