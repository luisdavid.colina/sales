import { CARD_BRANDS, PRODUCTS } from '@/constants'
import Company from '@/types/Company'
import { Rule } from '@/types/Product'
import ProductDetails from '@/types/ProductDetails'
import { GridValueGetterParams } from '@material-ui/data-grid'

export function getCompanyName(params: GridValueGetterParams) {
  const company = params.getValue('company') as Company
  return company.legalName || ''
}

export function getCnpj(params: GridValueGetterParams) {
  const company = params.getValue('company') as Company
  return company.cnpj || ''
}

export function getCpf(params: GridValueGetterParams) {
  const company = params.getValue('company') as Company
  return company.admin.cpf || ''
}

export function getId(params: GridValueGetterParams) {
  const id = params.getValue('id')
  return String(id).substr(0, 5) || ''
}

export function getOwner(params: GridValueGetterParams) {
  const company = params.getValue('company') as Company
  return company.admin.fullName || ''
}

export function getTradingName(params: GridValueGetterParams) {
  const company = params.getValue('company') as Company
  return company.tradingName || ''
}

export function getCommercialAgent(params: GridValueGetterParams) {
  const productDetails = params.getValue('productDetails') as ProductDetails
  return productDetails.commercialAgent.company.legalName || ''
}

export function getProductName(params: GridValueGetterParams) {
  const productDetails = params.getValue('productDetails') as ProductDetails

  const product =
    PRODUCTS.find((p) => p.value === productDetails.product)?.label || ''

  const brand =
    CARD_BRANDS.find(
      (productBrand) => productBrand.value === productDetails.productBrand,
    )?.label || ''

  return `${product} ${brand}`
}

export function getProductSalesChannel(params: GridValueGetterParams) {
  const rule = params.getValue('rule') as Rule

  return rule?.salesChannel?.name || ''
}

export function getAdministrationFee(params: GridValueGetterParams) {
  const from = params.getValue('from')
  const to = params.getValue('to')
  return `${from} - ${to}`
}
