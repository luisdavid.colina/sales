import Product from '@/types/Product'
import Table from 'components/Table'
import { format } from 'date-fns'

import { FC } from 'react'
import { getProductSalesChannel } from './getters'

const columns = [
  {
    field: 'name',
    headerName: 'Produto',
    flex: 1,
  },
  {
    field: 'productFamily',
    headerName: 'Familia',
    valueFormatter: (params: any) => params.value?.name || '',
    flex: 1,
  },
  {
    field: 'productCategory',
    headerName: 'Categoria',
    valueFormatter: (params: any) => params.value?.name || '',
    flex: 1,
  },
  {
    field: 'salesChannel',
    headerName: 'Canal',
    valueGetter: getProductSalesChannel,
    flex: 1,
  },
  {
    field: 'rule',
    headerName: 'Regra Do Produto',
    valueFormatter: (params: any) => params.value?.name || '',
    flex: 1,
  },
]

interface Props {
  products: Product[]
}

const ProductsTable: FC<Props> = ({ products }) => {
  return <Table columns={columns} rows={products} />
}

export default ProductsTable
