/* eslint-disable no-shadow */
import { FC, useCallback, useState } from 'react'
import {
  Button,
  createStyles,
  Grid,
  makeStyles,
  Theme,
  Typography,
} from '@material-ui/core'
import {
  DataGrid,
  GridEditRowsModel,
  GridEditCellPropsParams,
  GridColDef,
} from '@material-ui/data-grid'
import SettingsIcon from '@material-ui/icons/Settings'

import { useStepperContext } from '@/context/StepperContext'

import { dataGridLocalizations } from '@/util/localizations'
import CustomNoRowsOverlay from './CustomNoRowsOverlay'
import SimulatorModal from '../modals/SimulatorModal'
import SuccessModal from '../SuccessModal'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    footer: {
      display: 'flex',
      justifyContent: 'flex-end',
      padding: theme.spacing(4),
      background: '#f0f0f0',
      width: '100%',
      '& > *': {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
      },
    },
    root: {
      border: '1px solid #eff1f3',
      background: 'white',
      color:
        theme.palette.type === 'light'
          ? 'rgba(0,0,0,.85)'
          : 'rgba(255,255,255,0.85)',
      fontFamily: [
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
      ].join(','),
      WebkitFontSmoothing: 'auto',
      letterSpacing: 'normal',
      '& .MuiDataGrid-columnsContainer': {
        backgroundColor: theme.palette.type === 'light' ? '#fafafa' : '#1d1d1d',
      },
      '& .MuiDataGrid-iconSeparator': {
        display: 'none',
      },
      '& .MuiDataGrid-colCell, .MuiDataGrid-cell': {
        borderRight: `1px solid ${
          theme.palette.type === 'light' ? '#f0f0f0' : '#303030'
        }`,
      },
      '& .MuiDataGrid-columnsContainer, .MuiDataGrid-cell': {
        borderBottom: `1px solid ${
          theme.palette.type === 'light' ? '#f0f0f0' : '#303030'
        }`,
      },
      '& .MuiDataGrid-cell': {
        color:
          theme.palette.type === 'light'
            ? 'rgba(0,0,0,.85)'
            : 'rgba(255,255,255,0.65)',
      },
      '& .MuiPaginationItem-root': {
        borderRadius: 0,
      },
      '& .MuiDataGrid-cellEditing': {
        backgroundColor: 'rgb(255,215,115, 0.19)',
        color: '#1a3e72',
      },
      '& .Mui-error': {
        backgroundColor: `rgb(126,10,15, 0.1)`,
        color: '#750f0f !important',
      },
    },
  }),
)

const validateNumber = (value: any) => {
  return /^\d+(\.\d+)?$/.test(value)
}

interface Props {
  columns: GridColDef[]
  rows: any[]
  data: any
}

const CommissionsTable: FC<Props> = ({ rows, columns, data }) => {
  const { prev, actionModal, titleModal } = useStepperContext()
  const classes = useStyles()
  const [gridRows, setGridRows] = useState<any[]>(rows)
  const [editRowsModel, setEditRowsModel] = useState<GridEditRowsModel>({})

  const [open, setOpen] = useState(false)
  const [success, setSuccess] = useState(false)

  const openSimulator = () => {
    setOpen(true)
  }

  const closeSimulator = () => {
    setOpen(false)
  }

  const showSuccess = () => {
    setSuccess(true)
  }

  const closeSuccess = () => {
    actionModal()
  }

  const handleEditCellChange = useCallback(
    ({ id, field, props }: GridEditCellPropsParams) => {
      if (field.includes('commission')) {
        const data = props
        const isValid = validateNumber(data.value)

        const newState: GridEditRowsModel = {}
        newState[id] = {
          ...editRowsModel[id],
          [field]: { ...props, value: data.value, error: !isValid },
        }

        setEditRowsModel((state) => ({ ...state, ...newState }))
      }
    },
    [editRowsModel],
  )

  const handleEditCellChangeCommitted = useCallback(
    ({ id, field, props }: GridEditCellPropsParams) => {
      if (field.includes('commission')) {
        const data = props

        if (validateNumber(data.value)) {
          const updatedRows = gridRows.map((row) => {
            if (row.id === id) {
              return { ...row, [field]: Number(data.value) }
            }
            return row
          })

          setGridRows(updatedRows)
        }
      }
    },
    [gridRows],
  )

  return (
    <div>
      <Grid
        container
        direction="row"
        justify="space-between"
        alignItems="center"
        style={{ padding: 16 }}
      >
        <Typography variant="h5" component="h3" gutterBottom>
          Tabela de Comissão
        </Typography>
      </Grid>
      <div style={{ height: 350, width: '100%', background: 'white' }}>
        <div style={{ display: 'flex', height: '100%' }}>
          <DataGrid
            className={classes.root}
            columns={columns}
            rows={gridRows}
            columnBuffer={2}
            editRowsModel={editRowsModel}
            onEditCellChange={handleEditCellChange}
            onEditCellChangeCommitted={handleEditCellChangeCommitted}
            localeText={dataGridLocalizations}
            components={{
              Pagination: () => null,
              NoRowsOverlay: CustomNoRowsOverlay,
            }}
          />
        </div>
      </div>

      <div className={classes.footer}>
        <Button type="button" onClick={prev} variant="contained">
          Retornar
        </Button>
        <Button
          onClick={openSimulator}
          variant="contained"
          color="primary"
          endIcon={<SettingsIcon />}
        >
          Simulador
        </Button>
      </div>
      {open && (
        <SimulatorModal
          open={open}
          rows={gridRows}
          columns={columns}
          onClose={closeSimulator}
          onSuccess={showSuccess}
          data={data}
        />
      )}
      <SuccessModal title={titleModal} open={success} onClose={closeSuccess} />
    </div>
  )
}

export default CommissionsTable
