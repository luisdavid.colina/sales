import { FC } from 'react'
import Table, { Props as TableProps } from 'components/Table'
import CommercialAgent from '@/types/CommercialAgent'
import CustomNoRowsOverlay from './CustomNoRowsOverlay'
import {
  getCnpj,
  getCompanyName,
  getTradingName,
  getCpf,
  getId,
} from './getters'

function NoRowsOverlay() {
  return <CustomNoRowsOverlay text="No hay contratos registrados" />
}

const columns = [
  {
    field: 'companyName',
    headerName: 'Razão Social',
    valueGetter: getCompanyName,
    flex: 1,
  },
  {
    field: 'cnpj',
    headerName: 'CNPJ',
    valueGetter: getCnpj,
    flex: 1,
  },
  {
    field: 'tradingName',
    headerName: 'Nome',
    valueGetter: getTradingName,
    flex: 1,
  },
  {
    field: 'cpf',
    headerName: 'CPF',
    valueGetter: getCpf,
    flex: 1,
  },
  {
    field: 'commercialAgentId',
    headerName: 'Código',
    valueGetter: getId,
    flex: 1,
  },
]

type CommercialAgentsTableProps = {
  commercialAgents: CommercialAgent[]
} & Omit<TableProps, 'rows' | 'columns' | 'components'>

const CommercialAgentsTable: FC<CommercialAgentsTableProps> = ({
  commercialAgents,
  ...others
}) => {
  return (
    <Table
      columns={columns}
      rows={commercialAgents}
      components={{ NoRowsOverlay }}
      {...others}
    />
  )
}

export default CommercialAgentsTable
