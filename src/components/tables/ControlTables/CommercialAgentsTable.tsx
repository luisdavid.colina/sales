import Table from 'components/Table'

const columns = [
  {
    field: 'invoice_owner_name',
    headerName: 'Razão Social',
    flex: 1,
  },
  {
    field: 'cnpj',
    headerName: 'CNPJ',
    flex: 1,
  },
  {
    field: 'number',
    headerName: 'Numero',
    flex: 1,
  },
  {
    field: 'send_data',
    headerName: ' Data de Envio',
    flex: 1,
  },
  {
    field: 'payment_data',
    headerName: 'Data de Pagamento',
    flex: 1,
  },
  {
    field: 'value',
    headerName: 'Valor',
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
  },
]

const rows = [
  {
    id: 1,
    invoice_owner_name: 'Master',
    cnpj: '0000000000',
    number: '',
    send_data: '06/04/2020',
    payment_data: '',
    value: '',
    status: 'Ativo',
  },
  {
    id: 2,
    invoice_owner_name: 'Master',
    cnpj: '0000000000',
    number: '',
    send_data: '06/04/2020',
    payment_data: '',
    value: '',
    status: 'Ativo',
  },
]

function CommercialAgentsTable() {
  return <Table columns={columns} rows={rows} />
}

export default CommercialAgentsTable
