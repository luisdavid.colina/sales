import Table from 'components/Table'

const columns = [
  {
    field: 'user',
    headerName: 'Usuario',
    flex: 1,
  },
  {
    field: 'product_type',
    headerName: 'Produto',
    flex: 1,
  },
  {
    field: 'payment',
    headerName: 'Pagamento',
    flex: 1,
  },
  {
    field: 'invoice',
    headerName: 'Faturado',
    flex: 1,
  },
  {
    field: 'cpf',
    headerName: 'CPF',
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
  },
]

const rows = [
  {
    id: 1,
    user: '',
    product_type: '',
    payment: '',
    invoice: '',
    cpf: '',
    status: '',
  },
  {
    id: 2,
    user: '',
    product_type: '',
    payment: '',
    invoice: '',
    cpf: '',
    status: '',
  },
]

function PaymentsReleasesTable() {
  return <Table columns={columns} rows={rows} />
}

export default PaymentsReleasesTable
