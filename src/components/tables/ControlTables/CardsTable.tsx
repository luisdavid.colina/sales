import Table from 'components/Table'

const columns = [
  {
    field: 'banner',
    headerName: 'Bandeira',
    flex: 1,
  },
  {
    field: 'proxy',
    headerName: 'Proxy',
    flex: 1,
  },
  {
    field: 'number',
    headerName: 'Numero',
    flex: 1,
  },
  {
    field: 'validity',
    headerName: ' Validade',
    flex: 1,
  },
  {
    field: 'bearer',
    headerName: 'Portador',
    flex: 1,
  },
  {
    field: 'cpf_cnpj',
    headerName: 'CPF/CNPJ',
    flex: 1,
  },
  {
    field: 'order',
    headerName: 'Pedido',
    flex: 1,
  },
  {
    field: 'invoice',
    headerName: 'Faturado',
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
  },
]

const rows = [
  {
    id: 1,
    banner: 'Master',
    proxy: '0000000000',
    number: '00000000000',
    validity: '06/04/2020',
    bearer: 'Pedro',
    cpf_cnpj: '00000000',
    order: '',
    invoice: 'Martin',
    status: 'Ativo',
  },
  {
    id: 2,
    banner: 'Master',
    proxy: '0000000000',
    number: '00000000000',
    validity: '06/04/2020',
    bearer: 'Pedro',
    cpf_cnpj: '00000000',
    order: '',
    invoice: 'Martin',
    status: 'Ativo',
  },
]

function CardsTable() {
  return <Table columns={columns} rows={rows} />
}

export default CardsTable
