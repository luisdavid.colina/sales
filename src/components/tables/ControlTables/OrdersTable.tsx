import Table from 'components/Table'

const columns = [
  {
    field: 'nf',
    headerName: 'N° da NF/ND',
    flex: 1,
  },
  {
    field: 'product',
    headerName: 'Produto',
    flex: 1,
  },
  {
    field: 'name',
    headerName: 'Nome',
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
  },
]

const rows = [
  {
    id: 1,
    nf: '',
    product: '',
    name: '',
    status: 'Criado',
  },
  {
    id: 2,
    nf: '',
    product: '',
    name: '',
    status: 'Criado',
  },
]

function OrdersTable() {
  return <Table columns={columns} rows={rows} />
}

export default OrdersTable
