import Table from 'components/Table'

const columns = [
  {
    field: 'emission',
    headerName: 'Emissão',
    flex: 1,
  },
  {
    field: 'city',
    headerName: 'Cidade',
    flex: 1,
  },
  {
    field: 'number',
    headerName: 'Numero da Nota',
    flex: 1,
  },
  {
    field: 'order',
    headerName: ' Pedido',
    flex: 1,
  },
  {
    field: 'billed',
    headerName: 'Faturado',
    flex: 1,
  },
  {
    field: 'cpf_cnpj',
    headerName: 'CPF/CNPJ',
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
  },
]

const rows = [
  {
    id: 1,
    emission: 'Master',
    city: 'cidade',
    number: '00000000000',
    order: '06/04/2020',
    billed: 'Pedro',
    cpf_cnpj: '00000000',
    status: 'Ativo',
  },
  {
    id: 2,
    emission: 'Master',
    city: 'cidade',
    number: '00000000000',
    order: '06/04/2020',
    billed: 'Pedro',
    cpf_cnpj: '00000000',
    status: 'Ativo',
  },
]

function DebitNotesTable() {
  return <Table columns={columns} rows={rows} />
}

export default DebitNotesTable
