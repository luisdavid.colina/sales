import Table from 'components/Table'

const columns = [
  {
    field: 'cnpj',
    headerName: 'CNPJ',
    flex: 1,
  },
  {
    field: 'name',
    headerName: 'Nome',
    flex: 1,
  },
  {
    field: 'target',
    headerName: 'Dados Bancários',
    flex: 2,
  },
  {
    field: 'order_number',
    headerName: 'NF/ND',
    flex: 1,
  },
  {
    field: 'order',
    headerName: 'Pedido',
    flex: 1,
  },
  {
    field: 'processing',
    headerName: 'Processamento',
    flex: 2,
  },
  {
    field: 'conciliation',
    headerName: 'Conciliação',
    flex: 2,
  },
  {
    field: 'units',
    headerName: 'QTD',
    flex: 1,
  },
  {
    field: 'value',
    headerName: 'Valor',
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
  },
]

const rows = [
  {
    id: 1,
    user: '',
    product_type: '',
    payment: '',
    invoice: '',
    cpf: '',
    status: '',
  },
  {
    id: 2,
    user: '',
    product_type: '',
    payment: '',
    invoice: '',
    cpf: '',
    status: '',
  },
]

function TransfersTable() {
  return <Table columns={columns} rows={rows} />
}

export default TransfersTable
