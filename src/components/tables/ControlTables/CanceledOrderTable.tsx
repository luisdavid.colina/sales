import Table from 'components/Table'

const columns = [
  {
    field: 'order_number',
    headerName: 'N° do Pedido',
    flex: 1,
  },
  {
    field: 'invoice_number',
    headerName: 'NF',
    flex: 1,
  },
  {
    field: 'debit_notes',
    headerName: 'ND',
    flex: 1,
  },
  {
    field: 'cpf',
    headerName: 'CPF',
    flex: 1,
  },
  {
    field: 'proxy',
    headerName: 'Proxy ou Conta',
    flex: 1,
  },
  {
    field: 'value',
    headerName: 'Valor',
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
  },
]

const rows = [
  {
    id: 1,
    order_number: '',
    invoice_number: '',
    debit_notes: '',
    cpf: '',
    proxy: '',
    value: '',
    status: '',
  },
  {
    id: 2,
    order_number: '',
    invoice_number: '',
    debit_notes: '',
    cpf: '',
    proxy: '',
    value: '',
    status: '',
  },
]

function CanceledOrderTable() {
  return <Table columns={columns} rows={rows} />
}

export default CanceledOrderTable
