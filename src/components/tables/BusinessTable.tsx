import Table, { Props as TableProps } from 'components/Table'
import Business from '@/types/Business'

const columns = [
  {
    field: 'invoice_owner_name',
    headerName: 'Razão Social',
    flex: 1,
  },
  {
    field: 'invoice_number',
    headerName: 'N°NF',
    flex: 1,
  },
  {
    field: 'invoice_issue_date',
    headerName: 'Emissão NF',
    flex: 1,
  },
  {
    field: 'invoice_payment_date',
    headerName: 'Pagamento NF',
    flex: 1,
  },
  {
    field: 'invoice_price',
    headerName: 'Valor NF',
    flex: 1,
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
  },
]

type Props = {
  business: Business[]
} & Omit<TableProps, 'rows' | 'columns' | 'components'>

function BusinessTable({ business, ...others }: Props) {
  return <Table columns={columns} rows={business} {...others} />
}

export default BusinessTable
