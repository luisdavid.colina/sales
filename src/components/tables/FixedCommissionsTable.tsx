import { Commission } from '@/types/SalesCommission'
import { GridValueFormatterParams } from '@material-ui/data-grid'
import Table from 'components/Table'
import CustomNoRowsOverlay from './CustomNoRowsOverlay'
import { getAdministrationFee } from './getters'

function NoRowsOverlay() {
  return <CustomNoRowsOverlay text="No hay contratos registrados" />
}

const columns = [
  {
    field: 'administrationFee',
    headerName: 'Taxa ADM (R$)',
    valueGetter: getAdministrationFee,
    width: 140,
  },
  { field: 'salesChannel', headerName: 'Tipo de Venda', flex: 1 },
  {
    field: 'loads',
    headerName: 'Commissão s/ Cargas',
    flex: 1,
    valueFormatter: (params: GridValueFormatterParams) => `${params.value} %`,
  },
]

interface Props {
  commissions: Commission[]
}

function FixedCommissionsTable({ commissions }: Props) {
  const rows = commissions.map((commission, i) => ({
    ...commission,
    id: i,
  }))

  return (
    <Table
      height={280}
      rows={rows}
      columns={columns}
      components={{
        NoRowsOverlay,
        Pagination: () => null,
        Toolbar: () => null,
      }}
    />
  )
}

export default FixedCommissionsTable
