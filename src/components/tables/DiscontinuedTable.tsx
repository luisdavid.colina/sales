import Table, { Props as TableProps } from 'components/Table'

const columns = [
  {
    field: 'code',
    headerName: 'Codigo',
    flex: 1,
  },
  {
    field: 'product',
    headerName: 'Produto',
    flex: 1,
  },
  {
    field: 'family',
    headerName: 'Familia',
    flex: 1,
  },
  {
    field: 'category',
    headerName: 'Categoria',
    flex: 1,
  },
  {
    field: 'channel',
    headerName: 'Canal',
    flex: 1,
  },
  {
    field: 'validity',
    headerName: 'Vigência',
    flex: 1,
  },
]

type Props = {
  products: any[]
} & Omit<TableProps, 'rows' | 'columns' | 'components'>

function DiscontinuedTable({ products, ...props }: Props) {
  return <Table columns={columns} rows={products} {...props} />
}

export default DiscontinuedTable
