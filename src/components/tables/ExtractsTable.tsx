import React, { useState, useMemo } from 'react'

import Table, { Props as TableProps } from 'components/Table'
import Extract from '@/types/Extract'
import { format, subDays } from 'date-fns'

// import { GridFilterModelParams } from '@material-ui/data-grid'

import Cycle from '@/components/common/Cycle'

const columns = [
  {
    field: 'createdAt',
    headerName: 'Data',
    flex: 1,
    type: 'date',
  },
  {
    field: 'companyName',
    headerName: 'Razão Social',
    flex: 1,
    type: 'string',
  },
  {
    field: 'description',
    headerName: 'Descrição',
    flex: 1,
    type: 'string',
  },
  {
    field: 'nature',
    headerName: 'Natureza',
    flex: 1,
    type: 'string',
  },
  {
    field: 'price',
    headerName: 'Valor',
    flex: 1,
    type: 'string',
  },
  {
    field: 'status',
    headerName: 'Status',
    flex: 1,
    type: 'string',
  },
]

const options = [
  {
    value: 15,
    label: 'Fechado',
  },
  {
    value: 30,
    label: 'Ciclo Anterior',
  },
  {
    value: 365,
    label: 'Ano',
  },
]

type Props = {
  extracts: Extract[]
} & Omit<TableProps, 'rows' | 'columns'>

type datePickerType = {
  firstDate?: Date
  lastDate?: Date
}

// type operatorsType = {
//   firstDate: string
//   lastDate: string
// }

// const operatorForKey: operatorsType = {
//   firstDate: 'onOrAfter',
//   lastDate: 'onOrBefore',
// }

function ExtracsTable({ extracts, ...others }: Props) {
  const [datePicker, setDatePicker] = useState<datePickerType>({
    firstDate: subDays(new Date(), 15),
    lastDate: new Date(),
  })

  const changeDate = (value: any) => {
    setDatePicker(value)
  }

  // Sincronizando filtros dataGrid
  // const handleFilter = useCallback(
  //   ({ filterModel: { items } }: GridFilterModelParams) => {
  //     const currentDatePicker = { ...datePicker }
  //     const newDatePicker: datePickerType = {}
  //     const keys = Object.keys(currentDatePicker) as Array<keyof operatorsType>
  //     let isChanched = false
  //     keys.forEach((f: keyof operatorsType) => {
  //       const itemFilter = items.find(
  //         (i) =>
  //           i.columnField === 'createdAt' &&
  //           i.operatorValue === operatorForKey[f] &&
  //           i.value === format(currentDatePicker[f] as Date, 'yyyy-MM-dd'),
  //       )
  //       if (itemFilter) {
  //         newDatePicker[f] = currentDatePicker[f]
  //       } else {
  //         isChanched = true
  //       }
  //     })
  //     if (isChanched) {
  //       setDatePicker(newDatePicker)
  //     }
  //   },
  //   [datePicker],
  // )

  const filter = useMemo(() => {
    const data = []

    if (datePicker.firstDate) {
      data.push({
        columnField: 'createdAt',
        operatorValue: 'onOrAfter',
        value: format(datePicker.firstDate, 'yyyy-MM-dd'),
      })
    }

    if (datePicker.lastDate) {
      data.push({
        columnField: 'createdAt',
        operatorValue: 'onOrBefore',
        value: format(datePicker.lastDate, 'yyyy-MM-dd'),
      })
    }

    return data
  }, [datePicker])

  return (
    <>
      <Cycle
        title=""
        options={options}
        value={datePicker}
        onChange={changeDate}
      />
      <Table
        columns={columns}
        rows={extracts}
        filterModel={{ items: filter }}
        // onFilterModelChange={handleFilter}
        {...others}
      />
    </>
  )
}

export default ExtracsTable
