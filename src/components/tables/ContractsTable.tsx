import Table, { Props as TableProps } from 'components/Table'
import Contract from '@/types/Contract'
import { GridValueFormatterParams } from '@material-ui/data-grid'
import { localize } from '@/util/localizations'
import CustomNoRowsOverlay from './CustomNoRowsOverlay'
import { getCnpj, getCompanyName, getTradingName } from './getters'

function NoRowsOverlay() {
  return <CustomNoRowsOverlay text="No hay contratos registrados" />
}

const columns = [
  {
    field: 'companyName',
    headerName: 'Razão Social',
    valueGetter: getCompanyName,
    flex: 1,
  },
  { field: 'cnpj', headerName: 'CNPJ', valueGetter: getCnpj, flex: 1 },
  {
    field: 'tradingName',
    headerName: 'Nome',
    valueGetter: getTradingName,
    flex: 1,
  },

  {
    field: 'd4signStatus',
    headerName: 'Status',
    valueFormatter: (params: GridValueFormatterParams) => {
      const value = params.value as string
      return localize(value)
    },
    type: 'string',
    width: 200,
  },
  {
    field: 'createdAt',
    headerName: 'Data',
    valueFormatter: (params: GridValueFormatterParams) => {
      const value = params.value as string
      return value
    },
    width: 0,
  },
]

type Props = {
  contracts: Contract[]
} & Omit<TableProps, 'rows' | 'columns' | 'components'>

function ContractsTable({ contracts, ...others }: Props) {
  return (
    <Table
      columns={columns}
      rows={contracts}
      components={{ NoRowsOverlay }}
      sortModel={[{ field: 'createdAt', sort: 'desc' }]}
      {...others}
    />
  )
}

export default ContractsTable
