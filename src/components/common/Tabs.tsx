import Tabs from '@material-ui/core/Tabs'
import { withStyles } from '@material-ui/core/styles'

const customTabs = withStyles(() => ({
  indicator: {
    height: '100%',
  },
}))(Tabs)

export default customTabs
