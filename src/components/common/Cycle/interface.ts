export interface OptionType {
  value: number
  label: string
}

export interface ValueType {
  firstDate?: Date
  lastDate?: Date
}

export interface CycleProps {
  title: string
  options: Array<OptionType>
  value: ValueType
  onChange: (value: ValueType) => void
}

export interface CycleMonthProps {
  title: string
  maxValue?: Date
  minValue?: Date
  value: ValueType
  onChange: (value: ValueType) => void
}

export interface CyclePickProps {
  maxValue?: Date
  minValue?: Date
  value: ValueType
  onChange: (value: ValueType) => void
}

export interface CycleStaticProps {
  options: Array<OptionType>
  value: ValueType
  onChange: (value: ValueType) => void
}
