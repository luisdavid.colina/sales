import { makeStyles, createStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme: any) =>
  createStyles({
    radioGroup: {
      flexDirection: 'row',
    },
    groupBox: {
      display: 'flex',
      alignItems: 'center',
      flexDirection: 'row',
      marginTop: theme.spacing(2),
    },
    formControlLabel: {
      marginRight: theme.spacing(4),
    },
    paper: {
      padding: theme.spacing(3),
    },
  }),
)
