import { useEffect, forwardRef, useCallback } from 'react'
import useBoolean from '@/hooks/useBoolean'
import Checkbox from '@material-ui/core/Checkbox'

type CheckBoxType = {
  name: string
  defaultValue?: boolean
  disabled?: boolean
  onChange: (e: any) => void
}
const CheckboxForm = forwardRef(
  (
    { name, onChange, disabled, defaultValue, ...others }: CheckBoxType,
    ref: React.Ref<HTMLInputElement>,
  ) => {
    const { value: isChecked, toggle: toggleChecked } = useBoolean({
      initialValue: defaultValue,
    })
    const handleChange = useCallback(() => toggleChecked(), [])

    useEffect(() => {
      onChange({ target: { name, value: isChecked } })
    }, [isChecked])

    return (
      <Checkbox
        name={name}
        checked={isChecked}
        inputProps={{
          ...others,
          onChange: handleChange,
        }}
        inputRef={ref}
        disabled={disabled}
      />
    )
  },
)

export default CheckboxForm
