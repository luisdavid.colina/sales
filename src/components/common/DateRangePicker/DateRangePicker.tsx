import React, { FC, useState } from 'react'

import IconButton from '@material-ui/core/IconButton'

import { DatePicker } from '@material-ui/pickers'
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date.d'

import clsx from 'clsx'

import { format, isSameDay, isWithinInterval } from 'date-fns'
import useStyles from './DateRangePicker.styles'

function makeJSDateObject(date: Date | null) {
  if (date instanceof Date) {
    return new Date(date.getFullYear(), date.getMonth(), date.getDate())
  }

  throw new Error('Cannot properly parse argument passed to cloneCrossUtils')
}

interface ValueType {
  firstDate?: Date
  lastDate?: Date
}

interface DatePickerRangerProps {
  value: ValueType
  onChange: (value: { firstDate?: Date; lastDate?: Date }) => void
  minDate?: Date
  maxDate?: Date
  [index: string]: any
}

const DatePickerRanger: FC<DatePickerRangerProps> = ({
  value,
  onChange,
  minDate,
  maxDate,
  ...others
}) => {
  const classes = useStyles()
  const [state, setState] = useState<{ lastChanged: string }>({
    lastChanged: 'last',
  })

  const getStateToChange = (date: Date) => {
    if (state.lastChanged === 'last') {
      return {
        value: { ...value, firstDate: makeJSDateObject(date) },
        ...state,
        lastChanged: 'first',
      }
    }
    return {
      value: { ...value, lastDate: makeJSDateObject(date) },
      ...state,
      lastChanged: 'last',
    }
  }

  const disabledDates = (date: Date) => {
    if (maxDate && date > makeJSDateObject(maxDate)) return false
    if (minDate && date < makeJSDateObject(minDate as Date)) return false
    return true
  }

  const handleWeekChange = (date: MaterialUiPickersDate) => {
    const { firstDate, lastDate } = value

    if (!firstDate && !lastDate) {
      setState((_state) => ({ ..._state, lastChanged: 'first' }))
      onChange({
        firstDate: makeJSDateObject(date),
        lastDate: makeJSDateObject(date),
      })
    } else if (
      !firstDate ||
      !lastDate ||
      (lastDate >= (date as Date) && firstDate <= (date as Date))
    ) {
      const { value: newValue, ...others } = getStateToChange(date as Date)
      setState(others)
      onChange(newValue)
    } else if (lastDate < (date as Date)) {
      setState((_state) => ({ ..._state, lastChanged: 'last' }))
      onChange({ ...value, lastDate: makeJSDateObject(date) })
    } else if (firstDate > (date as Date)) {
      setState((_state) => ({ ..._state, lastChanged: 'first' }))
      onChange({
        ...value,
        firstDate: makeJSDateObject(date),
      })
    }
  }

  const renderWrappedWeekDay = (
    date: MaterialUiPickersDate,
    selectedDate: MaterialUiPickersDate,
    dayInCurrentMonth: boolean,
  ) => {
    const dateClone = makeJSDateObject(date)

    let start = makeJSDateObject(selectedDate)
    let end = makeJSDateObject(selectedDate)

    let dayIsBetween = false
    let isFirstDay = false
    let isLastDay = false

    if (value && value.firstDate && value.lastDate) {
      start = makeJSDateObject(value.firstDate as Date)
      end = makeJSDateObject(value.lastDate as Date)

      dayIsBetween = isWithinInterval(dateClone, { start, end })
      isFirstDay = isSameDay(dateClone, start)
      isLastDay = isSameDay(dateClone, end)
    } else if (value && value.firstDate) {
      start = makeJSDateObject(value.firstDate as Date)

      isFirstDay = isSameDay(dateClone, start)
      isLastDay = isSameDay(dateClone, start)
      dayIsBetween = isWithinInterval(dateClone, { start, end })
    } else if (value && value.lastDate) {
      end = makeJSDateObject(value.lastDate as Date)

      isFirstDay = isSameDay(dateClone, end)
      isLastDay = isSameDay(dateClone, end)
      dayIsBetween = isWithinInterval(dateClone, { start, end })
    }

    const wrapperClassName = clsx({
      [classes.highlight]: dayIsBetween,
      [classes.firstHighlight]: isFirstDay,
      [classes.endHighlight]: isLastDay,
    })

    const dayClassName = clsx(classes.day, {
      [classes.nonCurrentMonthDay]: !dayInCurrentMonth,
      [classes.highlightNonCurrentMonthDay]: !dayInCurrentMonth && dayIsBetween,
    })

    return (
      <div className={wrapperClassName}>
        <IconButton
          className={dayClassName}
          disabled={!disabledDates(dateClone)}
        >
          <span> {format(dateClone, 'd')} </span>
        </IconButton>
      </div>
    )
  }

  return (
    <DatePicker
      {...others}
      value={state.lastChanged === 'first' ? value?.firstDate : value?.lastDate}
      onChange={handleWeekChange}
      renderDay={renderWrappedWeekDay}
      maxDate={maxDate}
      minDate={minDate}
    />
  )
}

export default DatePickerRanger
