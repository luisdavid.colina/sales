import Tab from '@material-ui/core/Tab'
import { withStyles } from '@material-ui/core/styles'

const customTab = withStyles((theme) => ({
  root: {
    color: theme.palette.primary.main,
    textTransform: 'initial',
  },
  selected: {
    color: `${theme.palette.primary.contrastText} !important`,
    backgroundColor: theme.palette.primary.main,
  },
  wrapper: {
    zIndex: 2,
  },
}))(Tab)

export default customTab
