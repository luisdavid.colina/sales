import { forwardRef } from 'react'

import OutlinedInput, {
  OutlinedInputProps,
} from '@material-ui/core/OutlinedInput'

import { useStylesTextOutlined } from './styles'

const TextFieldOutlined = forwardRef(
  ({ ...others }: OutlinedInputProps, ref) => {
    const classes = useStylesTextOutlined()
    return (
      <OutlinedInput
        inputRef={ref}
        classes={classes}
        notched={false}
        {...others}
      />
    )
  },
)

export default TextFieldOutlined
