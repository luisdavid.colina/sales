import MuiFormHelperText from '@material-ui/core/FormHelperText'

import { useStylesHelperText } from './styles'

const HelperText = ({ ...others }) => {
  const classes = useStylesHelperText()
  return <MuiFormHelperText classes={classes} {...others} />
}

export default HelperText
