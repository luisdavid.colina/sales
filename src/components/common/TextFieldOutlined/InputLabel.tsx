import MuiInputLabel from '@material-ui/core/InputLabel'

import { useStylesLabel } from './styles'

const InputLabel = ({ ...others }) => {
  const classes = useStylesLabel()
  return <MuiInputLabel classes={classes} {...others} />
}

export default InputLabel
