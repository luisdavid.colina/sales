function YouTubeGetID(youtubeUrl: string | string[]) {
  let url = youtubeUrl
  let ID: string | string[] = ''
  url = String(url)
    .replace(/(>|<)/gi, '')
    .split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/)
  if (url[2] !== undefined) {
    ID = url[2].split(/[^0-9a-z_\-]/i)
    ID = String(ID[0])
  } else {
    ID = url
  }
  return ID
}

export const Video = ({ url }: { url?: string }) => {
  if (url !== undefined) {
    const urlNew = `https://www.youtube.com/embed/${YouTubeGetID(url)}`
    const src = `${urlNew}?autoplay=0&fs=0&iv_load_policy=3&showinfo=0&rel=0&cc_load_policy=0&start=0&end=0`
    return (
      <iframe
        frameBorder="0"
        scrolling="no"
        marginHeight={0}
        marginWidth={0}
        style={{
          minHeight: 'inherit',
        }}
        width="100%"
        height="100%"
        title="video"
        src={src}
      />
    )
  }
  return (
    <div
      style={{
        minHeight: 'inherit',
        width: '100%',
        height: '100%',
        backgroundColor: '#2898FF',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <img
        src="/assets/login/logoWhite.svg"
        alt="Element Logo"
        style={{ width: '250px', maxWidth: '100%' }}
      />
    </div>
  )
}

export default Video
