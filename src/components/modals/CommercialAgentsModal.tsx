import { FC } from 'react'

import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'

import Table from '@/components/Table'

import { GridColDef, GridValueGetterParams } from '@material-ui/data-grid'

import CommercialAgent from '@/types/CommercialAgent'
import { priceFormatter } from '@/util/currency'

type CommercialAgentsProps = {
  open: boolean
  onClose: () => void
  commercialAgent?: CommercialAgent
}

const fixedColumns: GridColDef[] = [
  {
    field: 'range',
    headerName: 'Taxa ADM (R$)',
    flex: 1,
    type: 'number',
    valueGetter: (params: GridValueGetterParams) =>
      `${priceFormatter.format(Number(params.getValue('from')))} -
    ${priceFormatter.format(Number(params.getValue('to')))}`,
  },
  {
    field: 'salesChannel',
    type: 'string',
    headerName: 'Tipo de Venda',
    flex: 1,
  },
  {
    field: 'loads',
    type: 'number',
    headerName: 'Comissão s/ Cargas',
    flex: 1,
    valueFormatter: ({ value }) => `${value}%`,
  },
]

const ratedColumns: GridColDef[] = [
  {
    field: 'range',
    headerName: 'Taxa ADM (%)',
    flex: 1,
    type: 'number',
    valueGetter: (params: GridValueGetterParams) =>
      `${params.getValue('from')}% - ${params.getValue('to')}%`,
  },
  {
    field: 'salesChannel',
    headerName: 'Tipo de Venda',
    flex: 1,
  },
  {
    field: 'loads',
    flex: 1,
    type: 'number',
    headerName: 'Comissão s/ Cargas',
    valueFormatter: ({ value }) => `${value}%`,
  },
]

const CommercialAgentsModal: FC<CommercialAgentsProps> = ({
  open,
  onClose,
  commercialAgent,
}) => {
  return (
    <Dialog
      fullWidth
      maxWidth="lg"
      onClose={onClose}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <DialogTitle>Comissão de vendas</DialogTitle>
      <Box mt={0} m={6}>
        <Grid container spacing={3}>
          <Grid item xs={6}>
            <Table
              rows={commercialAgent?.salesCommission?.fixedCommissions || []}
              columns={fixedColumns}
            />
          </Grid>
          <Grid item xs={6}>
            <Table
              rows={commercialAgent?.salesCommission?.ratedCommissions || []}
              columns={ratedColumns}
            />
          </Grid>
        </Grid>
      </Box>
    </Dialog>
  )
}

export default CommercialAgentsModal
