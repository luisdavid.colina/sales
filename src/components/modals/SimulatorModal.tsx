import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { FC, useState } from 'react'
import {
  CircularProgress,
  Grid,
  InputAdornment,
  makeStyles,
} from '@material-ui/core'
import { simulateRule } from '@/util/helpers'
import { authFetch } from '@/util/fetch'
import { GridColDef } from '@material-ui/data-grid'
import SimulatorResultsTable from '../tables/SimulatorResultsTable'

const useStyles = makeStyles((theme) => ({
  simulateAgain: {
    marginBottom: theme.spacing(2),
  },
  form: {
    width: '100%',
    maxWidth: '500px',
  },
  wrapper: {
    width: '100%',
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  loading: {
    height: '300px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}))

interface Props {
  data: any
  rows: any[]
  columns: any[]
  open: boolean
  onClose: () => void
  onSuccess?: () => void
}

interface Results {
  rows: any[]
  columns: GridColDef[]
  revenue: number
  participantsCommissions: any
  totalCommission: number
  paymentPerMonth: number
}

const SimulatorModal: FC<Props> = ({
  rows,
  columns,
  data,
  open,
  onClose,
  onSuccess = () => {},
}) => {
  const classes = useStyles()

  const [income, setIncome] = useState(0)

  const [loading, setLoading] = useState(false)
  const [results, setResults] = useState<Results | null>(null)
  const [submitting, setSubmitting] = useState(false)

  const simulate = async () => {
    setResults(null)
    setLoading(true)
    try {
      const params = {
        income,
        rows,
        columns,
        data,
      }
      const newResults = await simulateRule(params)
      setResults(() => newResults)
    } catch (err) {
      const error = err as any
       
      // eslint-disable-next-line no-console
      console.error(error)
    }
    setLoading(false)
  }

  const saveProduct = async () => {
    if (!results) return
    setSubmitting(true)
    try {
      const participantsCommissions = Object.values(
        results.participantsCommissions,
      )

      const payload = {
        ...data,
        totalValue: income,
        participantsCommissions,
        estimatedRevenue: results.revenue,
        totalCommission: results.totalCommission,
        paymentPerMonth: results.paymentPerMonth,
      }
      const response = await authFetch.post('rules', payload)
      if (response.status === 201) {
        onSuccess()
        onClose()
      }
    } catch (err) {
      const error = err as any
       
      // eslint-disable-next-line no-console
      console.log(error)
    }

    setSubmitting(false)
  }

  const restart = () => {
    setResults(null)
  }

  const handleClose = () => {
    restart()
    onClose()
  }

  return (
    <Dialog
      fullScreen
      open={open}
      onClose={handleClose}
      aria-labelledby="form-dialog-title"
    >
      <DialogTitle id="form-dialog-title">Simulador</DialogTitle>
      <DialogContent>
        <Grid container spacing={10}>
          <Grid item md={6}>
            <TextField
              value={income}
              onChange={(event) => setIncome(Number(event.target.value))}
              autoFocus
              size="small"
              margin="normal"
              id="name"
              label="Valor total"
              type="number"
              variant="outlined"
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">R$</InputAdornment>
                ),
                inputProps: { step: 1, min: 0 },
              }}
            />
          </Grid>
        </Grid>
        {loading && (
          <div className={classes.loading}>
            <CircularProgress size={24} />
          </div>
        )}
        {!loading && results && (
          <Grid container direction="column">
            <DialogContentText>Resultados</DialogContentText>
            <Button
              className={classes.simulateAgain}
              disabled={loading}
              color="primary"
              onClick={simulate}
            >
              Simular novamente
            </Button>
            <SimulatorResultsTable
              rows={results.rows}
              columns={results.columns}
            />
          </Grid>
        )}
      </DialogContent>
      {!results && (
        <DialogActions>
          <Button fullWidth onClick={handleClose} color="primary">
            Retornar
          </Button>

          <Button
            disabled={loading}
            onClick={simulate}
            color="primary"
            fullWidth
            variant="contained"
          >
            Começar
          </Button>
        </DialogActions>
      )}
      {results && (
        <DialogActions>
          <Button fullWidth onClick={handleClose} color="primary">
            Retornar
          </Button>
          <div className={classes.wrapper}>
            <Button
              disabled={submitting}
              onClick={saveProduct}
              color="primary"
              fullWidth
              variant="contained"
            >
              Concluir
            </Button>
            {submitting && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
          </div>
        </DialogActions>
      )}
    </Dialog>
  )
}

export default SimulatorModal
