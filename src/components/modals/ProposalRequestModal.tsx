import { FC, MouseEvent, useState } from 'react'

import Link from 'next/link'

import SpinnerPage from '@/components/SpinnerPage'

import Box from '@material-ui/core/Box'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'

import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import Typography from '@material-ui/core/Typography'

import { FaSignature } from 'react-icons/fa'

import { firestore } from '@/lib/firebase'
import { useCollectionData } from 'react-firebase-hooks/firestore'

import ProposalRequest from '@/types/ProposalRequest'
import useContextMenu from '@/hooks/useContextMenu'
import ProposalRequestTable from '../tables/ProposalRequestTable'

type ProposalRequestModalProps = {
  open: boolean
  close: () => void
}

const ProposalRequestModal: FC<ProposalRequestModalProps> = ({
  open,
  close,
}) => {
  const queryProposalRequest = firestore.collection('proposalRequests')

  const [
    proposalRequests,
    loadingProposalRequests,
  ] = useCollectionData<ProposalRequest>(queryProposalRequest)

  const [
    selectedRequest,
    setSelectedRequest,
  ] = useState<ProposalRequest | null>(null)
  const { state, onOpen, onClose } = useContextMenu()

  const onDoubleClick = (params: any, e: MouseEvent<any>) => {
    const proposalRequest = proposalRequests?.find(
      (proposalReq) => proposalReq.id === params.row.id,
    )

    if (proposalRequest) {
      setSelectedRequest(proposalRequest)
      onOpen(e)
    }
  }

  if (loadingProposalRequests) {
    return <SpinnerPage />
  }
  return (
    <Dialog
      fullWidth
      maxWidth="lg"
      onClose={close}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <DialogTitle>Pedido de Proposta</DialogTitle>
      <Box mt={0} m={6}>
        <ProposalRequestTable
          onRowDoubleClick={onDoubleClick}
          proposal={proposalRequests || []}
        />
      </Box>
      <Menu
        keepMounted
        open={state.mouseY !== null}
        onClose={onClose}
        anchorReference="anchorPosition"
        anchorPosition={
          state.mouseY !== null && state.mouseX !== null
            ? { top: state.mouseY, left: state.mouseX }
            : undefined
        }
      >
        <Link href={`/dashboard/proposals/create/${selectedRequest?.id}`}>
          <MenuItem onClick={onClose}>
            <ListItemIcon>
              <FaSignature />
            </ListItemIcon>
            <Typography>Gerar Proposta</Typography>
          </MenuItem>
        </Link>
      </Menu>
    </Dialog>
  )
}

export default ProposalRequestModal
