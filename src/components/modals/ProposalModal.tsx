import { FC } from 'react'

import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import Typography from '@material-ui/core/Typography'

import Proposal from '@/types/Proposal'

type ProposalModalProps = {
  open: boolean
  onClose: () => void
  proposal?: Proposal
}

const ProposalModal: FC<ProposalModalProps> = ({ open, onClose, proposal }) => {
  return (
    <Dialog
      fullWidth
      maxWidth="lg"
      onClose={onClose}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <DialogTitle>Resumo</DialogTitle>
      <Box mt={0} m={6}>
        <Grid container spacing={3}>
          <Grid item lg={4} sm={6} xs={12}>
            <Typography variant="subtitle1" color="initial">
              Razão Social:
            </Typography>
            <Typography variant="body2" color="initial">
              {proposal?.company.legalName || '-'}
            </Typography>
          </Grid>
          <Grid item lg={4} sm={6} xs={12}>
            <Typography variant="subtitle1" color="initial">
              Product:
            </Typography>
            <Typography variant="body2" color="initial">
              {proposal?.productDetails?.product || '-'}
            </Typography>
          </Grid>
          <Grid item lg={4} sm={6} xs={12}>
            <Typography variant="subtitle1" color="initial">
              Nome Completo:
            </Typography>
            <Typography variant="body2" color="initial">
              {proposal?.company.admin.fullName || '-'}
            </Typography>
          </Grid>
          <Grid item lg={4} sm={6} xs={12}>
            <Typography variant="subtitle1" color="initial">
              Taxa ADM:
            </Typography>
            <Typography variant="body2" color="initial">
              -
            </Typography>
          </Grid>
          <Grid item lg={4} sm={6} xs={12}>
            <Typography variant="subtitle1" color="initial">
              Email:
            </Typography>
            <Typography variant="body2" color="initial">
              {proposal?.company.admin.email || '-'}
            </Typography>
          </Grid>
          <Grid item lg={4} sm={6} xs={12}>
            <Typography variant="subtitle1" color="initial">
              Taxa ADM:
            </Typography>
            <Typography variant="body2" color="initial">
              {proposal?.productDetails.campaign.isAdministrationRate
                ? proposal?.productDetails.campaign.administrationRate
                : proposal?.productDetails.campaign.administrationFee}
            </Typography>
          </Grid>
          <Grid item lg={4} sm={6} xs={12}>
            <Typography variant="subtitle1" color="initial">
              Celular
            </Typography>
            <Typography variant="body2" color="initial">
              {proposal?.company.admin.mobile || '-'}
            </Typography>
          </Grid>
          <Grid item lg={4} sm={6} xs={12}>
            <Typography variant="subtitle1" color="initial">
              Celular
            </Typography>
            <Typography variant="body2" color="initial">
              {proposal?.company.admin.mobile || '-'}
            </Typography>
          </Grid>
          <Grid item lg={4} sm={6} xs={12}>
            <Typography variant="subtitle1" color="initial">
              Premiação Anual Estimada:
            </Typography>
            <Typography variant="body2" color="initial">
              -
            </Typography>
          </Grid>
          <Grid item lg={4} sm={6} xs={12}>
            <Typography variant="subtitle1" color="initial">
              Preço do Cartão:
            </Typography>
            <Typography variant="body2" color="initial">
              {proposal?.productDetails.issuer || '-'}
            </Typography>
          </Grid>
        </Grid>
      </Box>
    </Dialog>
  )
}

export default ProposalModal
