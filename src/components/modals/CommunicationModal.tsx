import { FC, useState } from 'react'
import Link from 'next/link'
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import Dialog from '@material-ui/core/Dialog'
import DialogTitle from '@material-ui/core/DialogTitle'
import Typography from '@material-ui/core/Typography'

import Communication from '@/types/Communication'
import { DialogContent } from '@material-ui/core'

type CommunicationModalProps = {
  open: boolean
  onClose: () => void
  communication: Communication
}

const CommunicationModal: FC<CommunicationModalProps> = ({
  open,
  onClose,
  communication,
}) => {
  return (
    <Dialog
      fullWidth
      maxWidth="lg"
      onClose={onClose}
      aria-labelledby="simple-dialog-title"
      open={open}
    >
      <DialogTitle>Resumo do Comunicação</DialogTitle>
      <DialogContent>
        <Box mt={0} m={6}>
          <Grid container spacing={3}>
            <Grid item xs={4}>
              <Typography variant="subtitle1" color="initial">
                Destinatario:
              </Typography>
              <Typography variant="body2" color="initial">
                {communication.to?.join(', ')}
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography variant="subtitle1" color="initial">
                Meio de envio:
              </Typography>
              <Typography variant="body2" color="initial">
                {communication.sendingMethod?.join(', ')}
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography variant="subtitle1" color="initial">
                Contenido:
              </Typography>
              <Typography variant="body2" color="initial">
                {communication.text}
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography variant="subtitle1" color="initial">
                Assunto:
              </Typography>
              <Typography variant="body2" color="initial">
                {communication.subject}
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography variant="subtitle1" color="initial">
                Status:
              </Typography>
              <Typography variant="body2" color="initial">
                {communication.status}
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography variant="subtitle1" color="initial">
                Arquivo:
              </Typography>

              <Typography variant="body2" color="initial">
                <Link href={communication?.object || ''}>
                  {communication.object?.replaceAll(
                    'https://element-sales-ecosystem-nfs.s3.amazonaws.com/',
                    '',
                  )}
                </Link>
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </DialogContent>
    </Dialog>
  )
}

export default CommunicationModal
