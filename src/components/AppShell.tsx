import { useSession } from '@/context/SessionContext'
import { useRouter } from 'next/router'
import { FC } from 'react'
import Layout from './Layout'
import SpinnerPage from './SpinnerPage'

const AppShell: FC = ({ children }) => {
  const router = useRouter()
  const session = useSession()

  if (session.loading) {
    return <SpinnerPage />
  }

  if (!session.isAuth) {
    router.push('/login')
    return <SpinnerPage />
  }

  return <Layout>{children}</Layout>
}

export default AppShell
