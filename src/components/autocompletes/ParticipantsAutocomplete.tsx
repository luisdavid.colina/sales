import { FC, useState } from 'react'
import { CircularProgress } from '@material-ui/core'
import TextField, { TextFieldProps } from '@material-ui/core/TextField'
import Autocomplete, {
  createFilterOptions,
} from '@material-ui/lab/Autocomplete'
import { firestore } from '@/lib/firebase'
import { useCollectionData } from 'react-firebase-hooks/firestore'
import { useParticipants } from '@/lib/API'

interface ParticipantOption {
  id?: number
  name: string
  inputValue?: string
}

type Props = {
  value: any
  onChange: (...event: any[]) => void
} & TextFieldProps

const filter = createFilterOptions<ParticipantOption>()

const ParticipantsAutocomplete: FC<Props> = ({
  value,
  onChange,
  disabled,
  ...props
}) => {
  const query = firestore.collection('participants')
  const [participants, loading] = useCollectionData(query)

  const [saving, setSaving] = useState(false)
  const { create: createParticipant } = useParticipants()

  return (
    <Autocomplete
      multiple
      value={value}
      disabled={disabled}
      onChange={async (event, newValues) => {
        setSaving(true)
        const newValue = newValues[newValues.length - 1]

        if (newValue && newValue.inputValue) {
          const { error, data } = await createParticipant({
            name: newValue.inputValue,
          })

          if (!error) {
            const values = newValues.slice()

            values[values.length - 1] = {
              id: data.participant.id,
              name: data.participant.name,
            }

            onChange(values)
          }
        } else {
          onChange(newValues)
        }
        setSaving(false)
      }}
      loading={loading || saving}
      freeSolo
      id="participants"
      disableClearable
      options={participants || []}
      selectOnFocus
      clearOnBlur
      handleHomeEndKeys
      getOptionLabel={(option) => {
        // Value selected with enter, right from the input
        if (typeof option === 'string') {
          return option
        }
        // Add "xxx" option created dynamically
        if (option.inputValue) {
          return option.inputValue
        }
        // Regular option
        return option.name
      }}
      filterOptions={(options, params) => {
        const filtered = filter(options, params)

        // Suggest the creation of a new value
        const participantsNames =
          participants?.map((participant) => participant.name) || []

        if (
          params.inputValue !== '' &&
          !participantsNames.includes(params.inputValue)
        ) {
          filtered.push({
            inputValue: params.inputValue,
            name: `Agregar "${params.inputValue}"`,
          })
        }

        return filtered
      }}
      renderOption={(option) => option.name}
      renderInput={(params) => (
        <TextField
          {...params}
          {...props}
          label="Selecione um participante"
          variant="outlined"
          margin="normal"
          size="small"
          InputProps={{
            ...params.InputProps,
            type: 'search',

            endAdornment: (
              <>
                {loading || saving ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </>
            ),
          }}
        />
      )}
    />
  )
}

export default ParticipantsAutocomplete
