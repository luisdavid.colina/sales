import { useProductCategories } from '@/lib/API'
import { firestore } from '@/lib/firebase'
import { CircularProgress } from '@material-ui/core'
import TextField, { TextFieldProps } from '@material-ui/core/TextField'
import Autocomplete, {
  createFilterOptions,
} from '@material-ui/lab/Autocomplete'
import { FC } from 'react'
import { useCollectionData } from 'react-firebase-hooks/firestore'

interface ProductOption {
  name: string
  id?: number
  inputValue?: string
}

const filter = createFilterOptions<ProductOption>()

type Props = {
  value: any
  onChange: (...event: any[]) => void
} & TextFieldProps

const ProductCategoryAutocomplete: FC<Props> = ({
  value,
  onChange,
  ...props
}) => {
  const { create: createCategory } = useProductCategories()

  const query = firestore.collection('productsCategories')
  const [categories, loading] = useCollectionData(query)

  return (
    <Autocomplete
      loading={loading}
      value={value}
      onChange={async (event, newValue) => {
        if (typeof newValue === 'string') {
          onChange({
            name: newValue,
          })
        } else if (newValue && newValue.inputValue) {
          // Create a new value from the user input
          const { error, data } = await createCategory({
            name: newValue.inputValue,
          })

          if (!error) {
            onChange({
              id: data.category.id,
              name: data.category.name,
            })
          }
        } else {
          onChange(newValue)
        }
      }}
      filterOptions={(options, params) => {
        const filtered = filter(options, params)

        // Suggest the creation of a new value
        if (
          params.inputValue !== '' &&
          options.every(
            (options) =>
              options.name.toLowerCase() !== params.inputValue.toLowerCase(),
          )
        ) {
          filtered.push({
            inputValue: params.inputValue,
            name: `Agregar "${params.inputValue}"`,
          })
        }

        return filtered
      }}
      selectOnFocus
      clearOnBlur
      handleHomeEndKeys
      id="product-category"
      options={categories || []}
      getOptionLabel={(option) => {
        // Value selected with enter, right from the input
        if (typeof option === 'string') {
          return option
        }
        // Add "xxx" option created dynamically
        if (option.inputValue) {
          return option.inputValue
        }
        // Regular option
        return option.name
      }}
      renderOption={(option) => option.name}
      freeSolo
      renderInput={(params) => (
        <TextField
          {...params}
          {...props}
          label="Categoria do produto"
          variant="outlined"
          fullWidth
          size="small"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </>
            ),
          }}
        />
      )}
    />
  )
}

export default ProductCategoryAutocomplete
