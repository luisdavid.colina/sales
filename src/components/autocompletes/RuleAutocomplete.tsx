import { FC } from 'react'
import { CircularProgress } from '@material-ui/core'
import TextField, { TextFieldProps } from '@material-ui/core/TextField'
import Autocomplete, {
  createFilterOptions,
} from '@material-ui/lab/Autocomplete'
import { firestore } from '@/lib/firebase'
import { useCollectionData } from 'react-firebase-hooks/firestore'

interface RuleOption {
  id?: number
  name: string
  inputValue?: string
}

type Props = {
  value: any
  onChange: (...event: any[]) => void
} & TextFieldProps

const filter = createFilterOptions<RuleOption>()

const RuleAutocomplete: FC<Props> = ({ value, onChange, ...props }) => {
  const query = firestore.collection('rules')
  const [rules, loading] = useCollectionData<RuleOption>(query)

  return (
    <Autocomplete
      value={value}
      onChange={(event, newValue) => {
        if (typeof newValue === 'string') {
          onChange({
            name: newValue,
          })
        } else if (newValue && newValue.inputValue) {
          onChange({
            name: newValue.inputValue,
          })
        } else {
          onChange(newValue)
        }
      }}
      loading={loading}
      freeSolo
      id="rule"
      disableClearable
      options={rules || []}
      selectOnFocus
      clearOnBlur
      handleHomeEndKeys
      getOptionLabel={(option) => {
        // Value selected with enter, right from the input
        if (typeof option === 'string') {
          return option
        }
        // Add "xxx" option created dynamically
        if (option.inputValue) {
          return option.inputValue
        }
        // Regular option
        return option.name
      }}
      filterOptions={(options, params) => {
        const filtered = filter(options, params)

        // Suggest the creation of a new value
        if (
          params.inputValue !== '' &&
          options.every(
            (options) =>
              options.name.toLowerCase() !== params.inputValue.toLowerCase(),
          )
        ) {
          filtered.push({
            inputValue: params.inputValue,
            name: `Agregar "${params.inputValue}"`,
          })
        }

        return filtered
      }}
      renderOption={(option) => option.name}
      renderInput={(params) => (
        <TextField
          {...params}
          {...props}
          label="Selecione Regra Do Produto"
          variant="outlined"
          size="small"
          InputProps={{
            ...params.InputProps,
            type: 'search',

            endAdornment: (
              <>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </>
            ),
          }}
        />
      )}
    />
  )
}

export default RuleAutocomplete
