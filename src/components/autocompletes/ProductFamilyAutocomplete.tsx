import { useProductFamilies } from '@/lib/API'
import { firestore } from '@/lib/firebase'
import { CircularProgress } from '@material-ui/core'
import TextField, { TextFieldProps } from '@material-ui/core/TextField'
import Autocomplete, {
  createFilterOptions,
} from '@material-ui/lab/Autocomplete'
import { FC } from 'react'
import { useCollectionData } from 'react-firebase-hooks/firestore'

type ProductOption = {
  name: string
  id?: number
  inputValue?: string
}

const filter = createFilterOptions<ProductOption>()

type Props = {
  value: any
  onChange: (...event: any[]) => void
} & TextFieldProps

const ProductFamilyAutocomplete: FC<Props> = ({
  value,
  onChange,
  ...props
}) => {
  const { create: createFamily } = useProductFamilies()

  const query = firestore.collection('productsFamilies')
  const [families, loading] = useCollectionData(query)

  return (
    <Autocomplete
      loading={loading}
      value={value}
      onChange={async (event, newValue) => {
        if (typeof newValue === 'string') {
          onChange({
            name: newValue,
          })
        } else if (newValue && newValue.inputValue) {
          // Create a new value from the user input
          const { error, data } = await createFamily({
            name: newValue.inputValue,
          })

          if (!error) {
            onChange({
              id: data.family.id,
              name: data.family.name,
            })
          }
        } else {
          onChange(newValue)
        }
      }}
      filterOptions={(options, params) => {
        const filtered = filter(options, params)

        // Suggest the creation of a new value
        if (
          params.inputValue !== '' &&
          options.every(
            (options) =>
              options.name.toLowerCase() !== params.inputValue.toLowerCase(),
          )
        ) {
          filtered.push({
            inputValue: params.inputValue,
            name: `Agregar "${params.inputValue}"`,
          })
        }

        return filtered
      }}
      selectOnFocus
      clearOnBlur
      handleHomeEndKeys
      id="product-family"
      options={families || []}
      getOptionLabel={(option) => {
        // Value selected with enter, right from the input
        if (typeof option === 'string') {
          return option
        }
        // Add "xxx" option created dynamically
        if (option.inputValue) {
          return option.inputValue
        }
        // Regular option
        return option.name
      }}
      renderOption={(option) => option.name}
      freeSolo
      renderInput={(params) => (
        <TextField
          {...params}
          {...props}
          label="Familia do produto"
          variant="outlined"
          fullWidth
          size="small"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </>
            ),
          }}
        />
      )}
    />
  )
}

export default ProductFamilyAutocomplete
