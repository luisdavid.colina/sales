import { firestore } from '@/lib/firebase'
import Participant from '@/types/Participant'
import TextField, { TextFieldProps } from '@material-ui/core/TextField'
import { CircularProgress } from '@material-ui/core'
import { Autocomplete, createFilterOptions } from '@material-ui/lab'
import { FC } from 'react'
import { useCollectionData } from 'react-firebase-hooks/firestore'

type Props = {
  value: any
  onNewOption: (value: string) => void
  onChange: (...event: any[]) => void
} & TextFieldProps

type ChannelOption = {
  id?: number
  name: string
  participants: Participant[]
  inputValue?: string
}

const filter = createFilterOptions<ChannelOption>()

const SalesChannelAutocomplete: FC<Props> = ({
  onNewOption,
  value,
  onChange,
  disabled,
  ...props
}) => {
  const query = firestore.collection('salesChannels')
  const [salesChannels, loading] = useCollectionData(query)

  return (
    <Autocomplete
      value={value}
      disabled={disabled}
      loading={loading}
      onChange={(event, newValue) => {
        if (typeof newValue === 'string') {
          onNewOption(newValue)
          onChange({
            name: newValue,
            participants: [],
          })
        } else if (newValue && newValue.inputValue) {
          onNewOption(newValue.inputValue)
          onChange({
            name: newValue.inputValue,
            participants: [],
          })
        } else {
          onChange(newValue)
        }
      }}
      filterOptions={(options, params) => {
        const filtered = filter(options, params)

        if (
          params.inputValue !== '' &&
          options.every(
            (options) =>
              options.name.toLowerCase() !== params.inputValue.toLowerCase(),
          )
        ) {
          filtered.push({
            inputValue: params.inputValue,
            name: `Agregar "${params.inputValue}"`,
            participants: [],
          })
        }

        return filtered
      }}
      options={salesChannels || []}
      getOptionLabel={(option) => {
        // Value selected with enter, right from the input
        if (typeof option === 'string') {
          return option
        }
        // Add "xxx" option created dynamically
        if (option.inputValue) {
          return option.inputValue
        }
        // Regular option
        return option.name
      }}
      selectOnFocus
      clearOnBlur
      handleHomeEndKeys
      renderOption={(option) => option.name}
      fullWidth
      freeSolo
      renderInput={(params) => (
        <TextField
          {...params}
          {...props}
          label="Canal da Venda"
          margin="normal"
          variant="outlined"
          fullWidth
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </>
            ),
          }}
        />
      )}
    />
  )
}

export default SalesChannelAutocomplete
