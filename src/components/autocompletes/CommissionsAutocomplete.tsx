import { firestore } from '@/lib/firebase'
import SalesCommission from '@/types/SalesCommission'
import TextField, { TextFieldProps } from '@material-ui/core/TextField'
import { CircularProgress } from '@material-ui/core'
import { Autocomplete } from '@material-ui/lab'
import { FC } from 'react'
import { useCollectionData } from 'react-firebase-hooks/firestore'

type Props = {
  value?: any
  onChange: (value: SalesCommission | null) => void
  disabled?: boolean
}

const SalesCommissionsAutocomplete: FC<Props> = ({
  onChange,
  disabled,
  ...props
}) => {
  const query = firestore.collection('salesCommissions')
  const [salesCommissions, loading] = useCollectionData<SalesCommission>(query)

  return (
    <Autocomplete
      disabled={disabled}
      loading={loading}
      onChange={(event, newValue) => {
        onChange(newValue as SalesCommission)
      }}
      options={salesCommissions || []}
      getOptionLabel={(option) => {
        // Value selected with enter, right from the input
        if (typeof option === 'string') {
          return option
        }
        // Regular option
        return option.name
      }}
      selectOnFocus
      clearOnBlur
      handleHomeEndKeys
      renderOption={(option) => option.name}
      fullWidth
      freeSolo
      renderInput={(params) => (
        <TextField
          {...params}
          {...props}
          label="Comissão de vendas"
          margin="normal"
          variant="outlined"
          fullWidth
          size="small"
          InputProps={{
            ...params.InputProps,
            endAdornment: (
              <>
                {loading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
              </>
            ),
          }}
        />
      )}
    />
  )
}

export default SalesCommissionsAutocomplete
