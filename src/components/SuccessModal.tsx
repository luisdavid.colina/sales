import React, { FC, useState } from 'react'

import Typography from '@material-ui/core/Typography'
import Dialog from '@material-ui/core/Dialog'
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'
import { CircularProgress, makeStyles } from '@material-ui/core'

interface SuccessProps {
  title: string
  open: boolean
  onClose: (event: any, reason: string) => void
  onDownload?: () => Promise<any>
}

const useStyles = makeStyles((theme) => ({
  buttonMargin: {
    marginTop: theme.spacing(3),
  },
}))
const SuccessModal: FC<SuccessProps> = ({
  title,
  open,
  onClose,
  onDownload,
}) => {
  const classes = useStyles()

  const [downloading, setDownloading] = useState(false)

  const handleDownload = async () => {
    if (onDownload) {
      setDownloading(true)
      await onDownload()
      setDownloading(false)
    }
  }

  return (
    <Dialog
      fullWidth
      maxWidth="xs"
      open={open}
      onClose={onClose}
      aria-labelledby="max-width-dialog-title"
    >
      <Box
        p={10}
        display="flex"
        flexDirection="column"
        justifyContent="space-between"
        alignItems="center"
      >
        <Box>
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            pb={5}
          >
            <img src="/assets/success.svg" alt="success" />
          </Box>
          <Box p={5}>
            <Typography variant="h5" color="primary" align="center" paragraph>
              {title}
            </Typography>
          </Box>
        </Box>
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="space-around"
        >
          {onDownload && !downloading && (
            <Button
              onClick={handleDownload}
              variant="contained"
              color="primary"
            >
              <span>Baixar</span>
            </Button>
          )}

          {onDownload && downloading && (
            <Box display="flex" justifyContent="center">
              <CircularProgress />
            </Box>
          )}
          <Button
            onClick={(e) => onClose(e, 'click close')}
            variant="contained"
            color="primary"
            className={classes.buttonMargin}
          >
            <span>Concluir</span>
          </Button>
        </Box>
      </Box>
    </Dialog>
  )
}

export default SuccessModal
