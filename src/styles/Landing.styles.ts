export const classesMd = {
    CopyRight: {
        backgroundColor: '#0a4d9c',
        padding: '10px',
        color: '#ffffff',
        textAlign: 'center',
    },
    Header: {
        minHeight: '650px',
        backgroundImage: 'url(/images/hero.jpg)',
        padding: '1em',
        backgroundPositionX: 'center',
        backgroundPositionY: 'bottom',
    },
    HeaderText: {
        padding: '0.8em',
        borderRadius: '15px',
        background:
            'linear-gradient(135deg, rgba(0,162,255,0.5) 35%, rgba(38,0,206,0.5) 100%)',
        fontFamily: '"Ubuntu", sans-serif',
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#f3f3f3',
        lineHeight: '1.2em',
        textShadow: '0 2px 0 rgb(0 0 0 / 7%)',
        marginBottom: '0.67em',
        fontSize: '2.3em',
        marginTop: '3em',
    },
    multiplierForm: {
        color: '#f3f3f3',
        backgroundColor: 'rgba(11, 0, 167, 0.7)',
        margin: '2em',
        borderRadius: '10px',
        padding: '2em',
        paddingTop: '0em',
        display: 'inline-grid',
    },
    multiplierForm2: {
        color: '#f3f3f3',
        borderRadius: '10px',
        padding: '2em',
        paddingTop: '0em',
        display: 'inline-grid',

        verticalAlign: 'top',
    },
    multiplierForm3: {
        color: '#f3f3f3',
        backgroundColor: 'rgba(11, 0, 167, 0.7)',
        borderRadius: '10px',
        padding: '2em',
        paddingTop: '0em',
        marginTop: '2em',
        display: 'inline-grid',
        verticalAlign: 'bottom',
    },

    formTitle: { fontSize: '2em', fontWeight: 'bold' },

    textWhite: {
        color: '#f3f3f3',
        fontSize: '1.4em',
    },

    imageContainer: {
        textAlign: 'center',
    },
    // Features Section
    featuresBackground: {
        position: 'absolute' as const,
        backgroundColor: '#0a4d9c',
        height: '600px',
        transform: 'skewY(-10deg)',
        transformOrigin: '100%',
        left: '0',
        right: '0',
        zIndex: -1,
        top: '-100px',
    },
    // Benefits Section

    benefits: {
        backgroundImage: 'url(/images/calc.png)',
        backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        height: 'auto',
        paddingBottom: '120px',
    },
    benefitsh2: {
        fontSize: '42px',
        color: '#0a4d9c',
        fontFamily: '"Ubuntu", sans-serif',
        fontWeight: 'bold',
        textAlign: 'center',
        lineHeight: '1.14',
        textShadow: '0 2px 0 rgb(0 0 0 / 7%)',
    },
    benefitsdesc: {
        margin: '0 auto',
        position: 'relative',
        justify: 'left',
        alignItems: 'left',
    },

    bli: {
        marginTop: '1em',
        fontFamily: '"Ubuntu", sans-serif',
        fontWeight: 'bold',
        fontSize: '1.5em',
        textAlign: 'left',
        color: '#1a0d00',
        lineHeight: '1.2em',
        marginBottom: '0.67em',
        listStyleType: 'none',
        marginLeft: '1em',
    },
    blis: {
        fontFamily: '"Ubuntu", sans-serif',
        fontWeight: 'bold',
        textAlign: 'left',
        color: '#0a4d9c',
        lineHeight: '1.2em',
    },

    tlabel: { color: '#f3f3f3', fontSize: '1.2em', lineHeight: '2' },

    root: {
        '&:hover': {
            backgroundColor: '',
        },
    },
    error: {
        '&.MuiFormHelperText-root.Mui-error': {
            color: 'white',
        },
    },
    icon: {
        borderRadius: '50%',
        width: '18px',
        height: '18px',
        border: '2px solid',
        color: 'white',
        transition: '0.2s',
        boxShadow:
            'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
        backgroundColor: 'rgba(255, 255, 255, 0.1)',
        backgroundImage: 'rgba(255, 255, 255, 0.1)',
        '$root.Mui-focusVisible &': {
            outline: '2px auto rgba(19,124,189,.6)',
            outlineOffset: 2,
        },
        'input:hover ~ &': {
            backgroundColor: '#0077ff',
            width: '21px',
            height: '21px',
        },
        'input:disabled ~ &': {
            boxShadow: 'none',
            background: 'rgba(206,217,224,.5)',
        },
    },
    icono: {
        borderRadius: '20%',
        width: '18px',
        height: '18px',
        border: '2px solid',
        color: 'white',
        transition: '0.2s',
        boxShadow:
            'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
        backgroundColor: 'rgba(255, 255, 255, 0.1)',
        backgroundImage: 'rgba(255, 255, 255, 0.1)',
        '$root.Mui-focusVisible &': {
            outline: '2px auto rgba(19,124,189,.6)',
            outlineOffset: 2,
        },
        'input:hover ~ &': {
            backgroundColor: '#0077ff',
            width: '21px',
            height: '21px',
        },
        'input:disabled ~ &': {
            boxShadow: 'none',
            background: 'rgba(206,217,224,.5)',
        },
    },
    checkedIcon: {
        backgroundColor: '#0077ff',
        width: '21px',
        height: '21px',
        backgroundImage: '#0077ff',
        '&:before': {
            display: 'block',
            width: 16,
            height: 16,
            content: '""',
        },
    },
    input: {
        color: 'white',
        fontSize: '1.3em',
        fontWeight: 'bold',
        backgroundColor: '#ffffff1A',
        border: '2px solid',
        borderRadius: '5px',
        maxWidth: '100%',
    },
    result: {
        color: 'white',
        fontSize: '22px',
        fontWeight: 'bold',
        backgroundColor: '#ffffff1A',
        border: '2px solid',
        borderRadius: '5px',
        maxWidth: '100%',
        height: '2.4em',
        marginTop: '0.3em',
    },
    submit: {
        backgroundColor: '#0077ff',
        fontSize: '22px',
        height: '2.4em',
    },

    textRadio: {
        color: '#f3f3f3',
        fontSize: '1em',
        marginLeft: '0.5em',
        marginRight: '2em',
    },
}
const classesUp: any = {
    Header: {
        padding: '2em',
        paddingLeft: '4em',
        backgroundPositionY: 'top',
        backgroundSize: 'cover',
    },
    HeaderText: {
        padding: '1.6em',
        fontSize: '2.8em',
        textAlign: 'left',
        maxWidth: '50%',
    },

    multiplierForm2: {
        maxWidth: '50%',
    },
    multiplierForm3: {
        maxWidth: '50%',
    },

    imageContainer: {
        textAlign: 'left',
    },
    // Features Section
    featuresBackground: {
        top: '-350px',
    },
}
const classesTmp: any = { ...classesMd }

Object.entries(classesMd).forEach(([key, value]) => {
    classesTmp[key] = { ...value, ...classesUp[key] }
})

const classes = { ...classesTmp }

export default classes
