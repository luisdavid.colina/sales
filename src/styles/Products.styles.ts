import { makeStyles, createStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles((theme) =>
  createStyles({
    button: {
      marginRight: theme.spacing(3),
    },
  }),
)
