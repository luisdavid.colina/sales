import { ADMIN_ROLE } from '@/constants'
import { ApiRequest } from '@/util/getHandler'
import { NextApiResponse, NextApiHandler } from 'next'
import verifyToken from './verifyToken'

function checkAdmin(handler: NextApiHandler) {
  return async (req: ApiRequest, res: NextApiResponse) => {
    const { authUser } = req

    if (authUser?.role !== ADMIN_ROLE) return res.status(403).end()
    return handler(req, res)
  }
}

export default (handler: NextApiHandler) => verifyToken(checkAdmin(handler))
