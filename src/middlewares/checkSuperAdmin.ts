import { SUPER_ADMIN_ROLE } from '@/constants'
import { ApiRequest } from '@/util/getHandler'
import { NextApiResponse, NextApiHandler } from 'next'
import verifyToken from './verifyToken'

function checkSuperAdmin(handler: NextApiHandler) {
  return async (req: ApiRequest, res: NextApiResponse) => {
    const { user } = req

    if (user?.role !== SUPER_ADMIN_ROLE) return res.status(403).end()
    return handler(req, res)
  }
}

export default (handler: NextApiHandler) =>
  verifyToken(checkSuperAdmin(handler))
