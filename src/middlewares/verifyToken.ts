import { NextApiHandler, NextApiResponse } from 'next'
import { auth, firestore } from 'lib/firebase-admin'
import { ApiRequest } from '@/util/getHandler'
import User from '@/types/User'
import {
  exchangeRefreshToken,
  getRefreshTokenCookie,
  getTokenCookie,
} from '@/pages/api/util'

export default function verifyToken(handler: NextApiHandler) {
  return async (req: ApiRequest, res: NextApiResponse) => {
    let token = getTokenCookie(req)
    const refreshToken = getRefreshTokenCookie(req)

    if (!token && !refreshToken) return res.status(401).end()

    if (!token) {
      token = await exchangeRefreshToken(res, refreshToken)
    }

    try {
      const decodedToken = await auth.verifyIdToken(token)
      req.authUser = decodedToken
      const userSnapshot = await firestore
        .doc(`users/${decodedToken.uid}`)
        .get()
      req.user = userSnapshot.data() as User
    } catch (err) {
      return res.status(400).end()
    }

    return handler(req, res)
  }
}
