import { ApiRequest } from '@/util/getHandler'
import { NextApiResponse } from 'next'
import User, {
  Role,
  InterfaceCompanyPermissions,
  CompanyPermission,
  Permisssion,
} from '@/types/User'

export const isRoleOrHasPermissions = (
  roles: Role[],
  permissions: InterfaceCompanyPermissions<Permisssion[]>,
  user: User,
) => {
  const modules = Object.keys(permissions) as CompanyPermission[]
  let hasPermission = true
  const hasRole = (user?.role && roles.includes(user.role)) || false

  modules.forEach((module: CompanyPermission) => {
    permissions[module]?.forEach((permission: Permisssion) => {
      if (
        !user?.permissions ||
        !user?.permissions[module] ||
        !user?.permissions[module][permission]
      ) {
        hasPermission = false
      }
    })
  })

  return hasPermission || hasRole
}

const checkRoleOrPermissions = (
  roles: Role[],
  permissions: InterfaceCompanyPermissions<Permisssion[]>,
  handle: (req: ApiRequest, res: NextApiResponse) => any,
) => (req: ApiRequest, res: NextApiResponse) => {
  if (!req?.user) {
    return res.status(401).end()
  }
  if (!isRoleOrHasPermissions(roles, permissions, req.user)) {
    return res.status(403).end()
  }
  return handle(req, res)
}

export default checkRoleOrPermissions
