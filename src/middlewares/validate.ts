import { NextApiRequest, NextApiResponse, NextApiHandler } from 'next'
import { ObjectShape, OptionalObjectSchema } from 'yup/lib/object'

export default function validate(
  schema: OptionalObjectSchema<ObjectShape>,
  handler: NextApiHandler,
) {
  return async (req: NextApiRequest, res: NextApiResponse) => {
    const schemaOptions = { stripUnknown: true, abortEarly: false }

    if (req.method === 'POST') {
      try {
        req.body = await schema.camelCase().validate(req.body, schemaOptions)
      } catch (err) {
      const error = err as any
       
        return res.status(400).json(error)
      }
    }
    const result = await handler(req, res)
    return result
  }
}
