import { FC, useContext, useState } from 'react'
import { Paper } from '@material-ui/core'
import StepperContext, { StepperProvider } from '@/context/StepperContext'

import { AxiosResponse } from 'axios'

import Contract from '@/types/Contract'
import CompanyForm from './CompanyForm'
import ContractReviewForm from './ContractReviewForm'
import DeliveryAddressForm from './DeliveryAddressForm'
import ProductDetailsForm from './ProductDetailsForm'
import RevenueForm from './RevenueForm'
import defaultContract from './defaultStates/contract'

interface StepFormProps {
  contract: Contract
}

const StepForm: FC<StepFormProps> = ({ contract }) => {
  const stepper = useContext(StepperContext)

  switch (stepper.step) {
    case 1:
      return <CompanyForm company={contract.company} />
    case 2:
      return <DeliveryAddressForm deliveryAddress={contract.deliveryAddress} />
    case 3:
      return <ProductDetailsForm productDetails={contract.productDetails} />
    case 4:
      return <RevenueForm revenue={contract.revenue} />
    default:
      return <ContractReviewForm contract={contract} />
  }
}

type ContractFormProps = {
  contract?: Contract
  disabled?: boolean
  onSuccess: (contract: Contract) => Promise<AxiosResponse<any>>
  code?: number
  titleModal?: string
  actionModal?: () => void
}

const ContractForm: FC<ContractFormProps> = ({
  contract = defaultContract,
  disabled = false,
  onSuccess,
  code,
  titleModal,
  actionModal,
}) => {
  const [data, setData] = useState<Contract>({
    ...contract,
    revenue: {
      ...contract.revenue,
      signatureDate: contract.revenue.signatureDate
        ? new Date(contract.revenue.signatureDate)
        : undefined,
      expirationDate: contract.revenue.expirationDate
        ? new Date(contract.revenue.expirationDate)
        : undefined,
    },
  })

  function updateData(stepData: Partial<Contract>) {
    if (stepData) {
      setData((prevData) => {
        return { ...prevData, ...stepData }
      })
    }
  }

  return (
    <Paper elevation={4}>
      <StepperProvider
        disabled={disabled}
        onChange={updateData}
        onSuccess={onSuccess}
        code={code}
        titleModal={titleModal}
        actionModal={actionModal}
      >
        <StepForm contract={data} />
      </StepperProvider>
    </Paper>
  )
}

export default ContractForm
