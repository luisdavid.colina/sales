import { useForm } from 'react-hook-form'
import {
  Button,
  Box,
  Container,
  Grid,
  makeStyles,
  TextField,
  Typography,
} from '@material-ui/core'
import { yupResolver } from '@hookform/resolvers/yup'
import ElementAccountInformationschema, {
  ElementAccountInformation,
} from '@/schemas/ElementAccountInformationSchema'

const useStyles = makeStyles((theme) => ({
  form: {
    minHeight: '500px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  content: {
    padding: theme.spacing(4),
    paddingTop: 0,
    marginBottom: theme.spacing(4),
  },

  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
  },
  formButton: {
    borderRadius: 10,
    padding: '13px 22px',
    '& > span': {
      fontSize: 18,
      fontWeight: 600,
    },
  },
}))

const ElementAccountInformationForm = ({ onSuccess }: any) => {
  const classes = useStyles()

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ElementAccountInformation>({
    resolver: yupResolver(ElementAccountInformationschema),
  })

  const onSubmit = (data: ElementAccountInformation) => {
    onSuccess(data)
  }

  return (
    <Box component="form" width="100%" onSubmit={handleSubmit(onSubmit)}>
      <Container className={classes.content}>
        <Typography variant="h5" component="h3" gutterBottom>
          Dados Pessoais
        </Typography>
        <Grid container spacing={10}>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('fullName') }}
              error={!!errors.fullName}
              helperText={errors.fullName?.message}
              label="Nome Completo"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('companyRole') }}
              error={!!errors.companyRole}
              helperText={errors.companyRole?.message}
              label="Cargo"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('companyArea') }}
              error={!!errors.companyArea}
              helperText={errors.companyArea?.message}
              label="Área de trabalho"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('phone') }}
              error={!!errors.phone}
              helperText={errors.phone?.message}
              label="Telefone"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('mobile') }}
              error={!!errors.mobile}
              helperText={errors.mobile?.message}
              label="Celular"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
        </Grid>
      </Container>
      <div className={classes.footer}>
        <Button
          type="submit"
          className={classes.formButton}
          variant="contained"
          color="primary"
          fullWidth
          size="large"
        >
          Confirme
        </Button>
      </div>
    </Box>
  )
}
export default ElementAccountInformationForm
