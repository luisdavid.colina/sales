import { FC, useContext, useState } from 'react'
import { Paper } from '@material-ui/core'
import StepperContext, { StepperProvider } from '@/context/StepperContext'

import { Product } from '@/schemas/ProductSchema'

import defaultProduct from '@/forms/defaultStates/product'

import RuleCommissions from '@/components/RuleCommissions'
import ProductFormData from '@/forms/ProductFormData'
import SelectRuleForm from '@/forms/SelectRuleForm'
import RuleForm from '@/forms/RuleForm'

import { AxiosResponse } from 'axios'

interface StepFormProps {
  data: Product
}

const StepForm: FC<StepFormProps> = ({ data }) => {
  const { step } = useContext(StepperContext)

  switch (step) {
    case 1:
      return <ProductFormData product={data} />
    case 2:
      return <SelectRuleForm rule={data.rule} />
    case 3:
      return (
        <RuleForm product={data} disabled={data.rule.id ? true : undefined} />
      )
    case 4:
      return <RuleCommissions product={data} rule={data.rule} />
    default:
      return null
  }
}

type ProductFormProps = {
  data?: Product
  disabled?: boolean
  onSuccess: (data: Product) => Promise<AxiosResponse<any>>
  code?: number
  titleModal?: string
  actionModal?: () => void
}

const ProductForm: FC<ProductFormProps> = ({
  data = defaultProduct,
  disabled = false,
  onSuccess,
  code,
  titleModal,
  actionModal,
}) => {
  const [form, setData] = useState<Product>(data)

  function updateData(stepData: Partial<Product>) {
    if (stepData) {
      setData((prevData) => {
        return { ...prevData, ...stepData }
      })
    }
  }

  return (
    <Paper elevation={4}>
      <StepperProvider
        disabled={disabled}
        onChange={updateData}
        onSuccess={onSuccess}
        code={code}
        titleModal={titleModal}
        actionModal={actionModal}
      >
        <StepForm data={form} />
      </StepperProvider>
    </Paper>
  )
}

export default ProductForm
