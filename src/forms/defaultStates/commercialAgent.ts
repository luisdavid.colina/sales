import CommercialAgent from '@/types/CommercialAgent'
import company from './company'

const commercialAgent: CommercialAgent = {
  id: '0',
  company,
  salesCommissionId: '',
}

export default commercialAgent
