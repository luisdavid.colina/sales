const rule = {
  commissionType: '',
  participants: [],
  salesChannel: {
    id: '',
    name: '',
    participants: [],
  },
  paymentFrequency: 'monthly',
  months: 1,
}

export default rule
