const deliveryAddress = {
  postalCode: '',
  street: '',
  number: '',
  complement: '',
  district: '',
  city: '',
  state: '',
  receiver: '',
  deliveryAgent: '',
  isCentralizedDelivery: false,
  deliveryService: '',
}

export default deliveryAddress
