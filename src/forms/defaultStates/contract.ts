import Contract from '@/types/Contract'
import DeliveryAddress from '@/types/DeliveryAddress'
import company from './company'
import productDetails from './productDetails'
import revenue from './revenue'

const deliveryAddress: DeliveryAddress = {
  postalCode: '',
  street: '',
  number: '',
  complement: '',
  district: '',
  city: '',
  state: '',
  receiver: '',
  deliveryAgent: 'logistics',
  isCentralizedDelivery: true,
  deliveryService: 'flash_azul',
}

const contract: Contract = {
  id: '0',
  isActive: true,
  pdfKey: '',
  company,
  productDetails,
  deliveryAddress,
  revenue,
}

export default contract
