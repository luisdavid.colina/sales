const product = {
  code: '',
  name: '',
  productFamily: { id: '', name: '' },
  productCategory: { id: '', name: '' },
  rule: { id: '', name: '' },
}

export default product
