const revenue = {
  calculation: 'normal',
  currency: 'brl',
  paymentMethod: 'transfer',
  isInvoiceTopup: false,
  issuingCompanyId: 1,
  bankAccountId: 1,
  serviceCodeId: 1,
  signatureDate: new Date(),
  expirationDate: new Date(),
  isSigned: false,
}

export default revenue
