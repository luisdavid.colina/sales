const communication = {
  id: '',
  object: '',
  status: '',
  text: '',
  to: ['luisdavid.colina@gmail.com'],
  subject: '',
  sendingMethod: ['E-Mail', 'WhatsApp', 'SMS']
}

export default communication
