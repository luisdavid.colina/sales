import ProductDetails from '@/types/ProductDetails'
import company from './company'

const campaign = {
  name: '',
  rechargeFrequency: 'monthly',
  annualEstimatedAward: 'R$ 0,00',
  totalCommissionRate: 0,
  isAdministrationRate: true,
  // only when is_administration_rate is true
  administrationRate: '5.00',
  // only when is_administration_rate is false
  administrationFee: 'R$ 0,00',
  // only when product category is a card
  issueFee: 'R$ 0,00',
}

export const operatorFees = {
  // For virtual_card and transfer
  emitionTicket: 'R$ 3,50',
  pixPf: 'R$ 6,00',
  pixPj: 'R$ 6,00',
  lotteryDeposit: 'R$ 0,00',
  lotteryWithdrawal: 'R$ 0,00',
  PDV: '0,00',
  aditionalCard: 'R$ 0,00',
  expiredCard: 'R$ 0,00',
  codeQR: 'R$ 8,00',
  TED: 'R$ 9,00',
  SMS: 'R$ 0,00',
  // only for cards
  monthlyFee: 'R$ 0,00',
  unlockFee: 'R$ 0,00',
  reissueFee: 'R$ 0,00',
  chargebackFee: 'R$ 0,00',
  atmWithdrawFee: 'R$ 0,00',
  markupRate: '0,00',
  rechargePortalFee: 'R$ 0,00',
  rechargeInvoiceFee: 'R$ 0,00',
  p2pTransferFee: 'R$ 0,00',
  // only for transfers
  transferFee: 'R$ 0,00',
  rejectedTransactionFee: 'R$ 0,00',
  // only for checks
  checkFee: 'R$ 0,00',
  // only for vouchers
  voucherFee: 'R$ 0,00',
}

export const companyOperatingFees = {
  KYC: 'R$ 0,00',
  balanceTransferFee: 'R$ 0,00',
  minimumLoadAmount: 'R$ 0,00',
  belowMinimumLoadFee: 'R$ 0,00',
  emergencyLoadFee: 'R$ 0,00',
  specialHandlingFee: 'R$ 0,00',
  chargebackRate: '0,00',
  rejectedTransactionFee: 'R$ 0,00',
}

export const cardFees = {
  physicalCard: 'R$ 8,50',
  sendCard: 'R$ 0,00',
  cardWithdrawal: 'R$ 0,00',
}

const commercialAgent: ProductDetails['commercialAgent'] = {
  id: 'CsFaIoewgVLlz5mZOgSI',
  company: {
    admin: {
      companyArea: 'venda',
      companyRole: 'vendedor',
      cpf: '11111111111',
      fullName: 'John Doe',
      email: 'churi410@gmail.com',
      mobile: '5804269900709',
      phone: '5804269900709',
    },
    cnpj: '11111111111111',
    comments: '',
    email: 'churi410@gmail.com',
    legalName: ' Element Marketing e Participações',
    tradingName: ' Element Marketing e Participações',
    mobile: '5804269900709',
    phone: '5804269900709',
    website: 'www.churi.com.br',
  },
  salesCommissionId: 'ga3ze2XuOSX7FsD9T80e',
}

const productDetails: ProductDetails = {
  operatorFees,
  cardFees,
  campaign,
  product: 'prepaid_card',
  productBrand: 'mastercard',
  commercialAgent,
  // only for cards
  companyOperatingFees,
  salesChannel: 'direct',
  classAccount: 'basic',
  isGiftCard: false,
  kindAccount: 'virtual_card',
  isInternationalCard: true,
  cardQuantity: 1,
  isChipCard: true,
  issuer: 'acesso',
}

export default productDetails
