export const commission = {
  from: 0,
  to: 0,
  loads: 0,
  salesChannel: 'direto',
}

const salesCommission = {
  name: '',
  fixedCommissions: [{ ...commission }],
  ratedCommissions: [{ ...commission }],
}

export default salesCommission
