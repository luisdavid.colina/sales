import Proposal from '@/types/Proposal'
import company from './company'
import productDetails from './productDetails'

const proposal: Proposal = {
  company,
  productDetails,
}

export default proposal
