const multiplier = {
  commercialAgentLinkId: '',
  contactFullName: '',
  cnpj: '',
  companyLegalName: '',
  companyRole: '',
  companyArea: '',
  phone: '',
  mobile: '',
  email: '',
  product: '',
  totalIncome: '',
  commercialAgentsQuantity: '',
  paymentFrequency: '',
}

export default multiplier
