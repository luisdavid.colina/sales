import Company from '@/types/Company'
import contact from './contact'

const company: Company = {
  cnpj: '',
  legalName: '',
  email: '',
  website: '',
  comments: '',
  tradingName: '',
  phone: '',
  mobile: '',
  admin: contact,
}

export default company
