import { useRouter } from 'next/router'
import Grid from '@material-ui/core/Grid'
import Box from '@material-ui/core/Box'
import useSteps from '@/hooks/useSteps'
import AccountVerificationForm from '@/forms/AccountVerificationForm'
import AccountInformationForm from '@/forms/AccountInformationForm'
import AccountSecurityForm from '@/forms/AccountSecurityForm'
import SuccessModal from '@/components/SuccessModal'
import { makeStyles, createStyles } from '@material-ui/core/styles'
import { FC, useState } from 'react'
import Invitation from '@/types/Invitation'
import { AccountVerification } from '@/schemas/AccountVerificationSchema'
import { AccountSecurity } from '@/schemas/AccountSecuritySchema'
import SpinnerPage from '@/components/SpinnerPage'
import axios from 'axios'
import { Button, Typography } from '@material-ui/core'
import { ElementAccountInformation } from '@/schemas/ElementAccountInformationSchema'
import { AccountInformationFormData } from '@/schemas/AccountInformationSchema'
import ElementAccountInformationForm from './ElementAccountInformationForm'

const useStyles = makeStyles((theme) =>
  createStyles({
    sectionForm: {
      height: '100vh',
      width: '100vw',
      backgroundColor: theme.palette.background.paper,
    },
    backgroundImage: {
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center center',
    },
    form: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100%',
      position: 'relative',
      padding: theme.spacing(0, 5),
      margin: theme.spacing(0, 5),
      [theme.breakpoints.down('sm')]: {
        margin: theme.spacing(0, 0),
      },
    },
  }),
)

const steps = [
  'accountverificationform',
  'accountinformationform',
  'accountsecurityform',
]

interface AccountFormProps {
  invitation: Invitation
}

const defaultAccount = {
  email: '',
  cpf: '',
  password: '',
}

const AccountForm: FC<AccountFormProps> = ({ invitation }) => {
  const classes = useStyles()
  const [form, setForm] = useState<any>(defaultAccount)
  const { step, next } = useSteps({ steps, initialStep: 0 })
  const router = useRouter()
  const [creatingAccount, setCreatingAccount] = useState(false)
  const [success, setSuccess] = useState(false)
  const [close, setClose] = useState(false)

  async function createUserAccount(form: any) {
    const payload = {
      ...form,
      invitationId: invitation.id,
    }

    setCreatingAccount(true)

    try {
      const response = await axios.post(
        `/api/${invitation.category}/users`,
        payload,
      )

      if (response.status === 201) {
        setSuccess(true)
      }
    } catch (err) {
      const error = err as any

      // TODO: show an UI for the error
      // eslint-disable-next-line no-console
      console.error(error.message)
    }

    setCreatingAccount(false)
  }

  function saveEmailAndCpf(verification: AccountVerification) {
    const { email, cpf } = verification

    setForm((prevForm: any) => {
      const newForm = { ...prevForm, email, cpf }
      return newForm
    })
    next()
  }

  function handleInformationSuccess(information: AccountInformationFormData) {
    setForm((prevForm: any) => {
      const newForm = { ...prevForm, ...information }
      return newForm
    })
    next()
  }

  function saveElementAccountInformation(
    information: ElementAccountInformation,
  ) {
    setForm((prevForm: any) => {
      const newForm = { ...prevForm, ...information }
      return newForm
    })
    next()
  }

  async function handleSecuritySuccess(security: AccountSecurity) {
    const { password } = security
    setForm((prevForm: any) => {
      const newForm = { ...prevForm, password }
      return newForm
    })

    const data = { ...form, password }
    await createUserAccount(data)
  }
  const onClose = () => {
    setClose(true)
    setTimeout(() => {
      router.push('/login')
    }, 3000)
  }

  if (creatingAccount) {
    return <SpinnerPage />
  }

  return (
    <>
      <SuccessModal
        title="Cadastro realizado com sucesso!"
        open={success}
        onClose={onClose}
      />
      <Grid container alignItems="stretch">
        <Grid
          item
          lg={step !== 1 ? 4 : 12}
          md={step !== 1 ? 5 : 12}
          sm={step !== 1 ? 8 : 12}
          xs={12}
          component={Box}
          className={classes.sectionForm}
        >
          <div className={classes.form}>
            <Box
              p={10}
              position="relative"
              width="100%"
              height="100"
              style={{
                textAlign: 'center',
              }}
            >
              <img
                src="/assets/login/logo.svg"
                alt="Logo"
                style={{ width: '250px' }}
              />
            </Box>
            <Box p={8} />
            {step === 0 && (
              <AccountVerificationForm
                invitation={invitation}
                onSuccess={saveEmailAndCpf}
              />
            )}
            {step === 1 &&
              (invitation.category === 'company' ||
                invitation.category === 'commercialAgent') && (
                <AccountInformationForm
                  invitation={invitation}
                  onSuccess={handleInformationSuccess}
                />
              )}

            {step === 1 && invitation.category === 'element' && (
              <ElementAccountInformationForm
                onSuccess={saveElementAccountInformation}
              />
            )}

            {step === 2 && (
              <AccountSecurityForm
                invitation={invitation}
                onSuccess={handleSecuritySuccess}
              />
            )}
          </div>
        </Grid>
        <Grid
          item
          lg={8}
          md={7}
          sm={4}
          className={classes.backgroundImage}
          style={{
            backgroundImage: 'url(/assets/login/background.jpeg)',
          }}
        />
      </Grid>
      {close && (
        <div
          className={classes.backgroundImage}
          style={{
            position: 'absolute',
            width: '100vw',
            height: '100vh',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            left: '-20px',
            zIndex: 1,
            backgroundImage: 'url(/assets/login/success.jpeg)',
          }}
        >
          <Typography
            variant="h1"
            component="h2"
            style={{
              color: '#fefefe',
              fontWeight: 400,
              textShadow: '3px 3px #333',
            }}
          >
            Obrigado!
          </Typography>
        </div>
      )}
    </>
  )
}

export default AccountForm
