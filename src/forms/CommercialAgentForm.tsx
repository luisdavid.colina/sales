import { FC, useContext, useState } from 'react'
import { Paper } from '@material-ui/core'
import StepperContext, { StepperProvider } from '@/context/StepperContext'
import CommercialAgent from '@/types/CommercialAgent'
import { AxiosResponse } from 'axios'
import defaultCommercialAgent from './defaultStates/commercialAgent'
import CompanyForm from './CompanyForm'
import AgentSalesCommissionForm from './AgentSalesCommissionForm'
import CommercialAgentReviewForm from './CommercialAgentReviewForm'

interface StepFormProps {
  commercialAgent: CommercialAgent
}

const StepForm: FC<StepFormProps> = ({ commercialAgent }) => {
  const stepper = useContext(StepperContext)

  switch (stepper.step) {
    case 1:
      return <CompanyForm company={commercialAgent.company} />
    case 2:
      return (
        <AgentSalesCommissionForm
          salesCommissionId={commercialAgent.salesCommissionId}
        />
      )
    default:
      return <CommercialAgentReviewForm commercialAgent={commercialAgent} />
  }
}

type CommercialAgentFormProps = {
  commercialAgent?: CommercialAgent
  disabled?: boolean
  onSuccess: (commercialAgent: CommercialAgent) => Promise<AxiosResponse<any>>
  code?: number
  titleModal?: string
  actionModal?: () => void
}

const CommercialAgentForm: FC<CommercialAgentFormProps> = ({
  commercialAgent = defaultCommercialAgent,
  onSuccess,
  code,
  titleModal,
  actionModal,
}) => {
  const [data, setData] = useState<CommercialAgent>(commercialAgent)

  function updateData(stepData: Partial<CommercialAgent>) {
    if (stepData) {
      setData((prevData) => {
        return { ...prevData, ...stepData }
      })
    }
  }

  return (
    <Paper elevation={4}>
      <StepperProvider
        onChange={updateData}
        onSuccess={onSuccess}
        code={code}
        titleModal={titleModal}
        actionModal={actionModal}
      >
        <StepForm commercialAgent={data} />
      </StepperProvider>
    </Paper>
  )
}

export default CommercialAgentForm
