import ProductCategoryAutocomplete from '@/components/autocompletes/ProductCategoryAutocomplete'
import ProductFamilyAutocomplete from '@/components/autocompletes/ProductFamilyAutocomplete'
import { ProductDataSchema, ProductData } from '@/schemas/ProductSchema'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Container,
  Button,
  Grid,
  makeStyles,
  TextField,
  Typography,
} from '@material-ui/core'
import StepperContext from '@/context/StepperContext'
import { FC, useContext } from 'react'
import { Controller, useForm } from 'react-hook-form'

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  content: {
    padding: theme.spacing(5),
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
    '& > *': {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  },
}))

interface Props {
  product: ProductData
}

const ProductFormData: FC<Props> = ({ product }) => {
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: product,
    resolver: yupResolver(ProductDataSchema),
  })

  const classes = useStyles()
  const { next } = useContext(StepperContext)

  const submit = (data: Partial<ProductData>) => {
    next({ ...data })
  }

  return (
    <form onSubmit={handleSubmit(submit)} className={classes.form}>
      <Container className={classes.content}>
        <Grid container spacing={7}>
          <Grid item xs={12}>
            <Typography variant="h5" component="h3">
              Informação do produto
            </Typography>
          </Grid>
          <Grid item sm={12} md={3}>
            <TextField
              inputProps={{ ...register('code') }}
              error={!!errors.code}
              helperText={errors.code?.message}
              label="Código do Produto"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item sm={12} md={3}>
            <TextField
              inputProps={{ ...register('name') }}
              error={!!errors.name}
              helperText={errors.name?.message}
              label="Nome do Produto"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item sm={12} md={3}>
            <Controller
              control={control}
              name="productCategory"
              render={(props) => (
                <ProductCategoryAutocomplete
                  value={props.field.value}
                  onChange={props.field.onChange}
                  error={!!errors.productCategory}
                  helperText={errors.productCategory?.id?.message}
                />
              )}
            />
          </Grid>
          <Grid item sm={12} md={3}>
            <Controller
              control={control}
              name="productFamily"
              render={(props) => (
                <ProductFamilyAutocomplete
                  value={props.field.value}
                  onChange={props.field.onChange}
                  error={!!errors.productFamily}
                  helperText={errors.productFamily?.id?.message}
                />
              )}
            />
          </Grid>
        </Grid>
      </Container>
      <div className={classes.footer}>
        <Button size="large" type="submit" variant="contained" color="primary">
          Avançar
        </Button>
      </div>
    </form>
  )
}

export default ProductFormData
