import { useState } from 'react'
import { useRouter } from 'next/router'
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid'
import useSteps from '@/hooks/useSteps'
import { auth } from '@/lib/firebase'
import axios from 'axios'
import useStyles from './AuthForm.styles'
import LoginForm from './LoginForm'
import TwoFactor from './TwoFactorForm'

type Credentials = {
  email: string
  password: string
}

const defaultCredentials = {
  email: '',
  password: '',
}

const steps = ['login', '2FA']

const AuthForm = () => {
  const classes = useStyles()
  const router = useRouter()

  const { step, next } = useSteps({ steps, initialStep: 0 })
  const [loading, setLoading] = useState(false)
  const [credentials, setCredentials] = useState<Credentials>(
    defaultCredentials,
  )

  function redirect() {
    setLoading(true)
    setTimeout(() => {
      router.push('/dashboard')
    }, 1000)
  }

  async function handleLoginSuccess(data: Credentials) {
    const { email, password } = data
    setCredentials({ ...credentials, email, password })
    next()
  }

  async function handleTwoAuthSuccess(authToken: string) {
    try {
      await auth.signInWithCustomToken(authToken)
      redirect()
    } catch (_) {
      // TODO: show error feedback
    }
  }

  return (
    <>
      {loading ? (
        <Box
          width="100vw"
          height="100vh"
          display="flex"
          justifyContent="center"
          alignItems="center"
        >
          <img
            src="/assets/login/icon.svg"
            alt="icon"
            width="100"
            height="100"
            className={classes.logo}
          />
          <Box component="span" pl={7} />
          <img
            src="/assets/login/text.svg"
            alt="name"
            width="200"
            height="100"
          />
        </Box>
      ) : (
        <Grid container alignItems="stretch">
          <Grid
            item
            lg={4}
            md={5}
            sm={8}
            xs={12}
            component={Box}
            className={classes.sectionForm}
          >
            <div className={classes.form}>
              <Box
                p={10}
                position="relative"
                width="100%"
                height="100"
                style={{
                  textAlign: 'center',
                }}
              >
                <img
                  src="/assets/login/logo.svg"
                  alt="Logo"
                  style={{ width: '250px' }}
                />
              </Box>
              <Box p={10} />
              {step === 0 && <LoginForm onSuccess={handleLoginSuccess} />}
              {step === 1 && (
                <TwoFactor
                  credentials={credentials}
                  onSuccess={handleTwoAuthSuccess}
                  email={credentials.email}
                />
              )}
            </div>
          </Grid>
          <Grid
            item
            lg={8}
            md={7}
            sm={4}
            className={classes.backgroundImage}
            style={{
              backgroundImage: 'url(/assets/login/background.jpeg)',
            }}
          />
        </Grid>
      )}
    </>
  )
}

export default AuthForm
