import axios from 'axios'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import FormControl from '@material-ui/core/FormControl'
import Box from '@material-ui/core/Box'
import InputAdornment from '@material-ui/core/InputAdornment'
import MailOutlineRoundedIcon from '@material-ui/icons/MailOutlineRounded'
import LockOpenRoundedIcon from '@material-ui/icons/LockOpenRounded'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  TextFieldOutlined as TextField,
  InputLabel,
  HelperText,
} from '@/components/common/TextFieldOutlined'
import { LoginSchema } from '@/schemas/AuthSchema'
import { useState } from 'react'
import useStyles from './AuthForm.styles'

type formData = {
  email: string
  password: string
}

const LoginForm = ({ onSuccess }: any) => {
  const classes = useStyles()
  const [errorMessage, setErrorMessage] = useState('')

  const {
    register,
    handleSubmit,
    formState: { isDirty, errors, isSubmitting },
    getValues,
  } = useForm<formData>({
    resolver: yupResolver(LoginSchema),
  })

  const onSubmit = async (data: formData) => {
    try {
      const response = await axios.post('/api/auth/check', data)
      if (response.status === 200) {
        onSuccess(data)
      }
    } catch (err) {
      const error = err as any

      setErrorMessage('Credenciais erradas')
    }
  }

  const resetPassword = async () => {
    if (getValues('email') !== '') {
      try {
        const data = { email: getValues('email') }
        const response = await axios.post('/api/auth/reset', data)
        if (response.status === 200) {
          setErrorMessage('O link de recuperação foi enviado para seu e-mail.')
        }
      } catch (err) {
        const error = err as any
        setErrorMessage('Credenciais erradas')
      }
    } else {
      setErrorMessage('Preencha o campo de e-mail')
    }
  }

  return (
    <>
      <Box component="form" width="100%" onSubmit={handleSubmit(onSubmit)}>
        <FormControl
          fullWidth
          variant="outlined"
          color="primary"
          error={!!errors.email}
        >
          <InputLabel htmlFor="email" shrink>
            Usuário
          </InputLabel>
          <TextField
            id="username"
            inputProps={{
              style: {
                paddingLeft: '60px',
              },
            }}
            {...register('email')}
          />
          <InputAdornment
            position="start"
            style={{
              position: 'absolute',
              margin: ' 28px 28px 28px 14px',
              opacity: '0.7',
            }}
          >
            <MailOutlineRoundedIcon color="primary" fontSize="large" />
          </InputAdornment>
          <HelperText>{errors?.email?.message}</HelperText>
        </FormControl>
        <Box p={7} />
        <FormControl
          fullWidth
          variant="outlined"
          color="primary"
          error={!!errors.password}
        >
          <InputLabel htmlFor="password" shrink>
            Senha
          </InputLabel>
          <TextField
            id="password"
            type="password"
            inputProps={{
              style: {
                paddingLeft: '60px',
              },
            }}
            {...register('password')}
          />
          <InputAdornment
            position="start"
            style={{
              position: 'absolute',
              margin: ' 28px 28px 28px 14px',
              opacity: '0.7',
            }}
          >
            <LockOpenRoundedIcon color="primary" fontSize="large" />
          </InputAdornment>
          <HelperText component="span">
            <Typography color="primary">
              <a onClick={resetPassword} style={{ cursor: 'pointer' }}>
                Esqueci a minha senha{' '}
              </a>
            </Typography>
          </HelperText>
        </FormControl>
        <Box p={4}>
          <HelperText error>{errorMessage}</HelperText>
        </Box>
        <Button
          type="submit"
          className={classes.formButton}
          variant="contained"
          color="primary"
          disabled={!isDirty || isSubmitting}
          fullWidth
          size="large"
        >
          Entrar
        </Button>
      </Box>
    </>
  )
}

export default LoginForm
