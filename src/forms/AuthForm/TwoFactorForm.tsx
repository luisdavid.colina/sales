import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import Box from '@material-ui/core/Box'
import InfoRoundedIcon from '@material-ui/icons/InfoRounded'
import Tooltip from '@material-ui/core/Tooltip'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useState } from 'react'
import Typography from '@material-ui/core/Typography'
import { firestore } from '@/lib/firebase'
import { useCollectionData } from 'react-firebase-hooks/firestore'
import Communication from '@/types/Communication'
import {
  TextFieldOutlined as TextField,
  InputLabel,
  HelperText,
} from '@/components/common/TextFieldOutlined'
import { TwoFactorSchema } from '@/schemas/AuthSchema'
import axios from 'axios'
import useStyles from './AuthForm.styles'

type formData = {
  otp: string
}

const TwoFactor = ({ credentials, onSuccess, email }: any) => {
  const classes = useStyles()
  const [errorMessage, setErrorMessage] = useState('')
  const {
    setError,
    register,
    handleSubmit,
    formState: { isDirty, errors, isSubmitting },
  } = useForm<formData>({
    resolver: yupResolver(TwoFactorSchema),
  })

  const onSubmit = async (data: formData) => {
    const { otp } = data
    const authCredentials = { ...credentials, otp }

    try {
      const response = await axios.post('/api/auth/login', authCredentials)
      if (response.status === 200) {
        const { authToken } = response.data
        onSuccess(authToken)
      }
    } catch (err) {
      const error = err as any

      if (error.response?.data) {
        const { data } = error.response

        if (data.error === 'otp') {
          setError('otp', { type: 'manual', message: 'Código inválido' })
        }
      }
    }
  }
  const queryUsers = firestore.collection('users')
  const [users, loadingUsers] = useCollectionData<any>(queryUsers)

  const resetQR = async () => {
    try {
      const user = users?.find((user) => user.email === email)

      const communication: Communication = {
        id: '1',
        object: '',
        text: `Baixar em ${user?.twoFactorAuth.qr}`,
        to: [user?.email],
        status: 'Enviado',
        subject: `Recuperar QR - Sales Ecosystem`,
      }
      const resp = await axios.post('/api/mail', communication)
      if (resp.status === 201) {
        setErrorMessage('O QR foi enviado para seu e-mail.')
      }
    } catch (err) {
      const error = err as any

      setErrorMessage('Error')
    }
  }

  return (
    <Box component="form" width="100%" onSubmit={handleSubmit(onSubmit)}>
      <FormControl
        fullWidth
        variant="outlined"
        color="primary"
        error={!!errors.otp}
      >
        <InputLabel style={{ pointerEvents: "auto" }} htmlFor="otp" shrink>
          Autenticação de dois fatores
          <Tooltip
            title="Veja o seu código no Google Authenticator do seu celular ou navegador"
            placement="top"
            arrow
          >
            <InfoRoundedIcon />
          </Tooltip>
        </InputLabel>
        <TextField id="otp" type="number" {...register('otp')} />
        <HelperText>{errors?.otp?.message}</HelperText>
      </FormControl>
      <HelperText component="span">
        <Typography color="primary">
          <a onClick={resetQR} style={{ cursor: 'pointer' }}>
            Recuperar QR
          </a>
        </Typography>
      </HelperText>
      <Box p={4} />
      <Box p={4}>
        <HelperText error>{errorMessage}</HelperText>
      </Box>
      <Button
        type="submit"
        className={classes.formButton}
        variant="contained"
        color="primary"
        disabled={!isDirty || isSubmitting}
        fullWidth
        size="large"
      >
        Entrar
      </Button>
    </Box>
  )
}

export default TwoFactor
