import { useState } from 'react'
import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import Box from '@material-ui/core/Box'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import {
  TextFieldOutlined as TextField,
  InputLabel,
  HelperText,
} from '@/components/common/TextFieldOutlined'

import AccountSecuritySchema, {
  AccountSecurity,
} from '@/schemas/AccountSecuritySchema'

import { makeStyles, createStyles } from '@material-ui/core/styles'
import { Grid, Typography } from '@material-ui/core'

const useStyles = makeStyles(
  createStyles({
    formButton: {
      borderRadius: 10,
      padding: '13px 22px',
      '& > span': {
        fontSize: 18,
        fontWeight: 600,
      },
    },
    paper: {
      display: 'flex',
      flexWrap: 'wrap',
      '& > *': {
        width: 16,
        height: 16,
      },
    },
  }),
)

const AccountVerificationForm = ({ invitation, onSuccess }: any) => {
  const classes = useStyles()
  const [step, setStep] = useState(false)

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<AccountSecurity>({
    resolver: yupResolver(AccountSecuritySchema),
  })

  const onSubmit = (data: AccountSecurity) => {
    if (step) onSuccess(data)
    else setStep(true)
  }

  return (
    <Box component="form" onSubmit={handleSubmit(onSubmit)} style={{ width: '100%' }}>
      <Box display="flex" alignItems="flex-start" justifyContent="center">
        {!step && (
          <Box>
            <Box p={7}>
              <FormControl
                fullWidth
                variant="outlined"
                color="primary"
                error={!!errors.password}
              >
                <InputLabel htmlFor="password" shrink>
                  Senha
                </InputLabel>
                <TextField
                  id="password"
                  type="password"
                  {...register('password')}
                />
              </FormControl>
            </Box>
            <Box p={7}>
              <FormControl
                fullWidth
                variant="outlined"
                color="primary"
                error={!!errors.password}
              >
                <InputLabel htmlFor="password" shrink>
                  Confirmar senha
                </InputLabel>
                <TextField
                  id="password"
                  type="password"
                  {...register('passwordConfirmation')}
                />
                <HelperText error>
                  <b>A sua senha deve conter:</b>
                </HelperText>
                <HelperText error>• No mínimo oito caracteres</HelperText>

                <HelperText error>• Letra maiúscula</HelperText>
                <HelperText error>
                  • Letra minúscula, um número e um caractere
                </HelperText>
                <HelperText error>• Caracter especiais</HelperText>
              </FormControl>
            </Box>
          </Box>
        )}
        {step && (
          <Box>
            <Box p={7}>
              <HelperText error>
                • Entre na loja de aplicativos do seu <b> smartphone</b>
              </HelperText>
              <HelperText error>
                • Instale o <b>Google Authenticator</b> em seu celular
              </HelperText>
              <HelperText error>
                • Leia o <b>Código QR</b>
              </HelperText>
            </Box>
            <br />
            <Box marginLeft="50px" p={7}>
              <Typography>Código QR</Typography>
              <img
                src={invitation.twoFactorAuth.qr}
                height="180px"
                width="180px"
                alt="codigo qr"
              />
            </Box>
          </Box>
        )}
      </Box>
      <Box p={4}>
        <Button
          type="submit"
          className={classes.formButton}
          variant="contained"
          color="primary"
          fullWidth
          size="large"
        >
          Confirmar
        </Button>
      </Box>
    </Box>
  )
}

export default AccountVerificationForm
