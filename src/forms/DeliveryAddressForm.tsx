import {
  DELIVERY_AGENTS,
  DELIVERY_SERVICES,
  hasDeliveryServices,
  STATES,
} from '@/constants'
import StepperContext from '@/context/StepperContext'
import DeliveryAddressSchema from '@/schemas/DeliveryAddressSchema'
import DeliveryAddress from '@/types/DeliveryAddress'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Button,
  Container,
  FormControlLabel,
  Grid,
  makeStyles,
  MenuItem,
  Switch,
  TextField,
  Typography,
  InputBaseComponentProps,
} from '@material-ui/core'
import { FC, useContext, useEffect, ChangeEvent, useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import NumberFormat from 'react-number-format'
import axios from 'axios'

const useStyles = makeStyles((theme) => ({
  form: {
    minHeight: '500px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  header: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(8),
  },
  content: {
    padding: theme.spacing(4),
  },
  section: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(8),
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
    '& > *': {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  },
  zipContainer: {
    marginBottom: theme.spacing(4),
  },
}))

interface Props {
  deliveryAddress: DeliveryAddress
}

interface DeliveryAddressCEP {
  cep: string
  logradouro: string
  complemento: string
  bairro: string
  localidade: string
  uf: string
  ibge: string
  gia: string
  ddd: string
  siafi: string
}

const DeliveryAddressCEPDefault: DeliveryAddressCEP = {
  cep: '',
  logradouro: '',
  complemento: '',
  bairro: '',
  localidade: '',
  uf: '',
  ibge: '',
  gia: '',
  ddd: '',
  siafi: '',
}

const DeliveryAddressForm: FC<Props> = ({ deliveryAddress }) => {
  const {
    register,
    handleSubmit,
    control,
    trigger,
    formState: { errors },
    watch,
    setValue,
  } = useForm({
    defaultValues: deliveryAddress,
    resolver: yupResolver(DeliveryAddressSchema),
  })
  const classes = useStyles()
  const stepper = useContext(StepperContext)

  const submit = (data: DeliveryAddress) => {
    stepper.next({ deliveryAddress: data })
  }

  const { deliveryAgent } = watch()

  const [cep, setCep] = useState<DeliveryAddressCEP>(DeliveryAddressCEPDefault)

  const handleChangeStreet = (e: ChangeEvent<HTMLInputElement>) => {
    setCep({
      ...cep,
      logradouro: String(e.target.value),
    })
  }
  const handleChangeDistrict = (e: ChangeEvent<HTMLInputElement>) => {
    setCep({
      ...cep,
      bairro: String(e.target.value),
    })
  }
  const handleChangeCity = (e: ChangeEvent<HTMLInputElement>) => {
    setCep({
      ...cep,
      localidade: String(e.target.value),
    })
  }
  const handleChangeComplement = (e: ChangeEvent<HTMLInputElement>) => {
    setCep({
      ...cep,
      complemento: String(e.target.value),
    })
  }

  useEffect(() => {
    if (deliveryAgent) {
      if (hasDeliveryServices(deliveryAgent)) {
        const newDeliveryService = DELIVERY_SERVICES[deliveryAgent].find(
          (service) => service.default,
        )
        setValue(
          'deliveryService',
          newDeliveryService ? newDeliveryService.value : '',
        )
      } else {
        setValue('deliveryService', '')
      }
    }
  }, [deliveryAgent])

  const getCEP = async (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault()
    const CEP = String(e.target.value)
    setCep({
      ...cep,
      cep: CEP,
    })
    if (
      (CEP.length === 8 && CEP[5] !== '-') ||
      (CEP.length === 9 && CEP[5] === '-')
    ) {
      try {
        const { data, status } = await axios.get(
          `https://viacep.com.br/ws/${CEP}/json/`,
        )

        if (status === 200) {
          setCep(data)
          setValue('street', data.logradouro)
          trigger('street')
          setValue('district', data.bairro)
          trigger('district')
          setValue('city', data.localidade)
          trigger('city')
          setValue('complement', data.complemento)
          trigger('complement')
          setValue('state', data.uf)
          trigger('state')
        }
      } catch (err) {
        const error = err as any

        // eslint-disable-next-line no-console
        console.error(error.message)
      }
    }
  }
  return (
    <form className={classes.form} onSubmit={handleSubmit(submit)}>
      <Container className={classes.content}>
        <Typography
          className={classes.header}
          variant="h5"
          component="h3"
          gutterBottom
        >
          Endereço de faturamento
        </Typography>
        <Grid container spacing={10} className={classes.zipContainer}>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('postalCode') }}
              error={!!errors.postalCode}
              helperText={errors.postalCode?.message}
              label="CEP"
              variant="outlined"
              size="small"
              value={cep.cep}
              onChange={getCEP}
              autoFocus
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <FormControlLabel
              label="Entrega centralizada?"
              labelPlacement="start"
              control={
                <Controller
                  control={control}
                  name="isCentralizedDelivery"
                  render={(props) => (
                    <Switch
                      onChange={(e) => props.field.onChange(e.target.checked)}
                      checked={props.field.value}
                      color="primary"
                    />
                  )}
                />
              }
            />
          </Grid>
        </Grid>
        <Grid container spacing={10}>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              label="Logradouro"
              value={cep.logradouro}
              variant="outlined"
              size="small"
              fullWidth
              inputProps={{ ...register('street') }}
              error={!!errors.street}
              helperText={errors.street?.message}
              onChange={handleChangeStreet}
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              id="district"
              name="district"
              type="text"
              value={cep.bairro}
              inputProps={{ ...register('district') }}
              error={!!errors.district}
              helperText={errors.district?.message}
              label="Bairro"
              variant="outlined"
              size="small"
              fullWidth
              onChange={handleChangeDistrict}
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('city') }}
              error={!!errors.city}
              helperText={errors.city?.message}
              label="Cidade"
              value={cep.localidade}
              onChange={handleChangeCity}
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <Controller
              control={control}
              name="state"
              render={(props) => (
                <TextField
                  select
                  onChange={(e) => props.field.onChange(e.target.value)}
                  value={props.field.value || cep.uf}
                  error={!!errors.state}
                  helperText={errors.state?.message}
                  label="Estado"
                  variant="outlined"
                  size="small"
                  fullWidth
                >
                  {STATES.map((state) => (
                    <MenuItem key={state} value={state}>
                      {state}
                    </MenuItem>
                  ))}
                </TextField>
              )}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('number') }}
              error={!!errors.number}
              helperText={errors.number?.message}
              label="Numero S/N"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={8} lg={9}>
            <TextField
              inputProps={{ ...register('complement') }}
              error={!!errors.complement}
              helperText={errors.complement?.message}
              label="Complemento"
              variant="outlined"
              size="small"
              value={cep.complemento}
              onChange={handleChangeComplement}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            <Controller
              control={control}
              name="deliveryAgent"
              render={(props) => (
                <TextField
                  select
                  onChange={(e) => props.field.onChange(e.target.value)}
                  value={props.field.value}
                  error={!!errors.deliveryAgent}
                  helperText={errors.deliveryAgent?.message}
                  label="Agente de Entrega"
                  variant="outlined"
                  size="small"
                  fullWidth
                >
                  {DELIVERY_AGENTS.map(({ label, value }) => (
                    <MenuItem key={value} value={value}>
                      {label}
                    </MenuItem>
                  ))}
                </TextField>
              )}
            />
          </Grid>
          {hasDeliveryServices(deliveryAgent) && (
            <Grid item xs={12} sm={6} md={4}>
              <Controller
                control={control}
                name="deliveryService"
                render={(props) => (
                  <TextField
                    select
                    onChange={(e) => props.field.onChange(e.target.value)}
                    value={props.field.value}
                    error={!!errors.deliveryService}
                    helperText={errors.deliveryService?.message}
                    label="Serviço de entrega"
                    variant="outlined"
                    size="small"
                    fullWidth
                  >
                    {DELIVERY_SERVICES[deliveryAgent].map(
                      ({ label, value }) => (
                        <MenuItem key={value} value={value}>
                          {label}
                        </MenuItem>
                      ),
                    )}
                  </TextField>
                )}
              />
            </Grid>
          )}
          <Grid item xs={12} sm={6} md={4}>
            <TextField
              inputProps={{ ...register('receiver') }}
              error={!!errors.receiver}
              helperText={errors.receiver?.message}
              name="receiver"
              label="Recebedor"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
        </Grid>
      </Container>
      <div className={classes.footer}>
        <Button onClick={stepper.prev} type="button" variant="contained">
          Voltar
        </Button>
        <Button type="submit" variant="contained" color="primary">
          Avançar
        </Button>
      </div>
    </form>
  )
}

export default DeliveryAddressForm
