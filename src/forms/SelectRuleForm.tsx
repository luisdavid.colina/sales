import RuleAutocomplete from '@/components/autocompletes/RuleAutocomplete'
import { RuleDataSchema, RuleData } from '@/schemas/ProductSchema'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Button,
  Grid,
  makeStyles,
  Container,
  Typography,
} from '@material-ui/core'
import StepperContext from '@/context/StepperContext'
import { FC, useContext } from 'react'
import { Controller, useForm } from 'react-hook-form'

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  content: {
    padding: theme.spacing(5),
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
    '& > *': {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  },
}))

type Props = RuleData

const ProductForm: FC<Props> = ({ rule }) => {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: { rule },
    resolver: yupResolver(RuleDataSchema),
  })

  const classes = useStyles()
  const { next, prev } = useContext(StepperContext)

  const submit = (data: RuleData) => {
    if (data.rule.id) {
      next({ rule: data.rule })
    } else {
      next({ rule: data.rule })
    }
  }

  return (
    <form onSubmit={handleSubmit((e) => submit(e))} className={classes.form}>
      <Container className={classes.content}>
        <Grid container spacing={7}>
          <Grid item xs={12}>
            <Typography variant="h5" component="h3">
              Selecione Regra
            </Typography>
          </Grid>
          <Grid item sm={12}>
            <Controller
              control={control}
              name="rule"
              render={(props) => (
                <RuleAutocomplete
                  value={props.field.value}
                  onChange={(e: any) => props.field.onChange(e)}
                  error={!!errors.rule}
                  helperText={errors.rule?.id?.message}
                />
              )}
            />
          </Grid>
        </Grid>
      </Container>
      <div className={classes.footer}>
        <Button onClick={prev} type="button" variant="contained">
          Retornar
        </Button>
        <Button size="large" type="submit" variant="contained" color="primary">
          Avançar
        </Button>
      </div>
    </form>
  )
}

export default ProductForm
