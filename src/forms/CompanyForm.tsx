import { FC, useContext } from 'react'
import { useForm } from 'react-hook-form'
import {
  Button,
  Container,
  Grid,
  makeStyles,
  TextField,
  Typography,
} from '@material-ui/core'
import StepperContext from '@/context/StepperContext'
import { yupResolver } from '@hookform/resolvers/yup'
import CompanySchema from '@/schemas/CompanySchema'
import Company from '@/types/Company'
import { Contract } from '@/schemas/ContractSchema'

const useStyles = makeStyles((theme) => ({
  form: {
    minHeight: '500px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  content: {
    padding: theme.spacing(4),
  },
  section: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(8),
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
  },
}))

interface Props {
  company: Company
}

const CompanyForm: FC<Props> = ({ company }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: company,
    resolver: yupResolver(CompanySchema),
  })
  const classes = useStyles()
  const stepper = useContext(StepperContext)

  const submit = (data: Partial<Contract>) => {
    stepper.next({ company: data })
  }

  return (
    <form className={classes.form} onSubmit={handleSubmit(submit)}>
      <Container className={classes.content}>
        <div className={classes.section}>
          <Typography variant="h5" component="h3" gutterBottom>
            Dados Empresariais
          </Typography>
          <Grid container spacing={10}>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <TextField
                inputProps={{ ...register('cnpj') }}
                error={!!errors.cnpj}
                helperText={errors.cnpj?.message}
                label="CNPJ"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <TextField
                inputProps={{ ...register('legalName') }}
                error={!!errors.legalName}
                helperText={errors.legalName?.message}
                label="Razão Social"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <TextField
                inputProps={{ ...register('tradingName') }}
                error={!!errors.tradingName}
                helperText={errors.tradingName?.message}
                label="Nome Fantasia"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <TextField
                inputProps={{ ...register('email') }}
                error={!!errors.email}
                helperText={errors.email?.message}
                label="Email"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <TextField
                inputProps={{ ...register('phone') }}
                error={!!errors.phone}
                helperText={errors.phone?.message}
                label="Telefone"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <TextField
                inputProps={{ ...register('mobile') }}
                error={!!errors.mobile}
                helperText={errors.mobile?.message}
                label="Celular"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <TextField
                inputProps={{ ...register('website') }}
                error={!!errors.website}
                helperText={errors.website?.message}
                label="Website URL"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4} lg={3}>
              <TextField
                inputProps={{ ...register('comments') }}
                error={!!errors.comments}
                helperText={errors.comments?.message}
                label="Comentarios"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
          </Grid>
        </div>
        <div className={classes.section}>
          <Typography variant="h5" component="h3" gutterBottom>
            Dados Pessoais
          </Typography>
          <Grid container spacing={10}>
            <Grid item xs={12} sm={6} md={4}>
              <TextField
                inputProps={{ ...register('admin.fullName') }}
                error={!!errors.admin?.fullName}
                helperText={errors.admin?.fullName?.message}
                label="Nome Completo"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextField
                inputProps={{ ...register('admin.cpf') }}
                error={!!errors.admin?.cpf}
                helperText={errors.admin?.cpf?.message}
                label="CPF"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={4}>
              <TextField
                inputProps={{ ...register('admin.companyRole') }}
                error={!!errors.admin?.companyRole}
                helperText={errors.admin?.companyRole?.message}
                label="Cargo"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <TextField
                inputProps={{ ...register('admin.companyArea') }}
                error={!!errors.admin?.companyArea}
                helperText={errors.admin?.companyArea?.message}
                label="Área de trabalho"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <TextField
                inputProps={{ ...register('admin.phone') }}
                error={!!errors.admin?.phone}
                helperText={errors.admin?.phone?.message}
                label="Telefone"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <TextField
                inputProps={{ ...register('admin.mobile') }}
                error={!!errors.admin?.mobile}
                helperText={errors.admin?.mobile?.message}
                label="Celular"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
            <Grid item xs={12} sm={6} md={3}>
              <TextField
                inputProps={{ ...register('admin.email') }}
                error={!!errors.admin?.email}
                helperText={errors.admin?.email?.message}
                label="Email"
                variant="outlined"
                size="small"
                fullWidth
              />
            </Grid>
          </Grid>
        </div>
      </Container>
      <div className={classes.footer}>
        <Button type="submit" variant="contained" color="primary">
          Avançar
        </Button>
      </div>
    </form>
  )
}
export default CompanyForm
