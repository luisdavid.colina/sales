import Button from '@material-ui/core/Button'
import FormControl from '@material-ui/core/FormControl'
import Box from '@material-ui/core/Box'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import {
  TextFieldOutlined as TextField,
  InputLabel,
  HelperText,
} from '@/components/common/TextFieldOutlined'

import schema, {
  AccountVerification,
} from '@/schemas/AccountVerificationSchema'

import { makeStyles, createStyles } from '@material-ui/core/styles'
import Invitation from '@/types/Invitation'

const useStyles = makeStyles(
  createStyles({
    formButton: {
      borderRadius: 10,
      padding: '13px 22px',
      '& > span': {
        fontSize: 18,
        fontWeight: 600,
      },
    },
  }),
)

interface AccountVerificationForm {
  invitation: Invitation
  onSucces: (data: AccountVerification) => void
}

const AccountVerificationForm = ({ invitation, onSuccess }: any) => {
  const classes = useStyles()

  const {
    register,
    handleSubmit,
    setError,
    formState: { errors },
  } = useForm<AccountVerification>({
    resolver: yupResolver(schema),
  })

  const validateInvitation = (data: AccountVerification) => {
    const matchEmail = data.email === invitation.email
    const matchCpf = data.cpf === invitation.cpf

    if (matchEmail && matchCpf) return true

    if (!matchEmail) {
      setError('email', {
        type: 'manual',
        message: 'O email não coincide com o convite',
      })
    }

    if (!matchCpf) {
      setError('cpf', {
        type: 'manual',
        message: 'O CPF não coincide com o convite',
      })
    }

    return false
  }

  const onSubmit = (data: AccountVerification) => {
    if (validateInvitation(data)) {
      onSuccess(data)
    }
  }

  return (
    <>
      <Box
        component="form"
        onSubmit={handleSubmit(onSubmit)}
        style={{ width: '100%' }}
      >
        <FormControl
          fullWidth
          variant="outlined"
          color="primary"
          error={!!errors.cpf}
        >
          <InputLabel htmlFor="cpf" shrink>
            CPF
          </InputLabel>
          <TextField id="cpf" {...register('cpf')} />
          <HelperText error>Somemente número</HelperText>
          <HelperText>{errors?.cpf?.message}</HelperText>
        </FormControl>
        <Box p={7} />

        <FormControl
          fullWidth
          variant="outlined"
          color="primary"
          error={!!errors.email}
        >
          <InputLabel htmlFor="email" shrink>
            E-mail
          </InputLabel>
          <TextField id="email" type="email" {...register('email')} />
          <HelperText>{errors?.email?.message}</HelperText>
        </FormControl>
        <Box p={4} />
        <Button
          type="submit"
          className={classes.formButton}
          variant="contained"
          color="primary"
          fullWidth
          size="large"
        >
          Confirmar
        </Button>
      </Box>
    </>
  )
}

export default AccountVerificationForm
