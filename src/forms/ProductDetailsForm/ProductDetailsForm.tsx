import { FC, useContext, useEffect, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import StepperContext from '@/context/StepperContext'
import ProductDetailsSchema from '@/schemas/ProductDetailsSchema'
import ProductDetails from '@/types/ProductDetails'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Box,
  Button,
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Grid,
  InputAdornment,
  makeStyles,
  MenuItem,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from '@material-ui/core'
import { Autocomplete } from '@material-ui/lab'
import {
  NumberFormatCustom,
  NumberFormatRate,
} from '@/components/NumberFormatCustom'

import { useCollectionDataOnce } from 'react-firebase-hooks/firestore'
import { PRODUCTS } from '@/constants'

import Company from '@/types/Company'
import { firestore } from '@/lib/firebase'
import SpinnerPage from '@/components/SpinnerPage'
import cardFees from '@/forms/defaultStates/cardFees'
import operatorFees from '@/forms/defaultStates/productDetails'
import { PrepaidCardLeft, PrepaidCardRight } from './PrepaidCard'
import { DigitalAccountLeft, DigitalAccountRight } from './DigitalAccount'
import { TransferLeft, TransferRight } from './Transfer'

type ProductDetailsKeys = keyof ProductDetails

const useStyles = makeStyles((theme) => ({
  form: {
    minHeight: '500px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  section: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(8),
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
    '& > *': {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  },
}))

interface Props {
  productDetails: ProductDetails
}

type CommercialAgentType = {
  id?: string
  company: Company
}

const ProductDetailsForm: FC<Props> = ({ productDetails }) => {
  const [productDetailsState, setProductDetailsState] = useState({
    ...productDetails,
  })
  const form = useForm({
    mode: 'onChange',
    defaultValues: productDetails,
    resolver: yupResolver(ProductDetailsSchema),
  })
  const {
    register,
    handleSubmit,
    watch,
    control,
    setValue,
    formState: { errors },
  } = form
  const commercialAgentsQuery = firestore.collection('commercialAgents')

  const [
    commercialAgents,
    loadingCommercialAgents,
  ] = useCollectionDataOnce<CommercialAgentType>(commercialAgentsQuery)

  const classes = useStyles()
  const stepper = useContext(StepperContext)

  const submit = (data: ProductDetails) => {
    stepper.next({ productDetails: data })
  }

  const product = watch('product')
  const { productBrand } = watch()

  useEffect(() => {
    switch (productBrand) {
      case 'mastercard':
        setValue('campaign.issueFee', cardFees.mastercard.operatorFees.price)
        setProductDetailsState({
          ...productDetailsState,
          campaign: {
            ...productDetailsState.campaign,
            issueFee: cardFees.mastercard.operatorFees.price,
          },
        })
        break
      default:
        setValue('campaign.issueFee', cardFees.visa.operatorFees.price)
        setProductDetailsState({
          ...productDetailsState,
          campaign: {
            ...productDetailsState.campaign,
            issueFee: cardFees.visa.operatorFees.price,
          },
        })
        break
    }
  }, [productBrand])

  useEffect(() => {
    if (product === 'digital_account') {
      setValue('campaign.issueFee', cardFees.visa.operatorFees.price)
      setProductDetailsState({
        ...productDetailsState,
        campaign: {
          ...productDetailsState.campaign,
          issueFee: cardFees.visa.operatorFees.price,
        },
        operatorFees: {
          ...productDetailsState.operatorFees,
          ...operatorFees,
        },
      })
    }
    if (product === 'transfer') {
      setValue('productBrand', '')
      setProductDetailsState({
        ...productDetailsState,
        productBrand: '',
      })
    }
  }, [product])

  const isAdministrationRate = watch('campaign.isAdministrationRate')

  if (loadingCommercialAgents) {
    return <SpinnerPage />
  }

  return (
    <form className={classes.form} onSubmit={handleSubmit(submit)}>
      <Grid container>
        <Grid item lg={6} md={6} sm={12} xs={12}>
          <Box p={7}>
            <Grid container spacing={4}>
              <Grid item xs={12}>
                <Typography variant="h5" component="h3">
                  Detalhes do Produto
                </Typography>
              </Grid>
              <Grid item lg={6} md={6} sm={6} xs={12}>
                <Controller
                  control={control}
                  name="product"
                  render={(props) => (
                    <TextField
                      onChange={(e) => {
                        Object.keys(productDetails).forEach((v: any) => {
                          const value: ProductDetailsKeys = v
                          setValue(value, productDetails[value])
                        })
                        return props.field.onChange(e.target.value)
                      }}
                      value={props.field.value}
                      fullWidth
                      select
                      variant="outlined"
                      size="small"
                      label="Categoria do Produto"
                      error={!!errors.product}
                      helperText={errors.product?.message}
                    >
                      {PRODUCTS.map((p) => (
                        <MenuItem key={p.value} value={p.value}>
                          {p.label}
                        </MenuItem>
                      ))}
                    </TextField>
                  )}
                />
              </Grid>
              {product === 'prepaid_card' && <PrepaidCardLeft form={form} />}
              {product === 'digital_account' && (
                <DigitalAccountLeft form={form} />
              )}
              {product === 'transfer' && <TransferLeft form={form} />}

              <Grid item xs={12}>
                <hr />
              </Grid>
              <Grid item lg={12} md={12} sm={6} xs={12}>
                <TextField
                  inputProps={{ ...register('campaign.name') }}
                  variant="outlined"
                  size="small"
                  fullWidth
                  label="Nome de Campanha"
                  name="campaign"
                  error={!!errors.campaign?.name}
                  helperText={errors.campaign?.name?.message}
                />
              </Grid>
              <Grid item lg={6} md={6} sm={12} xs={12}>
                <FormControl
                  component="fieldset"
                  size="small"
                  error={!!errors.campaign?.isAdministrationRate}
                >
                  <FormLabel component="legend">Honorários em</FormLabel>

                  <Controller
                    control={control}
                    name="campaign.isAdministrationRate"
                    render={(props) => (
                      <RadioGroup
                        onChange={(e) => {
                          props.field.onChange(e.target.value === 'true')
                        }}
                        value={String(props.field.value)}
                        aria-label="Possui"
                        row
                      >
                        <FormControlLabel
                          value="true"
                          control={<Radio />}
                          label="Percentual"
                        />
                        <FormControlLabel
                          value="false"
                          control={<Radio />}
                          label="Fixo"
                        />
                      </RadioGroup>
                    )}
                  />
                  <FormHelperText>
                    {errors.campaign?.isAdministrationRate?.message}
                  </FormHelperText>
                </FormControl>
              </Grid>
              <Grid item lg={6} md={6} sm={3} xs={5}>
                <Grid item xs={12}>
                  {isAdministrationRate && (
                    <TextField
                      variant="outlined"
                      size="small"
                      fullWidth
                      label="Taxa Adm"
                      value={productDetailsState.campaign.administrationRate}
                      onChange={(e) => {
                        setProductDetailsState({
                          ...productDetailsState,
                          campaign: {
                            ...productDetailsState.campaign,
                            administrationRate: e.target.value,
                          },
                        })
                      }}
                      error={!!errors.campaign?.administrationRate}
                      helperText={errors.campaign?.administrationRate?.message}
                      InputProps={{
                        inputComponent: NumberFormatRate,
                        endAdornment: (
                          <InputAdornment position="end">%</InputAdornment>
                        ),
                        inputProps: {
                          ...register('campaign.administrationRate'),
                        },
                      }}
                    />
                  )}
                  {!isAdministrationRate && (
                    <TextField
                      variant="outlined"
                      size="small"
                      fullWidth
                      label="Taxa Adm"
                      value={productDetailsState.campaign.administrationFee}
                      onChange={(e: any) => {
                        setProductDetailsState({
                          ...productDetailsState,
                          campaign: {
                            ...productDetailsState.campaign,
                            administrationFee: e.target.value,
                          },
                        })
                      }}
                      error={!!errors.campaign?.administrationFee}
                      helperText={errors.campaign?.administrationFee?.message}
                      InputProps={{
                        inputComponent: NumberFormatCustom,
                        inputProps: {
                          ...register('campaign.administrationFee'),
                        },
                      }}
                    />
                  )}
                </Grid>
                <Grid item xs={12}>
                  <Box marginTop={8}>
                    <TextField
                      variant="outlined"
                      size="small"
                      fullWidth
                      label="Premiação Anual Estimada"
                      InputLabelProps={{ shrink: true }}
                      onChange={(e: any) => {
                        setProductDetailsState({
                          ...productDetailsState,
                          campaign: {
                            ...productDetailsState.campaign,
                            annualEstimatedAward: e.target.value,
                          },
                        })
                      }}
                      value={productDetailsState.campaign.annualEstimatedAward}
                      error={!!errors.campaign?.annualEstimatedAward}
                      helperText={
                        errors.campaign?.annualEstimatedAward?.message
                      }
                      InputProps={{
                        inputComponent: NumberFormatCustom,
                        inputProps: {
                          ...register('campaign.annualEstimatedAward'),
                        },
                      }}
                    />
                  </Box>
                </Grid>
                {product === 'prepaid_card' && (
                  <Grid item xs={12}>
                    <Box marginTop={8}>
                      <TextField
                        variant="outlined"
                        size="small"
                        fullWidth
                        label="Preço do Cartão"
                        value={productDetailsState.campaign.issueFee}
                        onChange={(e: any) => {
                          setProductDetailsState({
                            ...productDetailsState,
                            campaign: {
                              ...productDetailsState.campaign,
                              issueFee: e.target.value,
                            },
                          })
                        }}
                        error={!!errors.campaign?.issueFee}
                        helperText={errors.campaign?.issueFee?.message}
                        InputProps={{
                          inputComponent: NumberFormatCustom,
                          inputProps: {
                            ...register('campaign.issueFee'),
                          },
                        }}
                        InputLabelProps={{ shrink: true }}
                      />
                    </Box>
                  </Grid>
                )}
                {product === 'digital_account' && (
                  <Grid item xs={12}>
                    <Box marginTop={8}>
                      <TextField
                        variant="outlined"
                        size="small"
                        fullWidth
                        InputLabelProps={{ shrink: true }}
                        label="Preço da Conta"
                        value={productDetailsState.campaign.issueFee}
                        onChange={(e: any) => {
                          setProductDetailsState({
                            ...productDetailsState,
                            campaign: {
                              ...productDetailsState.campaign,
                              issueFee: e.target.value,
                            },
                          })
                        }}
                        error={!!errors.campaign?.issueFee}
                        helperText={errors.campaign?.issueFee?.message}
                        InputProps={{
                          inputComponent: NumberFormatCustom,
                          inputProps: {
                            ...register('campaign.issueFee'),
                          },
                        }}
                      />
                    </Box>
                  </Grid>
                )}
              </Grid>
              <Grid item xs={12}>
                <FormControl
                  component="fieldset"
                  size="small"
                  error={!!errors.campaign?.rechargeFrequency}
                >
                  <FormLabel component="legend">Frequência</FormLabel>

                  <Controller
                    control={control}
                    name="campaign.rechargeFrequency"
                    render={(props) => (
                      <RadioGroup
                        onChange={(e) => {
                          props.field.onChange(e.target.value)
                        }}
                        value={props.field.value}
                        aria-label="Possui"
                        row
                      >
                        <FormControlLabel
                          value="monthly"
                          control={<Radio />}
                          label="Mensal"
                        />
                        <FormControlLabel
                          value="quarterly"
                          control={<Radio />}
                          label="Trimestral"
                        />
                        <FormControlLabel
                          value="yearly"
                          control={<Radio />}
                          label="Anual"
                        />
                        <FormControlLabel
                          value="only"
                          control={<Radio />}
                          label="Única"
                        />
                      </RadioGroup>
                    )}
                  />
                  <FormHelperText>
                    {errors.campaign?.rechargeFrequency?.message}
                  </FormHelperText>
                </FormControl>
              </Grid>
              {commercialAgents && commercialAgents.length > 0 && (
                <Grid item xs={12}>
                  <Box p={{ xs: 3, sm: 3, md: 4, lg: 5 }}>
                    <Controller
                      control={control}
                      name="commercialAgent"
                      render={(props) => (
                        <Autocomplete
                          disableClearable
                          value={props.field.value as CommercialAgentType}
                          options={commercialAgents}
                          getOptionLabel={(option: any) =>
                            option.company.legalName
                          }
                          getOptionSelected={(option: any) =>
                            option.id === props.field.value.id
                          }
                          onChange={(_, data) => {
                            props.field.onChange({ ...data })
                          }}
                          size="small"
                          renderInput={(params) => (
                            <TextField
                              {...params}
                              variant="outlined"
                              label="Associar parceiro comercial"
                            />
                          )}
                        />
                      )}
                    />
                  </Box>
                  <FormHelperText error>
                    {errors.commercialAgent?.id?.message}
                  </FormHelperText>
                </Grid>
              )}
            </Grid>
          </Box>
        </Grid>
        {product === 'prepaid_card' && <PrepaidCardRight form={form} />}
        {product === 'transfer' && <TransferRight form={form} />}
        {product === 'digital_account' && <DigitalAccountRight form={form} />}
      </Grid>

      <div className={classes.footer}>
        <Button onClick={stepper.prev} type="button" variant="contained">
          Voltar
        </Button>
        <Button type="submit" variant="contained" color="primary">
          Avançar
        </Button>
      </div>
    </form>
  )
}

export default ProductDetailsForm
