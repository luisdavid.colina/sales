import { FC, useEffect, useState, ChangeEvent } from 'react'
import {
  Box,
  Grid,
  TextField as MuiTextField,
  Typography,
  InputAdornment,
  makeStyles,
} from '@material-ui/core'
import { TextFieldProps } from '@material-ui/core/TextField'
import cardFees from '@/forms/defaultStates/cardFees'
import {
  NumberFormatCustom,
  NumberFormatRate,
} from '@/components/NumberFormatCustom'
import { operatorFees } from '@/forms/defaultStates/productDetails'

import CompanyOperatingFees from '@/types/CompanyOperatingFees'
import { FormProps } from '../FormProps'

const TextField: FC<TextFieldProps> = (props) => {
  return <MuiTextField variant="outlined" size="small" fullWidth {...props} />
}

const useStyles = makeStyles({
  commissions: {
    background: 'white',
  },
})

const PrepaidCardRight: FC<FormProps> = ({ form }) => {
  const {
    register,
    watch,
    setValue,
    formState: { errors },
    getValues,
  } = form
  const classes = useStyles()

  const { productBrand } = watch()

  const [operatorFee, setOperatorFee] = useState<any>({ ...operatorFees })

  const [cardFee, setCardFee] = useState<any>(getValues('cardFees'))

  const [
    companyOperatingFee,
    setCompanyOperatingFee,
  ] = useState<CompanyOperatingFees>(cardFees.default.companyOperatingFees)

  function handleChange(op: string) {
    if (op === 'operatorFees.monthlyFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          monthlyFee: String(e.target.value),
        })
      }
    }
    if (op === 'companyOperatingFees.KYC') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setCompanyOperatingFee({
          ...companyOperatingFee,
          KYC: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.emitionTicket') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          emitionTicket: String(e.target.value),
        })
      }
    }
    if (op === 'companyOperatingFees.pixPf') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          pixPf: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.lotteryDeposit') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          lotteryDeposit: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.lotteryWithdrawal') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          lotteryWithdrawal: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.aditionalCard') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          aditionalCard: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.expiredCard') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          expiredCard: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.codeQR') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          codeQR: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.TED') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          TED: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.SMS') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          SMS: String(e.target.value),
        })
      }
    }
    return (e: ChangeEvent<HTMLInputElement>) => {
      e.preventDefault()
      setOperatorFee({
        ...operatorFee,
      })
    }
  }

  useEffect(() => {
    switch (productBrand) {
      case 'visa':
        setValue('operatorFees', cardFees.visa.operatorFees)
        setValue('companyOperatingFees', cardFees.visa.companyOperatingFees)
        setOperatorFee(cardFees.visa.operatorFees)
        setOperatorFee({
          ...operatorFee,
          monthlyFee: 'R$ 8,00',
        })
        setCompanyOperatingFee(cardFees.visa.companyOperatingFees)
        break
      default:
        setValue('productBrand', 'visa')
        break
    }
  }, [productBrand])

  return (
    <Grid className={classes.commissions} item lg={6} md={6} sm={12} xs={12}>
      <Box
        p={{ xs: 3, sm: 3, md: 4, lg: 5 }}
        mb={{ xs: 3, sm: 3, md: 4, lg: 5 }}
      >
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography
              variant="subtitle1"
              color="primary"
              paragraph
              align="center"
            >
              Tarifas Operacionais Empresa
            </Typography>
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.companyOperatingFees?.KYC}
              helperText={
                errors.companyOperatingFees?.KYC?.message || 'KYC PF/PJ'
              }
              value={companyOperatingFee.KYC}
              onChange={handleChange('companyOperatingFees.KYC')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('companyOperatingFees.KYC'),
                },
              }}
            />
          </Grid>
        </Grid>
      </Box>

      <Box p={{ xs: 3, sm: 3, md: 4, lg: 5 }}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography
              variant="subtitle1"
              color="primary"
              paragraph
              align="center"
            >
              Tarifas Operacionais Portador
            </Typography>
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.operatorFees?.monthlyFee}
              helperText={
                errors.operatorFees?.monthlyFee?.message || 'Mensalidade'
              }
              value={operatorFee.monthlyFee}
              onChange={handleChange('operatorFees.monthlyFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.monthlyFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.operatorFees?.emitionTicket}
              helperText={
                errors.operatorFees?.emitionTicket?.message || 'Emissão Boleto'
              }
              value={operatorFee.emitionTicket}
              onChange={handleChange('operatorFees.emitionTicket')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.emitionTicket'),
                },
              }}
            />
          </Grid>

          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.operatorFees?.lotteryDeposit}
              helperText={
                errors.operatorFees?.lotteryDeposit?.message ||
                'Depósito Lotérica'
              }
              value={operatorFee.lotteryDeposit}
              onChange={handleChange('operatorFees.lotteryDeposit')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.lotteryDeposit'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.operatorFees?.TED}
              helperText={errors.operatorFees?.TED?.message || 'TED'}
              value={operatorFee.TED}
              onChange={handleChange('operatorFees.TED')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.TED'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.operatorFees?.pixPf}
              helperText={errors.operatorFees?.pixPf?.message || 'Pix PF'}
              value={operatorFee.pixPf}
              onChange={handleChange('operatorFees.pixPf')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.pixPf'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.operatorFees?.pixPj}
              helperText={errors.operatorFees?.pixPj?.message || 'Pix PJ'}
              value={operatorFee.pixPj}
              onChange={handleChange('operatorFees.pixPj')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.pixPj'),
                },
              }}
            />
          </Grid>

          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.operatorFees?.lotteryWithdrawal}
              helperText={
                errors.operatorFees?.lotteryWithdrawal?.message ||
                'Saque Lotérica'
              }
              value={operatorFee.lotteryWithdrawal}
              onChange={handleChange('operatorFees.lotteryWithdrawal')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.lotteryWithdrawal'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.operatorFees?.PDV}
              helperText={errors.operatorFees?.PDV?.message || 'Saque PDV'}
              InputProps={{
                inputComponent: NumberFormatRate,
                endAdornment: <InputAdornment position="end">%</InputAdornment>,
                inputProps: {
                  ...register('operatorFees.PDV'),
                },
              }}
              value={operatorFee.PDV}
              onChange={(e) => {
                setOperatorFee({
                  ...operatorFee,
                  PDV: e.target.value,
                })
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.operatorFees?.codeQR}
              helperText={errors.operatorFees?.codeQR?.message || 'QR Code'}
              value={operatorFee.codeQR}
              onChange={handleChange('operatorFees.codeQR')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.codeQR'),
                },
              }}
            />
          </Grid>

          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.operatorFees?.SMS}
              helperText={errors.operatorFees?.SMS?.message || 'SMS'}
              value={operatorFee.SMS}
              onChange={handleChange('operatorFees.SMS')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.SMS'),
                },
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <Typography
              variant="subtitle1"
              color="primary"
              paragraph
              align="center"
            >
              Tarifas Cartão Físico
            </Typography>
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              disabled
              error={!!errors.cardFees?.physicalCard}
              helperText={
                errors.cardFees?.physicalCard?.message || 'Cartão Físico'
              }
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('cardFees.physicalCard'),
                },
              }}
              value={cardFee.physicalCard}
              onChange={(e) => {
                setCardFee({
                  ...cardFee,
                  physicalCard: e.target.value,
                })
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.cardFees?.sendCard}
              helperText={
                errors.cardFees?.sendCard?.message || 'Envio de Cartão'
              }
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('cardFees.sendCard'),
                },
              }}
              value={cardFee.sendCard}
              onChange={(e) => {
                setCardFee({
                  ...cardFee,
                  sendCard: e.target.value,
                })
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.cardFees?.cardWithdrawal}
              helperText={
                errors.cardFees?.cardWithdrawal?.message || 'Saque ATM (B24H)'
              }
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('cardFees.cardWithdrawal'),
                },
              }}
              value={cardFee.cardWithdrawal}
              onChange={(e) => {
                setCardFee({
                  ...cardFee,
                  cardWithdrawal: e.target.value,
                })
              }}
            />
          </Grid>
        </Grid>
      </Box>
    </Grid>
  )
}

export default PrepaidCardRight
