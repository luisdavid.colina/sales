import { UseFormReturn } from 'react-hook-form'
import ProductDetails from '@/types/ProductDetails'

export type FormProps = {
  form: UseFormReturn<ProductDetails>
}
