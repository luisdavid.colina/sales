import { FC, useEffect } from 'react'
import { Controller } from 'react-hook-form'
import {
  FormControl,
  FormControlLabel,
  FormHelperText,
  FormLabel,
  Grid,
  MenuItem,
  Radio,
  RadioGroup,
  TextField,
} from '@material-ui/core'

import CheckboxForm from '@/components/common/Checkbox'
import {
  ISSUERS,
  ISSUERS_MC,
  ISSUERS_VISA,
  CARD_BRANDS_PRODUCT,
} from '@/constants'
import { FormProps } from '../FormProps.d'

const PrepaidCardLeft: FC<FormProps> = ({ form }) => {
  const {
    register,
    watch,
    control,
    getValues,
    formState: { errors },
  } = form

  const { productBrand } = watch()

  return (
    <>
      <Grid item lg={6} md={6} sm={6} xs={12}>
        <Controller
          control={control}
          name="issuer"
          render={(props) => (
            <TextField
              onChange={(e) => props.field.onChange(e.target.value)}
              value={props.field.value}
              fullWidth
              variant="outlined"
              size="small"
              select
              label="Emissor"
              error={!!errors.issuer}
              helperText={errors.issuer?.message}
            >
              {productBrand === 'mastercard' &&
                ISSUERS_MC.map((issuer) => (
                  <MenuItem key={issuer.value} value={issuer.value}>
                    {issuer.label}
                  </MenuItem>
                ))}
              {productBrand === 'visa' &&
                ISSUERS_VISA.map((issuer) => (
                  <MenuItem key={issuer.value} value={issuer.value}>
                    {issuer.label}
                  </MenuItem>
                ))}
              {!(productBrand === 'visa' || productBrand === 'mastercard') &&
                ISSUERS.map((issuer) => (
                  <MenuItem key={issuer.value} value={issuer.value}>
                    {issuer.label}
                  </MenuItem>
                ))}
            </TextField>
          )}
        />
      </Grid>
      <Grid item xs={12}>
        <Controller
          control={control}
          name="productBrand"
          render={(props) => (
            <TextField
              onChange={(e) => props.field.onChange(e.target.value)}
              value={props.field.value}
              variant="outlined"
              select
              size="small"
              label="Bandeira"
              defaultValue=""
              error={!!errors.productBrand}
              helperText={errors.productBrand?.message}
              fullWidth
            >
              {CARD_BRANDS_PRODUCT.prepaidCard.map((brand) => (
                <MenuItem key={brand.id} value={brand.value}>
                  {brand.label}
                </MenuItem>
              ))}
            </TextField>
          )}
        />
      </Grid>
      <Grid item xs={12}>
        <TextField
          inputProps={{
            ...register('cardQuantity'),
            step: 1,
            min: 1,
          }}
          variant="outlined"
          size="small"
          fullWidth
          type="number"
          label="Quantidade de Cartões"
          error={!!errors.cardQuantity}
          helperText={errors.cardQuantity?.message}
        />
      </Grid>
      <Grid item xs={12}>
        <FormControl
          component="fieldset"
          size="small"
          error={!!errors.isChipCard}
        >
          <FormLabel component="legend">Possui</FormLabel>
          <Controller
            control={control}
            name="isChipCard"
            render={(props) => (
              <RadioGroup
                onChange={(e) => {
                  props.field.onChange(e.target.value === 'true')
                }}
                value={String(props.field.value)}
                aria-label="Possui"
                row
              >
                <FormControlLabel
                  value="true"
                  control={<Radio />}
                  label="Chip"
                />
                <FormControlLabel
                  value="false"
                  control={<Radio />}
                  label="NFC"
                />
              </RadioGroup>
            )}
          />
          <FormHelperText>{errors.isChipCard?.message}</FormHelperText>
        </FormControl>
      </Grid>
      <Grid item xs={12}>
        <FormLabel component="legend">Cartão</FormLabel>
        <FormControl
          component="fieldset"
          size="small"
          error={!!errors.isInternationalCard}
        >
          <Controller
            control={control}
            name="isInternationalCard"
            render={(props) => (
              <RadioGroup
                onChange={(e) => {
                  props.field.onChange(e.target.value === 'true')
                }}
                value={String(props.field.value)}
                aria-label="Possui"
                row
              >
                <FormControlLabel
                  value="true"
                  control={<Radio />}
                  label="Internacional"
                />
                <FormControlLabel
                  value="false"
                  control={<Radio />}
                  label="Nacional"
                />
              </RadioGroup>
            )}
          />
          <FormHelperText>{errors.isInternationalCard?.message}</FormHelperText>
        </FormControl>
        <FormControlLabel
          label="Vale Presente"
          labelPlacement="end"
          style={{ float: 'right' }}
          control={
            <CheckboxForm
              {...register('isGiftCard')}
              defaultValue={getValues('isGiftCard')}
            />
          }
        />
      </Grid>
    </>
  )
}

export default PrepaidCardLeft
