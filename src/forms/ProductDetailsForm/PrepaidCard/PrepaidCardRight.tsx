import { FC, useEffect, ChangeEvent, useState } from 'react'
import {
  Box,
  Grid,
  TextField,
  Typography,
  InputAdornment,
  makeStyles,
  InputBaseComponentProps,
} from '@material-ui/core'
import {
  NumberFormatCustom,
  NumberFormatRate,
} from '@/components/NumberFormatCustom'
import cardFees from '@/forms/defaultStates/cardFees'
import OperatorFees from '@/types/OperatorFees'
import CompanyOperatingFees from '@/types/CompanyOperatingFees'
import { FormProps } from '../FormProps.d'

const useStyles = makeStyles({
  commissions: {
    background: 'white',
  },
})

const PrepaidCardRight: FC<FormProps> = ({ form }) => {
  const {
    register,
    watch,
    setValue,
    formState: { errors },
  } = form
  const classes = useStyles()

  const { productBrand } = watch()

  const [operatorFee, setOperatorFee] = useState<OperatorFees>(
    cardFees.default.operatorFees,
  )

  const [
    companyOperatingFee,
    setCompanyOperatingFee,
  ] = useState<CompanyOperatingFees>(cardFees.default.companyOperatingFees)

  function handleChange(op: string) {
    if (op === 'companyOperatingFees.balanceTransferFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setCompanyOperatingFee({
          ...companyOperatingFee,
          balanceTransferFee: String(e.target.value),
        })
      }
    }
    if (op === 'companyOperatingFees.minimumLoadAmount') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setCompanyOperatingFee({
          ...companyOperatingFee,
          minimumLoadAmount: String(e.target.value),
        })
      }
    }
    if (op === 'companyOperatingFees.belowMinimumLoadFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setCompanyOperatingFee({
          ...companyOperatingFee,
          belowMinimumLoadFee: String(e.target.value),
        })
      }
    }
    if (op === 'companyOperatingFees.emergencyLoadFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setCompanyOperatingFee({
          ...companyOperatingFee,
          emergencyLoadFee: String(e.target.value),
        })
      }
    }
    if (op === 'companyOperatingFees.specialHandlingFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setCompanyOperatingFee({
          ...companyOperatingFee,
          specialHandlingFee: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.monthlyFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          monthlyFee: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.unlockFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          unlockFee: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.reissueFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          reissueFee: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.chargebackFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          chargebackFee: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.atmWithdrawFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          atmWithdrawFee: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.rechargePortalFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          rechargePortalFee: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.rechargeInvoiceFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          rechargeInvoiceFee: String(e.target.value),
        })
      }
    }
    if (op === 'operatorFees.p2pTransferFee') {
      return (e: ChangeEvent<HTMLInputElement>) => {
        setOperatorFee({
          ...operatorFee,
          p2pTransferFee: String(e.target.value),
        })
      }
    }
    return (e: ChangeEvent<HTMLInputElement>) => {
      setOperatorFee({
        ...operatorFee,
      })
    }
  }

  useEffect(() => {
    switch (productBrand) {
      case 'mastercard':
        setValue('operatorFees', cardFees.mastercard.operatorFees)
        setValue(
          'companyOperatingFees',
          cardFees.mastercard.companyOperatingFees,
        )
        setCompanyOperatingFee(cardFees.mastercard.companyOperatingFees)
        setOperatorFee(cardFees.mastercard.operatorFees)
        break
      case 'visa':
        setValue('operatorFees', cardFees.visa.operatorFees)
        setValue('companyOperatingFees', cardFees.visa.companyOperatingFees)
        setCompanyOperatingFee(cardFees.visa.companyOperatingFees)
        setOperatorFee(cardFees.visa.operatorFees)
        break
      default:
        setValue('operatorFees', cardFees.default.operatorFees)
        setValue('companyOperatingFees', cardFees.default.companyOperatingFees)
        setOperatorFee(cardFees.default.operatorFees)
        break
    }
  }, [productBrand])

  return (
    <Grid className={classes.commissions} item lg={6} md={6} sm={12} xs={12}>
      <Box
        p={{ xs: 3, sm: 3, md: 4, lg: 5 }}
        mb={{ xs: 3, sm: 3, md: 4, lg: 5 }}
      >
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography
              variant="subtitle1"
              color="primary"
              paragraph
              align="center"
            >
              Tarifas Operacionais Empresa
            </Typography>
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.companyOperatingFees?.balanceTransferFee}
              helperText={
                errors.companyOperatingFees?.balanceTransferFee?.message ||
                'Transferência de Saldo de Cartão'
              }
              value={companyOperatingFee.balanceTransferFee}
              onChange={handleChange('companyOperatingFees.balanceTransferFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('companyOperatingFees.balanceTransferFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.companyOperatingFees?.minimumLoadAmount}
              helperText={
                errors.companyOperatingFees?.minimumLoadAmount?.message ||
                'Valor de Carga Mínima'
              }
              value={companyOperatingFee.minimumLoadAmount}
              onChange={handleChange('companyOperatingFees.minimumLoadAmount')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('companyOperatingFees.minimumLoadAmount'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.companyOperatingFees?.belowMinimumLoadFee}
              helperText={
                errors.companyOperatingFees?.belowMinimumLoadFee?.message ||
                'Carga inferior ao Mínimo'
              }
              value={companyOperatingFee.belowMinimumLoadFee}
              onChange={handleChange(
                'companyOperatingFees.belowMinimumLoadFee',
              )}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('companyOperatingFees.belowMinimumLoadFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.companyOperatingFees?.emergencyLoadFee}
              helperText={
                errors.companyOperatingFees?.emergencyLoadFee?.message ||
                'Carga de Emergência'
              }
              value={companyOperatingFee.emergencyLoadFee}
              onChange={handleChange('companyOperatingFees.emergencyLoadFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('companyOperatingFees.emergencyLoadFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.companyOperatingFees?.specialHandlingFee}
              helperText={
                errors.companyOperatingFees?.specialHandlingFee?.message ||
                'Manuseio'
              }
              value={companyOperatingFee.specialHandlingFee}
              onChange={handleChange('companyOperatingFees.specialHandlingFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('companyOperatingFees.specialHandlingFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.companyOperatingFees?.chargebackRate}
              helperText={
                errors.companyOperatingFees?.chargebackRate?.message ||
                'Taxa de Estorno'
              }
              InputProps={{
                inputComponent: NumberFormatRate,
                endAdornment: <InputAdornment position="end">%</InputAdornment>,
                inputProps: {
                  ...register('companyOperatingFees.chargebackRate'),
                },
              }}
              value={companyOperatingFee.chargebackRate}
              onChange={(e) => {
                setCompanyOperatingFee({
                  ...companyOperatingFee,
                  chargebackRate: e.target.value,
                })
              }}
            />
          </Grid>
        </Grid>
      </Box>

      <Box p={{ xs: 3, sm: 3, md: 4, lg: 5 }}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography
              variant="subtitle1"
              color="primary"
              paragraph
              align="center"
            >
              Tarifas Operacionais Portador
            </Typography>
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.operatorFees?.monthlyFee}
              helperText={
                errors.operatorFees?.monthlyFee?.message || 'Mensalidade'
              }
              value={operatorFee.monthlyFee}
              onChange={handleChange('operatorFees.monthlyFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.monthlyFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.operatorFees?.unlockFee}
              helperText={
                errors.operatorFees?.unlockFee?.message ||
                'Desbloqueio de Senha'
              }
              value={operatorFee.unlockFee}
              onChange={handleChange('operatorFees.unlockFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.unlockFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.operatorFees?.reissueFee}
              helperText={
                errors.operatorFees?.reissueFee?.message || 'Emissão de 2ª Via'
              }
              value={operatorFee.reissueFee}
              onChange={handleChange('operatorFees.reissueFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.reissueFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.operatorFees?.chargebackFee}
              helperText={
                errors.operatorFees?.chargebackFee?.message || 'Estorno'
              }
              value={operatorFee.chargebackFee}
              onChange={handleChange('operatorFees.chargebackFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.chargebackFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.operatorFees?.atmWithdrawFee}
              helperText={
                errors.operatorFees?.atmWithdrawFee?.message || 'Saque'
              }
              value={operatorFee.atmWithdrawFee}
              onChange={handleChange('operatorFees.atmWithdrawFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.atmWithdrawFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.operatorFees?.markupRate}
              helperText={
                errors.operatorFees?.markupRate?.message || 'Taxa de Markup'
              }
              InputProps={{
                inputComponent: NumberFormatRate,
                endAdornment: <InputAdornment position="end">%</InputAdornment>,
                inputProps: {
                  ...register('operatorFees.markupRate'),
                },
              }}
              value={operatorFee.markupRate}
              onChange={(e) => {
                setOperatorFee({
                  ...operatorFee,
                  markupRate: e.target.value,
                })
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.operatorFees?.rechargePortalFee}
              helperText={
                errors.operatorFees?.rechargePortalFee?.message ||
                'Recarga pelo Portal'
              }
              value={operatorFee.rechargePortalFee}
              onChange={handleChange('operatorFees.rechargePortalFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.rechargePortalFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.operatorFees?.rechargeInvoiceFee}
              helperText={
                errors.operatorFees?.rechargeInvoiceFee?.message ||
                'Recarga por N.F.'
              }
              value={operatorFee.rechargeInvoiceFee}
              onChange={handleChange('operatorFees.rechargeInvoiceFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.rechargeInvoiceFee'),
                },
              }}
            />
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              variant="outlined"
              size="small"
              fullWidth
              error={!!errors.operatorFees?.p2pTransferFee}
              helperText={
                errors.operatorFees?.p2pTransferFee?.message || 'Transferência'
              }
              value={operatorFee.p2pTransferFee}
              onChange={handleChange('operatorFees.p2pTransferFee')}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('operatorFees.p2pTransferFee'),
                },
              }}
            />
          </Grid>
        </Grid>
      </Box>
    </Grid>
  )
}

export default PrepaidCardRight
