import { FC } from 'react'
import { Grid, MenuItem, TextField } from '@material-ui/core'

import { SALES_CHANELS } from '@/constants'
import { FormProps } from '../FormProps'

const PrepaidCardLeft: FC<FormProps> = ({ form }) => {
  const {
    register,
    formState: { errors },
  } = form
  return (
    <>
      <Grid item lg={6} md={6} sm={6} xs={12}>
        <TextField
          select
          fullWidth
          size="small"
          variant="outlined"
          label="Canal de Vendas"
          error={!!errors.salesChannel}
          helperText={errors.salesChannel?.message}
          SelectProps={{
            ...register('salesChannel'),
          }}
        >
          {SALES_CHANELS.map(({ value, label }) => (
            <MenuItem key={value} value={value}>
              {label}
            </MenuItem>
          ))}
        </TextField>
      </Grid>
    </>
  )
}

export default PrepaidCardLeft
