import { FC, useEffect, useState, ChangeEvent } from 'react'
import {
  Box,
  Grid,
  TextField as MuiTextField,
  Typography,
  InputAdornment,
  makeStyles,
} from '@material-ui/core'
import { TextFieldProps } from '@material-ui/core/TextField'
import cardFees from '@/forms/defaultStates/cardFees'
import { NumberFormatCustom } from '@/components/NumberFormatCustom'
import { operatorFees } from '@/forms/defaultStates/productDetails'

import CompanyOperatingFees from '@/types/CompanyOperatingFees'
import { FormProps } from '../FormProps'

const TextField: FC<TextFieldProps> = (props) => {
  return <MuiTextField variant="outlined" size="small" fullWidth {...props} />
}

const useStyles = makeStyles({
  commissions: {
    background: 'white',
  },
})

const TransferRight: FC<FormProps> = ({ form }) => {
  const {
    register,
    setValue,
    formState: { errors },
    getValues,
  } = form
  const classes = useStyles()

  const [rejectedTransactionFee, setRejectedTransactionFee] = useState<
    CompanyOperatingFees['rejectedTransactionFee']
  >(getValues('companyOperatingFees.rejectedTransactionFee') || '')

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    event.preventDefault()
    const value = `${event.target.value}`
    setRejectedTransactionFee(value)
  }

  return (
    <Grid className={classes.commissions} item lg={6} md={6} sm={12} xs={12}>
      <Box
        p={{ xs: 3, sm: 3, md: 4, lg: 5 }}
        mb={{ xs: 3, sm: 3, md: 4, lg: 5 }}
      >
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography
              variant="subtitle1"
              color="primary"
              paragraph
              align="center"
            >
              Tarifas Operacionais Empresa
            </Typography>
          </Grid>
          <Grid item lg={4} md={4} sm={3} xs={6}>
            <TextField
              error={!!errors.companyOperatingFees?.KYC}
              helperText={
                errors.companyOperatingFees?.KYC?.message ||
                'Transação Rejeitada'
              }
              value={rejectedTransactionFee}
              onChange={handleChange}
              InputProps={{
                inputComponent: NumberFormatCustom,
                inputProps: {
                  ...register('companyOperatingFees.rejectedTransactionFee'),
                },
              }}
            />
          </Grid>
        </Grid>
      </Box>
    </Grid>
  )
}

export default TransferRight
