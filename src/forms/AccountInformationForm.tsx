import { FC } from 'react'
import { useForm } from 'react-hook-form'
import {
  Button,
  Box,
  Container,
  Grid,
  makeStyles,
  TextField,
  Typography,
} from '@material-ui/core'
import { yupResolver } from '@hookform/resolvers/yup'
import AccountInformationSchema, {
  AccountInformationFormData,
} from '@/schemas/AccountInformationSchema'
import Invitation from '@/types/Invitation'

const useStyles = makeStyles((theme) => ({
  form: {
    minHeight: '500px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  content: {
    padding: theme.spacing(4),
    paddingTop: 0,
    marginBottom: theme.spacing(4),
  },

  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    width: '100%',
  },
  formButton: {
    borderRadius: 10,
    padding: '13px 22px',
    '& > span': {
      fontSize: 18,
      fontWeight: 600,
    },
  },
}))

type AccountInformationFormProps = {
  invitation: Invitation
  onSuccess: (data: AccountInformationFormData) => void
}

const AccountInformationForm: FC<AccountInformationFormProps> = ({
  invitation,
  onSuccess,
}: any) => {
  const classes = useStyles()

  const { company } = invitation

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<AccountInformationFormData>({
    resolver: yupResolver(AccountInformationSchema),
    defaultValues: { company },
  })

  const onSubmit = (data: AccountInformationFormData) => {
    onSuccess(data)
  }

  return (
    <Box component="form" width="100%" onSubmit={handleSubmit(onSubmit)}>
      <Container className={classes.content}>
        <Typography variant="h5" component="h3" gutterBottom>
          Dados Empresariais
        </Typography>
        <Grid container spacing={10}>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('company.cnpj') }}
              error={!!errors.company?.cnpj}
              helperText={errors.company?.cnpj?.message}
              label="CNPJ"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('company.legalName') }}
              error={!!errors.company?.legalName}
              helperText={errors.company?.legalName?.message}
              label="Razão Social"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('company.tradingName') }}
              error={!!errors.company?.tradingName}
              helperText={errors.company?.tradingName?.message}
              label="Nome Fantasia"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('company.email') }}
              error={!!errors.company?.email}
              helperText={errors.company?.email?.message}
              label="Email"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('company.phone') }}
              error={!!errors.company?.phone}
              helperText={errors.company?.phone?.message}
              label="Telefone"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('company.mobile') }}
              error={!!errors.company?.mobile}
              helperText={errors.company?.mobile?.message}
              label="Celular"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('company.website') }}
              error={!!errors.company?.website}
              helperText={errors.company?.website?.message}
              label="Website URL"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={4} lg={3}>
            <TextField
              inputProps={{ ...register('company.comments') }}
              error={!!errors.company?.comments}
              helperText={errors.company?.comments?.message}
              label="Comentarios"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
        </Grid>
        <br />
        <Typography variant="h5" component="h3" gutterBottom>
          Dados Pessoais
        </Typography>
        <Grid container spacing={10}>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('fullName') }}
              error={!!errors.fullName}
              helperText={errors.fullName?.message}
              label="Nome Completo"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('cpf') }}
              error={!!errors.cpf}
              helperText={errors.cpf?.message}
              label="CPF"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('companyRole') }}
              error={!!errors.companyRole}
              helperText={errors.companyRole?.message}
              label="Cargo"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('companyArea') }}
              error={!!errors.companyArea}
              helperText={errors.companyArea?.message}
              label="Área de trabalho"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('phone') }}
              error={!!errors.phone}
              helperText={errors.phone?.message}
              label="Telefone"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('mobile') }}
              error={!!errors.mobile}
              helperText={errors.mobile?.message}
              label="Celular"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <TextField
              inputProps={{ ...register('email') }}
              error={!!errors.email}
              helperText={errors.email?.message}
              label="Email"
              variant="outlined"
              size="small"
              fullWidth
            />
          </Grid>
        </Grid>
      </Container>
      <div className={classes.footer}>
        <Button
          type="submit"
          className={classes.formButton}
          variant="contained"
          color="primary"
          fullWidth
          size="large"
        >
          Confirmar
        </Button>
      </div>
    </Box>
  )
}
export default AccountInformationForm
