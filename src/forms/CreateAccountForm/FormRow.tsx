import { FC, useCallback } from 'react'
import { Grid, TextField, FormControlLabel } from '@material-ui/core'
import CheckboxForm from '@/components/common/Checkbox'
import IconButton from '@material-ui/core/IconButton'
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle'
import useStyles from './FormRow.style'

type ErrorType =
  | {
      cpf:
        | {
            message: string
          }
        | undefined
      email:
        | {
            message: string
          }
        | undefined
    }
  | undefined

type AccountRowType = {
  id: string
  index: number
  onRemove: (index: number) => void
  nameList: string
  defaultValue: any
  register: any
  errors: ErrorType
  checkboxDisabled?: boolean
}

const AccountRow: FC<AccountRowType> = ({
  index,
  onRemove,
  nameList,
  register,
  errors,
  checkboxDisabled,
}) => {
  const classes = useStyles()
  const getFieldName = useCallback((field) => `${nameList}.${index}.${field}`, [
    nameList,
    index,
  ])

  return (
    <>
      <Grid item xs={12} sm={6} md={4}>
        <TextField
          inputProps={{ ...register(getFieldName('cpf')) }}
          error={!!errors?.cpf}
          helperText={errors?.cpf?.message}
          label="CPF"
          variant="outlined"
          size="small"
          fullWidth
        />
      </Grid>
      <Grid item xs={12} sm={6} md={4}>
        <TextField
          inputProps={{ ...register(getFieldName('email')) }}
          error={!!errors?.email}
          helperText={errors?.email?.message}
          label="Email"
          variant="outlined"
          size="small"
          fullWidth
        />
      </Grid>
      <Grid item xs={4} sm={2} md={1}>
        <FormControlLabel
          labelPlacement="start"
          control={
            <CheckboxForm
              {...register(getFieldName('businessRules'))}
              disabled={checkboxDisabled}
            />
          }
          label="RN"
        />
      </Grid>
      <Grid item xs={4} sm={2} md={1}>
        <FormControlLabel
          labelPlacement="start"
          control={
            <CheckboxForm
              {...register(getFieldName('communications'))}
              disabled={checkboxDisabled}
            />
          }
          label="CO"
        />
      </Grid>
      <Grid item xs={4} sm={2} md={1}>
        <FormControlLabel
          labelPlacement="start"
          control={
            <CheckboxForm
              {...register(getFieldName('profiles'))}
              disabled={checkboxDisabled}
            />
          }
          label="PE"
        />
      </Grid>
      <Grid item xs={4} sm={1}>
        <IconButton className={classes.remove} onClick={() => onRemove(index)}>
          <RemoveCircleIcon />
        </IconButton>
      </Grid>
    </>
  )
}

export default AccountRow
