import { FC } from 'react'
import { Button, Box, Container, Grid, Typography } from '@material-ui/core'
import { yupResolver } from '@hookform/resolvers/yup'
import Fab from '@material-ui/core/Fab'
import AddIcon from '@material-ui/icons/Add'
import CircularProgress from '@material-ui/core/CircularProgress'
import { useFieldArray, useForm } from 'react-hook-form'
import AccountListAgentsSchema from '@/schemas/AccountListAgentsSchema'
import useStyles from './CreateAccount.style'
import FormRow from './FormRow'

type AccountType = {
  cpf: string
  email: string
  profiles: boolean
  comunications: boolean
  businessRules: boolean
}

export type FormData = {
  agents: AccountType[]
}

type AccountExplorerFormType = {
  onSuccess: (data: FormData) => void
  checkboxDisabled?: boolean
  loading: boolean
}

const AccountExplorerForm: FC<AccountExplorerFormType> = ({
  onSuccess,
  checkboxDisabled,
  loading,
}) => {
  const classes = useStyles()
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(AccountListAgentsSchema),
  })

  const { fields, append, remove } = useFieldArray({
    control,
    name: 'agents',
  })

  const onSubmit = (data: FormData) => onSuccess(data)

  return (
    <Box component="form" width="100%" onSubmit={handleSubmit(onSubmit)}>
      <Container className={classes.content}>
        <Grid container spacing={4} justify="center">
          {fields.map((field, index) => (
            <FormRow
              key={field.id}
              index={index}
              id={field.id}
              nameList="agents"
              defaultValue={field}
              register={register}
              onRemove={remove}
              errors={errors.agents?.[index]}
              checkboxDisabled={checkboxDisabled}
            />
          ))}
          <Grid item xs={12} className={classes.center}>
            <Fab
              color="primary"
              size="medium"
              variant="extended"
              aria-label="add"
              onClick={() =>
                append({
                  cpf: '',
                  email: '',
                  profiles: false,
                  communications: false,
                  businessRules: false,
                })
              }
            >
              <AddIcon /> Agregar
            </Fab>
          </Grid>
        </Grid>
      </Container>
      <div className={classes.footer}>
        <Typography gutterBottom>
          RN = Regras de Negócios / CO = Comunicação / PE = Perfis
        </Typography>
        <div className={classes.wrapper}>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            disabled={fields.length === 0 || loading}
          >
            Enviar convite
          </Button>
          {loading && (
            <CircularProgress size={24} className={classes.buttonProgress} />
          )}
        </div>
      </div>
    </Box>
  )
}
export default AccountExplorerForm
