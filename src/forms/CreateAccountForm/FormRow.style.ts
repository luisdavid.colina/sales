import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  remove: {
    color: theme.palette.error.main,
  },
}))

export default useStyles
