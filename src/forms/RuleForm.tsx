import React, { FC, useState } from 'react'
import {
  Container,
  Button,
  Typography,
  Grid,
  makeStyles,
  MenuItem,
} from '@material-ui/core'
import TextField from '@material-ui/core/TextField'

import Autocomplete from '@material-ui/lab/Autocomplete'

import SalesChannelForm from '@/forms/SalesChannelForm'
import SalesChannel from '@/types/SalesChannel'
import { yupResolver } from '@hookform/resolvers/yup'
import RuleSchema, { Rule } from '@/schemas/RuleSchema'
import { Product } from '@/schemas/ProductSchema'
import { Controller, useForm } from 'react-hook-form'
import Participant from '@/types/Participant'
import SalesChannelAutocomplete from '@/components/autocompletes/SalesChannelAutocomplete'
import defaultRule from '@/forms/defaultStates/rule'
import { useStepperContext } from '@/context/StepperContext'
// import RuleSchema from '@/types/RuleSchema'
import { differenceInMonths, differenceInYears } from 'date-fns'

const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    justifyContent: 'center',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  content: {
    padding: theme.spacing(5),
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
    '& > *': {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  },
}))

const payments = [
  { value: 'unique', label: 'Unica' },
  { value: 'monthly', label: 'Mensual' },
  // { value: 'yearly', label: 'Anual' },
  { value: 'lifetime', label: 'Vitalicio' },
]

const commissions = [
  { value: 'rated', label: 'Variável' },
  { value: 'fixed', label: 'Fixo' },
]
interface Props {
  product: Product
  disabled?: true
}

const NewRule: FC<Props> = ({ product, disabled }) => {
  const classes = useStyles()
  const [open, toggleOpen] = useState(false)

  const { next, prev } = useStepperContext()

  const {
    handleSubmit,
    setValue,
    control,
    watch,
    formState: { errors },
  } = useForm<Rule>({
    defaultValues: { ...defaultRule, ...product.rule },
    resolver: yupResolver(RuleSchema),
  })

  const [channelName, setChannelName] = useState('')

  const handleClose = () => {
    toggleOpen(false)
  }

  const updateChannelName = async (newValue: string) => {
    setChannelName(newValue)
    toggleOpen(true)
  }

  const saveSalesChannel = (salesChannel: SalesChannel) => {
    setValue('salesChannel', salesChannel, { shouldValidate: true })
  }

  const submit = (data: Rule) => {
    next({ rule: data })
  }

  const salesChannel = watch('salesChannel')
  const paymentFrequency = watch('paymentFrequency')
  const participantsOptions: Participant[] = salesChannel?.participants || []

  const showMonths = paymentFrequency === 'monthly'

  return (
    <form onSubmit={handleSubmit(submit)} className={classes.form}>
      <Container className={classes.content}>
        <Grid container spacing={3}>
          <Grid item xs={12}>
            <Typography variant="h5" component="h3" gutterBottom>
              Matriz Da Venda
            </Typography>
          </Grid>
          <Grid item xs={6} sm={4} md={4}>
            <Controller
              control={control}
              name="commissionType"
              render={(props) => (
                <TextField
                  onChange={(e) => props.field.onChange(e.target.value)}
                  value={props.field.value}
                  variant="outlined"
                  select
                  margin="normal"
                  label="Tipo de comissão"
                  error={!!errors.commissionType}
                  helperText={errors.commissionType?.message}
                  fullWidth
                  size="small"
                  disabled={disabled}
                >
                  {commissions.map((commission) => (
                    <MenuItem key={commission.value} value={commission.value}>
                      {commission.label}
                    </MenuItem>
                  ))}
                </TextField>
              )}
            />
          </Grid>
          <Grid item xs={6} sm={4} md={4}>
            <Controller
              control={control}
              name="paymentFrequency"
              render={(props) => (
                <TextField
                  onChange={(e) => {
                    props.field.onChange(e.target.value)

                    if (e.target.value === 'lifetime') {
                      setValue('months', 1)
                    }
                  }}
                  value={props.field.value}
                  variant="outlined"
                  select
                  margin="normal"
                  label="Frequência Do Pagamento"
                  error={!!errors.paymentFrequency}
                  helperText={errors.paymentFrequency?.message}
                  fullWidth
                  size="small"
                  disabled={disabled}
                >
                  <MenuItem value="monthly">Mensual</MenuItem>
                  <MenuItem value="lifetime">Vitalicia</MenuItem>
                </TextField>
              )}
            />
          </Grid>
          <Grid item xs={6} sm={4} md={4}>
            {showMonths && (
              <Controller
                control={control}
                name="months"
                render={(props) => (
                  <TextField
                    onChange={(e) => props.field.onChange(+e.target.value)}
                    value={props.field.value}
                    variant="outlined"
                    margin="normal"
                    label="Meses"
                    error={!!errors.paymentFrequency}
                    helperText={errors.paymentFrequency?.message}
                    fullWidth
                    size="small"
                    disabled={disabled}
                    inputProps={{ type: 'number' }}
                  />
                )}
              />
            )}
          </Grid>

          <Grid item xs={12} sm={6} md={3}>
            <Controller
              control={control}
              name="salesChannel"
              render={(props) => (
                <SalesChannelAutocomplete
                  onNewOption={updateChannelName}
                  disabled={disabled}
                  value={props.field.value}
                  error={!!errors.salesChannel}
                  helperText={errors.salesChannel?.id?.message}
                  onChange={(value: any) => {
                    setValue('participants', [], {
                      shouldValidate: true,
                    })
                    props.field.onChange(value)
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={12} sm={6} md={9}>
            <Controller
              control={control}
              name="participants"
              render={(props) => (
                <Autocomplete
                  multiple
                  disableClearable
                  disabled={disabled}
                  getOptionLabel={(option) => option.name}
                  options={participantsOptions}
                  value={props.field.value}
                  onChange={(_, value) => props.field.onChange(value)}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Participantes"
                      margin="normal"
                      variant="outlined"
                      InputProps={{
                        ...params.InputProps,
                        type: 'search',
                      }}
                    />
                  )}
                />
              )}
            />
          </Grid>
        </Grid>
      </Container>
      <div className={classes.footer}>
        <Button onClick={prev} type="button" variant="contained">
          Retornar
        </Button>
        <Button type="submit" variant="contained" color="primary">
          Avançar
        </Button>
      </div>
      {open && (
        <SalesChannelForm
          open={open}
          handleClose={handleClose}
          newChannelName={channelName}
          onSuccess={saveSalesChannel}
        />
      )}
    </form>
  )
}

export default NewRule
