import React, { useState, FC, ChangeEvent } from 'react'
import axios from 'axios'
import {
  Grid,
  Button,
  FormLabel,
  Radio,
  RadioGroup,
  FormControlLabel,
  TextField,
  Box,
  InputBaseComponentProps,
} from '@material-ui/core'
import { RadioProps } from '@material-ui/core'
import NumberFormat from 'react-number-format'
import { yupResolver } from '@hookform/resolvers/yup'
import MultiplierSchema, { Multiplier } from '@/schemas/MultiplierSchema'
import { useForm, Controller } from 'react-hook-form'
import { useRouter } from 'next/router'
import { priceFormatter } from '@/util/currency'
import multiplier from './defaultStates/multiplier'

const rootStyle = {
  '&:hover': {
    backgroundColor: '',
  },
}
const icon = {
  borderRadius: '50%',
  width: '18px',
  height: '18px',
  border: '2px solid',
  color: 'white',
  transition: '0.2s',
  boxShadow:
    'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
  backgroundColor: 'rgba(255, 255, 255, 0.1)',
  backgroundImage: 'rgba(255, 255, 255, 0.1)',
  '$root.Mui-focusVisible &': {
    outline: '2px auto rgba(19,124,189,.6)',
    outlineOffset: 2,
  },
  'input:hover ~ &': {
    backgroundColor: '#0077ff',
    width: '21px',
    height: '21px',
  },
  'input:disabled ~ &': {
    boxShadow: 'none',
    background: 'rgba(206,217,224,.5)',
  },
}

const checkedIcon = {
  ...icon,
  backgroundColor: '#0077ff',
  width: '21px',
  height: '21px',
  backgroundImage: '#0077ff',
  '&:before': {
    display: 'block',
    width: 16,
    height: 16,
    content: '""',
  },
}
const root = {
  display: 'flex',
  '&:hover': {
    backgroundColor: '',
  },
}

const StyledRadio: FC<RadioProps> = ({ ...props }) => {
  //const {classes} = props
  return (
    <Radio
      style={root}
      disableRipple
      checkedIcon={<span style={checkedIcon} />}
      icon={<span style={icon} />}
      {...props}
    />
  )
}

const formatConcurrency = (value: string) => {
  return value
    .split(/[^0-9,]/)
    .filter(Boolean)
    .join('')
    .split(',')
    .map(Number)
    .join('.')
}

const NumberFormatCustom = (props: any) => {
  const { inputRef, onChange, value, ...other } = props
  const first = value.substr(0, 2) === 'R$'
  if (!first)
    return (
      <NumberFormat
        {...other}
        getInputRef={inputRef}
        onValueChange={(values) => {
          onChange({
            target: {
              name: props.name,
              value: values.value,
            },
          })
        }}
        decimalScale={2}
        fixedDecimalScale
        allowEmptyFormatting
        decimalSeparator=","
        thousandSeparator="."
        isNumericString
        prefix="R$ "
      />
    )
  return (
    <NumberFormat
      {...props}
      decimalSeparator=","
      thousandSeparator="."
      isNumericString
    />
  )
}

interface MultiplierProps {
  linkId: string
  classes: any
}

const MultiplierForm: FC<MultiplierProps> = ({ linkId, classes }) => {
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    defaultValues: { ...multiplier, commercialAgentLinkId: linkId },
    resolver: yupResolver(MultiplierSchema),
  })
  const [totalIncome, setTotalIncome] = useState<string>('R$ 1,23')
  const [totalQuantity, setTotalQuantity] = useState<number>()
  const [result, setResult] = useState<number>()
  const router = useRouter()

  const submit = async (data: Multiplier) => {
    try {
      const response = await axios.post('/api/proposal-requests', data)

      if (response.status === 201) {
        router.push('/multiplier/save')
      }
    } catch (err) {
      const error = err as any

      // eslint-disable-next-line no-console
      console.error(error)
    }
  }

  const [loading, setLoading] = useState(true)

  const volumen = (e: ChangeEvent<HTMLInputElement>) => {
    setTotalIncome(String(e.target.value))
  }
  const quantity = (e: ChangeEvent<HTMLInputElement>) => {
    setTotalQuantity(Number(e.target.value))
  }
  const simulate = () => {
    if (Number(totalQuantity) > 0 && Number(totalIncome) > 0) {
      setLoading(false)
      setResult((Number(totalIncome) + Number(totalQuantity) * 10 + 30) * 1.05)
    }
  }
  return (
    <>
      <div style={classes.multiplierForm}>
        <form onSubmit={handleSubmit(submit)}>
          <div style={classes.multiplierForm2}>
            <p style={classes.formTitle}> Dados de Contrato </p>
            <input
              id="commercialAgentLinkId"
              {...register('commercialAgentLinkId')}
              type="hidden"
            />
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <FormLabel style={classes.tlabel}>Nome Completo</FormLabel>
                <TextField
                  inputProps={{
                    ...register('contactFullName'),
                    style: classes.input,
                  }}
                  FormHelperTextProps={{ style: classes.error }}
                  error={!!errors.contactFullName}
                  helperText={errors.contactFullName?.message}
                  label=""
                  autoFocus
                  fullWidth
                  variant="outlined"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <FormLabel style={classes.tlabel}> Cargo </FormLabel>
                <TextField
                  name="companyRole"
                  variant="outlined"
                  fullWidth
                  id="companyRole"
                  label=""
                  autoFocus
                  FormHelperTextProps={{ style: classes.error }}
                  inputProps={{
                    ...register('companyRole'),
                    style: classes.input,
                  }}
                  error={!!errors.companyRole}
                  helperText={errors.companyRole?.message}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <FormLabel style={classes.tlabel}> Área</FormLabel>
                <TextField
                  variant="outlined"
                  fullWidth
                  id="companyArea"
                  label=""
                  name="companyArea"
                  FormHelperTextProps={{ style: classes.error }}
                  inputProps={{
                    ...register('companyArea'),
                    style: classes.input,
                  }}
                  error={!!errors.companyArea}
                  helperText={errors.companyArea?.message}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <FormLabel style={classes.tlabel}> Razão Social</FormLabel>
                <TextField
                  variant="outlined"
                  fullWidth
                  name="companyLegalName"
                  label=""
                  id="companyLegalName"
                  FormHelperTextProps={{ style: classes.error }}
                  inputProps={{
                    ...register('companyLegalName'),
                    style: classes.input,
                  }}
                  error={!!errors.companyLegalName}
                  helperText={errors.companyLegalName?.message}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <FormLabel style={classes.tlabel}> CNPJ </FormLabel>
                <TextField
                  name="cnpj"
                  variant="outlined"
                  fullWidth
                  id="cnpj"
                  label=""
                  FormHelperTextProps={{ style: classes.error }}
                  inputProps={{ ...register('cnpj'), style: classes.input }}
                  error={!!errors.cnpj}
                  helperText={errors.cnpj?.message}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <FormLabel style={classes.tlabel}> Email </FormLabel>
                <TextField
                  variant="outlined"
                  fullWidth
                  id="email"
                  label=""
                  name="email"
                  autoComplete="email"
                  FormHelperTextProps={{ style: classes.error }}
                  inputProps={{
                    ...register('email'),
                    style: classes.input,
                  }}
                  error={!!errors.email}
                  helperText={errors.email?.message}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <FormLabel style={classes.tlabel}> Celular </FormLabel>
                <TextField
                  name="mobile"
                  variant="outlined"
                  fullWidth
                  id="mobile"
                  label=""
                  type="tel"
                  autoFocus
                  autoComplete="tel"
                  FormHelperTextProps={{ style: classes.error }}
                  inputProps={{
                    ...register('mobile'),
                    style: classes.input,
                  }}
                  error={!!errors.mobile}
                  helperText={errors.mobile?.message}
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <FormLabel style={classes.tlabel}> Telefone </FormLabel>
                <TextField
                  variant="outlined"
                  fullWidth
                  id="phone"
                  label=""
                  type="tel"
                  name="phone"
                  autoComplete="tel"
                  FormHelperTextProps={{ style: classes.error }}
                  inputProps={{
                    ...register('phone'),
                    style: classes.input,
                  }}
                  error={!!errors.phone}
                  helperText={errors.phone?.message}
                />
              </Grid>
              <Grid item xs={6} md={6}>
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  style={classes.submit}
                  disabled={loading}
                >
                  Solicitar Proposta
                </Button>
              </Grid>
            </Grid>
          </div>
          <div style={classes.multiplierForm3}>
            <Box component="form" width="100%">
              <p style={classes.formTitle}>Simulador</p>

              <Grid container spacing={4}>
                <Grid item xs={12} md={5}>
                  <Controller
                    name="product"
                    control={control}
                    render={({ field }) => (
                      <RadioGroup
                        onChange={(_, value) => field.onChange(value)}
                        aria-label="product"
                        name="product"
                        row
                      >
                        <FormControlLabel
                          value="prepaid_card"
                          control={<StyledRadio />}
                          label={
                            <p style={classes.textRadio}>Cartão Joycard</p>
                          }
                        />
                        <FormControlLabel
                          value="digital_account"
                          control={<StyledRadio />}
                          label={
                            <p style={classes.textRadio}>Conta Digital Adoro</p>
                          }
                        />
                      </RadioGroup>
                    )}
                  />
                </Grid>
                <Grid item xs={12} md={7}>
                  <FormLabel style={classes.tlabel}>
                    Quantos colaboradores pretende premiar?
                  </FormLabel>
                  <TextField
                    id="commercialAgentsQuantity"
                    name="commercialAgentsQuantity"
                    label=""
                    type="number"
                    fullWidth
                    variant="outlined"
                    inputProps={{
                      ...register('commercialAgentsQuantity'),
                      style: classes.input,
                      step: 1,
                      min: 1,
                    }}
                    onChange={quantity}
                  />
                </Grid>
                <Grid item xs={12}>
                  <FormLabel style={classes.tlabel}>
                    Qual o volume financeiro estimado em cada pedido de
                    premiação?
                  </FormLabel>
                  <TextField
                    id="totalIncome"
                    name="totalIncome"
                    value={totalIncome}
                    fullWidth
                    variant="outlined"
                    InputProps={{
                      inputComponent: NumberFormatCustom,
                      inputProps: {
                        ...register('totalIncome'),
                        style: classes.input,
                      },
                    }}
                    onChange={volumen}
                  />
                </Grid>
                <Grid item xs={12}>
                  <FormLabel style={classes.tlabel}>
                    Qual frequência pretende premiar seus colaboradores?
                  </FormLabel>
                  <Controller
                    name="paymentFrequency"
                    control={control}
                    render={({ field }) => (
                      <RadioGroup
                        onChange={(_, value) => field.onChange(value)}
                        aria-label="frecuencia"
                        name="paymentFrequency"
                        id="paymentFrequency"
                        row
                      >
                        <br />
                        <FormControlLabel
                          value="monthly"
                          control={<StyledRadio />}
                          label={<p style={classes.textRadio}>Mensal</p>}
                        />
                        <FormControlLabel
                          value="quarterly"
                          control={<StyledRadio />}
                          label={<p style={classes.textRadio}>Trimestral</p>}
                        />
                        <FormControlLabel
                          value="yearly"
                          control={<StyledRadio />}
                          label={<p style={classes.textRadio}>Anual</p>}
                        />
                        <FormControlLabel
                          value="only"
                          control={<StyledRadio />}
                          label={<p style={classes.textRadio}>Única</p>}
                        />
                      </RadioGroup>
                    )}
                  />
                </Grid>
                <Grid item xs={4}>
                  <Button
                    onClick={simulate}
                    variant="contained"
                    color="primary"
                    fullWidth
                    style={classes.submit}
                  >
                    Calcular
                  </Button>
                </Grid>
                {result && (
                  <Grid item xs={5} style={classes.result}>
                    {priceFormatter.format(result)}
                  </Grid>
                )}
                {result && (
                  <p style={classes.tlabel}>
                    * Todos os serviços com NF incluídos - emissão, envio e
                    cargas
                  </p>
                )}
              </Grid>
            </Box>
          </div>
        </form>
      </div>
    </>
  )
}

export default MultiplierForm
