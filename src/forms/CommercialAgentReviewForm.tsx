import { FC, FormEvent, useContext, useState } from 'react'
import { useRouter } from 'next/router'
import StepperContext from '@/context/StepperContext'
import {
  Box,
  Button,
  CircularProgress,
  Container,
  Grid,
  makeStyles,
  Typography,
} from '@material-ui/core'
import { blue } from '@material-ui/core/colors'
import SuccessModal from '@/components/SuccessModal'
import CommercialAgent from '@/types/CommercialAgent'

const useStyles = makeStyles((theme) => ({
  form: {
    minHeight: '500px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  content: {
    padding: theme.spacing(4),
  },
  section: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(8),
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: theme.spacing(4),
    background: '#f0f0f0',
    width: '100%',
    '& > *': {
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
    },
  },
  wrapper: {
    position: 'relative',
  },
  buttonProgress: {
    color: blue[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}))

interface Props {
  commercialAgent: CommercialAgent
}

const CommercialAgentReviewForm: FC<Props> = ({ commercialAgent }) => {
  const classes = useStyles()
  const stepper = useContext(StepperContext)
  const [loading, setLoading] = useState(false)
  const [openSuccessModal, setOpenSuccessModal] = useState(false)
  const router = useRouter()

  const submit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault()
    setLoading(true)

    try {
      const response = await stepper.onSuccess(commercialAgent)

      if (response.status === stepper.code) {
        setOpenSuccessModal(true)
      }
    } catch (err) {
      const error = err as any
       
      /* eslint no-console: ["error", { allow: ["error"] }] */
      console.error(error)
    }

    setLoading(false)
  }

  const handleCloseModal = () => {
    setOpenSuccessModal(false)
    router.push('/dashboard/commercial-agents')
  }

  const { company } = commercialAgent

  return (
    <>
      <form className={classes.form} onSubmit={submit}>
        <Container className={classes.content}>
          <Typography variant="h5" component="h3" gutterBottom>
            Resumo de agente comercial
          </Typography>

          <Box bgcolor="background.gray" p={6}>
            <Grid container spacing={5}>
              <Grid item lg={4} sm={6} xs={12}>
                <Typography variant="subtitle1" color="initial">
                  Razão Social:
                </Typography>
                <Typography variant="body2" color="initial">
                  {company.legalName}
                </Typography>
              </Grid>
              <Grid item lg={4} sm={6} xs={12}>
                <Typography variant="subtitle1" color="initial">
                  Nome Fantasia:
                </Typography>
                <Typography variant="body2" color="initial">
                  {company.tradingName}
                </Typography>
              </Grid>
              <Grid item lg={4} sm={6} xs={12}>
                <Typography variant="subtitle1" color="initial">
                  CNPJ:
                </Typography>
                <Typography variant="body2" color="initial">
                  {company.cnpj}
                </Typography>
              </Grid>
              <Grid item lg={4} sm={6} xs={12}>
                <Typography variant="subtitle1" color="initial">
                  Telefone:
                </Typography>
                <Typography variant="body2" color="initial">
                  {company.phone}
                </Typography>
              </Grid>
              <Grid item lg={4} sm={6} xs={12}>
                <Typography variant="subtitle1" color="initial">
                  Nome de representante:
                </Typography>
                <Typography variant="body2" color="initial">
                  {company.admin.fullName}
                </Typography>
              </Grid>
              <Grid item lg={4} sm={6} xs={12}>
                <Typography variant="subtitle1" color="initial">
                  CPF de representante:
                </Typography>
                <Typography variant="body2" color="initial">
                  {company.admin.cpf}
                </Typography>
              </Grid>
              <Grid item lg={4} sm={6} xs={12}>
                <Typography variant="subtitle1" color="initial">
                  Email de representante:
                </Typography>
                <Typography variant="body2" color="initial">
                  {company.admin.email}
                </Typography>
              </Grid>
              <Grid item lg={4} sm={6} xs={12}>
                <Typography variant="subtitle1" color="initial">
                  Código da tabela selecionada
                </Typography>
                <Typography variant="body2" color="initial">
                  {commercialAgent.salesCommissionId}
                </Typography>
              </Grid>
            </Grid>
          </Box>
        </Container>
        <div className={classes.footer}>
          <Button onClick={stepper.prev} type="button" variant="contained">
            Voltar
          </Button>
          <div className={classes.wrapper}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              disabled={loading}
            >
              Salvar
            </Button>
            {loading && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
          </div>
        </div>
      </form>
      <SuccessModal
        title={stepper.titleModal}
        open={openSuccessModal}
        onClose={handleCloseModal}
      />
    </>
  )
}

export default CommercialAgentReviewForm
