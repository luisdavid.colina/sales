import ParticipantsAutocomplete from '@/components/autocompletes/ParticipantsAutocomplete'
import SalesChannelSchema from '@/schemas/SalesChannelSchema'
import { authFetch } from '@/util/fetch'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  makeStyles,
  TextField,
} from '@material-ui/core'
import { FC, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'
import salesChannel from './defaultStates/salesChannel'

interface Props {
  open: boolean
  handleClose: () => void
  onSuccess?: (data: any) => void
  newChannelName?: string
}

const useStyles = makeStyles((theme) => ({
  form: {
    width: '100%',
    maxWidth: '500px',
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
}))

const SalesChannelForm: FC<Props> = ({
  open,
  onSuccess = () => {},
  handleClose,
  newChannelName = '',
}) => {
  const classes = useStyles()

  const [loading, setLoading] = useState(false)

  const { register, handleSubmit, control, reset } = useForm({
    defaultValues: { ...salesChannel, name: newChannelName },
    resolver: yupResolver(SalesChannelSchema),
  })

  const submit = async (data: any) => {
    setLoading(true)

    try {
      const response = await authFetch.post('sales/channels', data)
      if (response.status === 201) {
        onSuccess(response.data.salesChannel)
        handleClose()
        reset()
      }
    } catch (err) {
      const error = err as any
       
      // eslint-disable-next-line no-console
      console.log(error)
    }

    setLoading(false)
  }

  const close = () => {
    if (!loading) {
      handleClose()
    }
  }

  return (
    <Dialog
      fullWidth
      maxWidth="xs"
      open={open}
      onClose={close}
      aria-labelledby="form-dialog-canal"
    >
      <form className={classes.form} onSubmit={handleSubmit(submit)}>
        <DialogContent>
          <DialogContentText>Novo Canal Da Venda</DialogContentText>
          <TextField
            inputProps={{ ...register('name') }}
            autoFocus
            margin="normal"
            id="name"
            fullWidth
            variant="outlined"
            label="Canal"
            type="text"
          />
          <Controller
            control={control}
            name="participants"
            render={(props) => (
              <ParticipantsAutocomplete
                value={props.field.value}
                onChange={props.field.onChange}
              />
            )}
          />
        </DialogContent>
        <DialogActions>
          <Button disabled={loading} onClick={handleClose} color="primary">
            Cancel
          </Button>
          <div className={classes.wrapper}>
            <Button
              disabled={loading}
              type="submit"
              variant="contained"
              color="primary"
            >
              Agregar
            </Button>
            {loading && (
              <CircularProgress size={24} className={classes.buttonProgress} />
            )}
          </div>
        </DialogActions>
      </form>
    </Dialog>
  )
}

export default SalesChannelForm
