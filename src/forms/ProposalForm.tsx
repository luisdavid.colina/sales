import { FC, useContext, useState } from 'react'
import { Paper } from '@material-ui/core'
import StepperContext, { StepperProvider } from '@/context/StepperContext'
import Proposal from '@/types/Proposal'
import { AxiosResponse } from 'axios'

import CompanyForm from './CompanyForm'
import ProductDetailsForm from './ProductDetailsForm'
import ProposalReviewForm from './ProposalReviewForm'
import defaultProposal from './defaultStates/proposal'

interface StepFormProps {
  proposal: Proposal
}

const StepForm: FC<StepFormProps> = ({ proposal }) => {
  const stepper = useContext(StepperContext)

  switch (stepper.step) {
    case 1:
      return <CompanyForm company={proposal.company} />
    case 2:
      return <ProductDetailsForm productDetails={proposal.productDetails} />
    default:
      return <ProposalReviewForm proposal={proposal} />
  }
}

type ProposalFormProps = {
  proposal?: Proposal
  disabled?: boolean
  onSuccess: (proposal: Proposal) => Promise<AxiosResponse<any>>
  code?: number
  titleModal?: string
  actionModal?: () => void
}

const ProposalForm: FC<ProposalFormProps> = ({
  proposal = defaultProposal,
  disabled,
  onSuccess,
  code,
  titleModal,
  actionModal,
}) => {
  const [data, setData] = useState<Proposal>(proposal)
  function updateData(stepData: Partial<Proposal>) {
    if (stepData) {
      setData((prevData) => {
        return { ...prevData, ...stepData }
      })
    }
  }

  return (
    <Paper elevation={4}>
      <StepperProvider
        disabled={disabled}
        onChange={updateData}
        onSuccess={onSuccess}
        code={code}
        titleModal={titleModal}
        actionModal={actionModal}
      >
        <StepForm proposal={data} />
      </StepperProvider>
    </Paper>
  )
}

export default ProposalForm
