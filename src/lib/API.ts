/* eslint-disable no-console */
import { ProductCategory, ProductFamily } from '@/types/Product'
import TutorialCategory from '@/types/TutorialCategory'
import Participant from '@/types/Participant'
import { authFetch } from '@/util/fetch'

export const useProductCategories = () => {
  async function create(payload: ProductCategory) {
    try {
      const response = await authFetch.post(`products/categories`, payload)
      return { error: null, data: response.data }
    } catch (err) {
      const error = err as any

      if (error.response) {
        //       console.error(error.response)
        return { error: error.response }
      }
      //      console.error(error.message)
      return { error }
    }
  }

  return { create }
}

export const useProductFamilies = () => {
  async function create(payload: ProductFamily) {
    try {
      const response = await authFetch.post(`products/families`, payload)
      return { error: null, data: response.data }
    } catch (err) {
      const error = err as any

      if (error.response) {
        console.error(error.response)
        return { error: error.response }
      }
      console.error(error.message)
      return { error }
    }
  }

  return { create }
}

export const useParticipants = () => {
  async function create(payload: Participant) {
    try {
      const response = await authFetch.post(`participants`, payload)
      return { error: null, data: response.data }
    } catch (err) {
      const error = err as any

      if (error.response) {
        console.error(error.response)
        return { error: error.response }
      }
      console.error(error.message)
      return { error }
    }
  }

  return { create }
}

export const useTutorialCategories = () => {
  async function create(payload: TutorialCategory) {
    try {
      const response = await authFetch.post(`tutorials/categories`, payload)
      return { error: null, data: response.data }
    } catch (err) {
      const error = err as any

      if (error.response) {
        //       console.error(error.response)
        return { error: error.response }
      }
      //      console.error(error.message)
      return { error }
    }
  }

  return { create }
}
