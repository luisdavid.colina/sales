import Contract from '@/types/Contract'
import { format } from 'date-fns'
import PDF from './PDF'

class ContractDocument extends PDF {
  contract: Contract

  constructor(contract: Contract) {
    super()
    this.contract = contract
  }

  moveLittleDown() {
    this.doc.moveDown(0.3)
  }

  moveVeryLittleDown() {
    this.doc.moveDown(0.2)
  }

  writeTableTwoColumn(
    title: string,
    rows: string[][],
    colWidth1: number,
    colWidth2: number,
    lastRow?: string,
  ) {
    const writeRow = (text1: string, text2: string) =>
      super.writeColumns([
        { text: text1, width: colWidth1 },
        {
          text: text2,
          width: colWidth2,
          align: 'center',
        },
      ])

    const initialRowYTitle = this.doc.y
    const initialRowXTitle = this.doc.x

    super.writeHr()
    this.moveVeryLittleDown()

    super.writeTitle(title, { align: 'center' })
    this.doc.moveUp()
    this.moveVeryLittleDown()

    const initialRowY = this.doc.y
    const initialRowX = this.doc.x

    rows.forEach((columns) => {
      super.writeHr()

      this.moveVeryLittleDown()
      writeRow(columns[0], columns[1])
      this.moveVeryLittleDown()
    })

    super.writeHr()

    super.writeLine(
      initialRowX + colWidth1,
      initialRowY,
      initialRowX + colWidth1,
      this.doc.y,
    )

    if (lastRow) {
      this.moveVeryLittleDown()
      super.writeColumns([{ text: lastRow, width: 550 }])
      this.moveVeryLittleDown()
      super.writeHr()
    }

    super.writeLine(
      initialRowXTitle,
      initialRowYTitle,
      initialRowXTitle,
      this.doc.y,
    )
    super.writeLine(550, initialRowYTitle, 550, this.doc.y)

    this.doc.moveDown()
  }

  writeContractPurpose() {
    super.writeTitle('CLÁUSULA PRIMEIRA: OBJETO DO CONTRATO')

    super.writeText(
      '1.1 A prestação de serviços de marketing motivacional, pela CONTRATADA, para operacionalização dos programas de motivação, com melhoria nos sistemas de maximização de resultados da CONTRATANTE.',
    )

    super.writeText(
      '1.2 O cumprimento do objeto do contrato ocorrerá pelo fornecimento, pela CONTRATADA, de contas de pagamento digital, cartões e/ou vouchers em QR codes, conforme definido no Anexo I e, pela CONTRATANTE, pelo fornecimento de lista de indicados e dos critérios a cada um destes aplicados.',
    )

    super.writeText(
      '1.3 Durante a vigência do presente contrato, a CONTRATADA se obriga a manter o funcionamento do sistema de utilização dos cartões, bem assim o do APP Adoro para utilização das contas de pagamento digital a fim de premiar os indicados pela empresa CONTRATANTE.',
    )

    super.writeText(
      '1.4 A CONTRATADA declara, neste ato, que os serviços avençados fazem parte de seu objeto social, bem como que está legalmente apta para prestação dos mesmos, possuindo as necessárias inscrições perante os competentes entes governamentais.',
    )
  }

  writeEngagementObligations() {
    super.writeTitle('CLÁUSULA SEGUNDA: OBRIGAÇÕES DA CONTRATADA')
    super.writeText(
      '2.1 Colocar à disposição da CONTRATANTE os serviços contratados, no prazo de até 10 (dez) dias úteis da solicitação do pedido.',
    )
    super.writeText(
      '2.2 Encaminhar nota fiscal de serviços e cobrança, com data de vencimento escolhida pela CONTRATANTE, contendo os valores dos prêmios, honorários, tarifas e serviços, no prazo máximo de 1 (um) dia útil, contado da data do recebimento do pedido, salvo motivo de força maior ou caso fortuito alheio à vontade  da CONTRATADA.',
    )
    super.writeText(
      '2.3 Adotar as providências necessárias junto às instituições financeiras credenciadas para que proceda ao registro, lançamento e pagamento dos créditos, nos termos deste contrato, contra o aporte dos referidos valores e solicitação da CONTRATANTE.',
    )
    super.writeText(
      '2.4 Disponibilizar os créditos em favor dos indicados pela CONTRATANTE, no prazo de 1 (um) dia útil contado a partir da identificação do pagamento, salvo motivo de força maior ou caso fortuito alheio à vontade da CONTRATADA, sob pena de pagamento à CONTRATANTE de correção monetária e juros legais, calculados até a data do efetivo repasse, sem prejuízo das demais cominações previstas em lei e no presente contrato.',
    )
    super.writeText(
      '2.5 Responder civil e criminalmente por todo e qualquer dano moral ou patrimonial que der causa, eximindo a CONTRATANTE de qualquer responsabilidade. Caso seja movida alguma ação judicial contra a CONTRATANTE, por fato imputável à CONTRATADA, relacionado ao presente contrato, a CONTRATANTE poderá denunciar a lide à CONTRATADA, nos termos do artigo 125, inciso II do Código de Processo Civil, sem prejuízo da cobrança da indenização acima mencionada, ou exercer o seu direito de regresso de forma autônoma, a qualquer tempo.',
    )
    super.writeText(
      '2.6 Orientar a CONTRATANTE e os usuários, sempre que necessário, sobre a correta utilização do sistema de premiação, cujas informações básicas encontram-se no Anexo II – Sistema de Premiação do presente instrumento.',
    )
  }

  writeContractorObligations() {
    super.writeTitle('CLÁUSULA TERCEIRA: OBRIGAÇÕES DA CONTRATANTE')

    super.writeText(
      '3.1 Autorizar a CONTRATADA, por escrito, a conceder os créditos, nos termos do presente contrato e seus anexos, observados os prazos da cláusula terceira.',
    )
    super.writeText(
      '3.2 Efetuar o pagamento dos créditos à CONTRATADA, que os repassará aos cartões ou contas de pagamento dos premiados no prazo de 2 (dois) dias úteis após a identificação do pagamento daqueles valores, somados à taxa de administração, tarifas e demais encargos contratados.',
    )

    super.writeText(
      '3.3 As solicitações de pedido de carga devem ser enviadas pela CONTRATANTE à CONTRATADA entre 8 - 12h para a liberação dos créditos em até 2 (dois) dias úteis (D+2).',
    )

    super.writeText(
      '3.4 Esclarecer e repassar aos beneficiários os termos e condições de utilização do sistema de premiação, com base nas informações prévias fornecidas pela CONTRATADA e no conteúdo do Anexo II – Sistema de Premiação do presente instrumento.',
    )
  }

  writeEngagementRemuneration() {
    super.writeTitle('CLÁUSULA QUARTA: REMUNERAÇÃO DA CONTRATADA')
    super.writeText(
      '4.1 A CONTRATANTE pagará à CONTRATADA, a título de honorários pelos serviços contratados, uma taxa de administração conforme descrito no Anexo I – Condições Comerciais. A taxa de administração incidirá sobre todos os serviços prestados e demais encargos contratados, indicados no Anexo I do presente instrumento.',
    )

    super.writeText(
      '4.2 A CONTRATADA enviará à CONTRATANTE nota fiscal e nota de débito (premiação) no valor dos créditos a serem reembolsados, acrescido dos seus honorários pelos serviços prestados de marketing motivacional, objeto deste contrato, além dos demais encargos contratados.',
    )

    super.writeText(
      '4.2.1 Fica convencionado que a Nota Fiscal relativa à prestação de serviços e a Nota de Débito (premiação) são os instrumentos de cobrança pelo qual a CONTRATANTE deverá pagar os referidos valores através de crédito em conta-corrente da CONTRATADA.',
    )
    super.writeText(
      '4.2.2 Alternativamente, e a critério das partes, a cobrança será feita através de boleto bancário, cujo envio pela CONTRATADA à CONTRATANTE deverá ocorrer até 1 (hum) dia antes do vencimento estabelecido e deve considerar 1 dia para sua compensação.',
    )

    super.writeText(
      '4.3 Se, por razões comerciais, a CONTRATADA optar por conceder prazos para pagamento dos créditos pela CONTRATANTE, fica ajustado que a inadimplência dos prazos avençados sujeitará a CONTRATANTE ao pagamento de juros de 1% (hum por cento) ao mês, mais multa, não compensatória, de 10% (dez por cento) sobre o valor devido, bem como correção monetária, se cabível, utilizando-se para tanto o índice IGPM.',
    )

    super.writeText(
      '4.4 A CONTRATANTE ficará isenta de multa se ficar impossibilitada de cumprir com suas obrigações em virtude de a CONTRATADA estar em situação irregular junto aos órgãos da Administração Pública Federal, Estadual e Municipal.',
    )
  }

  writeCreditValues() {
    super.writeTitle('CLÁUSULA  QUINTA: VALOR DOS CRÉDITOS')

    super.writeText(
      '5.1 O valor dos créditos será determinado para cada beneficiário conforme orientação da CONTRATANTE, podendo a CONTRATADA, se necessário, prestar auxílio e informação no estabelecimento de regras da campanha motivacional.',
    )

    super.writeText(
      '5.2 A premiação não conterá carga de valores até que os recursos sejam disponibilizados pela CONTRATADA no prazo minimo de 2 (dois) dias  úteis após o pagamento efetuado pela CONTRATANTE.',
    )

    super.writeText(
      '5.2.1 No caso de contratação da Conta Adoro, os créditos somente serão liberados após a conclusão pelo beneficiário de seu cadastramento pelo APP Adoro para identificação..',
    )
  }

  writeCardValidation() {
    super.writeTitle('CLÁUSULA SEXTA: VALIDADE DO CARTÃO PARA USO DA PREMIAÇÃO')

    super.writeText(
      '6.1 Os cartões terão validade de até 60 (sessenta) meses, enquanto a conta de pagamento possuirá 1 cartão virtual com validade de até 36 (trinta e seis) meses ou conforme venham a ajustar as partes, observadas as regras da instituição financeira credenciada, emissora do cartão e os custos associados ao referido ajuste entre as partes.',
    )
    super.writeText(
      '6.2 Expirado o prazo de validade do cartão, durante o período de disponibilidade de uso, um novo cartão será emitido a favor do portador, em substituição ao cartão vencido.',
    )
  }

  writeReadjusmentAndIncidentOfRates() {
    super.writeTitle('CLÁUSULA  SÉTIMA: REAJUSTE E INCIDÊNCIA DE TARIFAS')
    super.writeText(
      '7.1 A prestação de serviço , tarifas operacionais, tarifas de uso e demais encargos previstos no Anexo I do presente instrumento serão reajustados anualmente, com base na variação do IGPM - Índice Geral de Preços de Mercado, sempre com a devida comunicação prévia à CONTRATANTE.',
    )

    super.writeText(
      '7.1.1 O reajuste da taxa de administração será calculado proporcionalmente aos meses de  vigência do contrato em relação ao período de apuração.',
    )

    super.writeText(
      '7.1.2 Independentemente da forma de reajuste prevista no item 7.1.1 acima, os valores das tarifas e demais encargos do presente instrumento poderão ser revistos na hipótese de ocorrerem fatos imprevisíveis que prejudiquem o equilíbrio econômico do contrato, mediante acerto entre as partes.',
    )

    super.writeText(
      '7.2 As eventuais alterações nas tarifas relativas ao uso dos produtos financeiros não são de responsabilidade da CONTRATADA e serão aplicadas aos mesmos, quando for o caso, com a devida comunicação à CONTRATANTE, para aceitação dos valores.',
    )
  }

  writeExclusivity() {
    super.writeTitle('CLÁUSULA OITAVA: EXCLUSIVIDADE')

    super.writeText(
      '8.1 A marca, forma, cor, bandeira, dizeres e demais características de apresentação dos cartões e Conta de Pagamento pertencem exclusivamente à CONTRATADA, que poderá modificá-los conforme sua conveniência através de aviso prévio à CONTRATANTE com no mínimo 30 (trinta) dias.',
    )
  }

  writeTimeLimit() {
    super.writeTitle('CLÁUSULA NONA: PRAZO')

    super.writeText(
      '9.1 O presente contrato é celebrado pelo prazo de um ano, contando da data de sua assinatura, ficando automaticamente prorrogado por iguais períodos sucessivos, caso não haja manifestação contrária de qualquer das partes, por escrito, em até 30 (trinta) dias úteis antes de seu término.',
    )

    super.writeText(
      '9.2 A CONTRATADA está obrigada, a qualquer tempo, no encerramento ou quebra de contrato, a fornecer os relatórios disponíveis referentes às premiações realizadas.  ',
    )

    super.writeText(
      '9.3 Caso haja pendência de créditos não repassados aos beneficiários, a CONTRATADA deverá devolver os respectivos valores à CONTRATANTE no prazo de até 10 (dez) dias úteis da formalização do encerramento.',
    )

    super.writeText(
      '9.4 No caso de rescisão contratual, motivada ou imotivada, deverão ser apuradas a liquidação de eventuais créditos e obrigações pendentes entre as partes, além de perdas e danos no caso de rescisão motivada.',
    )

    super.writeText(
      '9.5 No término das relações contratuais entre as partes, por qualquer motivo, nas hipóteses previstas ou não nesta cláusula, a CONTRATADA deverá devolver à CONTRATANTE todos e quaisquer documentos, desenhos e/ou quaisquer outros elementos que lhe tiverem sido entregues, em decorrência da execução dos serviços ora contratados, no prazo de até 30 (trinta) dias úteis da formalização do encerramento.',
    )
  }

  writeAssociations() {
    super.writeTitle('CLÁUSULA DÉCIMA: VÍNCULOS')

    super.writeText(
      '10.1 Declaram as partes que o presente contrato não estabelece qualquer forma de associação, vínculo ou solidariedade entre seus signatários, nem tampouco determinam direitos a vínculos empregatícios entre seus funcionários, dirigentes, prepostos e/ou contratados, competindo a cada uma delas, particularmente e com exclusividade, o cumprimento das suas respectivas obrigações comerciais, contratuais, trabalhistas, sociais, previdenciárias, fiscais e tributárias, em conformidade com a legislação vigente e/ou práticas comerciais financeiras ou bancárias em vigor à época.',
    )

    super.writeText(
      '10.2 A CONTRATANTE não tem quaisquer responsabilidades sobre encargos sociais e trabalhistas relativos às pessoas que trabalhem com a CONTRATADA, e que venham a lhe prestar serviços em decorrência deste contrato.',
    )

    super.writeText(
      '10.3 Por sua vez, a CONTRATADA não tem qualquer responsabilidade sobre os encargos comerciais, contratuais, trabalhistas, sociais, previdenciários, fiscais e tributários que incidam sobre a execução do objeto deste contrato, no que tange aos prestadores de serviço da CONTRATANTE, seus funcionários ou terceiros.',
    )
  }

  writeTaxObligations() {
    super.writeTitle('CLÁUSULA DÉCIMA PRIMEIRA: TRIBUTOS')

    super.writeText(
      '11.1 Todas as obrigações tributárias pertinentes ao presente contrato serão de responsabilidade da parte que a lei designar como responsável tributário ou contribuinte, conforme estabelecido pelo Código Tributário Nacional, no caso dos tributos federais, bem como legislação municipal vigente à época no Município sede da CONTRATADA, para o recolhimento do ISS, sendo certo, que cada qual ficará diretamente responsável pelos respectivos recolhimentos, ficando desde já esclarecido à CONTRATANTE que os serviços objeto deste contrato não visam a proporcionar qualquer vantagem fiscal, trabalhista ou previdenciária, seja para a própria CONTRATANTE, o empregador, o empregado ou o terceiro participante dos programas de marketing motivacional, relacionamento, incentivo e/ou fidelização.',
    )
  }

  writeConfidentialityAgreement() {
    super.writeTitle('CLÁUSULA DÉCIMA SEGUNDA: SIGILO E CONFIDENCIALIDADE')

    super.writeText(
      '12.1 As partes obrigam-se mutuamente a manter sigilo sobre todos e quaisquer dados, materiais, pormenores, informações, documentos, especificações técnicas ou comerciais, inovações e aperfeiçoamentos a que tiverem acesso em decorrência deste contrato, não podendo direta ou indiretamente, divulgar, revelar, reproduzir, utilizar ou deles dar conhecimento a terceiros estranhos a esta contratação, respondendo civil e criminalmente sob as penas da lei, por eventuais prejuízos sofridos pela parte inocente.',
    )

    super.writeText(
      '12.2 É de responsabilidade da CONTRATADA, guardar e fazer com que seus sócios, prepostos, funcionários e quaisquer profissionais contratados para prestar serviços à CONTRATANTE mantenham sigilo profissional, mesmo após finda a relação contratual entre as partes, sobre todas as informações técnicas, comerciais, administrativas e industriais, assim como, desenhos, especificações e documentações, recebidas e/ou fornecidas pela CONTRATANTE e/ou desenvolvidos no decorrer da execução dos serviços acordados, bem como após a execução dos mesmos, comprometendo-se, ainda, pela não reprodução, transmissão ou cessão a terceiros de qualquer dado, documento ou elemento obtido na prestação dos serviços, mesmo depois do término deste contrato, salvo com expressa autorização, por escrito, da CONTRATANTE.',
    )
  }

  writeDataProtection() {
    super.writeTitle('CLÁUSULA DÉCIMA TERCEIRA: DA PROTEÇÃO DE DADOS')

    super.writeText(
      '13.1. Para viabilizar a utilização dapremiação, a CONTRATADA terá acesso aos dados pessoais, comerciais e das transações realizadas pelos premiados (“Dados dos Premiados”), na qualidade de operadora de dados pessoais, comprometendo-se a cumprir com a legislação aplicável à proteção de dados e privacidade, incluindo, mas não se limitando, à Lei Geral de Proteção de Dados.',
    )

    super.writeText(
      '13.1.1. A CONTRATANTE declara-se ciente e assume a obrigação de adotar todas as medidas para que os premiados fiquem cientes de que a CONTRATADA em decorrência do presente Contrato poderá ter acesso, utilizando, mantendo e processando, eletrônica e manualmente, informações e dados prestados acerca deles, inclusive dando ciência a eles e obtendo o seu aceite acerca das Políticas de Privacidade.',
    )

    super.writeText(
      '13.1.2. O acesso, utilização, tratamento e compartilhamento pela CONTRATADA dos Dados dos Premiados terá tempo e finalidade limitados à prestação dos serviços, ressalvada a guarda por determinação legal.',
    )

    super.writeText(
      '13.1.3. Os Dados dos Premiados serão de propriedade destes e controlados pela CONTRATANTE, que deverá manter registros das operações de tratamento dos Dados dos Premiados realizadas, não podendo a CONTRATADA utilizar quaisquer informações relacionadas aos Premiados ou às Transações realizadas em benefício próprio, devendo mantê-las em estrita segurança e sigilo, de acordo com as condições previstas na Política de Privacidade. Para fins do presente contrato, as Partes desde já acordam que a CONTRATANTE e os Premiados serão exclusivamente responsáveis pela veracidade e exatidão dos Dados dos Premiados controlados pela CONTRATANTE.',
    )

    super.writeText(
      '13.1.4. Sem prejuízo do disposto acima, a CONTRATANTE declara-se ciente de que a CONTRATADA possui o dever legal e regulatório de manter os Dados dos Premiados e de todas as Transações realizadas sob sua guarda e controle, para fins de cumprimento de regras de KYC e AML, além de ordens judiciais e administrativas, podendo a CONTRATADA, a qualquer tempo e mesmo após o término deste Contrato, realizar a coleta, tratamento, armazenamento e proteção dos Dados dos Premiados.',
    )

    super.writeText(
      '13.1.5. A CONTRATADA poderá utilizar os Dados dos Premiados para fins de análise e acompanhamento interno, desde que em bases anonimizadas e que não permitam a identificação dos Premiados, inclusive para formação de banco de dados e para oferecer produtos e serviços da CONTRATADA ou de seus parceiros comerciais.',
    )

    super.writeText(
      '13.1.6. A CONTRATADA e a CONTRATANTE deverão implementar medidas razoáveis e apropriadas para proteger os Dados dos Premiados contra perda, acesso ou revelação acidental ou ilegal (“Incidentes”), garantindo ainda backup e plano de recuperação de dados em caso de incidente, observando o disposto na Lei Geral de Proteção de Dados, na Circular nº 3.909 e/ou na Resolução nº 4.658/18, ambas do Banco Central, sem prejuízo das demais normas de proteção de dados pessoais aplicáveis. Os Incidentes deverão ser comunicados aos Premiados, enquanto titulares dos Dados dos Premiados, bem como reportado à Agência Nacional de Proteção de Dados, na forma da legislação aplicável. A CONTRATANTE anui e dá expressa ciência quanto a essa obrigação.',
    )

    super.writeText(
      '13.1.7. A CONTRATADA poderá divulgar os Dados dos Premiados e outras informações obtidas em razão deste Contrato com suas parceiras para execução dos serviços e viabilização do funcionamento dos cartões pré-pagos.',
    )

    super.writeText(
      '13.1.8. A CONTRATADA poderá divulgar os Dados dos Premiados e outras informações obtidas em razão deste Contrato, mediante solicitação judicial ou administrativa, inclusive por parte da Receita Federal, Banco Central do Brasil e Unidade de Inteligência Financeira.',
    )
  }

  writeAntiCrimes() {
    super.writeTitle(
      'CLÁUSULA DÉCIMA QUARTA: PRÁTICAS DE COMPLIANCE, ANTICORRUPÇÃO E PREVENÇÃO À LAVAGEM DE DINHEIRO',
    )

    super.writeText(
      '14.1. As Partes declaram que, direta ou indiretamente, atuam em seu negócio pautadas no profissionalismo e na ética, em conformidade com a Legislação brasileira, sempre respeitando o pactuado no presente Contrato e sem qualquer violação às previsões da presente cláusula, garantido para todos os efeitos que:',
    )
    super.writeText(
      '14.1.1	Cumprem toda a Legislação relacionada à anticorrupção, lavagem de dinheiro, antissuborno, antitruste e conflito de interesses, incluindo principalmente, mas não se limitando a, Lei Brasileira Anticorrupção (Lei 12.846/2013), Decreto Brasileiro Anticorrupção (Decreto n° 8.420/2015), Lei Brasileira de Licitações (Lei n° 8.666/1993) e qualquer legislação relativa à Lavagem de Dinheiro',
    )
    super.writeText(
      '14.1.2	Adotam políticas de prevenção e combate à corrupção, à lavagem de dinheiro e ao financiamento ao terrorismo, elaboradas em conformidade com as legislações aplicáveis, bem como desenvolvem suas atividades em estrita observância a estas políticas, não adotando qualquer prática vedada pela legislação aplicável ou utilizando em suas atividades quaisquer valores, bens ou direitos provenientes de infração penal',
    )
    super.writeText(
      '14.1.3	Não utilizam trabalho ilegal, se comprometendo a não utilizar práticas de trabalho análogo ao escravo ou mão de obra infantil, salvo esta última na condição de aprendiz, observadas as disposições constantes da Consolidação das Leis do Trabalho – CLT',
    )
    super.writeText(
      '14.1.4	Não empregam menores de 18 (dezoito) anos, inclusive menor aprendiz, em locais prejudiciais à sua formação, ao seu desenvolvimento físico, psíquico, moral e social, bem como em locais e serviços perigosos ou insalubres, em horário noturno e, ainda, em horários que não permitam a frequência destes empregados à escola',
    )
    super.writeText(
      '14.1.5	Cumprem a legislação trabalhista, quanto às horas de trabalho e aos direitos dos empregados e não dificultam a participação dos empregados em sindicatos',
    )
    super.writeText(
      '14.1.6	Não utilizam práticas de discriminação negativa e limitativas ao acesso à relação de emprego ou a sua manutenção, incluindo, mas sem limitação, práticas de discriminação e limitação em razão de sexo, origem, raça, cor, condição física, religião, estado civil, idade, situação familiar ou estado gravídico; e',
    )
    super.writeText(
      '14.1.7	Executam suas atividades em observância à Legislação vigente no que tange à proteção ao meio ambiente, comprometendo-se a prevenir e erradicar práticas danosas ao meio ambiente',
    )
    super.writeText(
      '14.2. A CONTRATADA declara, garante e aceita que, com relação a este Contrato e sua atividade:',
    )
    super.writeText(
      '14.2.1	Não houve e não haverá nenhum tipo de solicitação, cobrança, obtenção ou exigência, para si e para outrem, de vantagem indevida ou promessa de vantagem indevida, nem qualquer oferta ou promessa de pagamento de valor pecuniário ou outros benefícios, como presentes, favores, promessas ou vantagens, direta ou indiretamente, com pretexto de condicionar em ato praticado por qualquer funcionário uma da outra ou ainda a agentes públicos, políticos e/ou privados, partidos políticos e candidatos, ou ainda qualquer pessoa que atue em nome de uma organização pública nacional ou internacional, bem como seus familiares ou amigos; e',
    )
    super.writeText(
      '14.2.2 Não doa fundos, financia ou de qualquer forma subsidia atos ou práticas ilegais. ',
    )
    super.writeText(
      '14.3. A CONTRATADA se compromete em combater toda e qualquer atividade que seja contra a livre concorrência, especialmente, mas não se limitando às iniciativas indutoras à formação de cartel.',
    )
    super.writeText(
      '14.4. A CONTRATADA ficará sujeita a auditorias e visitas, bem como ao envio de documentos e evidências para verificação do cumprimento das práticas estabelecidas nesta cláusula, mediante solicitação, com antecedência mínima de 15 (quinze) dias úteis.',
    )
    super.writeText(
      '14.5. O não cumprimento ou violação pela CONTRATADA de quaisquer práticas estabelecidas neste título poderá ensejar a imediata rescisão deste contrato, sem que seja devida qualquer indemnização, a perdas e danos pela rescisão antecipada do presente Contrato.',
    )
  }

  writeTransferAndAssignment() {
    super.writeTitle(
      'CLÁUSULA DÉCIMA QUINTA: DA TRANSFERÊNCIA E CESSÃO DO CONTRATO',
    )

    super.writeText(
      '15.1 A CONTRATADA não poderá transferir, ceder ou subcontratar terceiros, parcial ou totalmente, para a execução dos serviços objeto do presente contrato, sem a prévia e expressa anuência da CONTRATANTE, sob pena de rescisão contratual, independentemente de aviso prévio.',
    )
  }

  writeNovationAgreement() {
    super.writeTitle('CLÁUSULA DÉCIMA SEXTA: DA NOVAÇÃO')

    super.writeText(
      '16.1 A eventual tolerância a qualquer infração às disposições contratuais não configurará novação contratual, renúncia ou perda de qualquer direito das partes por força deste contrato e da legislação vigente.',
    )
  }

  writeContractualAmendments() {
    super.writeTitle('CLÁUSULA DÉCIMA SÉTIMA: DAS ALTERAÇÕES CONTRATUAIS')

    super.writeText(
      '17.1 Toda e qualquer inclusão, alteração ou exclusão de cláusula contratual, assim como qualquer ajuste entre as partes que implique modificação do presente contrato será consignado em aditamentos, que passarão a fazer parte integrante deste instrumento.',
    )
  }

  writeGeneralProvisions() {
    super.writeTitle('CLÁUSULA DÉCIMA OITAVA: DISPOSIÇÕES GERAIS')

    super.writeText(
      '18.1 Toda e qualquer notificação ou comunicação entre as partes, para ser válida e eficaz, deverá ser feita por escrito, com comprovante de recebimento, endereçada às pessoas indicadas no quadro resumo do Anexo I – Condições Comerciais, podendo  alternativamente ser utilizado o canal de Correio Eletrônico (e-mail), obrigando-se as partes a comunicar entre si eventuais alterações de endereços dos seus escritórios ou dos responsáveis pelas correspondências.',
    )

    super.writeText(
      '18.2 O presente contrato tem caráter de prestação de serviço, pela CONTRATADA, portanto, impõe, na forma da lei, à CONTRATANTE, o recolhimento dos impostos e tributos vinculados à natureza dos pagamentos, seja de salário, prêmio, benefício, bônus, presente, doação e outros, quando previstos em lei.',
    )

    super.writeText(
      '18.3 Os casos fortuitos de perda, furto, roubo e/ou fraude serão submetidos pela CONTRATADA a processadora , banco e/ou, emissor dos cartões, para avaliação e parecer conclusivo de especialista ou autoridade sobre a ocorrência adversa, no prazo máximo de 120 (cento e vinte) dias, parecer esse a ser utilizado como base para caracterização do indício da fraude apontada pela CONTRATANTE e/ou titulares das contas de pagamento. Tal notificação não garante à parte reclamante qualquer ressarcimento.',
    )

    super.writeText(
      '18.4  A ocorrência dos casos fortuitos descritos no item 18.3 acima, deverá ser notificada imediatamente pela CONTRATANTE ou titular da conta à CONTRATADA, através dos canais de comunicação disponibilizados pela CONTRATADA, ficando estabelecido que tal ocorrência, a partir da notificação, será tratada exclusivamente pela CONTRATADA.',
    )

    super.writeText(
      '18.5 As fraudes que venham a ocorrer nas redes de terminais de auto atendimento e redes credenciadas de aceitação dos cartõese/ou portais de pagamento, se enquadram nas situações descritas no item 18.3 acima está limitada a indenização, caso se comprove a fraude por terceiros, no valor da última carga à crédito feito pela CONTRATADA.',
    )

    super.writeText(
      '18.6 As contas de pagamento são aprovadas pelo sistema brasileiro de pagamentos e não geram qualquer receita financeira para seus titulares. As contas de pagamento sem movimentação terão cobrança de Inatividade no ciclo de 180 (cento e oitenta dias) até seu cancelamento. É responsabilidade do CONTRATANTE informar tais características aos seus usuários.',
    )
  }

  writeSignatures() {
    super.writeTitle('CLÁUSULA DÉCIMA NONA: FORO ')

    super.writeText(
      '19.1 Fica eleito o foro da comarca de São Paulo, Capital, para dirimir quaisquer dúvidas ou litígios decorrentes deste contrato, com expressa renúncia de qualquer outro, por mais privilegiado que seja.',
    )

    super.writeText(
      'E, por estarem assim justas e contratadas,  firmam o presente Contrato, devendo o mesmo ser respeitado por eventuais sucessores, a qualquer título.',
    )
  }

  writeContractorAwardsSystem() {
    const { productDetails } = this.contract
    super.writeTitle('ANEXO II - SISTEMA DE PREMIAÇÃO PARA O CONTRATANTE', {
      align: 'center',
    })
    if (productDetails.product === 'prepaid_card') {
      super
        .writeText(
          'a.	Antecipe o pedido de carga para os cartões.  Os serviços iniciarão após identificação do pedido e dos recursos disponíveis. As cargas nos cartões ocorrerão na data indicada pelo cliente.',
        )
        .moveDown(0.1)
      super
        .writeText(
          'b.	O pagamento do serviço é pré pago e através da emissão de Nota Fiscal e Nota de Débito do valor principal.  Após o recebimento das NF e ND, poderá realizá-lo via transferência de valores (TED), boleto bancário ou depósito identificado.	Será indicado no corpo das Notas a conta corrente de transferência na conveniência do cliente como facilitador de conciliação bancária dos créditos.',
        )
        .moveDown(0.1)
      super
        .writeText(
          'c.	O recurso disponibilizado para a CONTRATADA incluirá na NF o valor de reembolso de emissão, da entrega, do manuseio e tarifas, honorários (taxa adm) e na ND o total de carga nos cartões. Sobre a soma de todos os serviços, incidirá a taxa de administração.',
        )
        .moveDown(0.1)
      super.writeText(
        'd.	As cargas ocorrem nos dias úteis em até 48 horas [D+2].  Cargas emergenciais podem ser processadas no mesmo dia [D+0], contudo estas receberão cobrança extra por cartão. ',
      )
      super
        .writeText(
          'e.	Os cartões obrigatoriamente deverão estar vinculados aos nomes e CPF válidos dos portadores. A ativação do cartão e sua respectiva liberação de uso, estará condicionada ao preenchimento de dados e sua validação diretamente pelo portador através do site www.elementprepagos.com.br ou pelo APP JoyCard.',
        )
        .moveDown(0.1)
    } else if (productDetails.product === 'digital_account') {
      super
        .writeText(
          'a.	Antecipe o pedido de carga nas contas de pagamento.  Os serviços iniciarão após identificação dos recursos disponíveis referente ao seu pedido. As cargas nas contas ocorrerão imediatamente após a conclusão do cadastro pelo titular indicado pelo cliente.',
        )
        .moveDown(0.1)
      super
        .writeText(
          'b.	O pagamento do serviço é pré pago e através da emissão de Nota Fiscal e Nota de Débito do valor principal.  Após o recebimento das NF e ND, poderá realizá-lo via transferência de valores (TED), boleto bancário, Pix ou depósito identificado. ',
        )
        .moveDown(0.1)
      super
        .writeText(
          'c.	Será indicado no corpo das Notas a conta corrente de transferência na conveniência do cliente como facilitador de conciliação bancária dos créditos.',
        )
        .moveDown(0.1)
      super
        .writeText(
          'd.	O recurso disponibilizado para a CONTRATADA incluirá na NF o valor de reembolso, de adesão às contas, emissão de cartões e honorários (taxa adm) e na ND o total de carga dos créditos nas contas de pagamento. Sobre a soma de todos os serviços, incidirá a taxa de administração.',
        )
        .moveDown(0.1)
    }

    super.writeTitle('- SISTEMA DE PREMIAÇÃO PARA O CONTRATANTE')

    if (productDetails.product === 'prepaid_card') {
      super.writeText(
        'a. TECNOLOGIA DE INTEGRAÇÃO E SEGURANÇA DA INFORMAÇÃO:  a ELEMENT concederá ao CLIENTE documentação para integração de dados via Web Service  de API proprietária (Application Protocol Interface) ou através de acesso restrito (Login, Senha e Chave de Segurança em 2 fatores) para operacionalizar e controlar todas suas atividades, por exemplo: Contratos, Pedidos, Notas, Liberação de carga, bloqueio/desbloqueio de cartões.  Tais instruções permitirão a emissão ou recarga de cartões, emissão de Nota Fiscal / Débito, logística de entrega centralizada ou porta a porta, histórico de pedidos, aferição dos pagamentos efetivados, pendentes e o eventual saldo de nota para uso futuro.',
      )
    } else if (productDetails.product === 'digital_account') {
      super.writeText(
        'a. TECNOLOGIA DE INTEGRAÇÃO E SEGURANÇA DA INFORMAÇÃO:  a ELEMENT concederá ao CLIENTE documentação para integração de dados via Web Service  de API proprietária (Application Protocol Interface) ou através de acesso restrito (Login, Senha e Chave de Segurança em 2 fatores) para operacionalizar e controlar todas suas atividades, por exemplo: Contratos, Pedidos, Notas, Liberação de cargaetc.  Tais instruções permitirão a emissão de contas ou sua recarga, emissão de Nota Fiscal / Débito, histórico de pedidos, aferição dos pagamentos efetivados, pendentes e o eventual saldo de nota para uso futuro.',
      )
    }

    super.writeTitle(
      '– IMPLANTAÇÃO DE REGULAMENTO DO PROGRAMA DE PAGAMENTO DE PRÊMIOS',
    )

    super.writeTitle('PASSO A PASSO:')

    super.writeText('a. Definir o público objetivo (= participantes).')

    super.writeText(
      'b. Definir a duração e o período para aferição de metas individuais ou coletivas',
    )

    let purpose =
      productDetails.product === 'prepaid_card'
        ? 'c. Definir o valor de conversão dos prêmios e o instrumento de pagamento (cartão pré-pago).'
        : 'c. Definir o valor de conversão dos prêmios e o instrumento de pagamento (conta de pagamento).'

    super.writeText(purpose)

    super.writeText(
      'd. Definir metas claras e específicas para cada público objetivo, que configurem desempenho superior ao esperado. Possibilidades: análise do desempenho individual e/ou coletivo com análise de critérios previamente estabelecidos, tais como lucro, resultado, desempenho da produção, vendas, qualidade, atendimento, etc.',
    )

    super.writeText(
      'IMPORTANTE: Os critérios deverão serão previamente estipulados, com a ciência do empregado quanto àquilo que normalmente se esperaria do seu desempenho individual ou do grupo.',
    )

    super.writeText(
      'e. Estabelecer mecânica da avaliação, ou seja, como e por quem o participante será avaliado. ',
    )

    super.writeText(
      'f. Na política prévia de pagamento de prêmios os valores devem ser preferencialmente variáveis. Em um mês o empregado poderá receber a premiação total, no outro poderá receber valor menor, em outro poderá não receber nada. Essa variação é importante, pois caso todos os empregados recebam os mesmos valores todos os meses, a Justiça do Trabalho entende tratar-se de salário travestido de prêmio e determinará a integração do valor ao salário, gerando todos os reflexos e encargos.',
    )

    super.writeText(
      'g. Quando da fixação do valor do prêmio a CONTRATANTE deverá pautar-se na razoabilidade, razão pela qual não poderá ser superior ao salário base recebido.',
    )

    super.writeText(
      'h. Quando do pagamento, não há obrigatoriedade de os valores relativos aos prêmios pagos via cartão transitarem na folha pagamento (=holerites dos empregados), pois não têm natureza salarial.',
    )

    super.writeText(
      'i. O participante deverá comprometer-se a informar no ajuste de rendimento não tributado à Receita Federal por seus ganhos extraordinários como aumento de patrimônio.',
    )

    if (productDetails.product === 'prepaid_card') {
      super.writeTitle('- SISTEMA DE PREMIAÇÃO PARA O PORTADOR DO CARTÃO')

      super.writeText(
        'a. O cartão com bandeira é um produto do sistema brasileiro de pagamentos.',
      )

      super.writeText(
        'b. Seu cartão é recarregável e possui validade por até 5 anos.',
      )

      super.writeText(
        'c. Seu cartão é Internacional para uso presencial no exterior. Transações em moedas estrangeiras terão seu câmbio aplicado no momento da compra.',
      )

      super.writeText(
        'd. Seu cartão encontra-se bloqueado para uso. Para ativá-lo, faça-o através do site www.elementprepagos.com.br área do Cliente ou pelo APP JoyCard (Play Store)',
      )

      super.writeText(
        'e. O cartão está vinculado ao seu nome e CPF eletronicamente.  Dados de celular, senha e e-mail pessoal, além da numeração do seu cartão, data de aniversário e nome da mãe garantirão confirmações de recuperação de senha,  bloqueio ou cancelamento em caso de perda ou extravio.',
      )

      super.writeText(
        'f. O Cartão poderá efetuar COMPRAS nos estabelecimentos credenciados e na Internet.',
      )

      super.writeText(
        'g. Tarifas de Uso: o uso do cartão para transações de Compras não incide tarifa.',
      )

      super.writeText('h. O cartão possui tarifa de manutenção mensal.')

      super.writeText(
        'i. A senha é pessoal e intransferível. Não divulgue ou informe a terceiros.',
      )

      super.writeText(
        'j. O recurso financeiro no cartão ($$) não possui seguro contra roubos ou fraudes.',
      )

      super.writeText(
        'k. Em caso de expiração de validade, seu cartão será  cancelado. Saldo para crédito poderá ser resgatado num novo cartão. Esta transação incide na tarifa de ativação (emissão, envio, carga).',
      )

      super.writeText(
        'l. Em caso de perda, extravio, roubo ou danificação do cartão, o portador deverá contatar o SAC pelo telefone número 11 3473 5300, pelo WhatsApp 11 3181 0646 ou pelo e-mail: sac@elementprepagos.com.br em horário comercial, de 2ª a 6ª. Feira, das 9:00h às 18:00h e solicitar a emissão de novo cartão. Para este serviço incidirá taxa de reposição para custear o novo plástico, envio e o saldo anterior.',
      )

      super.writeText(
        'm. Os valores creditados no cartão não geram receita financeira. Não utilize como poupança.',
      )

      super.writeText(
        'n. O cartão sem movimentação terá o crédito disponível por 180 dias.',
      )
    } else if (productDetails.product === 'digital_account') {
      super.writeTitle(
        '- SISTEMA DE PREMIAÇÃO PARA O TITULAR DA CONTA DE PAGAMENTO',
      )
      super.writeText(
        'a. O titular deverá baixar o APP Adoro na loja dos aplicativos Google Play ou Apple Store',
      )
      super.writeText(
        'b. O titular deverá identificar-se pelo CPF/token para concluir seu cadastro a fim de liberar seu crédito',
      )
      super.writeText(
        'c. O cartão virtual vinculado à sua conta de pagamento é Internacional. Transações em moedas estrangeiras terão seu câmbio e IOF aplicado no momento da compra.',
      )
      super.writeText('d. O cartão virtual poderá efetuar COMPRAS ONLINE')
      super.writeText('e. Tarifas de Uso: transações bancárias incidem tarifa.')
      super.writeText(
        'f. A conta de pagamento possui tarifa de manutenção mensal.',
      )
      super.writeText(
        'g. A senha é pessoal e intransferível. Não divulgue ou informe a terceiros.',
      )
      super.writeText(
        'h. O recurso financeiro na conta de pagamento é exatamente o mesmo disponível no cartão.',
      )
      super.writeText(
        'i. Compras no cartão de crédito virtual são consideradas à vista, com valor imediatamente deduzido',
      )
      super.writeText(
        'j. Os valores creditados na conta de pagamentos não geram receita financeira. Não utilize como poupança.',
      )
      super.writeText(
        'k. A conta de pagamento sem movimentação terá cobrança de inatividade no ciclo de 180 dias.',
      )
    }
  }

  writeServices() {
    this.doc.addPage()
    super.writeTitle('ANEXO III – SERVIÇOS', {
      align: 'center',
    })
    const { productDetails } = this.contract
    if (productDetails.product === 'digital_account') {
      this.writeTableTwoColumn(
        'Tarifas de Uso',
        [
          ['Compras Online ', 'Isento'],
          ['Consultas APP Adoro', 'Isento'],
          ['Manutenção Mensal', 'R$ 8,00'],
          ['Transferência TED', 'R$ 9,00'],
          ['Transferência PIX PJ ', 'R$ 6,00'],
          ['Transferência entre Contas P2P ', 'Isento'],
          ['Pagamento de Contas', 'Isento'],
        ],
        250,
        200,
      )
    }
  }

  writeConsiderations() {
    super.writeTitle('CONSIDERANDO QUE:')

    super.writeText(
      '(a) A CONTRATANTE pretende desenvolver um programa de marketing motivacional dirigido aos seus funcionários ou a pessoas de seu relacionamento comercial, de maneira a incrementar uma melhoria nos sistemas produtivos de maximização de resultados;',
    )

    super.writeText(
      '(b) A CONTRATANTE deseja premiar aqueles por ela indicados com base em critérios de desempenho, esforço e produtividade, com a utilização de produtos de incentivo desenvolvidos pela CONTRATADA;',
    )

    super
      .writeText(
        '(c) A CONTRATADA desenvolveu estudos e tem implantado um conjunto de programas de marketing motivacional que atendem o objetivo da CONTRATANTE.',
      )
      .moveDown()
  }

  writeDescription() {
    super
      .writeTitle(
        'CONTRATO DE PRESTAÇÃO DE SERVIÇOS DE MARKETING DE MOTIVAÇÃO',
        { align: 'center' },
      )
      .moveDown()

    super.writeText(
      `De um lado, ELEMENT MARKETING E PARTICIPAÇÕES EIRELI, com sede em Barueri no  Estado de São Paulo, na Alameda Grajaú, 219 - 3o. Andar - Alphaville Industrial - Barueri - SP CEP: 06454-050 - Fone: 11 3473 5300  e inscrita no CNPJ sob nº. 05.401.489/0001-27, (“CONTRATADA”); e de outro lado,`,
    )
    super
      .writeText(
        `CONTRATANTE, como tal definido aquele que aceitar os termos comerciais e condições deste instrumento (“Contrato”), têm entre si, justa e contratada, a prestação serviços de marketing de motivação (“Serviços”), que se regerá mediante as seguintes cláusulas e condições.`,
      )
      .moveDown()
  }

  writeReview() {
    const { company, deliveryAddress } = this.contract

    this.doc
      .font('Helvetica-Bold')
      .text('ANEXO I - CONDIÇÕES COMERCIAIS', { align: 'center' })
      .moveDown()

    this.doc
      .font('Helvetica-Bold')
      .text('- QUADRO RESUMO DO CONTRATANTE')
      .moveDown()

    const writeText = (text: string, textOptions = {}) =>
      this.doc.font('Helvetica').text(text, textOptions)

    writeText(`CNPJ: ${company.cnpj}`).moveDown(0.2)
    writeText(`Razão social: ${company.legalName}`).moveDown(0.2)

    if (deliveryAddress.complement) {
      writeText(
        `End. cobrança: ${deliveryAddress.street}, ${deliveryAddress.number} - ${deliveryAddress.complement} - ${deliveryAddress.postalCode} - ${deliveryAddress.district} -  ${deliveryAddress.city}, ${deliveryAddress.state}`,
      ).moveDown(0.2)
    } else {
      writeText(
        `End. cobrança: ${deliveryAddress.street}, ${deliveryAddress.number} - ${deliveryAddress.postalCode} - ${deliveryAddress.district} -  ${deliveryAddress.city}, ${deliveryAddress.state}`,
      ).moveDown(0.2)
    }

    if (deliveryAddress.complement) {
      writeText(
        `End. entrega: ${deliveryAddress.street}, ${deliveryAddress.number} - ${deliveryAddress.complement} - CEP ${deliveryAddress.postalCode} - ${deliveryAddress.district} - ${deliveryAddress.city}, ${deliveryAddress.state}`,
      ).moveDown(0.2)
    } else {
      writeText(
        `End. entrega: ${deliveryAddress.street}, ${deliveryAddress.number} - ${deliveryAddress.postalCode} - ${deliveryAddress.district} - ${deliveryAddress.city}, ${deliveryAddress.state}`,
      ).moveDown(0.2)
    }

    writeText(`Telefone Direto: ${company.phone}`).moveDown(0.2)

    writeText(`Interlocutor: ${company.admin.fullName}`, { continued: true })
    writeText(`                  Área: ${company.admin.companyArea}`).moveDown(
      0.2,
    )

    writeText(`E-mail: ${company.admin.email}`, { continued: true })
    writeText(`                     Celular: ${company.admin.mobile}`)
      .moveDown()
      .moveDown()
  }

  writeServicesTable() {
    const { productDetails } = this.contract

    super.writeTitle('- PRODUTOS DE INCENTIVO')
    let rows = [['', '']]
    const isInternationalCard = productDetails.isInternationalCard
      ? 'Internacional'
      : 'Nacional'
    const productBrand =
      productDetails.productBrand === 'mastercard' ? 'MasterCard' : 'Visa'

    if (productDetails.product !== 'digital_account') {
      super.writeText(
        'O produto de incentivo a ser utilizado pela CONTRATANTE para premiação de seus indicados, de acordo com o presente contrato, é o cartão pré-pago, emitido pelo Banco credenciado e administrado pela CONTRATADA. ',
      )
      if (productDetails.isGiftCard) {
        rows = [
          ['Marca', 'Joyard Vale Presente'],
          ['Bandeira', productBrand || '-'],
          ['Função', 'Compras'],
        ]
      } else {
        rows = [
          ['Marca', 'Joycard Incentive'],
          ['Bandeira', productBrand || '-'],
          ['Função', 'Saque e Compras'],
        ]
      }
      rows = [
        ...rows,
        ['Tecnologia', 'Chip'],
        ['Transação', 'Crédito'],
        ['Território', isInternationalCard],
        ['Rede Aceitação', 'Operadoras Mastercard / Cirrus '],
      ]
    }

    if (productDetails.product === 'digital_account') {
      super.writeText(
        'O produto de incentivo a ser utilizado pela CONTRATANTE para premiação de seus indicados é a conta de pagamento ADORO composta por instituições financeiras e de crédito credenciadas e administrada pela CONTRATADA, conforme tabela esquemática abaixo: ',
      )
      rows = [
        ['Marca', 'Conta Adoro'],
        ['Bandeira', 'Visa'],
        ['Tipo de Cartão', 'CRÉDITO'],
        ['Tecnologia', 'Virtual'],
        ['Função', 'Compras'],
        ['Território', isInternationalCard],
        ['Beneficiários', 'CPF / CNPJ'],
        ['Rede Aceitação', 'Gateways de Pagamento'],
      ]
    }

    this.writeTableTwoColumn('Descrição do Produto', rows, 250, 200)
  }

  writeGeneralRatesTable() {
    const { deliveryAddress, productDetails } = this.contract

    const delivery = deliveryAddress.isCentralizedDelivery
      ? 'Centralizada'
      : 'Descentralizada'
    let rows = [['', '']]
    let footer = ''
    let header = ''
    super.writeTitle('- HONORÁRIOS: ')
    super.writeText(
      'A CONTRATANTE pagará à CONTRATADA, a título de honorários pelos serviços contratados, Taxa de Administração incidente sobre serviços e créditos concedidos',
    )
    let rechargeFrequency = '-'
    if (productDetails.campaign.rechargeFrequency === 'monthly')
      rechargeFrequency = 'Mensal'

    if (productDetails.campaign.rechargeFrequency === 'quarterly')
      rechargeFrequency = 'Trimestral'

    if (productDetails.campaign.rechargeFrequency === 'yearly')
      rechargeFrequency = 'Anual'
    if (productDetails.campaign.rechargeFrequency === 'only')
      rechargeFrequency = 'Única'
    const annualEstimatedAward = `${productDetails.campaign.annualEstimatedAward}`
    const cardQuantity = `${productDetails.cardQuantity}`
    const issueFee = `${productDetails.campaign.issueFee}`
    const isAdministrationRate = productDetails.campaign.isAdministrationRate
      ? 'Percentual'
      : 'Fija'
    const administrationRate = `${productDetails.campaign.administrationRate} %`
    const administrationFee = `${productDetails.campaign.administrationFee}`
    const monthlyFee = `${productDetails.operatorFees.monthlyFee}`
    const atmWithdrawFee = `${productDetails.operatorFees.atmWithdrawFee}`
    const TED = `${productDetails.operatorFees.TED}`
    const KYC = `${productDetails.companyOperatingFees?.KYC}`
    const balanceTransferFee = `${productDetails.companyOperatingFees?.balanceTransferFee}`
    const unlockFee = `${productDetails.operatorFees?.unlockFee}`
    const cardWithdrawal = `${productDetails.cardFees?.cardWithdrawal}`
    const aditionalCard = `${productDetails.operatorFees?.aditionalCard}`
    const physicalCard = `${productDetails.cardFees?.physicalCard}`
    const p2pTransferFee = `${productDetails.operatorFees?.p2pTransferFee}`
    const emitionTicket = `${productDetails.operatorFees?.emitionTicket}`
    const chargebackRate = `${productDetails.companyOperatingFees?.chargebackRate} %`
    const pixPf = `${productDetails.companyOperatingFees?.pixPf}`
    const pixPj = `${productDetails.companyOperatingFees?.pixPj}`
    rows =
      productDetails.product !== 'digital_account'
        ? [
            ['Volume financeiro', annualEstimatedAward],
            ['Frequência carga', rechargeFrequency],
            ['Personalização', '???'],
            ['Quantidade ', cardQuantity],
            ['Emissão (por Unidade) ', issueFee],
            ['Modelo de Negócio ', isAdministrationRate],
          ]
        : [
            ['Volume financeiro', annualEstimatedAward],
            ['Frequência carga', rechargeFrequency],
            ['Personalização', '???'],
            ['Quantidade ', cardQuantity],
            ['Emissão (por Unidade) ', issueFee],
            ['Modelo de Negócio ', isAdministrationRate],
          ]
    this.writeTableTwoColumn('Proposta Comercial', rows, 250, 200)
    this.doc.addPage()

    rows =
      productDetails.product !== 'digital_account'
        ? [
            ['Taxa Adm. percentual', administrationRate],
            ['Taxa Adm. fixa (> 2.000 cargas/mês)* ', '???'],
            ['Taxa Adm. fixa (> 5.000 cargas/mês)*', '???'],
          ]
        : [
            ['Taxa Adm. percentual', administrationRate],
            ['Taxa Adm. fixa', administrationFee],
            ['Taxa Adm. fixa (> 3.000 cargas/mês)*', '???'],
          ]
    footer =
      productDetails.product === 'digital_account'
        ? '*Preço especial para modelo de negócio de carga fixa com fixação de mínimo garantido.'
        : '*exclusivo para modelo negócio de carga fixa com fixação de mínimo garantido.'
    this.writeTableTwoColumn('Taxa de Administração', rows, 250, 200, footer)

    rows =
      productDetails.product !== 'digital_account'
        ? [
            ['Transferência Saldo Cartão', balanceTransferFee],
            ['Operação Transferência Interna', '???'],
            ['Estorno de crédito Cartão', chargebackRate],
            ['Operação Estorno de crédito', '???'],
            ['Know Your Client (KYC)', '???'],
            ['IR Premiação por CPF', '???'],
          ]
        : [
            ['Transferência Internas ', '???'],
            ['Estorno de crédito', '???'],
            ['KYC CPF/PJ', KYC],
            ['IR Premiação por CPF', '???'],
          ]
    footer =
      productDetails.product === 'digital_account'
        ? '*Cobradas apenas em caso de ocorrência'
        : '*Cobradas da Empresa apenas em caso de ocorrência ou solicitação do item'
    this.writeTableTwoColumn(
      'Tarifas de Serviços Operacionais - Empresa*',
      rows,
      250,
      200,
      footer,
    )

    if (productDetails.product === 'digital_account') {
      rows = [
        ['Compras Online', 'Issento'],
        ['Consultas APP Adoro ', 'Issento'],
        ['Manutenção Mensal', monthlyFee],
        ['Emissão Boleto', emitionTicket],
        ['TED', TED],
        ['PIX PF', pixPf],
        ['PIX PJ', pixPj],
        ['P2P (Transf. entre contas)', '???'],
        ['Pagamento de Contas', '???'],
        ['Saque QR Code (B24H)', '???'],
        ['Saque QR Code (PDV)', '???'],
        ['IR Premiação por CPF', '11,38%'],
        ['Transações Online no Exterior (IOF)', '6,38%'],
      ]

      this.writeTableTwoColumn(
        'Tarifas de Uso – Correntista',
        rows,
        250,
        200,
        '*Cobradas apenas em caso de ocorrência / deduzido do saldo do cartão',
      )
    }

    rows =
      productDetails.product !== 'digital_account'
        ? [
            ['Transação Compras', 'Issento'],
            ['Consultas internet', 'Issento'],
            ['Consulta Via SAC Bot', 'Whatsapp'],
            ['Manutenção Mensal', monthlyFee],
            ['Saque Banco24Horas', cardWithdrawal],
            ['TED', TED],
            ['Pagamento de Contas', '???'],
            ['Recarga Celular', '???'],
            ['Desbloqueio [erro digitação por 3x]', unlockFee],
            ['Reposição de Saldo', '???'],
            ['Reposição de Cartão', aditionalCard],
            ['IR Premiação por CPF', '???'],
            ['Transações Presenciais no Exterior (ATM)', '???'],
            ['IR Premiação por CPF', '11,38%'],
            ['Transações Online no Exterior (IOF)', '6,38%'],
          ]
        : [
            ['Transação Compras', 'Issento'],
            ['Emissão Cartão Físico', physicalCard],
            ['Saque Banco24Horas', cardWithdrawal],
            ['Desbloqueio [erro digitação por 3x]', unlockFee],
            ['Reposição de Saldo', '???'],
            ['Transações Presenciais Exterior (ATM)', '???'],
          ]
    footer =
      '*Cobradas apenas em caso de ocorrência / deduzido do saldo do prêmio'

    this.writeTableTwoColumn(
      'Tarifas de Uso do Portador - CARTÃO FÍSICO',
      rows,
      250,
      200,
      footer,
    )
    header =
      productDetails.product !== 'digital_account'
        ? 'Limites'
        : 'Limites Conta Digital'
    rows =
      productDetails.product !== 'digital_account'
        ? [
            ['Limite de Carga e Transferência Entre Cartões', '???'],
            ['Limite de Compras', '???'],
            ['Limite de Saldo no Cartão', '???'],
            ['Limite de Saque', '???'],
            ['Limite de Pagamento de Contas por Dia', '???'],
            ['Recarga de Celular', '???'],
            ['Inatividade', '???'],
            ['Disponibilidade do Prêmio**', '???'],
          ]
        : [
            ['Limite de Carga e Transferência Entre Contas', '???'],
            ['Limite de Compras', '???'],
            ['Limite de Saque', '???'],
            ['Limite de Pagamento de Contas', '???'],
            ['Recarga de Celular', '???'],
            ['Inatividade', '???'],
            ['Disponibilidade do Prêmio**', '???'],
          ]
    this.doc.addPage()
    this.writeTableTwoColumn(
      header,
      rows,
      250,
      200,
      '** Cartão cancelado incorre em custo de reposição.',
    )
    this.doc.addPage()
  }

  generate() {
    const { productDetails } = this.contract

    this.writeDescription()
    this.writeConsiderations()

    // clauses
    this.writeContractPurpose()
    this.writeEngagementObligations()
    this.writeContractorObligations()
    this.writeEngagementRemuneration()
    this.writeCreditValues()
    this.writeCardValidation()
    this.writeReadjusmentAndIncidentOfRates()
    this.writeExclusivity()
    this.writeTimeLimit()
    this.writeAssociations()
    this.writeTaxObligations()
    this.writeConfidentialityAgreement()
    this.writeDataProtection()
    this.writeAntiCrimes()
    this.writeTransferAndAssignment()
    this.writeNovationAgreement()
    this.writeContractualAmendments()
    this.writeGeneralProvisions()

    // signatures with the court clause
    this.writeSignatures()

    // anexo I
    this.doc.addPage()
    this.writeReview()
    this.writeServicesTable()

    this.writeGeneralRatesTable()

    // attachments
    this.writeContractorAwardsSystem()

    //anexo 3 de conta
    if (productDetails.product === 'digital_account') this.writeServices()

    super.finish()
    return this.doc
  }
}

export default ContractDocument
