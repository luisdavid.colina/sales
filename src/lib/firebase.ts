/* eslint-disable import/no-duplicates */
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

export const firebaseConfig = {
  apiKey: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
  authDomain: process.env.NEXT_PUBLIC_FIREBASE_AUTH_DOMAIN,
  databaseURL: process.env.NEXT_PUBLIC_FIREBASE_DATABASE_URL,
  projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
  storageBucket: process.env.NEXT_PUBLIC_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: '1016607160687',
  appId: process.env.NEXT_PUBLIC_FIREBASE_APP_ID,
  measurementId: 'G-WZ3WJRLC0F',
}

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

const auth = firebase.auth()
const firestore = firebase.firestore()

const { FieldValue, Timestamp } = firebase.firestore
const { increment, serverTimestamp } = FieldValue
const { fromMillis, fromDate } = Timestamp

export {
  firestore,
  auth,
  FieldValue,
  increment,
  serverTimestamp,
  fromMillis,
  Timestamp,
  fromDate,
}
