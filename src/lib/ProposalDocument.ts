import Proposal from '@/types/Proposal'
import { format } from 'date-fns'
import elementImage from './imagesBase64/element'
import joycardRed from './imagesBase64/joycardRed'
import joycardBlue from './imagesBase64/joycardBlue'
import joycardOrange from './imagesBase64/joycardOrange'
import joycardGreen from './imagesBase64/joycardGreen'
import PDF from './PDF'

class ProposalDocument extends PDF {
  proposal: Proposal

  constructor(proposal: Proposal) {
    super()
    this.proposal = proposal
  }

  writeLogo() {
    this.doc
      .image(elementImage, 235, 15, {
        fit: [130, 65],
        align: 'center',
        valign: 'center',
      })
      .moveDown()
      .moveDown()
      .moveDown()
  }

  writeFooter() {
    this.doc
      .font('Helvetica')
      .fontSize(8)
      .text(
        'Alameda Grajaú, 219 - 3o. Andar - Alphaville Industrial - Barueri - SP CEP: 06454-050 - Fone: 11 3473 5300',
        130,
        710,
      )
  }

  writeHead() {
    const { company, createdAt, proposalNumber } = this.proposal
    const date = format(createdAt, 'dd/MM/yyyy')

    this.doc
      .font('Helvetica')
      .text(`São Paulo, ${date}.`, { align: 'right' })
      .moveDown()

    const writeTextBold = (text: string, textOptions = {}) =>
      this.doc.font('Helvetica-Bold').text(text, textOptions)
    const writeText = (text: string, textOptions = {}) =>
      this.doc.font('Helvetica').text(text, textOptions)

    writeTextBold(company.legalName.toUpperCase())
    writeTextBold(
      `A/C: ${company.admin.fullName}, ${company.admin.companyRole}`,
    ).moveDown()

    writeText(`Proposta: ${proposalNumber}`, { align: 'right' })
    writeText('Validade proposta: 15 dd', { align: 'right' }).moveDown()

    writeText('Ref.: Cartões de Premiação', {
      underline: true,
    }).moveDown()
  }

  writeTextContain() {
    const { company, productDetails } = this.proposal

    const writeTextBold = (text: string, textOptions = {}) =>
      this.doc
        .font('Helvetica-Bold')
        .text(text, { align: 'justify', ...textOptions })
    const writeText = (text: string, textOptions = {}) =>
      this.doc
        .font('Helvetica')
        .text(text, { align: 'justify', ...textOptions })

    writeText('Prezados Senhores,', { align: 'left' }).moveDown()

    writeText(
      `Agradecemos a oportunidade de apresentar nossas soluções em meios depagamentos para atender as necessidades da área ${company.admin.companyArea} do ${company.legalName} `,
      { continued: true },
    )
    if (
      productDetails.productBrand === 'visa' &&
      productDetails.product !== 'digital_account'
    )
      writeText(
        'na demanda de pagamentos de prêmios aos multiplicadores do negócio com modernidade, agilidade, segurança, praticidade e eficiência.',
      ).moveDown()
    else
      writeText(
        'na demanda de pagamentos de bônus internos e multiplicadores do negócio com modernidade, agilidade, segurança, praticidade e eficiência.',
      ).moveDown()

    writeTextBold(`Element Business Ecosystem `, { continued: true })
    writeText(
      'é uma empresa especializada em otimizar a dinâmica de seus negócios com sua cadeia de distribuição através de meios de pagamentos pré pagos. Nossas soluções são aplicadas para diversas verticais de negócios e distintos públicos alvo. Atendemos o público interno das empresas, sejam funcionários, colaboradores, terceiros pertencentes a cadeia de valor e seu consumidor. Atuamos há 17 anos no mercado, com mais de 500 clientes em carteira em todo o território nacional.',
    ).moveDown()
    if (productDetails.product !== 'digital_account') {
      writeText('Apresentamos o ', { continued: true })
      if (productDetails.productBrand === 'visa') {
        if (productDetails.isGiftCard)
          writeTextBold('Cartão Presente Personalizado Visa ', {
            continued: true,
          })
        else writeTextBold('Cartão Personalizado Visa ', { continued: true })
      } else if (productDetails.isGiftCard) {
        writeTextBold('Cartão JoyCard Presente MasterCard ', {
          continued: true,
        })
      } else {
        writeTextBold('Cartão JoyCard Incentive MasterCard ', {
          continued: true,
        })
      }

      writeText(
        ' como a solução mais indicada e aceita por 100% de seus premiados.',
      ).moveDown()

      writeTextBold('Veja suas características do produto em detalhe', {
        align: 'left',
      })
      writeText('').moveDown()
      if (productDetails.productBrand === 'visa') {
        let items
        items = !productDetails.isGiftCard
          ? [
              'Recebe até R$150.000 por CPF em pagamentos e resgate de prêmios',
              'Aceita carga para CPFs que apresentem restrição ao crédito (SPC/Serasa)',
              'Realiza Compras à vista na função Crédito sem custo adicional',
              'Realiza compras Online com total segurança sem custo adicional',
              'Realiza Saques no Banco24Horas',
              'Transfere recursos para conta corrente de mesmo CPF',
              'Consulta o saldo e extrato de movimentação pelo website responsivo',
              'Paga contas por boleto bancário',
              'Cartão Internacional',
            ]
          : [
              'Recebe até R$150.000 por CPF em pagamentos e resgate de prêmios',
              'Aceita carga para CPFs que apresentem restrição ao crédito (SPC/Serasa)',
              'Realiza Compras à vista na função Crédito sem custo adicional',
              'Realiza compras Online com total segurança sem custo adicional',
              'Consulta o saldo e extrato de movimentação pelo website responsivo',
              'Paga contas por boleto bancário de até R$5.000/dia (sem custo)',
              'Cartão Internacional',
            ]
        this.doc.list(items, { listType: 'numbered' }).moveDown()
        writeTextBold('Vantagens do Cartão Pré Pago:', {
          align: 'left',
        })
        writeText('').moveDown()
        items = !productDetails.isGiftCard
          ? [
              'Créditos podem ocorrer no cartão em até D+2: processamento padrão, em 24 horas',
              'Validade do cartão é de 2 anos',
              'Site Responsivo para Android e IOs',
              'Estoque avançado de cartões na empresa, caso haja necessidade',
              'Sem custo de manutenção até ativação',
            ]
          : [
              'Créditos podem ocorrer no cartão em até D+2: processamento padrão, em24 horas',
              'Validade do cartão é de 2 anos',
              'Prazo personalização: 15 dias após aprovação da arte',
              'Sem custo de manutenção até ativação',
            ]
        if (productDetails.productBrand === 'visa' && productDetails.isGiftCard)
          items = [
            'Créditos podem ocorrer no cartão em até D+2: processamento padrão, em 24 horas',
            'Validade do cartão é de 5 anos',
            'APP JoyCard para Android / Responsivo para IOs',
            'Estoque avançado de cartões na empresa, caso haja necessidade',
            'Sem custo de manutenção até ativação',
          ]
        this.doc.list(items, { listType: 'numbered' }).moveDown()
      } else {
        if (!productDetails.isGiftCard) {
          this.doc
            .list(
              [
                'Recebe até R$50.000 por CPF em pagamentos e resgate de prêmios',
                'Aceita carga para CPFs que apresentem restrição ao crédito (SPC/Serasa)',
                'Realiza Compras à vista na função Crédito sem custo adicional',
                'Realiza Saques no Banco24Horas',
                'Transfere recursos para conta corrente de mesmo CPF',
                'Realiza compras Online com total segurança sem custo adicional',
                'Consulta o saldo e extrato de movimentação pelo website responsivo / APP',
                'Paga contas por boleto bancário de até R$5.000/dia (sem custo)',
                'Recarrega Celular sem custo adicional',
                'Cartão Internacional',
              ],
              { listType: 'numbered' },
            )
            .moveDown()
        } else {
          this.doc
            .list(
              [
                'Recebe até R$50.000 por CPF em pagamentos e resgate de prêmios',
                'Aceita carga para CPFs que apresentem restrição ao crédito (SPC/Serasa)',
                'Realiza Compras à vista na função Crédito sem custo adicional',
                'Realiza compras Online com total segurança sem custo adicional',
                'Consulta o saldo e extrato de movimentação pelo website responsivo / APP',
                'Paga contas por boleto bancário de até R$5.000/dia (sem custo)',
                'Recarrega Celular sem custo adicional',
                'Cartão Internacional',
              ],
              { listType: 'numbered' },
            )
            .moveDown()
        }
        writeTextBold('Vantagens do Cartão Pré Pago:', {
          align: 'left',
        })
        writeText('').moveDown()
        this.doc
          .list(
            [
              'Créditos podem ocorrer no cartão em até D+2: processamento padrão, em24 horas',
              'Validade do cartão é de 5 anos',
              'APP JoyCard para Android / Responsivo para IOs',
              'Estoque avançado de cartões na empresa, caso haja necessidade',
              'Sem custo de manutenção até ativação',
            ],
            { listType: 'numbered' },
          )
          .moveDown()
      }
    }
    if (productDetails.product === 'digital_account') {
      writeText('A carteira digital ', { continued: true })
      writeTextBold('ADORO, produto da Element Business Ecosystem ', {
        continued: true,
      })
      writeText(
        ' como a solução mais indicada e aceita por 100% de seus usuários.',
      ).moveDown()
      writeTextBold('Veja as características do produto em detalhe:', {
        align: 'left',
      })
      writeText('').moveDown()
      this.doc
        .list(
          [
            'Recebe créditos, pagamentos e prêmios em nome da Pessoa Física e Jurídica',
            'Aceita créditos para CPFs mesmo com restrição (SPC/Serasa)',
            'A conta digital possui cartão virtual na bandeira VISA. ',
            'Realiza Compras à vista Online na função Crédito ',
            'Transfere recursos para qualquer outra conta corrente (TED/PIX)',
            'Transfere recursos instantaneamente para seus usuários correntistas (P2P) ',
            'Consulta todas as operações de movimentação pelo APP Adoro',
            'Realiza pagamento de contas',
          ],
          { listType: 'numbered' },
        )
        .moveDown()
      writeTextBold('Detalhes Operacionais:', {
        align: 'left',
      })
      writeText('').moveDown()
      this.doc
        .list(
          [
            'Instruções de créditos na Conta corrente em lote em até D+1',
            'Integração via API ou acesso restrito pelo sistema de clientes Control Ecosystem',
            'Sem custo de manutenção até ativação',
          ],
          { listType: 'numbered' },
        )
        .moveDown()
    }
  }

  moveLittleDown() {
    this.doc.moveDown(0.3)
  }

  moveVeryLittleDown() {
    this.doc.moveDown(0.2)
  }

  writeTableTwoColumn(
    title: string,
    rows: string[][],
    colWidth1: number,
    colWidth2: number,
    lastRow?: string,
  ) {
    const writeRow = (text1: string, text2: string) =>
      super.writeColumns([
        { text: text1, width: colWidth1 },
        {
          text: text2,
          width: colWidth2,
          align: 'center',
        },
      ])

    const initialRowYTitle = this.doc.y
    const initialRowXTitle = this.doc.x

    super.writeHr()
    this.moveVeryLittleDown()

    super.writeTitle(title, { align: 'center' })
    this.doc.moveUp()
    this.moveVeryLittleDown()

    const initialRowY = this.doc.y
    const initialRowX = this.doc.x

    rows.forEach((columns) => {
      super.writeHr()

      this.moveVeryLittleDown()
      writeRow(columns[0], columns[1])
      this.moveVeryLittleDown()
    })

    super.writeHr()

    super.writeLine(
      initialRowX + colWidth1,
      initialRowY,
      initialRowX + colWidth1,
      this.doc.y,
    )

    if (lastRow) {
      this.moveVeryLittleDown()
      super.writeColumns([{ text: lastRow, width: 550 }])
      this.moveVeryLittleDown()
      super.writeHr()
    }

    super.writeLine(
      initialRowXTitle,
      initialRowYTitle,
      initialRowXTitle,
      this.doc.y,
    )
    super.writeLine(550, initialRowYTitle, 550, this.doc.y)

    this.doc.moveDown()
  }

  writeProductTable() {
    const { productDetails } = this.proposal
    if (productDetails.product !== 'digital_account') {
      const rows = [['Marca', 'JoyCard Incentive']]
      const isGiftCard =
        productDetails.isGiftCard || productDetails.productBrand === 'visa'
          ? 'Compras'
          : 'Saque e Compras'

      const brand = !productDetails.isGiftCard
        ? 'Personalizado'
        : 'Personalizado'
      const networks = !productDetails.isGiftCard
        ? 'Cielo / Rede / Todas'
        : 'Cielo / Rede / GetNet / Stone / Outras'
      if (productDetails.productBrand === 'visa') {
        rows.pop()
        rows.push(
          ['Marca', brand],
          ['Bandeira', 'Visa'],
          ['Tipo de Cartão', 'CRÉDITO'],
          ['Tecnologia', 'CHIP'],
          ['Função', isGiftCard],
          ['Território', 'Internacional'],
          ['Rede Aceitação', networks],
        )
      } else {
        if (productDetails.isGiftCard) {
          rows.pop()
          rows.push(['Marca', 'JoyCard Presente'])
        }
        rows.push(
          ['Bandeira', 'Mastercard'],
          ['Tipo de Cartão', 'CRÉDITO'],
          ['Tecnologia', 'CHIP'],
          ['Função', isGiftCard],
          ['Território', 'Internacional'],
          ['Rede Aceitação', 'Cielo / Rede / GetNet / Stone / Outras'],
        )
      }
      if (productDetails.isGiftCard) {
        rows.pop()
        rows.push(['Rede Aceitação', 'Cielo / Rede / Todas'])
      }
      this.writeTableTwoColumn('Descrição do Produto', rows, 250, 200)
    }
    if (productDetails.product === 'digital_account') {
      const rows = [
        ['Marca e APP', 'ADORO'],
        ['Bandeira', 'VISA'],
        ['Tipo de Cartão', 'VIRTUAL'],
        ['Função', 'CRÉDITO'],
        ['Cobertura', 'Internacional'],
      ]
      this.writeTableTwoColumn('Descrição da Conta Digital', rows, 250, 200)
    }
  }

  writeProposalTable() {
    const { productDetails } = this.proposal
    if (productDetails.product !== 'digital_account') {
      const rows = [['', '']]
      rows.pop()

      rows.push([
        'Volume financeiro estimado',
        `${productDetails.campaign.annualEstimatedAward}`,
      ])
      if (productDetails.productBrand === 'visa') {
        rows.push(['Taxa de Personalização ', `R$ 600,00`])
      }
      let rechargeFrequency = '-'
      if (productDetails.campaign.rechargeFrequency === 'monthly')
        rechargeFrequency = 'Mensal'

      if (productDetails.campaign.rechargeFrequency === 'quarterly')
        rechargeFrequency = 'Trimestral'

      if (productDetails.campaign.rechargeFrequency === 'yearly')
        rechargeFrequency = 'Anual'
      if (productDetails.campaign.rechargeFrequency === 'only')
        rechargeFrequency = 'Única'

      rows.push(
        ['Freqüência carga', rechargeFrequency],
        ['Quantidade Cartões', productDetails.cardQuantity?.toString() || '-'],
      )

      rows.push(
        [
          'Emissão do Cartão Chip (Unidade)',
          `${productDetails.campaign.issueFee}`,
        ],
        [
          'Taxa de Administração',
          `${productDetails.campaign.administrationRate?.toString() || '-'} ${
            productDetails.campaign.isAdministrationRate ? '%' : ''
          }`,
        ],
      )
      if (!productDetails.campaign.isAdministrationRate) {
        rows.pop()
        rows.push(
          [
            'Taxa Adm. (até 1.000 cargas/mês)',
            productDetails.campaign.administrationRate?.toString() || '-',
          ],
          [
            'Taxa Adm.  (> 1.000 cargas/mês)',
            productDetails.campaign.administrationRate?.toString() || '-',
          ],
        )
      }

      this.writeTableTwoColumn('Proposta Comercial', rows, 250, 200, '')
    }
    if (productDetails.product === 'digital_account') {
      const rows = [
        ['Tarifas por evento', 'Valor'],
        [
          'Taxa Adm',
          `${productDetails.campaign.administrationRate?.toString() || '-'} ${
            productDetails.campaign.isAdministrationRate ? '%' : ''
          }`,
        ],
        ['Adesão Conta Digital', `???`],
        ['Emissão Cartão Virtual', `${productDetails.campaign.issueFee}`],
        [
          'KYC (Conheça seu cliente)',
          `${productDetails.companyOperatingFees?.KYC}`,
        ],
      ]
      this.writeTableTwoColumn('Proposta Cliente', rows, 250, 200)
    }
  }

  writeOperatingFeesTable() {
    const { productDetails } = this.proposal
    if (productDetails.product !== 'digital_account') {
      if (productDetails.productBrand !== 'visa') {
        const rows = [
          ['Transferência saldo entre cartões', 'R$ 10,00 + 2% do saldo'],
          ['Estorno de crédito nos cartões', 'R$ 10,00 + 2% do saldo'],
          ['Informe Consolidado de Premiação Anual/CPF', 'ND'],
        ]

        this.writeTableTwoColumn(
          'Tarifas Operacionais - Empresa*',
          rows,
          250,
          200,
          '*Cobradas apenas em caso de ocorrência',
        )
      } else {
        const rows = [
          ['Transferência saldo entre cartões', 'ND'],
          ['Estorno de crédito nos cartões', 'ND'],
          ['Informe Consolidado de Premiação Anual/CPF', 'ND'],
        ]

        this.writeTableTwoColumn(
          'Tarifas Operacionais - Empresa*',
          rows,
          250,
          200,
          '*Visa não permite estorno de carga nos cartões.',
        )
      }
    }
  }

  writeUsageFeesTable() {
    const { productDetails } = this.proposal
    if (productDetails.product !== 'digital_account') {
      const monthlyFeet =
        productDetails.isGiftCard || productDetails.productBrand === 'visa'
          ? 'Isento'
          : productDetails.operatorFees.monthlyFee
      const rows = [
        ['Transação de Compras', 'Isento'],
        ['Manutenção Mensal', `${monthlyFeet}` || '-'],
      ]
      if (!productDetails.isGiftCard && productDetails.productBrand !== 'visa')
        rows.push(
          ['Saque no Banco24Horas', 'R$ 9,00'],
          ['Transferência para Conta Corrente', 'R$ 9,75'],
        )
      if (productDetails.productBrand === 'visa') {
        rows.push(
          ['Pagamento de Contas', 'R$1,00'],
          ['Consultas pela internet', 'Isento'],
          ['Consulta Saldo', 'verso cartão'],
          ['Inatividade (90 dias)', 'R$ 5,00'],
          ['Transações no Exterior', '6,38% IOF + 11,73% ATM'],
          ['Reposição de Cartão', 'verso cartão'],
        )
      } else {
        rows.push(
          ['Pagamento de Contas', 'Isento'],
          ['Recarga de Celular', 'Isento'],
          ['Desbloqueio cartão [erro digitação por 3x]', 'R$ 10,00'],
          ['Consultas pela internet', 'Isento'],
          ['Consulta Saldo via SAC Robot', 'Whatsapp'],
        )

        rows.push(
          ['Transações presenciais no Exterior', '6,38% IOF + 11,38% ATM'],
          ['Reposição de Cartão e Saldo', 'R$ 10,00 + 2% do saldo'],
        )
      }
      const footer =
        productDetails.productBrand !== 'visa'
          ? '*Cobradas apenas em caso de ocorrência / deduzido do saldo do cartão'
          : ''
      this.writeTableTwoColumn(
        'Tarifas de Uso – Portador',
        rows,
        250,
        200,
        footer,
      )
    }
    if (productDetails.product === 'digital_account') {
      this.writeTableTwoColumn(
        'Tarifas de Uso – Portador',
        [
          [
            'Mensalidade',
            productDetails.operatorFees.monthlyFee === 'R$ 0,00'
              ? 'Isento'
              : productDetails.operatorFees.monthlyFee,
          ],
          ['Transferência Via TED', `${productDetails.operatorFees.TED}`],
          ['PIX PJ', `${productDetails.operatorFees.pixPj}`],
          [
            'P2P',
            `${
              productDetails.operatorFees.p2pTransferFee === 'R$ 0,00'
                ? 'Isento'
                : productDetails.operatorFees.p2pTransferFee
            }`,
          ],
        ],
        250,
        200,
      )
    }
  }

  writeLimitTable() {
    const { productDetails } = this.proposal
    if (productDetails.product !== 'digital_account') {
      const rows = [['Limite do Carga', 'R$ 50.000,00']]
      if (productDetails.productBrand === 'visa') {
        rows.pop()
        rows.push([
          'Limite de Carga e Transferência Entre Cartões',
          'R$ 150.000,00',
        ])
        if (!productDetails.isGiftCard)
          rows.push(['Limite de Compras', 'R$30.000 / dia'])
        else rows.push(['Limite de Compras', 'Não tem'])

        if (!productDetails.isGiftCard)
          rows.push(['Limite de Saque', 'R$ 5.000 / mês'])
        else rows.push(['Limite de Saque', 'ND'])

        rows.push(
          ['Limite de Pagamento de Contas', 'R$ 5.000,00 por Dia'],
          ['Limite de Saldo no Cartão', 'R$ 150.000,00'],
        )
        //
      } else {
        rows.pop()
        rows.push([
          'Limite de Carga e Transferência Entre Cartões',
          'R$ 50.000,00',
        ])
        rows.push(['Limite de Compras', 'Não tem'])

        if (productDetails.isGiftCard) rows.push(['Limite de Saque', 'ND'])
        else rows.push(['Limite de Saque', 'Não tem'])

        rows.push(
          ['Limite de Pagamento de Contas', 'R$ 5.000,00 por Dia'],
          ['Recarga de Celular', 'R$ 100,00 por Dia'],
          ['Limite de Saldo no Cartão', 'R$ 50.000,00'],
          ['Disponibilidade do Prêmio no Cartão **', '180 dd'],
        )
      }

      this.writeTableTwoColumn(
        'Limites',
        rows,
        250,
        200,
        '** Cartão cancelado. Incorre em custo de reposição.',
      )
    }
  }

  writeCardTable() {
    const { productDetails } = this.proposal
    const initialRowYTitle = this.doc.y
    const initialRowXTitle = this.doc.x

    super.writeHr()

    this.moveLittleDown()
    super.writeTitle('Imagem Frente do Cartão', { align: 'center' })
    this.doc.moveUp()
    this.moveLittleDown()

    super.writeHr()
    const initialRowY = this.doc.y

    this.moveLittleDown()

    if (productDetails.productBrand === 'mastercard') {
      if (productDetails.isGiftCard) {
        this.doc.image(joycardRed, this.doc.x + 5, this.doc.y, {
          width: 260,
        })
      } else {
        this.doc.image(joycardBlue, this.doc.x + 5, this.doc.y, {
          width: 260,
        })
      }
    } else if (productDetails.product === 'digital_account') {
      this.doc.image(joycardGreen, this.doc.x + 5, this.doc.y, {
        width: 260,
      })
    } else if (productDetails.isGiftCard) {
      this.doc.image(joycardOrange, this.doc.x + 5, this.doc.y, {
        width: 260,
      })
    } else {
      this.doc.moveDown()
      this.doc.moveDown()
      this.doc.moveDown()
      this.doc.moveDown()
      this.doc.moveDown()
      this.doc.moveDown()
      this.doc.moveDown()
      this.doc.moveDown()
    }

    this.moveLittleDown()
    const finalRowY = this.doc.y
    super.writeHr()
    this.moveLittleDown()
    this.doc.moveDown()
    this.doc.font('Helvetica-Bold').text('PROPOSTA APROVADA')
    this.doc.font('Helvetica').text('DATA: ____/_____/______.')
    this.doc.font('Helvetica').text('RESPONSÁVEL: ______________________')
    this.doc.font('Helvetica').text('ASSINATURA: _______________________')
    this.doc.moveDown()
    this.writeFooter()
    if (productDetails.productBrand === 'visa') {
      this.doc
        .font('Helvetica-Bold')
        .fontSize(18)
        .text('Personalizado? Vamos conversar!', 350, finalRowY - 100)
      this.doc.moveDown()
    } else {
      this.doc
        .font('Helvetica-Bold')
        .fontSize(18)
        .text('Personalizado?', 350, finalRowY - 100)

      this.doc
        .font('Helvetica-Bold')
        .fontSize(18)
        .text('Vamos conversar!', 350, finalRowY - 80)
    }
    super.writeLine(
      initialRowXTitle,
      initialRowYTitle,
      initialRowXTitle,
      finalRowY,
    )
    super.writeLine(345, initialRowY, 345, finalRowY)
    super.writeLine(550, initialRowYTitle, 550, finalRowY)
  }

  generate() {
    const { productDetails } = this.proposal

    this.doc.fontSize(10)

    this.writeLogo()
    this.writeHead()

    this.writeTextContain()
    this.writeFooter()
    this.doc.addPage()

    this.writeLogo()
    this.writeProductTable()
    this.writeProposalTable()
    this.writeOperatingFeesTable()
    this.writeUsageFeesTable()
    if (productDetails.product !== 'digital_account') {
      this.writeFooter()
      this.doc.addPage()

      this.writeLogo()
      this.writeLimitTable()
    }
    this.writeCardTable()
    this.writeFooter()

    super.finish()
    return this.doc
  }
}

export default ProposalDocument
