import Proposal from "@/types/Proposal";
import Contract from "@/types/Contract";
import Product from "@/types/Product";
import Invoice from "@/types/Invoice";
import User from "@/types/User";
import * as admin from "firebase-admin";
import { NextApiRequest } from "next";
import { parseISO } from "date-fns";

let FIREBASE_PRIVATE_KEY = process.env.FIREBASE_PRIVATE_KEY;

FIREBASE_PRIVATE_KEY = FIREBASE_PRIVATE_KEY
  ? FIREBASE_PRIVATE_KEY.replace(/\\n/g, "\n")
  : "";

const serviceAccount = {
  type: "service_account",
  projectId: process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
  privateKeyId: process.env.FIREBASE_PRIVATE_KEY_ID,
  privateKey: FIREBASE_PRIVATE_KEY,
  clientEmail: process.env.FIREBASE_CLIENT_EMAIL,
  clientId: process.env.FIREBASE_CLIENT_ID,
  authUri: process.env.FIREBASE_AUTH_URI,
  tokenUri: process.env.FIREBASE_TOKEN_URI,
  authProviderX509CertUrl: process.env.FIREBASE_AUTH_PROVIDER_CERT_URL,
  clientX509CertUrl: process.env.FIREBASE_CLIENT_CERT_URL,
};

if (!admin.apps.length) {
  admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
  });
}

const firestore = admin.firestore();
const auth = admin.auth();

const { FieldValue, Timestamp } = admin.firestore;
const { increment, serverTimestamp } = FieldValue;
const { fromMillis, fromDate } = Timestamp;

export {
  firestore,
  auth,
  FieldValue,
  increment,
  serverTimestamp,
  fromMillis,
  fromDate,
};

const asDate = (data: string | number | Date) => {
  switch (typeof data) {
    case "string":
      return fromDate(parseISO(data));

    case "number":
      return fromMillis(data);

    default:
      return fromDate(data);
  }
};

export const getContract = (req: NextApiRequest) => {
  const { revenue } = req.body;

  if (revenue.isSigned && revenue.signatureDate) {
    revenue.signatureDate = asDate(revenue.signatureDate);
  }

  if (revenue.isSigned && revenue.expirationDate) {
    revenue.expirationDate = asDate(revenue.expirationDate);
  }

  return {
    ...req.body,
    d4signStatus: "AwaitingUploadFile",
    revenue,
  };
};

export const getNewContract = (req: NextApiRequest) => {
  const contract = getContract(req);
  contract.createdAt = serverTimestamp();
  contract.signers = [];

  return contract;
};

export const jsonUser = (user: User) => ({
  uid: user.uid,
  id: user.uid,
  companyRole: user.companyRole,
  companyArea: user.companyArea,
  phone: user.phone,
  mobile: user.mobile,
  role: user.role,
  companyId: user.companyId,
  cpf: user.cpf,
  email: user.email,
  fullName: user.fullName,
  permissions: user.permissions,
});

export const jsonContract = (contract: Contract) => {
  const { revenue } = contract;

  const signatureDate =
    revenue.isSigned && revenue.signatureDate
      ? revenue.signatureDate?.toMillis()
      : undefined;

  const expirationDate =
    revenue.isSigned && revenue.expirationDate
      ? revenue.expirationDate?.toMillis()
      : undefined;

  // const createdAt = contract.createdAt.toMillis()
  return {
    ...contract,
    revenue: { ...revenue, signatureDate, expirationDate },
    // createdAt,
  };
};

export const jsonProduct = (product: Product) => {
  const createdAt = product.createdAt?.toMillis();
  return {
    ...product,
    createdAt,
  };
};

export const jsonProposal = (proposal: Proposal) => {
  const createdAt = proposal.createdAt.toMillis();

  return {
    ...proposal,
    createdAt,
  };
};

export const jsonInvoice = (invoice: Invoice) => {

  return invoice
};
