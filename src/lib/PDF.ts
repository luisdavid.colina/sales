import PdfDocument from 'pdfkit'

class PDF {
  doc: PDFKit.PDFDocument

  constructor(config = {}) {
    this.doc = new PdfDocument(config)
  }

  writeTitle(title: string, textOptions = {}) {
    return this.doc
      .fontSize(12)
      .font('Helvetica-Bold')
      .text(title, textOptions)
      .moveDown()
  }

  writeText(text: string, textOptions = {}) {
    return this.doc
      .fontSize(11)
      .font('Helvetica')
      .text(text, { align: 'justify', ...textOptions })
      .moveDown()
  }

  writeHr() {
    this.doc
      .strokeColor('#aaaaaa')
      .lineWidth(1)
      .moveTo(this.doc.x, this.doc.y)
      .lineTo(550, this.doc.y)
      .stroke()
  }

  writeHorizontalLine(width: number) {
    this.doc
      .strokeColor('#aaaaaa')
      .lineWidth(1)
      .moveTo(this.doc.x, this.doc.y)
      .lineTo(width, this.doc.y)
      .stroke()
  }

  writeLine(x1: number, y1: number, x2: number, y2: number) {
    this.doc
      .strokeColor('#aaaaaa')
      .lineWidth(1)
      .moveTo(x1, y1)
      .lineTo(x2, y2)
      .stroke()
  }

  writeRowWithTwoItems(item1: string | number, item2: string | number) {
    return this.doc
      .fontSize(11)
      .font('Helvetica')
      .text(String(item1), { continued: true, height: 50 })
      .fontSize(11)
      .font('Helvetica')
      .text(String(item2), { align: 'right', height: 50 })
  }

  writeColumns(columns: any[], config: any = {}) {
    let { doc } = this

    const defaultConfig = { font: 'Helvetica', ...config }
    const { font, fontSize = 10 } = defaultConfig

    const initialX = doc.x
    const initialY = doc.y

    let position = initialX + 10

    columns.forEach((column) => {
      const width = column.width || 170
      const size = column.fontSize || fontSize
      const align = column.align || 'lelft'

      doc = doc
        .fontSize(size)
        .font(font)
        .text(column.text, position, initialY, {
          width,
          align,
        })
      position += width
    })

    this.doc.text('', initialX)
    return doc
  }

  writeHeadColumns(columns: any[]) {
    const config = { font: 'Helvetica-Bold', fontSize: 9 }
    this.writeColumns(columns, config)
    this.writeHr()
    this.doc.moveDown()
  }

  finish() {
    this.doc.end()
  }
}

export default PDF
