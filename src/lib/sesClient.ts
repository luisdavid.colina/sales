import { SESClient } from "@aws-sdk/client-ses";

const REGION = process.env.SE_AWS_SES_REGION || "";
const ACCESS_KEY_ID = process.env.SE_AWS_ACCESS_KEY_ID || "";
const SECRET_ACCESS_KEY = process.env.SE_AWS_SECRET_ACCESS_KEY || "";

const credentials = {
  accessKeyId: ACCESS_KEY_ID,
  secretAccessKey: SECRET_ACCESS_KEY,
};

const sesClient = new SESClient({ region: REGION, credentials });

export const SENDER = process.env.SE_AWS_SES_SENDER || "";

export const send = async (obj: any) => {
  const data = await sesClient.send(obj);
  return data;
};

export default sesClient;
