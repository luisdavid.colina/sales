import { S3Client } from '@aws-sdk/client-s3'
import { HttpRequest } from '@aws-sdk/protocol-http'
import { S3RequestPresigner } from '@aws-sdk/s3-request-presigner'
import { parseUrl } from '@aws-sdk/url-parser'
import { Hash } from '@aws-sdk/hash-node'
import { Sha256 } from '@aws-crypto/sha256-browser'
import { formatUrl } from '@aws-sdk/util-format-url'

const ACCESS_KEY_ID = process.env.SE_AWS_ACCESS_KEY_ID || ''
const SECRET_ACCESS_KEY = process.env.SE_AWS_SECRET_ACCESS_KEY || ''

const region = process.env.SE_AWS_S3_REGION || ''

const credentials = {
  accessKeyId: ACCESS_KEY_ID,
  secretAccessKey: SECRET_ACCESS_KEY,
}

const s3Client = new S3Client({ region, credentials })

export async function getS3FileUrl(
  bucket: string,
  key: string,
  platform = 'browser',
) {
  const s3ObjectUrl = parseUrl(
    `https://${bucket}.s3.${region}.amazonaws.com/${key}`,
  )

  const sha256 = platform === 'browser' ? Sha256 : Hash.bind(null, 'sha256')

  const presigner = new S3RequestPresigner({
    credentials,
    region,
    sha256,
  })

  const url = await presigner.presign(new HttpRequest(s3ObjectUrl))
  return formatUrl(url)
}

export { s3Client }
