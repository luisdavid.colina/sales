/* eslint-disable no-console */
/* eslint-disable no-shadow */
import Contract from '@/types/Contract'
import axios from 'axios'
import FormData from 'form-data'
import { Readable } from 'stream'

const API_URL = process.env.D4SIGN_URL
const TOKEN = process.env.D4SIGN_TOKEN
const KEY = process.env.D4SIGN_SECRET_KEY
const SAFE = process.env.D4SIGN_SAFE

const CREDENTIALS = `tokenAPI=${TOKEN}&cryptKey=${KEY}`

type Safe = {
  uuid_safe: string
  'name-safe': string
}

type File = {
  content: Readable
  name: string
}

export async function uploadFile(file: File) {
  let result = null
  const endpoint = `${API_URL}/safes?${CREDENTIALS}`

  try {
    const response = await axios.get(endpoint)

    const d4signSafe = response.data.find(
      (safe: Safe) => safe['name-safe'] === SAFE,
    )

    if (d4signSafe.uuid_safe) {
      const formData = new FormData()

      formData.append('file', file.content, {
        filename: file.name,
        contentType: 'application/pdf',
      })

      const response = await axios.post(
        `${API_URL}/documents/${d4signSafe.uuid_safe}/upload?${CREDENTIALS}`,
        formData,
        {
          headers: {
            ...formData.getHeaders(),
          },
          timeout: 240000,
        },
      )

      if (response.status === 200) {
        result = {
          uuidDocument: response.data.uuid,
        }
      }
    }
  } catch (err) {
      const error = err as any
       
    console.log(error.message)
  }

  return result
}

export async function sendSignatureList(contract: Contract) {
  let result = null

  const commercialAgentEmail =
    contract.productDetails.commercialAgent.company.admin.email

  const emails = [
    contract.company.admin.email,
    commercialAgentEmail,
    'comercial@elementmkt.com.br',
    'pedrofraga@elementprepagos.com.br',
  ]

  const signers = emails.map((email) => ({
    email,
    act: '1',
    foreign: '0',
    certificadoicpbr: '0',
  }))

  const endpoint = `${API_URL}/documents/${contract.d4signUuidDocument}/createlist?${CREDENTIALS}`
  const payload = { signers }

  try {
    const response = await axios.post(endpoint, payload, {
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      timeout: 240000,
    })

    if (response.status === 200) {
      result = true
    }
  } catch (err) {
      const error = err as any
       
    console.log(error)
  }

  return result
}

export async function sendDocumentToSign(contract: Contract) {
  let result = null
  const endpoint = `${API_URL}/documents/${contract.d4signUuidDocument}/sendtosigner?${CREDENTIALS}`

  const payload = {
    message: 'Segue documento para assinatura.',
    workflow: '0',
    skip_email: '0',
  }

  const options = {
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    timeout: 240000,
  }

  try {
    const response = await axios.post(endpoint, payload, options)

    if (response.status === 200) {
      result = true
    }
  } catch (err) {
      const error = err as any
       
    console.log(error)
  }

  return result
}

export async function registerWebhook(contract: Contract) {
  let result = null
  const endpoint = `${API_URL}/documents/${contract.d4signUuidDocument}/webhooks?${CREDENTIALS}`

  const payload = {
    url: `https://${process.env.VERCEL_URL}/api/contracts/${contract.id}/webhook`,
  }

  const options = {
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
    },
    timeout: 240000,
  }

  try {
    const response = await axios.post(endpoint, payload, options)

    if (response.status === 200) {
      result = true
    }
  } catch (err) {
      const error = err as any
       
    console.log(error)
  }

  return result
}
