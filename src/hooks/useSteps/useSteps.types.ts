export type useStepsReturn = {
  step: number
  next: () => void
  prev: () => void
  go: (newStep: number) => void
  getStep: () => any
  completed: boolean
}

export type useStepsParams = {
  steps: Array<any>
  initialStep?: number
}
