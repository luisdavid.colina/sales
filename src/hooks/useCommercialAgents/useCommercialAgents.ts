import { firestore } from '@/lib/firebase'
import { useCollectionDataOnce } from 'react-firebase-hooks/firestore'

import CommercialAgent from '@/types/CommercialAgent'
import SalesComisión from '@/types/SalesCommission'

const useCommercialAgents = () => {
  // get all commercial agents
  const agentsQuery = firestore.collection('commercialAgents')
  const [agents = [], loadingAgents] = useCollectionDataOnce<CommercialAgent>(
    agentsQuery,
  )

  // get all sales commissions
  const salesCommissionsMap: { [key: string]: SalesComisión } = {}
  const commissionsQuery = firestore.collection('salesCommissions')
  const [
    salesCommissions = [],
    loadingCommissions,
    error,
  ] = useCollectionDataOnce<SalesComisión>(commissionsQuery)

  salesCommissions.forEach((commission) => {
    salesCommissionsMap[commission.id] = commission
  })

  const commercialAgents = agents.map((agent) => {
    return {
      ...agent,
      salesCommission: salesCommissionsMap[agent.salesCommissionId],
    }
  })
  const loading = loadingAgents || loadingCommissions
  return { commercialAgents, loading, error }
}

export default useCommercialAgents
