import { useState, useCallback } from 'react'

type stateType = {
  mouseX: null | number
  mouseY: null | number
}

const initialState: stateType = {
  mouseX: null,
  mouseY: null,
}

const useContextMenu = () => {
  const [state, setState] = useState<stateType>(initialState)

  const onOpen = useCallback((event: React.MouseEvent<any>) => {
    event.preventDefault()
    setState({
      mouseX: event.clientX - 2,
      mouseY: event.clientY - 4,
    })
  }, [])

  const onClose = useCallback(() => {
    setState(initialState)
  }, [])

  return { state, onOpen, onClose }
}

export default useContextMenu
