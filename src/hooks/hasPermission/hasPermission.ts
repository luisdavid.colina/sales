import { useMemo } from 'react'

import { isRoleOrHasPermissions } from '@/middlewares/checkRoleOrPermissions'
import { useSession } from '@/context/SessionContext'
import { ADMINS } from '@/constants'
import { Role, InterfaceCompanyPermissions, Permisssion } from '@/types/User'

const hasPermission = (
  permissions: InterfaceCompanyPermissions<Permisssion[]>,
  roles?: Role[],
) => {
  const { user } = useSession()
  const hasPermission = useMemo(
    () => isRoleOrHasPermissions(roles || ADMINS, permissions, user),
    [user],
  )
  return hasPermission
}

export default hasPermission
