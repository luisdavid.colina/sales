import cancellablePromise, { delay } from '@/util/cancellablePromise'
import useCancellablePromises from './useCancellablePromises'

type functionType = (...args: Array<any>) => void

const useClickPreventionOnDoubleClick = (
  onClick: functionType,
  onDoubleClick: functionType,
) => {
  const api = useCancellablePromises()

  const handleClick = async (...args: Array<any>) => {
    api.clearPendingPromises()
    const waitForClick = cancellablePromise(delay(300))
    api.appendPendingPromise(waitForClick)

    return waitForClick.promise
      .then(() => {
        api.removePendingPromise(waitForClick)
        onClick(...args)
      })
      .catch((errorInfo: any) => {
        api.removePendingPromise(waitForClick)
        if (!errorInfo.isCanceled) {
          throw errorInfo.error
        }
      })
  }

  const handleDoubleClick = (...args: Array<any>) => {
    api.clearPendingPromises()
    onDoubleClick(...args)
  }

  return [handleClick, handleDoubleClick]
}

export default useClickPreventionOnDoubleClick
