import { useRef } from 'react'

type item = {
  promise: Promise<any | void>
  cancel: () => void
}

const useCancellablePromises = () => {
  const pendingPromises = useRef<item[]>([])

  const appendPendingPromise = (promise: item) => {
    pendingPromises.current = [...pendingPromises.current, promise]
  }

  const removePendingPromise = (promise: item) => {
    pendingPromises.current = pendingPromises.current.filter(
      (p) => p !== promise,
    )
  }

  const clearPendingPromises = () =>
    pendingPromises.current.map((p) => p.cancel())

  return {
    appendPendingPromise,
    removePendingPromise,
    clearPendingPromises,
  }
}

export default useCancellablePromises
