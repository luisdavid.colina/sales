import { useState, useCallback } from 'react'

export type handleActiveType = (_: any, value: number) => void

export type useTabReturn = {
  active: number
  handleActive: handleActiveType
}

const useTab: (initialState?: number) => useTabReturn = (initialState = 0) => {
  const [active, setActive] = useState<number>(initialState)

  const handleActive: handleActiveType = useCallback((_, value) => {
    setActive(value)
  }, [])

  return {
    active,
    handleActive,
  }
}

export default useTab
