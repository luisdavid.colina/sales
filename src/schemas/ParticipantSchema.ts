import { object, string, TypeOf } from 'yup'

const ParticipantSchema = object({
  id: string().notRequired(),
  name: string()
    .required('Campo obrigatório')
    .min(3, 'Debe contener pelo un menos 3 letras'),
})

export type Participant = TypeOf<typeof ParticipantSchema>

export default ParticipantSchema
