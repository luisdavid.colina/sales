import { string, object, TypeOf, array, mixed } from 'yup'

const BulkTutorialSchema = object().shape({
  invoices: mixed(),
  status: string(),
})

export type BulkTutorial = TypeOf<typeof BulkTutorialSchema>

export default BulkTutorialSchema
