import { object, string, array, TypeOf, number } from 'yup'

const Participants = array()
  .of(
    object()
      .shape({
        id: string().required('Campo obrigatório'),
        name: string().required('Campo obrigatório'),
      })
      .required('Campo obrigatório')
      .typeError('Campo obrigatório'),
  )
  .min(1, 'Selecione pelo menos um participante')

const RuleSchema = object({
  commissionType: string().required('Campo obrigatório'),
  months: number().min(1).required('Campo obrigatório'),
  paymentFrequency: string()
    .oneOf(['monthly', 'lifetime'])
    .required('Campo obrigatório'),
  participants: Participants.clone().required('Campo obrigatório'),
  salesChannel: object({
    id: string().required('Campo obrigatório'),
    name: string().required('Campo obrigatório'),
    participants: Participants.clone().required('Campo obrigatório'),
  })
    .required('Campo obrigatório')
    .typeError('Campo obrigatório'),
})

export type Rule = TypeOf<typeof RuleSchema>

export default RuleSchema
