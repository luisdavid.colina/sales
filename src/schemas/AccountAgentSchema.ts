import { string, object } from 'yup'

const schema = object().shape({
  cpf1: string()
    .required('Campo obrigatório')
    .matches(/[0-9]{11}/, 'Insira um cpf valido de 11 dígitos'),
  email1: string()
    .required('Campo obrigatório')
    .email('Campo deve ser um email válido'),
})

export default schema
