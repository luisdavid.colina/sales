import { object, string, TypeOf } from 'yup'

const ElementUserSchema = object({
  cpf: string()
    .matches(/[0-9]{11}/, 'Insira um cpf valido de 11 dígitos')
    .required('Campo obrigatório'),
  email: string().required('Campo obrigatóri').email().lowercase(),
  password: string().required('Campo obrigatório').min(8),
  invitationId: string().required('Campo obrigatório'),
  fullName: string().required('Campo obrigatório'),
  companyRole: string().required('Campo obrigatório'),
  companyArea: string().required('Campo obrigatório'),
  phone: string().required('Campo obrigatório'),
  mobile: string().required('Campo obrigatório'),
})

export type ElementUser = TypeOf<typeof ElementUserSchema>

export default ElementUserSchema
