import { object, string } from 'yup'

const ProductCategorySchema = object({
  name: string().min(3).required(),
})

export default ProductCategorySchema
