import { string, object, array, boolean, TypeOf } from 'yup'

const agent = object()
  .shape({
    cpf: string()
      .required('Campo obrigatório')
      .matches(/[0-9]{11}/, 'Insira um cpf valido de 11 dígitos'),
    email: string()
      .required('Campo obrigatório')
      .email('Campo deve ser um email válido'),
    businessRules: boolean(),
    communications: boolean(),
    profiles: boolean(),
  })
  .required()

const schema = object().shape({
  agents: array().compact().of(agent),
})

export type AccountListAgentsType = TypeOf<typeof schema>
export type AccountAgentType = TypeOf<typeof agent>
export default schema
