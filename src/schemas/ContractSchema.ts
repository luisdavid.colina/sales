import { object, TypeOf } from 'yup'
import CompanySchema from './CompanySchema'
import DeliveryAddressSchema from './DeliveryAddressSchema'
import ProductDetailsSchema from './ProductDetailsSchema'
import RevenueSchema from './RevenueSchema'

const ContractSchema = object({
  company: CompanySchema.required(),
  deliveryAddress: DeliveryAddressSchema.required(),
  productDetails: ProductDetailsSchema.required(),
  revenue: RevenueSchema.required(),
})

export type Contract = TypeOf<typeof ContractSchema>

export default ContractSchema
