import { object, array, number } from 'yup'
import ParticipantSchema from './ParticipantSchema'
import ProductSchema from './ProductSchema'
import RuleSchema from './RuleSchema'

const ParticipantsCommissions = array().of(
  object({
    commissions: array(),
    totalCommission: number(),
    commissionsPerMonth: array(),
    quantity: number().required(),
    participant: ParticipantSchema.required(),
  }),
)

const ProductRuleSchema = object({
  product: ProductSchema.required(),
  rule: RuleSchema.required(),
  estimatedRevenue: number().required(),
  totalCommission: number().required(),
  participantsCommissions: ParticipantsCommissions.required(),
  totalValue: number().required(),
  paymentPerMonth: number().min(0).optional().typeError('Deve ser um número'),
})

export default ProductRuleSchema
