import { object, array, string } from 'yup'

const SalesChannelSchema = object({
  name: string()
    .required('Campo obrigatório')
    .min(3, 'Debe contener pelo un menos 3 letras'),
  participants: array()
    .of(
      object({
        id: string().required('Campo obrigatório'),
        name: string().required('Campo obrigatório'),
      }).typeError('Campo obrigatório'),
    )
    .required('Campo obrigatório')
    .min(1, 'Selecione pelo menos um participante'),
})

export default SalesChannelSchema
