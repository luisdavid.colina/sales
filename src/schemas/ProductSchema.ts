import { object, string, number, TypeOf } from 'yup'

const ProductCategorySchema = object()
  .shape({
    id: string().required('Campo obrigatório'),
    name: string().required('Campo obrigatório').min(3),
  })
  .required('Campo obrigatório')
  .typeError('Campo obrigatório')

const ProductFamilySchema = object()
  .shape({
    id: string().required('Campo obrigatório'),
    name: string().required('Campo obrigatório').min(3),
  })
  .required('Campo obrigatório')
  .typeError('Campo obrigatório')

export const RuleSchema = object({
  id: string(),
  name: string().required('Campo obrigatório').min(3),
}).typeError('Campo obrigatório')

const productData = {
  productCategory: ProductCategorySchema,
  code: string().required('Campo obrigatório'),
  name: string().required('Campo obrigatório').min(3),
  productFamily: ProductFamilySchema,
}

const RuleData = {
  rule: RuleSchema,
}

const ProductSchema = object({
  ...productData,
  ...RuleData,
})

export const ProductDataSchema = object({
  ...productData,
})

export const RuleDataSchema = object({
  ...RuleData,
})

export type Rule = TypeOf<typeof RuleSchema>
export type Product = TypeOf<typeof ProductSchema>
export type RuleData = TypeOf<typeof RuleDataSchema>
export type ProductData = TypeOf<typeof ProductDataSchema>

export default ProductSchema
