import { string, object, TypeOf, number } from 'yup'

const MultiplierSchema = object({
  commercialAgentLinkId: string(),
  contactFullName: string().required('Campo obrigatório'),
  cnpj: string()
    .required('Campo obrigatório')
    .matches(/[0-9]{14}/, 'Insira um cnpj valido de 14 dígitos'),
  companyLegalName: string().required('Campo obrigatório'),
  companyRole: string().required('Campo obrigatório'),
  companyArea: string().required('Campo obrigatório'),
  email: string()
    .required('Campo obrigatório')
    .email('Campo deve ser um email válido'),
  phone: string().required('Campo obrigatório'),
  mobile: string(),
  product: string().required(),
  totalIncome: string(),
  commercialAgentsQuantity: number(),
  paymentFrequency: string().required(),
})

export type Multiplier = TypeOf<typeof MultiplierSchema>

export default MultiplierSchema
