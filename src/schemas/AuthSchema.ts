import { object, string } from 'yup'

export const TwoFactorSchema = object({
  otp: string().required(),
})

export const LoginSchema = object({
  email: string().email().required(),
  password: string().required(),
})

const AuthSchema = object().shape({
  email: string().email().required(),
  password: string().required(),
  otp: string().required(),
})

export default AuthSchema
