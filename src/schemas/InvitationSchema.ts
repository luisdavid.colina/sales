import { object, string } from 'yup'

const InvitationSchema = object({
  cpf: string()
    .required('Campo obrigatório')
    .matches(/[0-9]{11}/, 'Insira um cpf valido de 11 dígitos'),
  email: string().email().required(),
  category: string().oneOf(['company', 'element']).required(),
  companyId: string().optional(),
})

export default InvitationSchema
