import { object, string } from 'yup'
import { AccountShape } from './AccountInformationSchema'

const CompanyUserSchema = object({
  ...AccountShape,
  password: string().required('Campo obrigatório').min(8),
  invitationId: string().required(),
})

export default CompanyUserSchema
