import { object, TypeOf } from 'yup'
import CompanySchema from './CompanySchema'
import ProductDetailsSchema from './ProductDetailsSchema'

const ProposalSchema = object({
  company: CompanySchema.required(),
  productDetails: ProductDetailsSchema.required(),
})

export type Proposal = TypeOf<typeof ProposalSchema>

export default ProposalSchema
