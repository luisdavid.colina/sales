import { object, TypeOf, string } from 'yup'
import CompanySchema from './CompanySchema'

const CommercialAgentSchema = object({
  company: CompanySchema.required(),
  salesCommissionId: string().required('Campo obrigatório'),
})

export type CommercialAgent = TypeOf<typeof CommercialAgentSchema>

export default CommercialAgentSchema
