import { object, string } from 'yup'

const ProductFamilySchema = object({
  name: string().min(3).required(),
})

export default ProductFamilySchema
