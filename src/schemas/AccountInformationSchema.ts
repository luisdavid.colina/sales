import { string, object, TypeOf } from 'yup'

export const AccountShape = {
  fullName: string().required('Campo obrigatório'),
  cpf: string()
    .required('Campo obrigatório')
    .matches(/[0-9]{11}/, 'Insira um cpf valido de 11 dígitos'),
  companyRole: string().required('Campo obrigatório'),
  companyArea: string().required('Campo obrigatório'),
  email: string()
    .required('Campo obrigatório')
    .email('Campo deve ser um email válido'),
  phone: string().required('Campo obrigatório'),
  mobile: string(),
  company: object({
    legalName: string().required('Campo obrigatório'),
    tradingName: string().required('Campo obrigatório'),
    cnpj: string()
      .required('Campo obrigatório')
      .matches(/[0-9]{14}/, 'Insira um cnpj valido de 14 dígitos'),
    email: string()
      .required('Campo obrigatório')
      .email('Campo deve ser um email válido'),
    phone: string().required('Campo obrigatório'),
    mobile: string(),
    website: string(),
    comments: string(),
  }),
}

const AccountInformationSchema = object(AccountShape)

export type AccountInformationFormData = TypeOf<typeof AccountInformationSchema>

export default AccountInformationSchema
