import { string, object, TypeOf } from 'yup'

const ElementAccountInformationschema = object({
  fullName: string().required('Campo obrigatório').default(''),
  companyRole: string().required('Campo obrigatório').default(''),
  companyArea: string().required('Campo obrigatório').default(''),
  phone: string().required('Campo obrigatório').default(''),
  mobile: string().required('Campo obrigatório').default(''),
})

export type ElementAccountInformation = TypeOf<
  typeof ElementAccountInformationschema
>

export default ElementAccountInformationschema
