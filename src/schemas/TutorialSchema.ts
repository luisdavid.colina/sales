import { string, object, TypeOf, array } from 'yup'

const TutorialSchema = object({
  id: string(),
  tittle: string().required(),
  section: string(),
  subjetc: string(),
  media: string(),
  data: string(),
  status: string(),
  attachments: array(),
})

export type Tutorial = TypeOf<typeof TutorialSchema>

export default TutorialSchema
