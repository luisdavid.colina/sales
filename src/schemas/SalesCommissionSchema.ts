import { object, string, number, array, TypeOf } from 'yup'

const CommissionSchema = object({
  to: number().default(0).min(0),
  from: number().default(0).min(0),
  loads: number().default(0).min(0),
  salesChannel: string(),
})

const SalesCommissionSchema = object({
  name: string()
    .required('Campo obrigatório')
    .min(3, 'Debe contener pelo un menos 3 letras'),
  fixedCommissions: array().of(CommissionSchema).notRequired().default([]),
  ratedCommissions: array().of(CommissionSchema).notRequired().default([]),
})

export type Commission = TypeOf<typeof CommissionSchema>
export type SalesCommission = TypeOf<typeof SalesCommissionSchema>

export default SalesCommissionSchema
