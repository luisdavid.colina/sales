import { string, object, TypeOf, number, array, boolean } from 'yup'

const CommunicationSchema = object({
  id: string(),
  object: string(),
  status: boolean(),
  subject: string(),
  text: string(),
  to: array(),
})

export type Communication = TypeOf<typeof CommunicationSchema>

export default CommunicationSchema
