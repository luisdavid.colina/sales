import { string, object, TypeOf, array, mixed } from 'yup'

const BulkInvoiceSchema = object().shape({
  invoices: mixed(),
  status: string(),
})

export type BulkInvoice = TypeOf<typeof BulkInvoiceSchema>

export default BulkInvoiceSchema
