import { string, object, TypeOf } from 'yup'

const AccountVerificationSchema = object({
  cpf: string()
    .matches(/[0-9]{11}/, 'Insira um cpf valido de 11 dígitos')
    .required('Campo obrigatório')
    .default(''),
  email: string()
    .email('Campo deve ser um email válido')
    .required('Campo obrigatório')
    .default(''),
})

export type AccountVerification = TypeOf<typeof AccountVerificationSchema>

export default AccountVerificationSchema
