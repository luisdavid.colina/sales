import { string, object, ref, TypeOf } from 'yup'

const AccountSecuritySchema = object({
  password: string()
    .required('Campo obrigatório')
    .min(8, 'A senha deve conter pelo menos 8 caracteres')
    .default(''),
  passwordConfirmation: string()
    .oneOf([ref('password'), null], 'A senha deve corresponder')
    .default(''),
})

export type AccountSecurity = TypeOf<typeof AccountSecuritySchema>

export default AccountSecuritySchema
