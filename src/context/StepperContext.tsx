import { createContext, FC, useState, useContext } from 'react'

import { AxiosResponse } from 'axios'

interface ContextProps {
  step: number
  disabled?: boolean
  code: number
  titleModal: string
  actionModal: () => void
  onChange: (data?: any) => void
  next: (data?: any) => void
  prev: () => void
  handleStep: (step: number | ((step: number) => number)) => void
  onSuccess: (data?: any) => Promise<AxiosResponse<any>>
}

const StepperContext = createContext<ContextProps>({} as ContextProps)
interface Props {
  disabled?: boolean
  code?: number
  titleModal?: string
  actionModal?: () => void
  onChange?: (data?: any) => void
  onSuccess: (data?: any) => Promise<AxiosResponse<any>>
}

type stateType = {
  step: number
}

export const StepperProvider: FC<Props> = ({
  onChange = () => {},
  disabled = false,
  code = 201,
  titleModal = '',
  actionModal = () => {},
  children,
  onSuccess,
}) => {
  const [value, setValue] = useState<stateType>({
    step: 1,
  })

  const next = (data: any) => {
    onChange(data)
    setValue((prevState) => ({
      ...prevState,
      step: prevState.step + 1,
    }))
  }

  const prev = () => {
    onChange()
    setValue((prevState) => ({
      ...prevState,
      step: prevState.step - 1,
    }))
  }

  const handleStep = (step: number | ((step: number) => number)) => {
    setValue((prevState) => ({
      ...prevState,
      step: typeof step === 'number' ? step : step(prevState.step),
    }))
  }

  return (
    <StepperContext.Provider
      value={{
        ...value,
        disabled,
        onSuccess,
        next,
        prev,
        code,
        titleModal,
        actionModal,
        onChange,
        handleStep,
      }}
    >
      {children}
    </StepperContext.Provider>
  )
}

export const useStepperContext = () => useContext(StepperContext)

export default StepperContext
