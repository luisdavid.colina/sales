import { createContext, FC, useContext, useEffect, useState } from 'react'
import { useAuthState } from 'react-firebase-hooks/auth'
import { useRouter } from 'next/router'
import { auth, firestore } from '@/lib/firebase'
import User from '@/types/User'

const defaultUser = {
  uid: '',
  fullName: '',
  cpf: '',
  companyRole: '',
  companyArea: '',
  phone: '',
  mobile: '',
  email: '',
  companyId: '',
}

type Claims = {
  [key: string]: any
}

type ContextProps = {
  user: User
  auth: any
  claims: Claims
  isAuth: boolean
  loading: boolean
  logOut: () => void
}

const SessionContext = createContext<ContextProps>({
  user: defaultUser,
  auth: null,
  claims: {},
  isAuth: false,
  logOut: () => {},
  loading: true,
})

export const SessionProvider: FC = ({ children }) => {
  const router = useRouter()
  const [user, setUser] = useState<User>(defaultUser)
  const [loadingUser, setLoadingUser] = useState(true)
  const [loadingClaims, setLoadingClaims] = useState(true)
  const [authState, loadingAuthState] = useAuthState(auth)

  const [claims, setClaims] = useState<Claims>({})

  const logOut = async () => {
    return auth.signOut().then(() => {
      router.push('/login')
    })
  }

  useEffect(() => {
    let unsubscribe

    if (authState) {
      const ref = firestore.collection('users').doc(authState.uid)

      unsubscribe = ref.onSnapshot(
        (doc) => {
          const user = doc.data() as User
          if (user) {
            setUser(() => {
              setLoadingUser(() => false)
              return user
            })
          }
        },
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        (_) => {
          setLoadingUser(false)
        },
      )
    } else {
      setUser(defaultUser)
      setLoadingUser(false)
    }

    return unsubscribe
  }, [authState])

  useEffect(() => {
    if (authState && !loadingAuthState) {
      const getAuthClaims = async () => {
        const result = await authState.getIdTokenResult()
        setClaims(() => {
          setLoadingClaims(() => false)
          return result.claims
        })
      }
      getAuthClaims()
    } else if (!loadingAuthState) {
      setLoadingClaims(false)
    }
  }, [authState, loadingAuthState])

  const loading = loadingAuthState || loadingUser || loadingClaims

  const isAuth = Boolean(authState && claims?.isTwoFactorVerified)

  const value = {
    user,
    claims,
    logOut,
    isAuth,
    loading,
    auth: authState,
  }

  return (
    <SessionContext.Provider value={value}>{children}</SessionContext.Provider>
  )
}

export const useSession: () => ContextProps = () => useContext(SessionContext)

export default SessionContext
