/* eslint-disable no-console */
import { getS3FileUrl, s3Client } from '@/lib/s3Client'
import { PutObjectCommand } from '@aws-sdk/client-s3'
import { Readable } from 'stream'
import Invoice from '@/types/Invoice'

export async function getNFurl(invoice?: Invoice) {
  const bucket = 'element-sales-ecosystem-nfs'
  const key = invoice?.fileKey || ''

  const url = await getS3FileUrl(bucket, key)
  return url
}

export async function uploadNF(body: Readable | Buffer, fileKey: string) {
  console.log(body, fileKey)
  const command = new PutObjectCommand({
    Bucket: 'element-sales-ecosystem-nfs',
    Key: fileKey,
    Body: body,
  })

  const response = await s3Client.send(command)
  console.log(response)
  return response
}