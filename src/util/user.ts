import { firestore } from '@/lib/firebase-admin'
import { SendTemplatedEmailCommand } from '@aws-sdk/client-ses'
import { send, SENDER } from '@/lib/sesClient'
import Contract from '@/types/Contract'
import { generateTwoFactorAuth } from './utils'

export const createAdminUser = async (contract: Contract) => {
  const { company } = contract
  const { admin } = company

  const twoFactorAuth = generateTwoFactorAuth(admin.email)

  const invitationRef = await firestore.collection('invitations').add({
    company,
    twoFactorAuth,
    category: 'company',
    cpf: admin.cpf,
    email: admin.email,
    role: 'admin',
    status: 'open',
  })
  await invitationRef.update({ id: invitationRef.id })

  const params = {
    COMPANY_NAME: company.legalName,
    LINK_FINAL_FORM: `https://${process.env.VERCEL_URL}/invitations/${invitationRef.id}`,
  }

  await send(
    new SendTemplatedEmailCommand({
      Destination: {
        CcAddresses: [],
        ToAddresses: [admin.email],
      },
      Source: SENDER,
      Template: 'NEW_USER',
      TemplateData: JSON.stringify(params),
      ConfigurationSetName: 'sales-ecosystem',
      ReplyToAddresses: [],
    }),
  )
}
