import axios from 'axios'
import { NextPageContext } from 'next'

const BASE_URL = process.env.NEXT_PUBLIC_VERCEL_URL
  ? `https://${process.env.NEXT_PUBLIC_VERCEL_URL}/api`
  : `https://${process.env.NEXT_PUBLIC_API_URL}`

const PRIVATE_BASE_URL = process.env.VERCEL_URL
  ? `https://${process.env.VERCEL_URL}/api`
  : `https://${process.env.NEXT_PUBLIC_API_URL}`

const publicFetch = axios.create({
  baseURL: BASE_URL,
})

const privateFetch = (context: NextPageContext) =>
  axios.create({
    baseURL: PRIVATE_BASE_URL,
    headers:
      context &&
      context.req &&
      context.req.headers &&
      context.req.headers.cookie
        ? { cookie: context.req.headers.cookie }
        : undefined,
  })

const authFetch = axios.create({
  baseURL: '/api',
  withCredentials: true,
})

export { publicFetch, privateFetch, authFetch }
