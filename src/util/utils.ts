/* eslint-disable no-console */
import { COMMERCIAL_AGENT_ROLE, COMPANY_PERMISSIONS } from '@/constants'
import ContractDocument from '@/lib/ContractDocument'
import { firestore } from '@/lib/firebase-admin'
import ProposalDocument from '@/lib/ProposalDocument'
import { getS3FileUrl, s3Client } from '@/lib/s3Client'
import { AccountAgentType } from '@/schemas/AccountListAgentsSchema'
import Company from '@/types/Company'
import Contract from '@/types/Contract'
import { DestinationType } from '@/types/EmailBulk'
import Invitation from '@/types/Invitation'
import Proposal from '@/types/Proposal'
import TwoFactorAuth from '@/types/TwoFactorAuth'
import { CompanyPermissions, CompanyRole } from '@/types/User'
import { GetObjectCommand, PutObjectCommand } from '@aws-sdk/client-s3'
import { format } from 'date-fns'
import { generateSecret } from 'node-2fa'
import { Readable } from 'stream'
import CommercialAgent from '@/types/CommercialAgent'
import { SendTemplatedEmailCommand } from '@aws-sdk/client-ses'
import { send, SENDER } from '@/lib/sesClient'

import Communication from '@/types/Communication'

import Invoice from '@/types/Invoice'

import communicationDefault from '@/forms/defaultStates/communication'

const communication: Communication = communicationDefault

export function streamToString(stream: Readable): Promise<string> {
  return new Promise((resolve, reject) => {
    const chunks: Uint8Array[] = []
    stream.on('data', (chunk) => chunks.push(chunk))
    stream.on('error', reject)
    stream.on('end', () => resolve(Buffer.concat(chunks).toString('utf-8')))
  })
}

export function streamToBuffer(stream: Readable): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    const chunks: Uint8Array[] = []
    stream.on('data', (chunk) => chunks.push(chunk))
    stream.on('error', reject)
    stream.on('end', () => resolve(Buffer.concat(chunks)))
  })
}

export async function getContractPdf(pdfKey: string) {
  const command = new GetObjectCommand({
    Bucket: 'element-sales-ecosystem-contracts',
    Key: pdfKey,
  })

  const { Body } = await s3Client.send(command)
  return Body as Readable
}

export async function getNF(fileKey: string) {
  const command = new GetObjectCommand({
    Bucket: 'element-sales-ecosystem-nfs',
    Key: fileKey,
  })

  const { Body } = await s3Client.send(command)
  return Body as Readable
}

export async function getProposalPdf(pdfKey: string) {
  const command = new GetObjectCommand({
    Bucket: 'element-sales-ecosystem-proposals',
    Key: pdfKey,
  })

  const { Body } = await s3Client.send(command)
  return Body as Readable
}

export async function getNFurl(invoice: Invoice) {
  const bucket = 'element-sales-ecosystem-nfs'
  const key = invoice.fileKey || ''

  const url = await getS3FileUrl(bucket, key)
  return url
}

export async function getContractPdfUrl(contract: Contract) {
  const bucket = 'element-sales-ecosystem-contracts'
  const key = contract.pdfKey

  const url = await getS3FileUrl(bucket, key)
  return url
}

export async function getProposalPdfUrl(proposal: Proposal) {
  const bucket = 'element-sales-ecosystem-proposals'
  const key = proposal.pdfKey

  const url = await getS3FileUrl(bucket, key || '')
  return url
}

export async function uploadNF(body: Readable | Buffer, fileKey: string) {
  console.log(body, fileKey)
  const command = new PutObjectCommand({
    Bucket: 'element-sales-ecosystem-nfs',
    Key: fileKey,
    Body: body,
  })

  const response = await s3Client.send(command)
  console.log(response)
  return response
}

export async function uploadFile(body: Readable | Buffer, fileKey: string) {
  const command = new PutObjectCommand({
    Bucket: 'element-sales-ecosystem-contracts',
    Key: fileKey,
    Body: body,
  })

  const response = await s3Client.send(command)
  return response
}

export async function uploadProposalFile(
  body: Readable | Buffer,
  fileKey: string,
) {
  const command = new PutObjectCommand({
    Bucket: 'element-sales-ecosystem-proposals',
    Key: fileKey,
    Body: body,
  })

  const response = await s3Client.send(command)
  return response
}

type PdfResult = {
  error: null | any
  data: null | { pdfKey: string }
}

export async function generateContractPdf(contract: Contract) {
  const result: PdfResult = { error: null, data: null }

  const date = format(Date.now(), 'dd-mm-yyyy-hh-mm')
  const pdfKey = `contract-${contract.company.legalName}-${date}.pdf`

  const data = {
    ...contract,
    createdAt: Date.now(),
  } as Contract

  try {
    const contractDocument = new ContractDocument(data)
    const pdfStream = (contractDocument.generate() as unknown) as Readable
    const pdf = await streamToBuffer(pdfStream)

    const response = await uploadFile(pdf, pdfKey)

    if (response.$metadata.httpStatusCode === 200) {
      result.data = { pdfKey }
    } else {
      console.log('response', response)
      throw new Error('Could not create the pdf file')
    }
  } catch (err) {
    const error = err as any

    console.log('error', error.message)
    result.error = error
  }

  return result
}

export const updateGenerateProposalPdf = async (proposal: Proposal) => {
  const result: PdfResult = { error: null, data: null }

  const date = format(Date.now(), 'dd-mm-yyyy-hh-mm')
  const pdfKey =
    proposal.pdfKey || `proposal-${proposal.company.legalName}-${date}.pdf`
  const data = {
    ...proposal,
    createdAt: Date.now(),
  } as Proposal

  try {
    const proposalDocument = new ProposalDocument(data)
    const pdfStream = (proposalDocument.generate() as unknown) as Readable
    const pdf = await streamToBuffer(pdfStream)

    const response = await uploadProposalFile(pdf, pdfKey)

    if (response.$metadata.httpStatusCode === 200) {
      result.data = { pdfKey }
    } else {
      console.log('response', response)
      throw new Error('Could not create the pdf file')
    }
  } catch (err) {
    const error = err as any

    console.log('error', error.message)
    result.error = error
  }

  return result
}

export const generateProposalPdf = async (proposal: Proposal) => {
  const result: PdfResult = { error: null, data: null }

  const date = format(Date.now(), 'dd-mm-yyyy-hh-mm')
  const pdfKey = `proposal-${proposal.company.legalName}-${date}.pdf`

  const data = {
    ...proposal,
    createdAt: Date.now(),
  } as Proposal

  try {
    const proposalDocument = new ProposalDocument(data)
    const pdfStream = (proposalDocument.generate() as unknown) as Readable
    const pdf = await streamToBuffer(pdfStream)

    const response = await uploadProposalFile(pdf, pdfKey)

    if (response.$metadata.httpStatusCode === 200) {
      result.data = { pdfKey }
    } else {
      console.log('response', response)
      throw new Error('Could not create the pdf file')
    }
  } catch (err) {
    const error = err as any

    console.log('error', error.message)
    result.error = error
  }

  return result
}

const getPermissions = (account: AccountAgentType) => {
  const permissions: CompanyPermissions = {}

  COMPANY_PERMISSIONS.forEach((permission) => {
    if (account[permission]) {
      permissions[permission] = {
        create: true,
        write: true,
        read: true,
        delete: true,
      }
    }
  })

  return permissions
}

export const generateTwoFactorAuth = (email: string): TwoFactorAuth => {
  return generateSecret({
    name: process.env.APP || 'sales-ecosystem',
    account: email,
  })
}

export const createCompanyInvitation = async (
  account: AccountAgentType,
  company: Company,
  role: CompanyRole,
): Promise<Invitation> => {
  const twoFactorAuth = generateTwoFactorAuth(account.email)

  const invitation = {
    ...account,
    role,
    company,
    twoFactorAuth,
    status: 'open',
    category: 'company',
    permissions: getPermissions(account),
  }

  const docRef = await firestore.collection('invitations').add(invitation)

  await docRef.update({ id: docRef.id })
  invitation.id = docRef.id

  return invitation
}

export const updateUser = async (
  account: AccountAgentType,
  companyId: string,
) => {
  const docRef = firestore.doc(`users/${account.uid}`)

  if (companyId === (await docRef.get()).data()?.companyId)
    await docRef.update({ permissions: getPermissions(account) })
}

export const createBulkDestination = (
  invitation: Invitation,
): DestinationType => {
  return {
    Destination: {
      ToAddresses: [invitation.email],
    },
    ReplacementTemplateData: JSON.stringify({
      LINK_FINAL_FORM: `https://${process.env.VERCEL_URL}/invitations/${invitation.id}`,
    }),
  }
}

export const createCommercialAgentInvitation = async (
  commercialAgent: CommercialAgent,
  companyId: string,
) => {
  const { company } = commercialAgent

  const invitation = {
    category: 'commercialAgent',
    cpf: company.admin.cpf,
    legalName: company.legalName,
    company,
    companyId,
    email: company.admin.email,
    role: COMMERCIAL_AGENT_ROLE,
    status: 'open',
    commercialAgentId: commercialAgent.id,
  }

  const invitationRef = await firestore
    .collection('invitations')
    .add(invitation)

  await invitationRef.update({ id: invitationRef.id })

  const params = {
    COMPANY_NAME: company.legalName,
    LINK_FINAL_FORM: `https://${process.env.VERCEL_URL}/invitations/${invitationRef.id}`,
  }

  const email = {
    Destination: {
      CcAddresses: [],
      ToAddresses: [company.admin.email],
    },
    Source: SENDER,
    Template: 'NEW_USER',
    TemplateData: JSON.stringify(params),
    ConfigurationSetName: 'sales-ecosystem',
    ReplyToAddresses: [],
  }

  await send(new SendTemplatedEmailCommand(email))
}

export const createCommunication = async (communication: Communication) => {
  const communicationRef = await firestore
    .collection('communications')
    .add(communication)

  await communicationRef.update({ id: communicationRef.id })
  const params = {
    COMMUNICATION_CONTENT: communication.text,
    COMMUNICATION_SUBJECT: communication.subject,
  }

  const email = {
    Destination: {
      CcAddresses: [],
      ToAddresses: communication.to,
    },
    Source: SENDER,
    Template: 'NEW_COMMUNICATION',
    TemplateData: JSON.stringify(params),
    ConfigurationSetName: 'sales-ecosystem',
    ReplyToAddresses: [],
  }

  try {
    await send(new SendTemplatedEmailCommand(email))
  } catch (err) {
    const error = err as any

    console.log(error.message)
  }
}
