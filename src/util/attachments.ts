/* eslint-disable no-console */
import { s3Client } from '@/lib/s3Client'
import { PutObjectCommand } from '@aws-sdk/client-s3'
import { Readable } from 'stream'

export async function uploadAttachments(
  body: Readable | Buffer,
  fileKey: string,
) {
  console.log(body, fileKey)
  const command = new PutObjectCommand({
    Bucket: 'element-sales-ecosystem-attachments',
    Key: fileKey,
    Body: body,
  })

  const response = await s3Client.send(command)
  console.log(response)
  return response
}
