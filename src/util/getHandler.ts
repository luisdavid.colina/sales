import { NextApiRequest, NextApiResponse } from 'next'
import nextConnect from 'next-connect'
import cors from 'cors'

import User from '@/types/User'

const { ALLOWED_LIST } = process.env

const allowedOrigins = ALLOWED_LIST ? ALLOWED_LIST.split(',') : [/localhost/]

const options: cors.CorsOptions = {
  origin: allowedOrigins,
  credentials: true,
  exposedHeaders: ['Set-Cookie'],
  allowedHeaders: ['Accept'],
}

export type ApiRequest = NextApiRequest & {
  authUser?: any
  user?: User
}

export default function getHandler() {
  return nextConnect<ApiRequest, NextApiResponse>({
    onError(error, req, res) {
      if (error?.response?.status === 400) {
        return res.status(400).end()
      }

      return res
        .status(501)
        .json({ error: `Sorry something Happened! ${error.message}` })
    },
    onNoMatch(req, res) {
      res.status(405).json({ error: `Method ${req.method} Not Allowed` })
    },
  }).use(cors(options))
}
