const cancellablePromise = (promise: any) => {
  let isCanceled = false

  const wrappedPromise = new Promise((resolve, reject) => {
    promise.then(
      (value: any) =>
        // eslint-disable-next-line prefer-promise-reject-errors
        isCanceled ? reject({ isCanceled, value }) : resolve(value),
      // eslint-disable-next-line prefer-promise-reject-errors
      (error: any) => reject({ isCanceled, error }),
    )
  })

  return {
    promise: wrappedPromise,
    cancel: () => {
      isCanceled = true
    },
  }
}

export const delay = (n: number) =>
  new Promise((resolve) => setTimeout(resolve, n))

export default cancellablePromise
