/* eslint-disable no-shadow */
import Participant from '@/types/Participant'
import { GridColDef, GridValueFormatterParams } from '@material-ui/data-grid'
import { differenceInMonths, differenceInYears, format } from 'date-fns'

import { priceFormatter } from './currency'

const getMonthsBetweenDates = (dateLeft: Date, dateRight: Date): number => {
  return differenceInMonths(dateLeft, dateRight)
}

const getYearsBetweenDates = (dateLeft: Date, dateRight: Date): number => {
  return differenceInYears(dateLeft, dateRight)
}

const rateFormatter = (params: GridValueFormatterParams) => `${params.value}%`

const fixedFormatter = (params: GridValueFormatterParams) =>
  priceFormatter.format(Number(params.value))

interface ruleParams {
  commissionType: string
  months: number
  participants: Participant[]
}

const getColumnsNumber = (data: ruleParams): number => {
  return data.months
}

export const createRuleColumns = (params: ruleParams) => {
  const { commissionType } = params

  const formatter = commissionType === 'rated' ? rateFormatter : fixedFormatter

  const getColumnName = (colIdx: number) => {
    return `Mês ${colIdx + 1}`
  }

  const columns: GridColDef[] = [
    {
      field: 'quantity',
      headerName: 'Quantidade',
      headerAlign: 'center',
      disableColumnMenu: true,
      align: 'center',
      width: 100,
    },
    {
      field: 'participant',
      headerName: 'Participantes',
      valueFormatter: (params: any) => params.value?.name,
      width: 150,
    },
  ]

  const size = getColumnsNumber(params)

  for (let i = 0; i < size; i += 1) {
    let columnName: string
    if (size === 1) {
      columnName = 'Vitalício'
    } else {
      columnName = getColumnName(i)
    }
    columns.push({
      width: 180,
      editable: true,
      headerName: columnName,
      field: `commission.${i}`,
      valueFormatter: formatter,
    })
  }

  return columns
}

export const createRuleRows = (params: ruleParams) => {
  const rows: any[] = []
  const { participants } = params
  const size = getColumnsNumber(params)

  participants.forEach((participant) => {
    const rowData: any = {
      id: participant.id,
      participant,
      quantity: 1,
    }
    for (let i = 0; i < size; i += 1) {
      rowData[`commission.${i}`] = 0
    }
    rows.push(rowData)
  })

  return rows
}

interface Results {
  rows: any[]
  columns: GridColDef[]
  revenue: number
  participantsCommissions: any
  totalCommission: number
  paymentPerMonth: number
}

interface SimulatorParams {
  income: number
  rows: any[]
  columns: any[]
  data: any
}

export const simulateRule = ({
  rows,
  income,
  columns,
  data,
}: SimulatorParams): Promise<Results> => {
  return new Promise((resolve) => {
    const { commissionType, months } = data.rule

    let total = income * months
    const paymentPerMonth = income

    let totalCommission = 0
    let newRows = rows.slice()
    let newColumns = columns.map((col) => {
      if (col.field.includes('commission')) {
        return {
          ...col,
          editable: false,
          valueFormatter: fixedFormatter,
        }
      }

      return col
    })

    const commissions = columns
      .map((col) => col.field)
      .filter((field) => field.includes('commission'))

    const participantsCommissions: any = {}

    commissions.forEach((commission) => {
      let sum = 0
      newRows = newRows.map((row) => {
        let commissionRevenue: number
        if (commissionType === 'rated') {
          commissionRevenue = (paymentPerMonth * Number(row[commission])) / 100
        } else {
          commissionRevenue = Number(row[commission])
        }

        sum += commissionRevenue
        totalCommission += commissionRevenue

        const { id } = row.participant

        if (!participantsCommissions[id]) {
          participantsCommissions[id] = {
            quantity: row.quantity,
            participant: row.participant,
            commissions: [],
            commissionsPerMonth: [],
            totalCommission: 0,
          }
        }
        participantsCommissions[id].commissions.push(row[commission])
        participantsCommissions[id].commissionsPerMonth.push(commissionRevenue)
        participantsCommissions[id].totalCommission += commissionRevenue

        return {
          ...row,
          [commission]: commissionRevenue,
        }
      })
      total -= sum
    })

    newRows = newRows.map((row) => {
      return {
        ...row,
        totalCommission: participantsCommissions[row.participant.id],
      }
    })
    newColumns = newColumns.concat({
      width: 180,
      headerName: 'Total',
      field: 'totalCommission',
      hide: true,
      valueFormatter: fixedFormatter,
    })

    resolve({
      rows: newRows,
      revenue: total,
      columns: newColumns,
      participantsCommissions,
      paymentPerMonth,
      totalCommission,
    })
  })
}

export function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms))
}

type StatusTranslation = 'opened'

export function formatProposalStatus(status: StatusTranslation) {
  const statusTranslations = {
    opened: 'Aberto',
  }

  return statusTranslations[status] || status
}
