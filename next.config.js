const nextConfig = {
  /* config options here */
  webpack(config, options) {
    const { dev, isServer } = options;

    config.optimization.splitChunks.cacheGroups = {};
    config.optimization.minimize = true;

    return config;
  },
  async rewrites() {
    return [
      {
        source: "/",
        destination: "/dashboard",
      },
    ];
  },
  env: {
    NEXT_API_URL: process.env.NEXT_API_URL,
    FIREBASE_CLIENT_ID: process.env.FIREBASE_CLIENT_ID,
    FIREBASE_CLIENT_EMAIL: process.env.FIREBASE_CLIENT_EMAIL,
    FIREBASE_PRIVATE_KEY_ID: process.env.FIREBASE_PRIVATE_KEY_ID,
    FIREBASE_AUTH_URI: process.env.FIREBASE_AUTH_URI,
    FIREBASE_TOKEN_URI: process.env.FIREBASE_TOKEN_URI,
    FIREBASE_AUTH_PROVIDER_CERT_URL:
      process.env.FIREBASE_AUTH_PROVIDER_CERT_URL,
    FIREBASE_CERT_URL: process.env.FIREBASE_CERT_URL,
    FIREBASE_PRIVATE_KEY: process.env.FIREBASE_PRIVATE_KEY,
    SE_AWS_ACCESS_KEY_ID: process.env.SE_AWS_ACCESS_KEY_ID,
    SE_AWS_SECRET_ACCESS_KEY: process.env.SE_AWS_SECRET_ACCESS_KEY,
    SE_AWS_S3_REGION: process.env.SE_AWS_S3_REGION,
    SE_AWS_SES_REGION: process.env.SE_AWS_SES_REGION,
    SE_AWS_SES_SENDER: process.env.SE_AWS_SES_SENDER,
    VERCEL_URL: process.env.VERCEL_URL,
    NEXT_PUBLIC_FIREBASE_PROJECT_ID:
      process.env.NEXT_PUBLIC_FIREBASE_PROJECT_ID,
    ALLOWED_LIST: process.env.ALLOWED_LIST,
    NEXT_PUBLIC_FIREBASE_API_KEY: process.env.NEXT_PUBLIC_FIREBASE_API_KEY,
  },
  webpack5: false,
};

module.exports = nextConfig;
