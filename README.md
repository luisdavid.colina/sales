Application to manage financial services and products.

## 🚀 How to install and start the project

Clone this repository and then follow the next intructions inside the project folder:

1. Run `npm install` or `yarn`;
2. Configure the environment variables on a .env file, see the .env.example file for more context.
3. Finally, run `yarn dev`;

To view the project open `http://localhost:3000`.

## 🤝 Contributing

1. Create your branch: `git checkout -b my-new-feature`;
2. Commit your changes: `git commit -m 'Add some feature'`;
3. Push to the branch: `git push origin my-new-feature`.

**After your pull request is merged**, you can safely delete your branch.

